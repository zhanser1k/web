<?php
include(dirname(__FILE__).'/config/web.config.php');

/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wp_'.$theme);

/** Имя пользователя MySQL */
define('DB_USER', $DBUser);

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', $DBPassword);

/** Имя сервера MySQL */
define('DB_HOST', $DBHost);

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

define('UPLOADS','wp-content/uploads/'.$theme);

define('WP_DEFAULT_THEME', $theme);

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ni`yk3]S9^w8Qbv=,.(sjIn3S@eQy2b9JGB@XZl?du-M/.@@RJq+/%O J_c$MIPR');
define('SECURE_AUTH_KEY',  '>b;[8)kU#l0~ ?3}oeZz^!mB;{Z21Z<I!?n{ (M0.J_7!MWDUkCm!U<^vA+`txE5');
define('LOGGED_IN_KEY',    '6%}9k$3ra2NZ]i~]+(QWk6i).gulSDRtJ%{R](MA)tTB<#!6;*-~RN%:`Ez*2qrx');
define('NONCE_KEY',        'l(|fs7umxM}Fzx9vJmCA]A&<43Xf_i**]}sA;%z3<m;%pVoO(CBl|~CG{V_|>yoy');
define('AUTH_SALT',        '2C~PCP5EMNY?b|x@eLY5quNRKpbD-5:L)Yr~?/&;].l;;1cAW2DX%rc[<H-`60Jj');
define('SECURE_AUTH_SALT', '0t.c<6A,*Lb#E)MY7@()@mytk;O|hi_^5,>)bjSU$.}fxY.N(DAsGDdLGx%#?Y65');
define('LOGGED_IN_SALT',   'X+{TzJ)|1CnCOaFSNfrU6@7[H^*,Uz`<]7~7uo0#D<1d[}rwpi{Xwq1c,IYZwq<X');
define('NONCE_SALT',       'OpnRi)B!N!xBZfeV/z}`Ym=5s `v,~.%RUuHC+<0-5l,]T|!0|2|F33kMf)EflOJ');
$host = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'];
define('WP_SITEURL',       $host);
define('WP_HOME',          $host);

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
