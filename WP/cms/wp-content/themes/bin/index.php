<?php get_header("guest");?>

<div class="main-page"
     ng-show="!siteStructure.hasOwnProperty(appLocationParts[0])">
    <div class="slider-container" ng-class="{'_news-page': appLocationParts[1]}">
        <div ng-show="!appLocationParts[1]">
            <?php echo do_shortcode("[metaslider id=105 percentwidth=100]"); ?>
        </div>
        <div class="head-action">
            <div class="head-action-wrapper">
                <ul class="links">
                    <li><a class="link" href="/faq#get-card">Как получить карту?</a></li>
                    <li><a ng-if="!isAuth() && !authInProcess" class="link" href="#registration">Активировать карту</a></li>
                    <li><a ng-if="!isAuth() && !authInProcess" class="link" href="#resetPassword"">Забыли пароль?</a></li>
                </ul>
                <div class="authentication" ng-if="!isAuth()">
                    <authentication></authentication>
                </div>
                <div class="user-info" ng-if="isAuth() && userInfo">
                    <div class="initials">{{userInfo.lastName}} {{userInfo.firstName}} {{userInfo.patronymicName}}</div>
                    <button type="button" class="btn sign-out-btn" ng-click="logout()"><img src="wp-content/themes/bin/assets/img/header/sign_out.svg" alt="">Выход</button>
                </div>
                <a ng-if="isAuth()" href="#cabinet" class="go-to-cabinet link">Личный кабинет</a>
            </div>
        </div>
    </div>
</div>

<notifications ng-class="{'_main': !siteStructure.hasOwnProperty(appLocationParts[0])}"></notifications>

<div ng-if="!appLocationParts">
    <lmx-announcement ng-if="!appLocationParts[1]" space="News"></lmx-announcement>
    <lmx-announcement ng-if="appLocationParts[1]" announcement-id="appLocationParts[1]"></lmx-announcement>
</div>

<div class="cabinet" ng-if="isAuth() && siteStructure.hasOwnProperty(appLocationParts[0])">
    <h2 ng-if="currentLocation === cabinet" class="center">Последние операции</h2>
    <div class="left-bar">
        <?php if ( is_active_sidebar( 'side-bar' ) ) : ?>
            <div id="side-bar">
                <ul>
                    <?php dynamic_sidebar( 'side-bar' ); ?>
                </ul>
            </div>
        <?php endif; ?>

        <div class="user">
            <dl class="user-balance">
                <dt class="user-balance__key">Ваш баланс:</dt>
                <dd class="user-balance__value">
                    <span ng-bind="userInfo.balanceShortInfo.balance.amount"></span>
                    <span
                            ng-if="userInfo.balanceShortInfo.balance.amount % 1 === 0"
                            ng-bind-html="userInfo.balanceShortInfo.balance.amount | translatePlural: {plural: 'pointPlurals.point_clear'}"
                    ></span>
                    <span ng-if="userInfo.balanceShortInfo.balance.amount % 1 !== 0">балла</span>
                </dd>
            </dl>
            <dl class="user-accumulated">
                <dt class="user-accumulated__key">Получено бонусов:</dt>
                <dd class="user-accumulated__value">
                        <span ng-bind="userInfo.balanceShortInfo.accumulated.amount"></span>
                        <span
                            ng-if="userInfo.balanceShortInfo.accumulated.amount % 1 === 0"
                            ng-bind-html="userInfo.balanceShortInfo.accumulated.amount | translatePlural: {plural: 'pointPlurals.point_clear'}"
                        ></span>
                        <span ng-if="userInfo.balanceShortInfo.accumulated.amount % 1 !== 0">балла</span>
                </dd>
            </dl>
            <dl class="user-paid">
                <dt class="user-paid__key">Израсходовано:</dt>
                <dd class="user-paid__value">
                    <span ng-bind="cardInfo.paid.amount"></span>
                    <span
                            ng-if="cardInfo.paid.amount % 1 === 0"
                            ng-bind-html="cardInfo.paid.amount | translatePlural: {plural: 'pointPlurals.point_clear'}"
                    ></span>
                    <span ng-if="cardInfo.paid.amount % 1 !== 0">балла</span>
                </dd>
            </dl>
        </div>

        <nav class="side-menu">
            <a
                class="menu-item"
                ng-repeat="menuItem in siteStructure | toArray | filter: {isMenuItem: true} | orderBy: 'menuItemOrder'"
                ng-href="#{{:: menuItem.$key}}"
                ng-class="{'active': appLocationParts[0] == menuItem.$key}"
                ng-bind=":: menuItem.name"
            ></a>
        </nav>
    </div>
    <div class="content">
        <div class="last-operations" ng-if="currentLocation === 'cabinet'">
            <history></history>
        </div>
        <div class="sections" ng-switch="appLocationParts[0]">
            <cards ng-switch-when="cards"></cards>
            <history ng-switch-when="history"></history>
            <section class="personal-offers-section" ng-switch-when="personalOffers">
                <personal-offers ng-if="!appLocationParts[1]"></personal-offers>
                <personal-offer ng-if="appLocationParts[1] && !appLocationParts[2]" offer-id="appLocationParts[1]"></personal-offer>
            </section>
            <personal-offers-map
                    ng-if="appLocationParts[0] === 'personalOffers' && appLocationParts[1] === 'map'"
                    offer-id="appLocationParts[2]"
                    detail-id="appLocationParts[3]"
            ></personal-offers-map>
            <section ng-switch-when="personalGoods">
                <personal-goods ng-if="!appLocationParts[1]"></personal-goods>
                <personal-goods-offer ng-if="appLocationParts[1]" offer-id="appLocationParts[1]"></personal-goods-offer>
            </section>
            <questionnaire edit-mode ng-switch-when="personal"></questionnaire>
            <section class="settings" ng-switch-when="settings">
                <h2 class="title">Настройки</h2>
                <h4 class="settings-title">Смена телефонного номера</h4>
                <user-phone></user-phone>
                <h4 class="settings-title">Смена электронной почты</h4>
                <user-email></user-email>
                <h4 class="settings-title password">Изменить пароль</h4>
                <user-password></user-password>
            </section>

        </div>
    </div>
</div>

<?php get_footer("guest");?>
