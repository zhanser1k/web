<?php get_header("guest");?>

<div class="main-page"
     ng-show="!siteStructure.hasOwnProperty(appLocationParts[0])">
    <div class="slider-container page" ng-class="{'_news-page': appLocationParts[1]}">
        <div ng-show="appLocationParts[0] == ''">
            <?php echo do_shortcode("[metaslider id=105 percentwidth=100]"); ?>
        </div>
        <div class="head-action">
            <div class="head-action-wrapper">
                <ul class="links">
                    <li><a class="link" href="/faq#get-card">Как получить карту?</a></li>
                    <li><a ng-if="!isAuth() && !authInProcess" class="link" href="/#/registration">Активировать карту</a></li>
                    <li><a ng-if="!isAuth() && !authInProcess" class="link" href="/#/resetPassword"">Забыли пароль?</a></li>
                </ul>
                <div class="authentication" ng-if="!isAuth()">
                    <authentication></authentication>
                </div>
                <div class="user-info" ng-if="isAuth() && userInfo">
                    <div class="initials">{{userInfo.lastName}} {{userInfo.firstName}} {{userInfo.patronymicName}}</div>
                    <button type="button" class="btn sign-out-btn" ng-click="logout()"><img src="wp-content/themes/bin/assets/img/header/sign_out.svg" alt="">Выход</button>
                </div>
                <a ng-if="isAuth()" href="/#/cabinet" class="go-to-cabinet link">Личный кабинет</a>
            </div>
        </div>
    </div>
    <div class="page-content">
        <?php
        while ( have_posts() ) : the_post();
            the_content();
        endwhile;
        ?>
    </div>
</div>

<?php get_footer("guest");?>
