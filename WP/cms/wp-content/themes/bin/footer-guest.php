</div>
<footer class="footer-wrapper">
    <div class="footer">
        <div class="footer-info">
            <p>&copy;&nbsp;2017 &laquo;Баллуем своих&raquo;</p>
            <p>Разработано <a href="http://loymax.ru/" target="_blank">Loymax.ru</a></p>
        </div>

        <div class="footer-logo">
            <a href="https://www.instagram.com/nikolaevskiy.info/" target="_blank"><img src="wp-content/themes/bin/assets/img/footer/logo-h.png" alt="" /></a>
            <a href="https://www.instagram.com/sputnik_uu/" target="_blank"><img src="wp-content/themes/bin/assets/img/footer/logo-s.svg" width="40" height="40" alt="" /></a>
            <a href="https://www.instagram.com/sputnik_chita_/" target="_blank"><img src="wp-content/themes/bin/assets/img/footer/logo-sputnik.svg" width="140" height="34"  alt="" /></a>
        </div>

        <div class="hotline"><a href="tel:88005001562">8 800 500-15-62</a></div>
    </div>

</footer>
<?php wp_footer(); ?>
</body>
</html>
