<?php
// Удаление отступа для дефолтного логин-бара WordPress
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
add_theme_support( 'menus' );

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'bin' ),
) );

function bin_widgets_init() {
    register_sidebar(array (
        'name' => __( 'Sidebar', 'bin' ),
        'id' => 'side-bar',
        'description' => __( 'Sidebar', '' ),
        'class' => 'side-bar',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div>',
        'after_title' => '</div>',
    ));
}
add_action( 'widgets_init', 'bin_widgets_init' );

require get_template_directory() . '/inc/class-lmx-widget-user-info.php';

function register_lmxWidget() {
    register_widget( 'LmxUserInfoWidget' );
}
add_action( 'widgets_init', 'register_lmxWidget' );