<?php get_header('general')?>
<div class="main">
    <header class="header">
        <div class="header-wrapper">
            <div class="card-image" ng-if="!siteStructure.hasOwnProperty(appLocationParts[0])">
                <a href="<?php echo home_url().'/#/'; ?>">
                    <img src="wp-content/themes/bin/assets/img/header/logo-card.png" alt="" width="380" height="240">
                </a>
            </div>
            <div class="logo" ng-if="siteStructure.hasOwnProperty(appLocationParts[0])">
                <a href="<?php echo home_url().'/#/'; ?>">
                    <img src="wp-content/themes/bin/assets/img/header/logo.png" alt="" width="280" height="180">
                </a>
            </div>
            <div class="hotline"><a href="tel:88005001562">8 800 500-15-62</a></div>
            <div class="header-menu">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_class' => 'top-menu',
                    'container' => false,
                    'depth' => 0
                ));
                ?>
            </div>
            <a ng-if="isAuth() && siteStructure.hasOwnProperty(appLocationParts[0])" class="pseudo-link sign-out-btn" ng-click="logout()"><img src="wp-content/themes/bin/assets/img/header/sign_out.svg" width="20" height="20" alt="">Выход</a>
        </div>
    </header>
    <div id="modal-wrapper" ng-class="{'block' : ['registration', 'resetPassword'].contains(appLocationParts[0])}">
        <div id="modal">
            <registration ng-if="currentLocation=='registration'"></registration>
            <reset-password ng-if="currentLocation=='resetPassword'"></reset-password>
            <a ng-click="logout(true); changeLocation('');" id="modal-close-ico">&times;</a>
        </div>
    </div>