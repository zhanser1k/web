<?php
/**
 * Widget API: LmxUserInfoWidget class
 */

class LmxUserInfoWidget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'widget-userinfo',
            'description' => __( 'Показывает информацию о пользователе' ),
            'customize_selective_refresh' => true,
        );
        $control_ops = array(
            'width' => 400,
            'height' => 350,
        );
        parent::__construct( 'lmx-user-info', __( 'User-info' ), $widget_ops, $control_ops );
    }

    public function widget( $args, $instance ) {

        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters( 'widget_userinfo', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        $text = ! empty( $instance['text'] ) ? $instance['text'] : '';

        $text = apply_filters( 'widget_userinfo', $text, $instance, $this );

        if ( isset( $instance['filter'] ) ) {
            if ( 'content' === $instance['filter'] ) {
                $text = apply_filters( 'widget_userinfo_content', $text, $instance, $this );

            } elseif ( $instance['filter'] ) {
                $text = wpautop( $text ); // Back-compat for instances prior to 4.8.
            }
        }

        echo $args['before_widget'];
        if ( ! empty( $title ) ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        ?>
        <div class="left-user-info" ng-if="isAuth() && userInfo">
            <div class="initials">
                {{userInfo.lastName}}
                {{userInfo.firstName}}
                {{userInfo.patronymicName}}
            </div>
            <div class="card-info">
                Карта: <span class="card" ng-if="cardInfo">{{cardInfo.number | charsDivide: 4}}</span>
            </div>
        </div>
        <?php
        echo $args['after_widget'];
    }
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        if ( current_user_can( 'unfiltered_html' ) ) {
            $instance['text'] = $new_instance['text'];
        } else {
            $instance['text'] = wp_kses_post( $new_instance['text'] );
        }

        $instance['filter'] = 'content';

        return $instance;
    }

    public function form( $instance ) {
        $instance = wp_parse_args(
            (array) $instance,
            array(
                'title' => '',
                'text' => '',
            )
        );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title') ?>">Заголовок:</label>
            <input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?>" value="<?php if( isset($title) ) echo esc_attr( $title ); ?>" class="widefat">
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('text') ?>">Информация:</label>
            <textarea class="widefat" name="<?php echo $this->get_field_name('text') ?>" id="<?php echo $this->get_field_id('text') ?>" cols="20" rows="5"><?php if( isset($text) ) echo esc_attr( $text ); ?></textarea>
        </p>

        <?php
    }
}
