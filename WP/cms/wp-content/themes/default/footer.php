</div> 
<!-- #main -->

<?php do_action( 'basic_before_footer' ); ?>

<div class="hFooter"></div>
<footer id="footer" class="<?php echo apply_filters( 'basic_footer_class', 'footer' );?>">

	<?php do_action( 'basic_before_footer_menu' ); ?>

	<?php if (has_nav_menu('bottom')) : ?>
	<div class="<?php echo apply_filters( 'basic_footer_menu_class', 'footer-menu maxwidth' );?>">
		<?php
		wp_nav_menu( array(
				'theme_location' => 'bottom',
				'menu_id' => 'footer-menu',
				'depth' => 1,
				'container' => false,
				'items_wrap' => '<ul class="footmenu clearfix">%3$s</ul>'
			));
		?>
	</div>
	<?php endif; ?>

	<?php do_action( 'basic_before_footer_copyrights' ); ?>
    <?php if ( apply_filters( 'basic_footer_copyrights_enabled', true ) ) : ?>
	<div class="<?php echo apply_filters( 'basic_footer_copyrights_class', 'copyrights maxwidth grid' );?>">
		<div class="<?php echo apply_filters( 'basic_footer_copytext_class', 'copytext col4' );?>">
			<p id="copy">
				&copy; <?php echo date("Y",time()); ?> «<!--noindex--><?php bloginfo('name'); ?><!--/noindex-->»
				<br/>
				<span class="copyright-text"><?php echo basic_get_theme_option('copyright_text'); ?></span>
			</p>
		</div>

        <div class="callcenter col3">
            <p id="callcenter">
                <span class="callcenter-text"></span>
                <span class="callcenter-phone">88000000000</span>
            </p>
        </div>

        <div class="links col5">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'social',
                'container' => false,
                'items_wrap' => '<ul id="links">%3$s</ul>',
                'depth' => 1
            ) );
            ?>
            <span id="developer">Разработано <a href="http://loymax.ru/" target="_blank">Loymax.ru</a></span>
		</div>
	</div>
    <?php endif; ?>
	<?php do_action( 'basic_after_footer_copyrights' ); ?>

</footer>
<?php do_action( 'basic_after_footer' ); ?>


</div> 
<!-- .wrapper -->

<a id="toTop">&#10148;</a>

<?php wp_footer(); ?>

</body>
</html>