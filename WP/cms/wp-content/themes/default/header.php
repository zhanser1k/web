<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" /><![endif]-->
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body
    ng-class="{'no-scroll': showModal}"
    ng-app="lmxApp"
    ng-controller="mainController"
    ng-init="siteStructure = {<?php do_action('init_loymax_structure'); ?>}"
    ng-cloak
    <?php body_class(); ?>>

    <div
        class="lmx-body-loader"
        ng-hide="allRequestsCompleted"
    ></div>
    <title ng-bind="(currentLocation && siteStructure[appLocationParts[0]] ? (siteStructure[appLocationParts[0]].name + ' « ') : '') + '<?php echo wp_get_document_title();?>'"></title>

    <div class="main-wrapper cleafix">

        <?php do_action( 'basic_before_header' ); ?>
        <!-- BEGIN header -->
        <header id="header" class="<?php echo apply_filters( 'basic_header_class', 'clearfix' ); ?>">

            <?php do_action( 'basic_before_topnav' ); ?>
            <div class="<?php echo apply_filters( 'basic_header_topnav_class', 'topnav grid' ); ?>">

                <div class="top-menu maxwidth">
                    <?php do_action( 'basic_before_sitelogo' );
                    if ( is_home() && ! is_paged() ) { ?>
                        <div id="logo">
                    <?php } else { ?>
                        <a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                    <?php }
                            do_action( 'basic_before_blogname_in_logo' );
                            do_action( 'basic_after_blogname_in_logo' );

                    if ( is_home() && ! is_paged() ) { ?>
                        </div>
                    <?php } else { ?>
                        </a>
                    <?php } ?>
                    <?php do_action( 'basic_after_sitelogo' ); ?>

                    <div class="panel">
                        <ul class="navigation clearfix">
                            <menu-item href="/#/news" target-location="news">Новости</menu-item>
                        </ul>

                        <nav>
                            <?php if ( has_nav_menu( 'top' ) ) :
                            wp_nav_menu( array(
                                'theme_location' => 'top',
                                'menu_id'        => 'navpages',
                                'container'      => false,
                                'items_wrap'     => '<ul class="wp-navigation clearfix">%3$s</ul>'
                            ) );
                            endif; ?>
                        </nav>

                        <ul class="user-portal-actions clearfix" ng-switch="isAuth()">
                            <menu-item ng-switch-when="false" target-location="registration">Зарегистрироваться</menu-item>
                            <menu-item ng-switch-when="false" target-location="login">Войти</menu-item>

                            <menu-item
                                ng-switch-when="true"
                                ng-if="appLocationParts[0] === 'news'"
                                target-location="cabinet"
                            >
                                Личный кабинет
                            </menu-item>
                            <menu-item
                                ng-switch-when="true"
                                ng-if="appLocationParts[0] !== 'news'"
                                target-location="feedback"
                            >
                                Задать вопрос
                            </menu-item>
                            <menu-item ng-switch-when="true" callback="logout()">Выйти</menu-item>
                        </ul>
                    </div>
                </div>

            </div>
            <?php do_action( 'basic_after_topnav' ); ?>

        </header>
        <!-- END header -->

        <?php do_action( 'basic_after_header' ); ?>


        <div id="main" class="maxwidth clearfix">

            <!-- BEGIN content -->

