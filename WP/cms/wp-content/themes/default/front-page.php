﻿<?php get_header(); ?>

<div ng-init="standalonePages = ['login', 'registration', 'reset-password', 'news']"></div>
<div ng-if="appLocationParts[0] === 'news'">
    <lmx-announcement ng-if="appLocationParts[1]" announcement-id="appLocationParts[1]"></lmx-announcement>
    <lmx-announcement ng-if="!appLocationParts[1]" space="offerSpace"></lmx-announcement>
</div>

<!-- Условие !isAuth() не подходит, т.к. не показывается окно завершения регистрации (пользователь на этот момент уже авторизован) -->
<div ng-if="standalonePages.contains(currentLocation)" ng-switch="currentLocation">
    <authentication ng-if="currentLocation === 'login'"></authentication>
    <registration ng-if="currentLocation === 'registration'"></registration>
    <reset-password ng-if="currentLocation === 'reset-password'"></reset-password>
</div>

<div class="content clearfix" ng-if="isAuth() && !standalonePages.contains(appLocationParts[0])">
    <aside class="content-column__left">
        <div class="config-button" ng-click="changeLocation('personal')" title="Настройки"></div>
        <div class="user clearfix">
            <div class="user-info">
                <div class="user-name">
                    <span ng-bind="userInfo.lastName"></span>
                    <span ng-bind="userInfo.firstName"></span>
                    <span ng-bind="userInfo.patronymicName"></span>
                </div>

                <dl class="user-balance clearfix">
                    <dt class="user-balance__key">Баланс:&nbsp;</dt>
                    <dd class="user-balance__value">
                        <span ng-bind=":: (userInfo.balanceShortInfo.balance.amount | number: 2)"></span>&nbsp;{{:: 'currency.BNS' | translate}}
                    </dd>

                    <dt class="user-balance__key">Бонусов к активации:&nbsp;</dt>
                    <dd class="user-balance__value">
                        <span ng-bind=":: (userInfo.balanceShortInfo.notActivated.amount | number: 2)"></span>&nbsp;{{:: 'currency.BNS' | translate}}
                    </dd>

                    <dt class="user-balance__key">Получено бонусов:&nbsp;</dt>
                    <dd class="user-balance__value">
                        <span ng-bind=":: (userInfo.balanceShortInfo.accumulated.amount | number: 2)"></span>&nbsp;{{:: 'currency.BNS' | translate}}
                    </dd>

                    <dt ng-if=":: userInfo.attributes['BNS_UserStatus']" class="user-balance__key">
                        <span ng-bind=":: userInfo.attributes['BNS_UserStatus'].info.name + ':&nbsp;'"></span>
                    </dt>
                    <dd ng-if=":: userInfo.attributes['BNS_UserStatus']" class="user-balance__value">
                        <span ng-bind=":: userInfo.attributes['BNS_UserStatus'].value.stringValue"></span>
                    </dd>

                    <dt ng-if=":: userInfo.attributes['BNS_UserPurchasesAmount']" class="user-balance__key">
                        <span ng-bind=":: userInfo.attributes['BNS_UserPurchasesAmount'].info.name + ':&nbsp;'"></span>
                    </dt>
                    <dd ng-if=":: userInfo.attributes['BNS_UserPurchasesAmount']" class="user-balance__value">
                        <span ng-bind=":: userInfo.attributes['BNS_UserPurchasesAmount'].value.doubleValue | number: 2"></span>&nbsp;{{:: 'currency.RUB' | translate}}
                    </dd>
                </dl>
            </div>
        </div>

        <nav class="menu">
            <a class="menu-item"
               ng-class="{'active': appLocationParts[0] == menuItem.$key}"
               ng-repeat="menuItem in siteStructure | toArray | filter : {isMenuItem: true} | orderBy: 'menuItemOrder'"
               ng-href="#{{:: menuItem.$key}}"
               ng-bind=":: menuItem.name"
            ></a>
        </nav>
    </aside>

    <section class="content-column__right" ng-switch="currentLocation">
        <offers ng-switch-when="cabinet"></offers>

        <cards ng-switch-when="cards"></cards>

        <history ng-switch-when="history"></history>

        <div class="settings" ng-if="['personal', 'contacts', 'accounts'].contains(currentLocation)">
            <nav class="menu grid clearfix lmx-margin-bottom">
                <a class="menu-item col4" ng-class="{'active': currentLocation == 'personal'}" ng-click="changeLocation('personal')">Личные данные</a>
                <a class="menu-item col4" ng-class="{'active': currentLocation == 'contacts'}" ng-click="changeLocation('contacts')">Контакты</a>
                <a class="menu-item col4" ng-class="{'active': currentLocation == 'accounts'}" ng-click="changeLocation('accounts')">Аккаунты</a>
            </nav>

            <div class="personal" ng-switch-when="personal">
                <questionnaire registration="false" edit-mode is-disabled></questionnaire>
            </div>

            <div class="contacts" ng-switch-when="contacts">
                <div class="lmx-container">
                    <div class="row lmx-settings">
                        <user-phone></user-phone>
                        <user-email></user-email>
                    </div>
                </div>
            </div>

            <div class="accounts" ng-switch-when="accounts">
                <div class="lmx-container">
                    <div class="row lmx-settings">
                        <user-password></user-password>
                        <lmx-social is-binding></lmx-social>
                    </div>
                </div>
            </div>
        </div>

        <feedback ng-switch-when="feedback"></feedback>

        <section class="personal-offers-section" ng-if="appLocationParts[0] === 'personal-offers'">
            <personal-offers ng-if="!appLocationParts[1]"></personal-offers>
            <personal-offer ng-if="appLocationParts[1] && !appLocationParts[2]" offer-id="appLocationParts[1]"></personal-offer>
        </section>
        <section ng-if="appLocationParts[0] === 'personal-goods'">
            <personal-goods ng-if="!appLocationParts[1]"></personal-goods>
            <personal-goods-offer ng-if="appLocationParts[1]" offer-id="appLocationParts[1]"></personal-goods-offer>
        </section>
    </section>
</div>

<div ng-if="siteStructure.hasOwnProperty(appLocationParts[0]) && !isAuth() && !standalonePages.contains(appLocationParts[0])"
     ng-init="changeLocation('login')"></div>
<div ng-if="(currentLocation === '' && allRequestsCompleted) || (!siteStructure.hasOwnProperty(appLocationParts[0]) && currentLocation !== '')"
    ng-init="changeLocation(isAuth() ? 'news' : 'login')"></div>

<notifications></notifications>

<?php get_footer(); ?>
