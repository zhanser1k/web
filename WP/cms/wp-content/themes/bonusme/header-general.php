<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <script src="//yastatic.net/jquery/3.1.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="wp-content/themes/bonusme/assets/js/jquery.min.js"><\/script>')</script>

    <link rel="stylesheet" href="<?php echo get_stylesheet_uri() ?>" />
    <?php wp_head(); ?>
</head>

<body
    ng-class="{'no-scroll': showModal}"
    ng-app="lmxApp"
    ng-controller="mainController"
    ng-init="pageTitle = {
      'login': 'BonusMe',
      'home': 'BonusMe',
      'cabinet': 'Личный кабинет',
      'history': 'Личный кабинет',
      'cards': 'Личный кабинет',
      'questionnaire': 'Личный кабинет',
      'settings': 'Личный кабинет',
      'feedback': 'Личный кабинет',
      'resetPassword': 'Восстановление пароля',
      'registration': 'Активация карты'
    }"
    ng-cloak
    <?php body_class(); ?>>
    <div
        class="body-loader"
        ng-hide="allRequestsCompleted"
        style="background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzgiIGhlaWdodD0iMzgiIHZpZXdCb3g9IjAgMCAzOCAzOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBzdHJva2U9IiMzRDNBODciPiAgICA8ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPiAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSIgc3Ryb2tlLXdpZHRoPSIyIj4gICAgICAgICAgICA8Y2lyY2xlIHN0cm9rZS1vcGFjaXR5PSIuNSIgY3g9IjE4IiBjeT0iMTgiIHI9IjE4Ii8+ICAgICAgICAgICAgPHBhdGggZD0iTTM2IDE4YzAtOS45NC04LjA2LTE4LTE4LTE4Ij4gICAgICAgICAgICAgICAgPGFuaW1hdGVUcmFuc2Zvcm0gICAgICAgICAgICAgICAgICAgICAgICBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT0icm90YXRlIiAgICAgICAgICAgICAgICAgICAgICAgIGZyb209IjAgMTggMTgiICAgICAgICAgICAgICAgICAgICAgICAgdG89IjM2MCAxOCAxOCIgICAgICAgICAgICAgICAgICAgICAgICBkdXI9IjFzIiAgICAgICAgICAgICAgICAgICAgICAgIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIi8+ICAgICAgICAgICAgPC9wYXRoPiAgICAgICAgPC9nPiAgICA8L2c+PC9zdmc+);"
    ></div>
    <title ng-bind="(currentLocation ? pageTitle[appLocationParts[0]] : pageTitle.home)">BonusMe</title>
