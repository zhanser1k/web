    <div class="hFooter"></div>
</div>

<footer class="footer-wrapper">
    <div class="footer">
        <div class="footer-info">
            <p>&copy;&nbsp;2018&nbsp;&laquo;BonusMe&raquo; Все права защищены</p>
            <p><a ng-href="{{::apiHost}}api/Files/{{::options.opdAgreementFileId}}" target="_blank">Политика обработки персональных данных</a></p>
        </div>

        <div class="footer-links">
            <div class="footer-develop">Разработано ТОО «Цифровые решения»</div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
