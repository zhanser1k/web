<?php
// Удаление отступа для дефолтного логин-бара WordPress
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
add_theme_support( 'menus' );
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'bonusme' ),
) );
