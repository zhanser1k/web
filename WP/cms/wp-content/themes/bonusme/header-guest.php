<?php get_header('general')?>
<div class="main">

    <header class="header">
        <div class="header-inner">
            <div class="header-logo">
                <a href="https://cabinet.bonusme.kz/#/"><img src="wp-content/themes/bonusme/assets/img/header/logo.png" alt="" width="120" height="60"></a>
            </div>

            <a class="header-phone" href="tel:87277777777"><span>8 (727) 777-77-77</span></a>

            <div class="header-logout" ng-click="logout()" ng-if="isAuth()"><span>Выход</span></div>

            <div class="header-menu">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_class' => 'top-menu',
                    'container' => false,
                    'depth' => 0
                ));
                ?>
            </div>
        </div>
    </header>