<?php get_header("guest");?>

<notifications></notifications>

<authentication ng-if="['login', 'home'].contains(currentLocation) || currentLocation === '' && !isAuth()"></authentication>
<registration ng-if="currentLocation === 'registration'"></registration>
<reset-password ng-if="currentLocation === 'resetPassword'"></reset-password>

<main class="content clearfix" ng-if="isAuth() && !['login', 'registration', 'resetPassword', 'confirmEmail'].contains(currentLocation)">

    <div class="content-column__left">
        <div class="user clearfix">
            <div class="user-info">
                <h2 class="user-name">
                    <span ng-bind="userInfo.lastName"></span>
                    <span ng-bind="userInfo.firstName"></span>
                    <span ng-bind="userInfo.patronymicName"></span>
                </h2>

                <dl class="user-balance clearfix">
                    <dt class="user-balance__key">Баланс</dt>
                    <dd class="user-balance__value" data-count="{{userInfo.balanceShortInfo.balance.amount}}">
                        <strong ng-bind="userInfo.balanceShortInfo.balance.amount"></strong>
                        <span ng-bind-html="userInfo.balanceShortInfo.balance.amount | translatePlural: {plural: 'bonusPlurals.bonus_clear'}"></span>
                    </dd>
                </dl>
                <dl class="user-accumulated clearfix">
                    <dt class="user-balance__key">Получено на карту</dt>
                    <dd class="user-balance__value" data-count="{{userInfo.balanceShortInfo.accumulated.amount}}">
                        <strong ng-bind="userInfo.balanceShortInfo.accumulated.amount"></strong>
                        <span
                                ng-if="userInfo.balanceShortInfo.accumulated.amount % 1 === 0"
                                ng-bind-html="userInfo.balanceShortInfo.accumulated.amount | translatePlural: {plural: 'bonusPlurals.bonus_clear'}"
                        ></span>
                        <span ng-if="userInfo.balanceShortInfo.accumulated.amount % 1 !== 0">бонуса</span>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="angularMenu-wrapper" ng-if="isAuth() && !['login', 'registration', 'resetPassword'].contains(currentLocation)">
            <div class="angularMenu">
                <div class="angularMenu-item" ng-class="{'_active': currentLocation === 'cabinet'}"><a href="#cabinet">Личный кабинет</a></div>
                <div class="angularMenu-item" ng-class="{'_active': currentLocation === 'cards'}"><a href="#cards">Бонусный счет</a></div>
                <div class="angularMenu-item" ng-class="{'_active': currentLocation === 'history'}"><a href="#history">История операций</a></div>
                <div class="angularMenu-item" ng-class="{'_active': currentLocation === 'questionnaire'}"><a href="#questionnaire">Личные данные</a></div>
                <div class="angularMenu-item" ng-class="{'_active': currentLocation === 'settings'}"><a href="#settings">Настройки</a></div>
                <div class="angularMenu-item" ng-class="{'_active': currentLocation === 'feedback'}"><a href="#feedback">Обратная связь</a></div>
            </div>
        </div>
    </div>
    <div class="content-column__right">
        <cards ng-if="currentLocation === 'cards'"></cards>

        <history ng-if="currentLocation === 'cabinet' || currentLocation === ''" on-page="5"></history>
        <history ng-if="currentLocation === 'history'"></history>

        <div class="questionnaire" ng-if="currentLocation === 'questionnaire'">
            <h1 class="questionnaire-title">Основные данные</h1>
            <questionnaire registration="false" edit-mode is-disabled></questionnaire>
        </div>
        <div class="feedback form" ng-show="currentLocation === 'feedback'">
            <h2 class="feedback-title">Обратная связь</h2>
            <?php echo do_shortcode('[contact-form-7 id="6" title="Main contact form"]');?>
        </div>
        <div class="settings" ng-if="currentLocation === 'settings'">
            <h1 class="settings-title">Настройки личного кабинета</h1>
            <user-phone></user-phone>
            <user-email></user-email>
            <user-password></user-password>
            <div class="social-settings">
                <lmx-social is-binding></lmx-social>
            </div>
        </div>
    </div>
</main>

<?php get_footer("guest");?>
