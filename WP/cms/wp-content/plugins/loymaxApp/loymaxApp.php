<?php
/**
 * @package Loymax
 * @version 1.0
 */
/*
Plugin Name: Loymax
Plugin URI: http://loymax.ru/
Description: Loymax loyalty program User Portal modules.
Author: Kosov Maxim
Version: 1.0
*/

global $wpdb, $configs_table_name, $structure_table_name, $pluginTitle, $optionsPageSlug, $configPageSlug, $structurePageSlug;
$configs_table_name = $wpdb->prefix . 'loymax';
$structure_table_name = $wpdb->prefix . 'loymax_structure';
$pluginTitle = 'Настройка портала пользователя Loymax';
$optionsPageSlug = 'loymax-plugin';
$configPageSlug = 'loymax-plugin_configs';
$structurePageSlug = 'loymax-plugin_structure';

$wpdb->show_errors();

function createTables () {
    global $wpdb, $configs_table_name, $structure_table_name;

    $charset_collate = $wpdb->get_charset_collate();

    $configsSql = "CREATE TABLE $configs_table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		config_key varchar(50) NOT NULL,
	    config_value text DEFAULT '' NOT NULL,
	    config_type varchar(20) DEFAULT 'text' NOT NULL,
	    is_option tinyint(1) NOT NULL,
		PRIMARY KEY  (id),
	    UNIQUE KEY config_key (config_key)
	) $charset_collate;";

    $structureSql = "CREATE TABLE $structure_table_name (
		id tinyint(2) NOT NULL AUTO_INCREMENT,
		url varchar(50) NOT NULL,
	    name varchar(50) NOT NULL,
	    is_menu_item tinyint(1) NOT NULL,
	    menu_item_order tinyint(2) NULL,
		PRIMARY KEY  (id),
	    UNIQUE KEY url (url)
	) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $createConfigsResult = dbDelta($configsSql);
    if (count($createConfigsResult) > 0)
        initConfigsTable();

    $createStructureResult = dbDelta($structureSql);
    if (count($createStructureResult) > 0)
        initStructureTable();
}

function initConfigsTable () {
    include($_SERVER['DOCUMENT_ROOT'].'/config/web.config.php');
    global $wpdb, $configs_table_name;

    $configData = array(
        array('key' => 'appHost', 'value' => 'http://master.iis3.local/', 'type' => 'text', 'isOption' => false),
        array('key' => 'apiHost', 'value' => 'http://public.master.iis.local/api/', 'type' => 'text', 'isOption' => false),
        array('key' => 'showcaseApiHost', 'value' => 'http://public.master.iis.local/showcase/', 'type' => 'text', 'isOption' => false),
        array('key' => 'viewsPath', 'value' => '/wp-content/themes/'.$theme.'/assets/views/', 'type' => 'text', 'isOption' => false),
        array('key' => 'imagesPath', 'value' => '/wp-content/themes/'.$theme.'/assets/images/', 'type' => 'text', 'isOption' => false),
        array('key' => 'acceptTenderOfferByCheck', 'value' => 'false', 'type' => 'bool', 'isOption' => true),
        array('key' => 'authorizeOnRegistrationComplete', 'value' => 'true', 'type' => 'bool', 'isOption' => true),
        array('key' => 'offerPdfFileId', 'value' => '6926', 'type' => 'text', 'isOption' => true),
        array('key' => 'offerHtmlFileId', 'value' => '42ee8186-4557-4bae-30c7-eb6c54a1ac79', 'type' => 'text', 'isOption' => true),
        array('key' => 'opdAgreementFileId', 'value' => 'c0b8ccb1-e25c-45f2-7900-aa8f271735a7', 'type' => 'text', 'isOption' => true),
        array('key' => 'redirectUrlOnRegistrationComplete', 'value' => "#news", 'type' => 'text', 'isOption' => true),
        array('key' => 'redirectUrlOnLogin', 'value' => '#news', 'type' => 'text', 'isOption' => true),
        array('key' => 'redirectUrlOnLogout', 'value' => '/', 'type' => 'text', 'isOption' => true),
        array('key' => 'redirectUrlOnEmailConfirmForSettings', 'value' => '#contacts', 'type' => 'text', 'isOption' => true),
        array('key' => 'redirectUrlOnEmailConfirmForRegistration', 'value' => '#registration', 'type' => 'text', 'isOption' => true),
        array('key' => 'redirectUrlOnEmailConfirmWithoutToken', 'value' => '#login', 'type' => 'text', 'isOption' => true),
        array('key' => 'redirectUrlOnResetPasswordEmailConfirm', 'value' => '#reset-password', 'type' => 'text', 'isOption' => true)
    );


    foreach ($configData as $config) {
        $wpdb->insert(
            $configs_table_name,
            array(
                'config_key' => $config['key'],
                'config_value' => $config['value'],
                'config_type' => $config['type'],
                'is_option' => $config['isOption']
            )
        );
    }
}

function initStructureTable () {
    global $wpdb, $structure_table_name;

    $structureData = array(
        array('url' => 'login', 'name' => 'Вход', 'is_menu_item' => false),
        array('url' => 'reset-password', 'name' => 'Восстановление пароля', 'is_menu_item' => false),
        array('url' => 'registration', 'name' => 'Регистрация', 'is_menu_item' => false),
        array('url' => 'cabinet', 'name' => 'Личный кабинет', 'is_menu_item' => true, 'menu_item_order' => 1),
        array('url' => 'cards', 'name' => 'Мои карты', 'is_menu_item' => true, 'menu_item_order' => 2),
        array('url' => 'history', 'name' => 'История операций', 'is_menu_item' => true, 'menu_item_order' => 3),
        array('url' => 'personal-offers', 'name' => 'Персональные предложения', 'is_menu_item' => true, 'menu_item_order' => 4),
        array('url' => 'personal-goods', 'name' => 'Персональные товары', 'is_menu_item' => true, 'menu_item_order' => 5),
        array('url' => 'personal', 'name' => 'Личные данные', 'is_menu_item' => false),
        array('url' => 'contacts', 'name' => 'Контакты', 'is_menu_item' => false),
        array('url' => 'accounts', 'name' => 'Аккаунты', 'is_menu_item' => false),
        array('url' => 'news', 'name' => 'Новости', 'is_menu_item' => false),
        array('url' => 'merchants', 'name' => 'Магазины', 'is_menu_item' => false),
        array('url' => 'feedback', 'name' => 'Обратная связь', 'is_menu_item' => false),
    );


    foreach ($structureData as $structureItem) {
        $wpdb->insert(
            $structure_table_name,
            array(
                'url' => $structureItem['url'],
                'name' => $structureItem['name'],
                'is_menu_item' => $structureItem['is_menu_item'],
                'menu_item_order' => $structureItem['menu_item_order']
            )
        );
    }
}

function getConfigs($optionsOnly = null) {
    global $wpdb, $configs_table_name;

    $configsSql = 'SELECT config_key, config_value, config_type, is_option FROM ' . $configs_table_name;
    if (!is_null($optionsOnly)) {
        $configsSql .= ' WHERE is_option = ' . ($optionsOnly ? 'TRUE' : 'FALSE');
    }
    $configs = $wpdb->get_results($configsSql, OBJECT_K);

    foreach ($configs as $key => $config) {
        if ($config->config_type == 'bool') {
            $configs[$key]->config_value = filter_var($config->config_value, FILTER_VALIDATE_BOOLEAN);
        }
    }

    return $configs;
}

function getStructure() {
    global $wpdb, $structure_table_name;

    $structure = $wpdb->get_results('SELECT url, name, is_menu_item, menu_item_order FROM ' . $structure_table_name, OBJECT_K);

    foreach ($structure as $key => $structureItem) {
        $structure[$key]->is_menu_item = filter_var($structureItem->is_menu_item, FILTER_VALIDATE_BOOLEAN);

        $structure[$key]->menu_item_order = filter_var($structureItem->menu_item_order, FILTER_VALIDATE_INT) ?: null;
    }

    return $structure;
}

function saveConfigsForm() {
    global $wpdb, $configs_table_name;

    if ($_POST && !empty($_POST)) {
        switch ($_POST['action']) {
            case 'add':
                $replaceResult = $wpdb->replace(
                    $configs_table_name,
                    array(
                        'config_key' => wp_unslash($_POST['optionName']),
                        'config_type' => wp_unslash($_POST['optionType']),
                        'config_value' => wp_unslash($_POST['optionValue']),
                        'is_option' => 1
                    )
                );
                animateUpdatedLine($replaceResult, 'form#loymax-configs tr#'.$_POST['optionName'].'_row');
                break;
            case 'delete':
                $wpdb->query(
                    $wpdb->prepare("DELETE FROM {$configs_table_name} WHERE config_key = %s", $_POST['itemName'])
                );
                break;
            default:
                foreach ($_POST as $key => $configValue) {
                    $wpdb->update(
                        $configs_table_name,
                        array('config_value' => wp_unslash($configValue)),
                        array('config_key' => $key)
                    );
                }
                break;
        }
    }
}

function saveStructureForm() {
    global $wpdb, $structure_table_name;

    if ($_POST && !empty($_POST)) {
        switch ($_POST['action']) {
            case 'add':
                $replaceResult = $wpdb->replace(
                    $structure_table_name,
                    array(
                        'url' => wp_unslash($_POST['itemUrl']),
                        'name' => wp_unslash($_POST['itemName']),
                        'is_menu_item' => $_POST['isMenuItem'],
                        'menu_item_order' => $_POST['menuItemOrder']
                    )
                );
                animateUpdatedLine($replaceResult, 'form#loymax-structure tr#'.$_POST['itemUrl'].'_row');
                break;
            case 'delete':
                $wpdb->query(
                    $wpdb->prepare("DELETE FROM {$structure_table_name} WHERE url = %s", $_POST['itemName'])
                );
                break;
            default:
                foreach ($_POST as $key => $configValue) {
                    $wpdb->update(
                        $structure_table_name,
                        array(
                            'url' => $configValue['url'],
                            'name' => $configValue['name'],
                            'is_menu_item' => filter_var($configValue['is_menu_item'], FILTER_VALIDATE_BOOLEAN),
                            'menu_item_order' => filter_var($configValue['menu_item_order'], FILTER_VALIDATE_INT) ?: null
                        ),
                        array('url' => $key)
                    );
                }
                break;
        }
    }
}

function animateUpdatedLine($updateResult, $lineSelector) {
    if ($updateResult !== false) {?>
        <script type="text/javascript">
            jQuery(function () {
                jQuery('html, body').animate({ scrollTop: jQuery(document).height() }, {
                    duration: 'fast',
                    complete: function() {
                        jQuery('<?=$lineSelector?>').addClass('highlight_updated');
                    }
                });
            });
        </script><?php
    }
}

function pluginPageInit($pageSlug) {
    ?>
    <script type="text/javascript">
        var deleteItemModal;

        jQuery(function () {
            deleteItemModal = jQuery('#loymax-deleteItem').dialog({
                autoOpen: false,
                width: 400,
                modal: true,
                draggable: false
            });
        });

        function loymaxShowDeleteModal(itemName, isOption) {
            deleteItemModal.find('input#itemName').val(itemName);
            deleteItemModal.find('#itemNameField').html((isOption ? 'опцию' : 'элемент') + ' <strong>' + itemName + '</strong>');
            deleteItemModal.dialog('open');
        }
    </script>

    <!-- Шаблон модального окна удаления опции, используемый jQueryUI Dialog-->
    <div style="display: none;">
        <form id="loymax-deleteItem" method="post" action="<?php echo admin_url('admin.php?page='.$pageSlug); ?>">
            <div>
                Вы уверены, что хотите удалить <span id="itemNameField"></span>?
            </div>
            <input type="hidden" id="itemName" name="itemName">
            <input type="hidden" name="action" value="delete">
            <div class="controls">
                <button class="button button-secondary" type="button" onclick="deleteItemModal.dialog('close')">Отмена</button>
                <button class="button button-primary" type="submit" autofocus>Да</button>
            </div>
        </form>
    </div>
    <?php
}

function configsPageInit($isOptionsPage = true){
    global $pluginTitle, $optionsPageSlug;

    saveConfigsForm();
    $configs = getConfigs($isOptionsPage);

    pluginPageInit($optionsPageSlug);
    ?>

    <script type="text/javascript">
        var addOptionModal;

        jQuery(function () {
            addOptionModal = jQuery('#loymax-addOption').dialog({
                autoOpen: false,
                width: 450,
                modal: true,
                draggable: false,
                title: 'Добавление опции',
                close: function () {
                    jQuery('form#loymax-addOption')[0].reset();
                }
            });

            jQuery('form#loymax-configs a.add-option').click(event, function () {
                event.preventDefault();
                addOptionModal.dialog('open');
            });
        });

        function loymaxChangeOptionValueInput(valueType) {
            var inputContainer = jQuery('form#loymax-addOption #optionValue').parent(),
                inputId = 'optionValue',
                newInput;

            switch (valueType) {
                case 'bool':
                    newInput = [
                        jQuery('<input type="hidden" id="' + inputId + '_hidden" name="' + inputId + '" value="false">'),
                        jQuery('<input type="checkbox" id="' + inputId + '" name="' + inputId + '" value="true">')
                    ];
                    break;
                case 'object':
                    newInput = jQuery('<textarea id="' + inputId + '" name="' + inputId + '" required></textarea>');
                    break;
                default:
                    newInput = jQuery('<input type="text" id="' + inputId + '" name="' + inputId + '" required>');
                    break;
            }

            inputContainer.empty();
            inputContainer.append(newInput);
            if (Array.isArray(newInput)) {
                newInput.forEach(function (input) {
                    input.focus();
                });
            } else {
                newInput.focus();
            }
        }
    </script>
    <h1><?php
        if ($isOptionsPage)
            echo($pluginTitle);
        else
            echo 'Настройка конфигурации';
        ?></h1>

    <form id="loymax-configs" name="loymax-configs" method="post" enctype="multipart/form-data">
        <table class="widefat striped">
            <?php foreach ($configs as $config):
                $type = $config->config_type == 'bool' ? 'checkbox' : $config->config_type ?>
                <tr id="<?=$config->config_key?>_row">
                    <td><label for="<?=$config->config_key?>"><?=$config->config_key?></label></td>
                    <td>
                        <?php if ($type == 'checkbox'): ?>
                            <input type="hidden"
                                   id="<?=$config->config_key?>_hidden"
                                   name="<?=$config->config_key?>"
                                   value="false"
                            />
                        <?php endif; ?>

                        <?php if ($type == 'object'): ?>
                            <textarea
                                    id="<?=$config->config_key?>"
                                    name="<?=$config->config_key?>"
                            ><?php echo $config->config_value ?></textarea>
                        <?php else: ?>
                            <input type="<?=$type?>"
                                   id="<?=$config->config_key?>"
                                   name="<?=$config->config_key?>"
                                   value="<?php echo $type == 'checkbox' ? 'true' : $config->config_value ?>"
                                <?php if ($type == 'checkbox' && $config->config_value == true) echo 'checked' ?>
                            />
                        <?php endif; ?>
                    </td>
                    <?php if ($isOptionsPage): ?>
                        <td class="remover">
                            <a href="#" onclick="event.preventDefault(); loymaxShowDeleteModal('<?=$config->config_key?>', true);" title="Удалить">
                                <svg stroke-linejoin="round"
                                     stroke-linecap="round"
                                     stroke-width="2"
                                     stroke="currentColor"
                                     fill="none"
                                     viewBox="0 0 20 20"
                                     height="20"
                                     width="20"
                                     xmlns="http://www.w3.org/2000/svg"
                                >
                                    <line x1="16" y1="4" x2="4" y2="16"></line>
                                    <line x1="4" y1="4" x2="16" y2="16"></line>
                                </svg>
                            </a>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </table>
        <div class="controls">
            <button class="button button-primary" type="submit">Сохранить</button>
            <?php if ($isOptionsPage): ?>
                <a href="#" class="add-option">Добавить</a>
            <?php endif; ?>
        </div>
    </form>

    <!-- Шаблон модального окна добавления опции, используемый jQueryUI Dialog -->
    <div style="display: none;">
        <form id="loymax-addOption" method="post" action="<?php echo admin_url('admin.php?page='.$optionsPageSlug); ?>">
            <table>
                <tr>
                    <td><label for="optionName">Опция</label></td>
                    <td><input type="text" id="optionName" name="optionName" required autofocus></td>
                </tr>
                <tr>
                    <td><label for="optionType">Тип значения</label></td>
                    <td>
                        <select id="optionType" name="optionType" required onchange="loymaxChangeOptionValueInput(this.value)">
                            <option value="text" selected>Строковое</option>
                            <option value="bool">Логическое</option>
                            <option value="object">Объект</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="optionValue">Значение</label></td>
                    <td><input type="text" id="optionValue" name="optionValue" required></td>
                </tr>
            </table>
            <input type="hidden" name="action" value="add">
            <button class="button button-primary" type="submit">Добавить</button>
        </form>
    </div>
    <?php
}

function structurePageInit(){
    global $structurePageSlug;

    saveStructureForm();
    $structure = getStructure();

    pluginPageInit($structurePageSlug);
    ?>
    <script type="text/javascript">
        var addStructureItemModal;

        jQuery(function () {
            addStructureItemModal = jQuery('#loymax-addStructureItem').dialog({
                autoOpen: false,
                width: 550,
                modal: true,
                draggable: false,
                title: 'Добавление элемента',
                close: function () {
                    jQuery('form#loymax-addStructureItem')[0].reset();
                }
            });

            jQuery('form#loymax-structure a.add-item').click(event, function () {
                event.preventDefault();
                addStructureItemModal.dialog('open');
            });
        });

        function loymaxEnableMenuItemOrderInput(isMenuItem) {
            var menuItemOrderElement = jQuery('form#loymax-addStructureItem #menuItemOrder');
            menuItemOrderElement.prop('disabled', !isMenuItem);
            menuItemOrderElement.prop('required', isMenuItem);
        }
    </script>

    <h1>Настройка структуры сайта</h1>

    <form id="loymax-structure" name="loymax-structure" method="post" enctype="multipart/form-data">
        <table class="widefat striped">
            <thead>
                <tr>
                    <th>URL</th>
                    <th>Название</th>
                    <th>Пункт меню</th>
                    <th>Очередность пункта меню</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($structure as $structureItem): ?>
                <tr id="<?=$structureItem->url?>_row">
                    <td><input type="text"
                               id="<?=$structureItem->url?>[url]"
                               name="<?=$structureItem->url?>[url]"
                               value="<?=$structureItem->url?>"
                        />
                    <td>
                        <input type="text"
                               id="<?=$structureItem->url?>[name]"
                               name="<?=$structureItem->url?>[name]"
                               value="<?=$structureItem->name?>"
                        />
                    </td>
                    <td>
                        <input type="hidden"
                               id="<?=$structureItem->url?>[is_menu_item]_hidden"
                               name="<?=$structureItem->url?>[is_menu_item]"
                               value="0"
                        />
                        <input type="checkbox"
                               id="<?=$structureItem->url?>[is_menu_item]"
                               name="<?=$structureItem->url?>[is_menu_item]"
                               value="1"
                            <?php if ($structureItem->is_menu_item == true) echo 'checked' ?>
                        />
                    </td>
                    <td>
                        <input type="number"
                               id="<?=$structureItem->url?>[menu_item_order]"
                               name="<?=$structureItem->url?>[menu_item_order]"
                               value="<?=$structureItem->menu_item_order?>"
                        />
                    </td>
                    <td class="remover">
                        <a href="#" onclick="event.preventDefault(); loymaxShowDeleteModal('<?=$structureItem->url?>');" title="Удалить">
                            <svg stroke-linejoin="round"
                                 stroke-linecap="round"
                                 stroke-width="2"
                                 stroke="currentColor"
                                 fill="none"
                                 viewBox="0 0 20 20"
                                 height="20"
                                 width="20"
                                 xmlns="http://www.w3.org/2000/svg"
                            >
                                <line x1="16" y1="4" x2="4" y2="16"></line>
                                <line x1="4" y1="4" x2="16" y2="16"></line>
                            </svg>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="controls">
            <button class="button button-primary" type="submit">Сохранить</button>
            <a href="#" class="add-item">Добавить</a>
        </div>
    </form>

    <!-- Шаблон модального окна добавления элемента структуры сайта, используемый jQueryUI Dialog -->
    <div style="display: none;">
        <form id="loymax-addStructureItem" method="post" action="<?php echo admin_url('admin.php?page='.$structurePageSlug); ?>">
            <table>
                <tr>
                    <td><label for="itemUrl">URL</label></td>
                    <td><input type="text" id="itemUrl" name="itemUrl" required autofocus></td>
                </tr>
                <tr>
                    <td><label for="itemUrl">Название</label></td>
                    <td><input type="text" id="itemName" name="itemName" required></td>
                </tr>
                <tr>
                    <td><label for="isMenuItem">Является пунктом меню</label></td>
                    <td>
                        <input type="hidden" id="isMenuItem_hidden" name="isMenuItem" value="0">
                        <input type="checkbox" id="isMenuItem" name="isMenuItem" value="1" onchange="loymaxEnableMenuItemOrderInput(event.target.checked)">
                    </td>
                </tr>
                <tr>
                    <td><label for="menuItemOrder">Очередность пункта меню</label></td>
                    <td><input type="number" id="menuItemOrder" name="menuItemOrder" disabled></td>
                </tr>
            </table>
            <input type="hidden" name="action" value="add">
            <button class="button button-primary" type="submit">Добавить</button>
        </form>
    </div>
    <?php
}

function loymaxPluginSetupMenu(){
    global $pluginTitle, $optionsPageSlug, $configPageSlug, $structurePageSlug;

    add_menu_page( $pluginTitle, 'Loymax', 'manage_options', 'loymax-plugin' );
    add_submenu_page( 'loymax-plugin', $pluginTitle, 'Опции', 'manage_options', $optionsPageSlug, function() { configsPageInit(true); } );
    add_submenu_page( 'loymax-plugin', $pluginTitle, 'Конфигурация', 'manage_options', $configPageSlug, function() { configsPageInit(false); } );
    add_submenu_page( 'loymax-plugin', $pluginTitle, 'Структура сайта', 'manage_options', $structurePageSlug, 'structurePageInit' );
}

function includeScripts() {
    global $e2e;

    $configs = getConfigs();

    if ($e2e) {
        $e2e = json_decode($e2e);
        $configs['appHost']->config_value = $e2e->host ? $e2e->host : 'https://cdn.loymax.tech/' . $e2e->version . '/';
    }
    ?>

    <script type="text/javascript">
        (function(e){
            'use strict';

            e.config = {
                host: "<?php echo($configs['apiHost']->config_value); ?>",
                templatesPath: "<?php echo($configs['viewsPath']->config_value); ?>",
                imagesPath: "<?php echo($configs['imagesPath']->config_value); ?>",
                <?php if ($configs['showcaseApiHost']) echo('showcase: "'. $configs['showcaseApiHost']->config_value . '",' . "\r\n"); ?>
                options: {
                    <?php foreach ($configs as $config):
                        if ($config->is_option): ?>
                            <?=$config->config_key?>:
                            <?php if ($config->config_type == 'bool') {
                                echo($config->config_value ? 'true' : 'false');
                            } else if ($config->config_type == 'object') {
                                echo wp_kses_stripslashes($config->config_value);
                            } else {
                                echo('"'. $config->config_value . '"');
                            } ?>,
                        <?php endif;?>
                    <?php endforeach; ?>
                }
            };
        })(window);

        <?php
            if ($e2e) {
        ?>
            localStorage.setItem('ls.e2e', 'true');
        <?php
            }
        ?>
    </script>
    <?php

    $currentTheme = wp_get_theme();
    $isDefaultTheme = $currentTheme->get('Name') === 'Loymax Default';
    $isChildTheme = $currentTheme->get('Template') === 'default';

    if ($isDefaultTheme || $isChildTheme) {
        wp_enqueue_style('basic-theme-styles', '/wp-content/themes/default/assets/basic-theme-style.css', array(), md5(microtime()));
        wp_enqueue_style('loymax-materialize-styles', '/wp-content/themes/default/assets/lib/materialize/css/materialize.min.css', array(), null);
        wp_enqueue_style('loymax-glyphicons', '/wp-content/themes/default/assets/lib/glyphicons/css/glyphicons.min.css', array(), null);
        wp_enqueue_style('loymax-wp-styles', get_stylesheet_uri(), array('basic-theme-styles', 'loymax-materialize-styles', 'loymax-styles'), md5(microtime()));

        wp_enqueue_script('loymax-jquery', $configs['appHost']->config_value . 'assets/lib/jquery/jquery.min.js', array(), null);
        wp_enqueue_script('loymax-lodash', $configs['appHost']->config_value . 'assets/lib/lodash/lodash.custom.min.js', array(), null);
        wp_enqueue_script('loymax-materialize-js', $configs['appHost']->config_value . 'assets/lib/materialize/js/materialize.min.js', array(), null);
        wp_enqueue_script('loymax-materialize-actions-js', $configs['appHost']->config_value . 'assets/lib/materialize/js/materialize-actions.js', array(), md5(microtime()));
    }

    wp_enqueue_script( 'loymax-dependencies', $configs['appHost']->config_value.'assets/lib/vendor/dependencies.js', array(), '2.0.0');
    wp_enqueue_script( 'loymax-bowser', $configs['appHost']->config_value.'assets/lib/bowser/bowser.js', array(), '1.0.0');
    if ($e2e) {
        wp_enqueue_script( 'loymax-mocke2e', 'http://cdn.iis3.local/libs/angular-mocks/1.6.10/angular-mocks.min.js', array());
    }
    wp_enqueue_script( 'loymax-app', $configs['appHost']->config_value.'app/app.js', array(), md5(microtime()));


    if ($isDefaultTheme) {
        wp_enqueue_script('loymax-templatecache', $configs['appHost']->config_value.'app/templates.js', array('loymax-app'), md5(microtime()));
        wp_enqueue_style('loymax-styles', $configs['appHost']->config_value . 'assets/css/style.min.css', array(), md5(microtime()));

    } else if ($isChildTheme) {
        wp_enqueue_style('loymax-styles', get_stylesheet_directory_uri() . '/assets/css/style.min.css', array(), md5(microtime()));
    }
}

function initLoymaxStructure() {

    $structureData = getStructure();
    foreach ($structureData as $structureItem) {
        $structureInfo = "\r\n\t'" . $structureItem->url . "': {\r\n";

        $structureInfo .= "\t\tname: '" . $structureItem->name . "',\r\n";
        $structureInfo .= "\t\tisMenuItem: " . ($structureItem->is_menu_item ? "true" : "false"). ",\r\n";
        $structureInfo .= "\t\tmenuItemOrder: " . ($structureItem->menu_item_order ?: "null") . "\r\n";

        $structureInfo .= "\t},";
        echo($structureInfo);
    }
}

add_action('wp_enqueue_scripts', 'includeScripts');
add_action('admin_menu', 'loymaxPluginSetupMenu');
add_action('init_loymax_structure', 'initLoymaxStructure');
add_action('admin_enqueue_scripts', function ($hook) {
    if (strpos($hook, 'page_loymax-plugin') !== false) {
        wp_enqueue_style("jquery-ui-css", "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.min.css");
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-dialog');

        wp_enqueue_style('loymaxStyle', plugins_url('assets/style.css', __FILE__));
    }
});
register_activation_hook(__FILE__, 'createTables');
?>
