{
  "rules": {
    "color-no-invalid-hex": true,

    "font-family-no-duplicate-names": true,

    "function-calc-no-unspaced-operator": true,
    "function-linear-gradient-no-nonstandard-direction": true,

    "string-no-newline": true,

    "unit-no-unknown": true,

    "shorthand-property-no-redundant-values": true,

    "property-no-unknown": true,

    "keyframe-declaration-no-important": true,

    "declaration-block-no-duplicate-properties": [
      true,
      {
        "ignore": [
          "consecutive-duplicates-with-different-values"
        ]
      }
    ],
    "declaration-block-no-redundant-longhand-properties": true,
    "declaration-block-no-shorthand-property-overrides": true,

    "block-no-empty": true,

    "selector-pseudo-class-no-unknown": true,
    "selector-pseudo-element-no-unknown": true,
    "selector-type-no-unknown": [
      true,
      {
        "ignoreTypes": ["/^ng-/", "form-group"]
      }
    ],

    "media-feature-name-no-unknown": true,

    "at-rule-no-unknown": true,

    "comment-no-empty": true,

    "no-descending-specificity": true,
    "no-duplicate-selectors": true,
    "no-extra-semicolons": true,
    "no-empty-source": true,
    "no-invalid-double-slash-comments": true,
    "no-unknown-animations": true,

    "function-url-scheme-blacklist": ["ftp", "/^http/"],

    "number-max-precision": 2,

    "time-min-milliseconds": 100,

    "value-no-vendor-prefix": true,

    "property-no-vendor-prefix": true,

    "declaration-block-single-line-max-declarations": 1,

    "selector-max-empty-lines": 0,
    "selector-no-vendor-prefix": true,

    "media-feature-name-no-vendor-prefix": true,

    "at-rule-no-vendor-prefix": true,

    "color-hex-case": "lower",
    "color-hex-length": "long",

    "font-family-name-quotes": "always-where-recommended",

    "function-comma-space-after": "always-single-line",
    "function-comma-space-before": "never",
    "function-comma-newline-after": "never-multi-line",
    "function-comma-newline-before": "never-multi-line",
    "function-max-empty-lines": 0,
    "function-name-case": "lower",
    "function-parentheses-newline-inside": "never-multi-line",
    "function-parentheses-space-inside": "never",
    "function-url-quotes": [
      "always",
      {
        "except": [
          "empty"
        ]
      }
    ],
    "function-whitespace-after": "always",

    "number-no-trailing-zeros": true,

    "string-quotes": "single",

    "length-zero-no-unit": true,

    "unit-case": "lower",

    "value-keyword-case": [
      "lower",
      {
        "ignoreProperties": [
          "@site-font-family"
        ]
      }
    ],

    "value-list-comma-newline-after": "never-multi-line",
    "value-list-comma-newline-before": "never-multi-line",
    "value-list-comma-space-after": "always-single-line",
    "value-list-comma-space-before": "never",
    "value-list-max-empty-lines": 0,

    "custom-property-empty-line-before": "always",

    "property-case": "lower",

    "declaration-bang-space-after": "never",
    "declaration-bang-space-before": "always",
    "declaration-colon-newline-after": "always-multi-line",
    "declaration-colon-space-after": "always",
    "declaration-colon-space-before": "never",
    "declaration-empty-line-before": "never",

    "declaration-block-semicolon-newline-after": "always",
    "declaration-block-semicolon-newline-before": "never-multi-line",
    "declaration-block-semicolon-space-before": "never",
    "declaration-block-trailing-semicolon": "always",

    "block-closing-brace-empty-line-before": "never",
    "block-closing-brace-newline-after": "always",
    "block-closing-brace-newline-before": "always",
    "block-opening-brace-newline-after": "always",
    "block-opening-brace-space-before": "always",

    "selector-attribute-brackets-space-inside": "never",
    "selector-attribute-operator-space-after": "never",
    "selector-attribute-operator-space-before": "never",
    "selector-attribute-quotes": "always",
    "selector-combinator-space-after": "always",
    "selector-combinator-space-before": "always",
    "selector-descendant-combinator-no-non-space": true,
    "selector-pseudo-class-case": "lower",
    "selector-pseudo-class-parentheses-space-inside": "never",
    "selector-pseudo-element-case": "lower",
    "selector-pseudo-element-colon-notation": "double",
    "selector-type-case": "lower",

    "selector-list-comma-newline-after": "always",
    "selector-list-comma-newline-before": "never-multi-line",
    "selector-list-comma-space-before": "never",

    "rule-empty-line-before": [
      "always",
      {
        "except": [
          "after-single-line-comment",
          "first-nested",
          "inside-block-and-after-rule"
        ]
      }
    ],

    "media-feature-colon-space-after": "always",
    "media-feature-colon-space-before": "never",
    "media-feature-name-case": "lower",
    "media-feature-parentheses-space-inside": "never",
    "media-feature-range-operator-space-after": "always",
    "media-feature-range-operator-space-before": "always",

    "media-query-list-comma-newline-after": "never-multi-line",
    "media-query-list-comma-newline-before": "never-multi-line",
    "media-query-list-comma-space-after": "always",
    "media-query-list-comma-space-before": "never",

    "at-rule-empty-line-before": [
      "always",
      {
        "except": [
          "blockless-after-same-name-blockless",
          "blockless-after-blockless",
          "first-nested"
        ],
        "ignore": [
          "after-comment"
        ]
      }
    ],
    "at-rule-name-case": "lower",
    "at-rule-name-space-after": "always",
    "at-rule-semicolon-newline-after": "always",
    "at-rule-semicolon-space-before": "never",

    "comment-empty-line-before": [
      "always",
      {
        "except": [
          "first-nested"

        ],
        "ignore": [
          "after-comment",
          "stylelint-commands"
        ]
      }
    ],
    "comment-whitespace-inside": "always",

    "indentation": [
      4,
      {
        "indentClosingBrace": false
      }
    ],
    "max-empty-lines": [
      1,
      {
        "ignore": [
          "comments"
        ]
      }
    ],
    "no-eol-whitespace": true,
    "no-missing-end-of-source-newline": true
  },
  "ignoreFiles": [
    "cms/wp-content/themes/bin/**/*.less",
    "cms/wp-content/themes/binreg/**/*.less",
    "cms/wp-content/themes/cosmik/**/*.less",
    "cms/wp-content/themes/gulliver/**/*.less",
    "cms/wp-content/themes/malinka/**/*.less",
    "cms/wp-content/themes/petrovskiy/**/*.less",
    "cms/wp-content/themes/svetloe/**/*.less",
    "cms/wp-content/themes/twentyfifteen/**/*.less",
    "cms/wp-content/themes/twentyseventeen/**/*.less",
    "cms/wp-content/themes/twentysixteen/**/*.less",
    "cms/wp-content/themes/sangi/assets/less/common/main.less",
    "cms/wp-content/themes/**/*.css"
  ]
}
