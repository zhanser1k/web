const gulp = require('gulp');
const {plugins} = require('./gulp/base');
const createTheme = require('./gulp/createTheme.tasks');
const deploy = require('./gulp/deploy.tasks');
const e2e = require('./gulp/e2e.tasks');
const watch = require('./gulp/watch.tasks');
const wp = require('./gulp/wp.tasks');

plugins.taskTime.init();
plugins.task.configure(gulp);

const watchDependencies = ['buildCss'];

//region = main tasks =
gulp.task('build', ['buildCss']);

gulp.task('default', ['build']);

gulp.task('createTheme', createTheme.init);

gulp.task('createTheme:css', createTheme.css);

gulp.task('createTheme:main', createTheme.main);

plugins.task('buildCss', wp.buildCss);

gulp.task('build-main-style', wp.buildMainStyle);
//endregion

//region = watch tasks =
gulp.task('watch', ['browser-sync', 'watch:css', 'watch:views']);

gulp.task('watch:css', watchDependencies, watch.css);

gulp.task('watch:views', watch.views);

gulp.task('browser-sync', watchDependencies, wp.browserSync);
//endregion

//region = deploy tasks =
gulp.task('deploy', ['deploy:config', 'deploy:defaultTheme', 'deploy:theme', 'deploy:uploads']);

gulp.task('clean', wp.clean);

gulp.task('wpBase', ['clean'], wp.wpBase);

gulp.task('deploy:config', ['wpBase'], deploy.config);

gulp.task('deploy:defaultTheme', ['clean'], deploy.defaultTheme);

gulp.task('deploy:theme', ['clean'], deploy.theme);

gulp.task('deploy:uploads', ['clean'], deploy.uploads);
//endregion

//region = e2e tasks =
gulp.task('e2e', e2e.init);

gulp.task('php-server:start', e2e.phpServerStart);

gulp.task('e2e:web-driver-update', plugins.webDriverUpdate);

gulp.task('e2e:run', ['e2e:web-driver-update'], e2e.run);

plugins.task('e2e:clean', e2e.clean);

plugins.task('e2e:protractor', e2e.protractor);

plugins.task('php-server:stop', e2e.phpServerStop);
//endregion
