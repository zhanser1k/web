const {
    appConfig,
    buildConfig,
    plugins,
} = require('./base');

const gulp = require('gulp');

const config = () => {
    return gulp
        .src(['config/web.config.template'])
        .pipe(plugins.replace('{{theme}}', appConfig.theme))
        .pipe(plugins.replace('{{version}}', appConfig.version))
        .pipe(plugins.replace('{{DBHost}}', appConfig.DBHost))
        .pipe(plugins.replace('{{DBUser}}', appConfig.DBUser))
        .pipe(plugins.replace('{{DBPassword}}', appConfig.DBPassword))
        .pipe(plugins.rename({basename: 'web.config', extname: '.php'}))
        .pipe(gulp.dest(buildConfig.configPath));
};

const defaultTheme = () => {
    // todo доработать проверку необходимости наличия папки default в итоговорм проекте
    const path = 'wp-content/themes/default';
    return gulp
        .src(`cms/${path}/**`)
        .pipe(gulp.dest(`${buildConfig.targetPath}/${path}`));
};

const theme = () => {
    //todo добработать генерацию register_nav_menus в functions.php
    const path = `wp-content/themes/${appConfig.theme}`;
    return gulp
        .src(`cms/${path}/**`)
        .pipe(gulp.dest(`${buildConfig.targetPath}/${path}`));
};

const uploads = () => {
    const path = `wp-content/uploads/${appConfig.theme}`;
    return gulp
        .src(`cms/${path}/**`)
        .pipe(gulp.dest(`${buildConfig.targetPath}/${path}`));
};

module.exports = {
    config,
    defaultTheme,
    theme,
    uploads,
};
