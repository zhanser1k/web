const {
    e2e,
    plugins,
} = require('./base');

const versions = JSON.parse(plugins.fs.readFileSync('./config/e2e/versions.json', 'utf8'));

let {server, config} = require('./base');
let host;

function runE2eForCurrentTheme() {
    e2e.currentTheme = e2e.e2eThemes.pop();

    const phpE2e = {
        version: versions[e2e.currentTheme],
        host
    };

    switchTheme(e2e.currentTheme);

    plugins.fs.appendFileSync(e2e.configPath + e2e.configFileName, `$e2e = '${JSON.stringify(phpE2e)}';`, 'utf8');

    return plugins.task.run('e2e:protractor').then(function () {
        return runNextThemeOrStop();
    });
}

function switchTheme(theme) {
    return gulp
        .src(e2e.configPath + e2e.configFileName)
        .pipe(plugins.insert.append('$theme = "'+theme+'";'))
        .pipe(gulp.dest(e2e.configPath));
}


function runNextThemeOrStop(){
    if (e2e.e2eThemes.length) {
        return runE2eForCurrentTheme();
    } else {
        return plugins.task.run('php-server:stop');
    }
}

const gulp = require('gulp');

const init = callback => {
    return plugins.runSequence(
        'php-server:start',
        'e2e:run',
        callback
    );
};

const phpServerStart = () => {
    const port = plugins.argv.port || 8888;
    e2e.freePort = port;

    config = plugins.fs.readFileSync(e2e.configPath + e2e.configFileName, 'utf8');

    server = new plugins.phpServer();

    switch (typeof plugins.argv.appHost) {
        case "boolean": // просто --appHost
            if (plugins.fs.existsSync('./config/e2e/apphost.local')) {
                host = plugins.fs.readFileSync('./config/e2e/apphost.local', 'utf8');
            } else {
                console.log('error, file ./config/e2e/apphost.local not exist');
                process.exit(1);
            }
            break;
        case "string": // --appHost указан со значением
            host = plugins.argv.appHost;
            break;
        default: // --appHost не указан
            host = false;
    }

    server.server({
        port,
        base: e2e.basePath,
        stdio: 'ignore',
    });
};

const run = () => {
    e2e.e2eThemes = plugins.argv.theme ? [plugins.argv.theme] : Object.keys(versions);
    runE2eForCurrentTheme();
};

const protractor = () => {
    const section = plugins.argv.section ? plugins.argv.section : '**';

    return gulp
        .src(`./e2e/${section}/specs/${e2e.currentTheme}.spec.js`)
        .pipe(plugins.protractor({
            configFile: "./config/e2e/protractor.conf.js",
            args: ['--baseUrl', 'http://127.0.0.1:8888']
        }))
        .on('error', function() {
            return plugins.task.run('php-server:stop');
        });
};

const phpServerStop = () => {
    server.closeServer();
    return plugins.task.run('e2e:clean');
};

const clean = () => {
    plugins.fs.writeFile(e2e.configPath + e2e.configFileName, config, function (err) {
        if (err) throw err;
    });
};

module.exports = {
    clean,
    init,
    phpServerStart,
    phpServerStop,
    protractor,
    run,
};
