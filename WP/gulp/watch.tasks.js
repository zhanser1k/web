const {
    plugins,
    path,
} = require('./base');

const css = () => {
    const styles = [
        `${path}/assets/**/*.css`,
        `${path}/assets/**/*.less`
    ];

    return plugins.watch(styles, function () {
        return plugins.task.run('buildCss').then(plugins.browserSync.reload);
    });
};

const views = () => {
    const views = [
        `${path}/*.php`,
        `${path}/assets/views/**/*.html`
    ];

    return plugins.watch(views, plugins.browserSync.reload);
};

module.exports = {
    css,
    views,
};
