function getAppConfig() {
    const defaultConfig = plugins.loadJson.sync('config/web.config.default.json');
    const localConfigFilePath = 'config/config.local.json';
    const localConfig = (plugins.fs.existsSync(localConfigFilePath))
        ? plugins.loadJson.sync(localConfigFilePath)
        : null;

    const argConfig = getArgConfig(defaultConfig);

    return {...defaultConfig, ...localConfig, ...argConfig};
}

function getArgConfig(defaultConfig) {
    const argConfig = {};
    let config;

    for (let key in defaultConfig) {
        config = plugins.argv[key];
        if (config) {
            argConfig[key] = config;
        }
    }

    return argConfig;
}

const plugins = {
    argv: require('yargs').argv,
    autoprefixer: require('gulp-autoprefixer'),
    browserSync: require('browser-sync').create(),
    concat: require('gulp-concat'),
    cssNano: require('gulp-cssnano'),
    del: require('del'),
    download: require('gulp-download2'),
    fs: require('fs'),
    fsReaddirRecursive: require('fs-readdir-recursive'),
    insert: require('gulp-insert'),
    less: require('gulp-less'),
    lessLists: require('less-plugin-lists'),
    loadJson: require('load-json-file'),
    merge: require('merge-stream'),
    phpServer: require('gulp-connect-php'),
    protractor: require("gulp-protractor").protractor,
    rename: require("gulp-rename"),
    replace: require('gulp-replace'),
    run: require('gulp-run'),
    runSequence: require('run-sequence'),
    task: require('gulp-task'),
    taskTime: require('gulp-total-task-time'),
    watch: require('gulp-watch'),
    webDriverUpdate: require("gulp-protractor").webdriver_update,
};

const buildConfig = {
    configPath: 'build/config',
    targetPath: 'build'
};
const appConfig = getAppConfig();
const phpConfig = plugins.fs.readFileSync('cms/config/web.config.php', 'utf8');
const phpConfigThemeRegExp = /\$theme\s*=\s*"([^"]+)";/g;
const theme = phpConfigThemeRegExp.exec(phpConfig)[1];
const themesDirPath = './cms/wp-content/themes';
const path = `${themesDirPath}/${theme}`;

const e2e = {
    basePath: 'cms',
    configFileName: 'web.config.php',
    configPath: './cms/config/',
    themesDirPath: './e2e/',
};

let server;
let config;

module.exports = {
    appConfig,
    buildConfig,
    config,
    e2e,
    path,
    phpConfig,
    phpConfigThemeRegExp,
    plugins,
    server,
    theme,
    themesDirPath,
};
