const {
    plugins,
    themesDirPath,
} = require('./base');

const gulp = require('gulp');

const init = callback => {
    if (plugins.argv.name) {
        if (!plugins.fs.existsSync(`${themesDirPath}/${plugins.argv.name}`)) {
            return plugins.runSequence(
                'createTheme:css',
                'createTheme:main',
                callback
            );
        } else {
            console.log(`Theme "${plugins.argv.name}" already exists`);
        }
    } else {
        console.log('Theme name should be provided (--name=newName)');
    }
};

const css = () => {
    const childThemeDir = `${themesDirPath}/${plugins.argv.name}`;
    const cssUrl = `http://${plugins.argv.branchName || 'release'}.iis3.local/assets/css/style.min.css`;

    return plugins.download(cssUrl, {
        errorCallback: function (code) {
            console.log(`Error ${code} returned from ${cssUrl}`);
            process.exit(1);
        }
    }).pipe(gulp.dest(`${childThemeDir}/assets/css`));
};

const main = () => {
    const themeName = plugins.argv.name;
    const childThemeDir = `${themesDirPath}/${themeName}`;

    const header = gulp
        .src(`${themesDirPath}/default/assets/theme-header.txt`)
        .pipe(plugins.replace(/default/gi, function (match) {
            let result = themeName;
            if (match[0] === match[0].toUpperCase()) {
                result = result[0].toUpperCase() + result.slice(1);
            } else {
                result = result.toLowerCase();
            }
            return result;
        }))
        .pipe(plugins.replace('*/', '* Template: default\n*/'))
        .pipe(gulp.dest(`${childThemeDir}/assets`));

    const less = gulp
        .src(`${themesDirPath}/default/assets/less/**/*.less`)
        .pipe(gulp.dest(`${childThemeDir}/assets/less`));

    return plugins.merge(header, less);
};

module.exports = {
    css,
    init,
    main,
};
