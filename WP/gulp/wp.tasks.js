function getVhostConfig() {
    let defaultVhostConfig = plugins.loadJson.sync('./config/vhost.config');
    let localVhostConfig;
    let localVhostConfigFilePath = './config/vhost.local.config';
    if (plugins.fs.existsSync(localVhostConfigFilePath)) {
        localVhostConfig = plugins.loadJson.sync(localVhostConfigFilePath);
    }

    return {...defaultVhostConfig, ...localVhostConfig};
}

const {
    buildConfig,
    plugins,
    path,
} = require('./base');

const gulp = require('gulp');

const buildCss = () => {
    const headerPath =`${path}/assets/theme-header.txt`;
    const cssHeader = plugins.fs.existsSync(headerPath) ? plugins.fs.readFileSync(headerPath, 'utf8') : '';
    const lessLists = new plugins.lessLists();

    return gulp
        .src(`${path}/assets/less/main.less`)
        .pipe(plugins.less({
            plugins: [lessLists]
        }))
        .pipe(plugins.autoprefixer({
            browsers: ['last 2 versions', '> 1% in RU', 'ie 9-11', 'Firefox ESR']
        }))
        .pipe(plugins.cssNano({zindex: false}))
        .pipe(plugins.concat('style.css'))
        .pipe(plugins.insert.prepend(cssHeader))
        .pipe(gulp.dest(path));
};

const buildMainStyle = () => {
    const less = plugins.fsReaddirRecursive(`${path}/assets/less/`).reduce((filtered,item)=> {
        let importText = `@import "./${item.replace('\\','/')}";\r\n`;
        if (!item.includes('main.less') && !item.includes('variables.less')) {
            if (item.startsWith('common\\')) {
                filtered.unshift(importText);
            } else {
                filtered.push(importText);
            }
        }
        return filtered;
    },[]);

    plugins.fs.writeFile(`${path}/assets/less/main.less`, `@import "./common/main.less";\r\n${less.join('')}`, function(err) {
        if(err) {
            return console.log(err);
        }
    });
};

const browserSync = () => {
    if (plugins.argv.syncBrowser) {
        const {proxy, host, port, portUi} = getVhostConfig();

        plugins.browserSync.init({
            proxy,
            host,
            port,
            ui: {
                port: portUi
            },
            notify: false,
            open: 'external'
        });
    }
};

const clean = callback => {
    // callback нужен, чтобы таск выполнился синхронно
    return plugins.del([
        buildConfig.targetPath
    ], callback);
};

const wpBase = () => {
    return gulp
        .src(['cms/**', '!cms/wp-content/themes/**', '!cms/wp-content/uploads/**'], {dot:true})
        .pipe(gulp.dest(buildConfig.targetPath));
};

module.exports = {
    buildCss,
    buildMainStyle,
    browserSync,
    clean,
    wpBase,
};
