// fork: https://github.com/sindresorhus/jshint-stylish/blob/master/index.js

'use strict';
var chalk = require('chalk');
var table = require('text-table');
var logSymbols = require('log-symbols');
var stringLength = require('string-length');
var plur = require('plur');
var path = require('path');

module.exports = {
    toString: function () {
        return __filename;
    },
    reporter: function (result, config, options) {
        var total = result.length;
        var ret = '';
        var headers = [];
        var errorCount = 0;
        var warningCount = 0;
        var basePath = process.cwd();

        options = options || {};

        ret += table(result.map(function (el, i) {
                var err = el.error;
                // E: Error, W: Warning, I: Info
                var isError = err.code && err.code[0] === 'E';

                var line = [
                    '',
                    chalk.gray('line ' + err.line),
                    chalk.gray('col ' + err.character),
                    isError ? chalk.red(err.reason) : chalk.blue(err.reason)
                ];

                var filePath = path.isAbsolute(el.file) ? el.file : path.join(basePath, el.file);
                headers[i] = filePath + ':' + err.line + ':' + err.character;

                if (options.verbose) {
                    line.push(chalk.gray('(' + err.code + ')'));
                }

                if (isError) {
                    errorCount++;
                } else {
                    warningCount++;
                }

                return line;
            }), {
                stringLength: stringLength
            }).split('\n').map(function (el, i) {
                return headers[i] ? '\n' + chalk.underline(headers[i]) + '\n' + el : el;
            }).join('\n') + '\n\n';

        if (total > 0) {
            if (errorCount > 0) {
                ret += '  ' + logSymbols.error + '  ' + errorCount + ' ' + plur('error', errorCount) + (warningCount > 0 ? '\n' : '');
            }

            ret += '  ' + logSymbols.warning + '  ' + warningCount + ' ' + plur('warning', total);
        } else {
            ret += '  ' + logSymbols.success + ' No problems';
            ret = '\n' + ret.trim();
        }

        console.log(ret + '\n');
    }
};