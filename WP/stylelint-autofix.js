const shell = require('shelljs');
const yargs = require('yargs').argv;

shell.exec(`node node_modules/stylelint/bin/stylelint "cms/wp-content/themes/${yargs.theme}/assets/less/**/*.less" --fix`);
