# Расширение тестов

При необходимости тесты можно расширять и переопределять из других тестов

## DOM-элементы
Ссылки на DOM-элементы можно импортировать из pageObject-файла другой темы и, при необходимости, дополнить или переопределить

``` js {1,5}
const defaultPageObject = require('./default')();

const pageObject = function() {
    return {
        ...defaultPageObject,
        helpBlockError: $$('.help-block'),
        submit: element(by.css('.questionnaire-submit button[type="submit"]')),
    }
};

module.exports = pageObject;
```

## Тесты
Можно переопределить и дополнить тесты для какой-то конкретной темы

``` js {4,7-32}
...
const mocks = require('../mocks');
const pageObject = require('../pageObject/gulliver');
let tests = require('../tests')('gulliver');

describe('Gulliver questionnaire', function() {
    const po = new pageObject();
    let descriptions;

    tests = {
        ...tests,
        'Should be transformed from view mode to edit mode': () => {
            expect(po.submitWrapper.isDisplayed()).toBe(true);
        },
        'Should be transformed from edit mode to view mode if questionnaire changes are saved': () => {
            helpers.clearAndSetValue(po.lastName, 'Мурзиков');
            helpers.clearAndSetValue(po.firstName, 'Арарат');
            helpers.clearAndSetValue(po.patronymic, 'Маркович');
            po.submit.click();
            expect(po.datePreview.isDisplayed()).toBe(true);
            expect(po.submitWrapper.isDisplayed()).toBe(false);
        },
        'Should cancel questionnaire changes': () => {
            helpers.clearAndSetValue(po.lastName, 'Мурзиков');
            helpers.clearAndSetValue(po.firstName, 'Арарат');
            helpers.clearAndSetValue(po.patronymic, 'Маркович');
            po.cancel.click();
            expect(po.lastName).toHaveValue('Ракитина123');
            expect(po.firstName).toHaveValue('Анна');
            expect(po.patronymic).toHaveValue('Анатольевна');
        },
    };

    beforeEach(() => {
...
```
