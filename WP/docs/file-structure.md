# Файловая структура

Обычная структура тестов состоит из 5 файлов
* pageObject/{themeName}.js
* specs/{themeName}.spec.js
* httpResponses.js
* mocks.js
* tests.js

## pageObject/{themeName}.js
Здесь содержатся указатели на DOM-элементы

## specs/{themeName}.spec.js
Здесь содержится логика обработки тестов. Некоторые пояснения по коду:

``` js
if (!descriptions) {
    descriptions = this.children.map(({result}) => result.description);
} else {
    descriptions.shift();
}

mocks(descriptions[0]).forEach(({requestMethod, url, respond}) => {
    $httpBackend.when(requestMethod, url).respond(respond);
});
```

Здесь происходит мокание запросов в зависимости от названия теста.

``` js
for (const name in tests) {
    if (tests.hasOwnProperty(name)) {
        it(name, tests[name]);
    }
}
```

Здесь происходит поочерёдное выполнение тестов

## httpResponses.js
Здесь содержатся http-ответы для последующего мокания

## mocks.js
Происходит мокание запросов. В случае, если на разные тесты надо отдавать разные моки, можно это делать в зависимости от имени теста

``` js
const userAnswers = {
    'Should display error message for incorrect birthday': httpResponses.userAnswersIncorrectBirthdate,
    'Should display error message for fields with long values': httpResponses.userAnswersIncorrectLastName,
};

mocks.push({
    requestMethod: 'POST',
    url: /api\/User\/Answers$/,
    respond: userAnswers[description] || httpResponses.userAnswers,
});
```

## tests.js

Сами тесты. Возвращается объект, у которого в качестве ключей используется имя теста, а в качестве значений используется функция с тестами.
