# Тесты на регистрацию

Отличие структуры файлов тестов на регистрацию от тестов на другие разделы состоит в том, что для более удобной работы идёт структурирование по шагам регистрации:
1) Ввод номера телефона
2) Принятие оферты
3) Подтверждение номера телефона
4) Ввод e-mail
5) Ввод пароля
6) Анкета
7) Привязка карты

Разбиение по шагам отражено в:
1) Вместо одного tests.js используются разбиение по шагам регистрации
2) В pageObject используется структурирование по шагам регистрации
3) В spec.js используется разбиение describe по шагам регистрации

## tests
Добавлен файл index.js, который группирует и отдаёт все тесты

## pageObject
Помимо разбиения по шагам регистрации используется common для обозначения DOM-элементов, которые могут быть на любых шагах регистрации

## spec.js
Запуск тестов вынесен в отдельную функцию runTests
``` js
const runTests = testName => {
    for (const name in tests[testName]) {
        if (tests[testName].hasOwnProperty(name)) {
            it(name, tests[testName][name]);
        }
    }
};
```

Из-за проблем с токенами переход на страницу регистрации происходит окольными путями через функцию goToRegistration:
1. Сначала заходим на главную страницу личного кабинета
2. Устанавливаем токены авторизации, регистрации
3. В localStorage прописываем замоканные http-ответы от сервера
4. При необходимости устанавливаем опции личного кабинета
5. Идём непосредственно на страницу регистрации
``` js
 const goToRegistration = description => {
     browser.get('/#');
     browser.executeScript(`return localStorage.setItem('ls.isRegistrationToken', 'true');`);
     browser.executeScript(`return localStorage.setItem('ls.authorizationToken', 'e2e_tests_token');`);
     browser.executeScript(`return localStorage.setItem('ls.e2eHttpResponses', '${JSON.stringify(httpResponses)}');`);

     if (description === 'Accept tender offer') {
         browser.executeScript(`return localStorage.setItem('ls.newOptions', '{"offerHtmlFileId": "offer", "opdAgreementFileId": "opd"}');`);
     }

     browser.get('/#/registration');
 };
```

Мокание actions и некоторых других запросов проводится через callback-функцию, это нужно чтобы при повторном запросе браузера был отдан тот или иной мок в зависимости от очерёдности запросов. Однако callback будет выполняться уже непосредственно в браузере, а не до исполнения app.js, поэтому необходимо прокидывать в localStorage дополнительные данные и их определение в глобальном объекте window.