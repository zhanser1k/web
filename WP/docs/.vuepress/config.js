module.exports = {
    title: 'Worpress documentation',
    description: 'Just look around',
    themeConfig: {
        sidebar: [
            '/',
            '/running-tests',
            '/file-structure',
            'expansion-tests',
            'additional-tricks',
            'registration',
            'troubleshooting'
        ]
    }
};
