# Дополнительные трюки

## Расширение опций

Иногда необходимо при тестировании личного кабинета переопределить опции, прописанные в БД личного кабинета. Это можно сделать так:
1. Объявляем переменную, где через инжекторы прописываем объект с нужными опциями
2. В ходе теста выполняем browser.executeScript с переменной, где прописаны опции

``` js {4-10,14}
module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();
    const updateOptions = `
        return angular.element(document.body).injector().get('$rootScope').$apply(function() {
            angular.element(document.body).injector().get('$rootScope').newOptions = {
                "redirectUrlOnLogin": "#news",
            };
        });
    `;

    return {
        'Should successfully reset password': () => {
            browser.executeScript(updateOptions);
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.inputConfirmCode.sendKeys('111111');
            po.inputNewPassword.sendKeys('111111');
            po.inputRepeatedPassword.sendKeys('111111');
            po.buttonConfirm.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/news`);
        },
...
```

## Очистка токенов
При работе с тестами, где не предполагается наличие по умолчанию авторизаонного токена, при завершении теста нужно очищать авторизационный токен для корректной работы следующего теста

``` js {2}
afterEach(() => {
    browser.executeScript(`return localStorage.removeItem('ls.authorizationToken');`);
    ngMockE2E.clearMockModules();
});
```

## Прикрепление файла к форме

``` js
const path = require('path');
const remote = require('../../node_modules/selenium-webdriver/remote');
browser.setFileDetector(new remote.FileDetector());

const fileToUpload = './null.gif';
const absolutePath = path.resolve(__dirname, fileToUpload);

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should successfully add and remove file': () => {
            po.inputFile.sendKeys(absolutePath);
            browser.driver.sleep(100);
            expect(po.removeFile).toBePresent();
            po.removeFile.click();
            expect(po.inputFile).toHaveValue('');
        },
    }
};
```
