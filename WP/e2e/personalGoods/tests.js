require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        // TODO перевести bin на 1.5
        /*'Should display an error message when trying to add user offers': () => {
            po.moreInfoForMyOffers.first().click();
            po.offerGoods.first().click();
            expect(po.alert.isPresent()).toBe(true);
        },*/
        'Should successfully back to offer list from user offers': () => {
            po.moreInfoForMyOffers.first().click();
            po.backToOffers.click();
            expect(po.offerInfo.count()).toBe(10);
        },
        'Should display a message when trying to change active offers': () => {
            po.moreInfoForActiveOffers.first().click();
            expect(po.overlay).toBePresent();
        },
        'Should successfully back to offer list from active offers': () => {
            po.moreInfoForActiveOffers.first().click();
            po.backToOffers.click();
            expect(po.offerInfo.count()).toBe(10);
        },
        'Should successfully add active offers to user offers': () => {
            po.moreInfoForActiveOffers.get(1).click();
            po.offerGoods.get(1).click();
            po.offerGoods.get(2).click();
            po.offerGoods.get(3).click();
            po.approve.first().click();
            po.yesIWantSetApprove.click();
            expect(po.alert.isPresent()).toBe(false);
        },
    }
};
