const pageObject = function() {
    return {
        moreInfoForMyOffers: $$("#my-offers .details a"),
        moreInfoForActiveOffers: $$(".active-offers .details a"),
        offerInfo: $$(".personal-goods-offer__info"),
        backToOffers: element(by.id('back-to-offers')),
        offerGoods: element.all(by.repeater('goods in offerGoods')),
        alert: element(by.css('.user-alert__message')),
        overlay: element(by.css('.goods-list-overlayer')),
        approve: $$('.approve button[type="button"]'),
        yesIWantSetApprove: element(by.css(".user-alert__footer .btn-default")),
    }
};

module.exports = pageObject;
