const httpResponses = require('./httpResponses');

module.exports = () => {
    return [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/Announcements\?filter/,
        respond: httpResponses.announcements,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\?filter\.count=\d+&filter\.offerState=CommingSoon/,
        respond: httpResponses.offerCommingSoon,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\?filter\.count=\d+\&filter\.offerState\=Active/,
        respond: httpResponses.offerActive,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/357\/details$/,
        respond: httpResponses.offerDetails357,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/365\/details$/,
        respond: httpResponses.offerDetails365,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/358\/details$/,
        respond: httpResponses.offerDetails358,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/369\/details$/,
        respond: httpResponses.offerDetails369,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/366\/details$/,
        respond: httpResponses.offerDetails366,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/151\/details$/,
        respond: httpResponses.offerDetails151,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/368\/details$/,
        respond: httpResponses.offerDetails368,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/362\/details$/,
        respond: httpResponses.offerDetails362,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/359\/details$/,
        respond: httpResponses.offerDetails359,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/204\/details$/,
        respond: httpResponses.offerDetails204,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/362$/,
        respond: httpResponses.offer362,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/151$/,
        respond: httpResponses.offer151,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/204$/,
        respond: httpResponses.offer204,
    }, {
        requestMethod: 'POST',
        url: /api\/user\/attributes\/personalOffer\/values\/\d+/,
        respond: httpResponses.personalOffer,
    }];
};
