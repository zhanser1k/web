module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    announcements: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerCommingSoon: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerActive: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1917,
                    "startFillDate": "2018-04-01T00:00:00Z",
                    "endFillDate": "2018-04-30T00:00:00Z",
                    "maxGoodsCount": 33,
                    "goodsGroup": {
                        "id": 306474,
                        "name": "7878787887878"
                    }
                },
                "canSelectGoods": false,
                "id": 357,
                "title": "суперакция123",
                "description": "суперакция123",
                "begin": "2018-06-03T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                        "name": "Лама",
                        "images": [
                            {
                                "fileId": "3a4570d8-ad55-4e4f-85b0-1882d8ef868f",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "f4b21676-e710-4fcd-9c56-48073937d39d",
                                "description": "marker"
                            }
                        ]
                    },
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                "images": [
                    {
                        "fileId": "50155a00-70fe-4f86-b162-6a82d5af8df7",
                        "description": "мегаакция"
                    }
                ],
                "instructions": [],
                "merchantsCount": 230
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1850,
                    "startFillDate": "2017-05-22T00:00:00Z",
                    "endFillDate": "2017-09-07T00:00:00Z",
                    "maxGoodsCount": 5,
                    "goodsGroup": {
                        "id": 193849,
                        "name": "Мегаакция"
                    }
                },
                "canSelectGoods": false,
                "id": 365,
                "title": "Angular: Empty Fiddle1",
                "description": "Angular: Empty Fiddle12",
                "begin": "2018-05-29T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "41dcc33a-f087-4151-b125-a282496dfb36",
                    "1d78395c-3a06-4ffe-abb0-384d27692e3a",
                    "ed7c7c20-4b19-4608-b387-ee4b4943083e",
                    "880ed950-9f7a-41df-89d1-62dcf9d64b8b",
                    "62846543-0b4c-4f74-b1c0-1609d6170e8b",
                    "68bfb082-b308-4e8f-be2b-475f2e5169dd",
                    "b99826b3-cb6b-48ff-953a-e426baaafa26",
                    "8bc2045e-9f7a-404b-ac59-df531f13770c",
                    "3b51e1e6-9a11-42a0-ac70-405f8e39328d",
                    "376203bd-7455-4d9f-9af5-3506e376a688",
                    "3ae4cc80-144e-4ff9-8b05-ff8f40ef2b23",
                    "78d11410-225d-4e11-8d50-5bd7a07875c8",
                    "219a2bed-d32d-471f-8312-66281d8cf56d",
                    "c9be45c5-5034-4bb4-bbc8-1d7832192289",
                    "9480539f-3675-4903-9087-7aed8e5faa71"
                ],
                "brands": [
                    {
                        "id": "41dcc33a-f087-4151-b125-a282496dfb36",
                        "name": "Бренд DeviceEmulationControllerTests1145508",
                        "images": []
                    },
                    {
                        "id": "1d78395c-3a06-4ffe-abb0-384d27692e3a",
                        "name": "Бренд DeviceEmulationControllerTests2145508",
                        "images": []
                    },
                    {
                        "id": "ed7c7c20-4b19-4608-b387-ee4b4943083e",
                        "name": "Бренд DeviceEmulationControllerTests4145508",
                        "images": []
                    },
                    {
                        "id": "880ed950-9f7a-41df-89d1-62dcf9d64b8b",
                        "name": "Бренд DeviceEmulationControllerTests0145509",
                        "images": []
                    },
                    {
                        "id": "62846543-0b4c-4f74-b1c0-1609d6170e8b",
                        "name": "Бренд DeviceEmulationControllerTests1145509",
                        "images": []
                    },
                    {
                        "id": "68bfb082-b308-4e8f-be2b-475f2e5169dd",
                        "name": "Бренд DeviceEmulationControllerTests2145509",
                        "images": []
                    },
                    {
                        "id": "b99826b3-cb6b-48ff-953a-e426baaafa26",
                        "name": "Бренд DeviceEmulationControllerTests4145509",
                        "images": []
                    },
                    {
                        "id": "8bc2045e-9f7a-404b-ac59-df531f13770c",
                        "name": "Бренд DeviceEmulationControllerTests0145510",
                        "images": []
                    },
                    {
                        "id": "3b51e1e6-9a11-42a0-ac70-405f8e39328d",
                        "name": "Бренд DeviceEmulationControllerTests1145510",
                        "images": []
                    },
                    {
                        "id": "376203bd-7455-4d9f-9af5-3506e376a688",
                        "name": "Бренд DeviceEmulationControllerTests2145510",
                        "images": []
                    },
                    {
                        "id": "3ae4cc80-144e-4ff9-8b05-ff8f40ef2b23",
                        "name": "Бренд DeviceEmulationControllerTests4145510",
                        "images": []
                    },
                    {
                        "id": "78d11410-225d-4e11-8d50-5bd7a07875c8",
                        "name": "Бренд DeviceEmulationControllerTests0145511",
                        "images": []
                    },
                    {
                        "id": "219a2bed-d32d-471f-8312-66281d8cf56d",
                        "name": "Бренд DeviceEmulationControllerTests1145511",
                        "images": []
                    },
                    {
                        "id": "c9be45c5-5034-4bb4-bbc8-1d7832192289",
                        "name": "Бренд DeviceEmulationControllerTests2145511",
                        "images": []
                    },
                    {
                        "id": "9480539f-3675-4903-9087-7aed8e5faa71",
                        "name": "Бренд DeviceEmulationControllerTests4145511",
                        "images": []
                    }
                ],
                "brandId": "41dcc33a-f087-4151-b125-a282496dfb36",
                "images": [],
                "instructions": [],
                "merchantsCount": 15
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1919,
                    "startFillDate": "2018-05-12T19:00:00Z",
                    "endFillDate": "2018-06-10T01:00:00Z",
                    "maxGoodsCount": 5,
                    "goodsGroup": {
                        "id": 306493,
                        "name": "мегаакция2"
                    }
                },
                "canSelectGoods": false,
                "id": 358,
                "title": "СУПЕРАКЦИЯ",
                "description": "СУПЕРАКЦИЯ",
                "begin": "2018-05-22T12:00:00Z",
                "end": "2018-08-24T00:00:00Z",
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 225
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1918,
                    "startFillDate": "2018-05-02T00:00:00Z",
                    "endFillDate": "2018-07-12T00:00:00Z",
                    "maxGoodsCount": 4,
                    "goodsGroup": {
                        "id": 306492,
                        "name": "мега акция"
                    }
                },
                "canSelectGoods": true,
                "id": 366,
                "title": "Angular: Empty Fiddle1",
                "description": "Angular: Empty Fiddle12",
                "begin": "2018-05-22T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "3e51b3a3-db7a-454f-8328-e9454c1450a0",
                    "73263683-8d8f-4c74-91a2-aed0d664cfce",
                    "41dcc33a-f087-4151-b125-a282496dfb36",
                    "1d78395c-3a06-4ffe-abb0-384d27692e3a",
                    "ed7c7c20-4b19-4608-b387-ee4b4943083e",
                    "880ed950-9f7a-41df-89d1-62dcf9d64b8b",
                    "62846543-0b4c-4f74-b1c0-1609d6170e8b",
                    "68bfb082-b308-4e8f-be2b-475f2e5169dd",
                    "b99826b3-cb6b-48ff-953a-e426baaafa26",
                    "8bc2045e-9f7a-404b-ac59-df531f13770c",
                    "3b51e1e6-9a11-42a0-ac70-405f8e39328d",
                    "376203bd-7455-4d9f-9af5-3506e376a688",
                    "3ae4cc80-144e-4ff9-8b05-ff8f40ef2b23",
                    "78d11410-225d-4e11-8d50-5bd7a07875c8",
                    "219a2bed-d32d-471f-8312-66281d8cf56d",
                    "c9be45c5-5034-4bb4-bbc8-1d7832192289",
                    "9480539f-3675-4903-9087-7aed8e5faa71"
                ],
                "brands": [
                    {
                        "id": "3e51b3a3-db7a-454f-8328-e9454c1450a0",
                        "name": "Бренд DeviceEmulationControllerTests4145507",
                        "images": []
                    },
                    {
                        "id": "73263683-8d8f-4c74-91a2-aed0d664cfce",
                        "name": "Бренд DeviceEmulationControllerTests0145508",
                        "images": []
                    },
                    {
                        "id": "41dcc33a-f087-4151-b125-a282496dfb36",
                        "name": "Бренд DeviceEmulationControllerTests1145508",
                        "images": []
                    },
                    {
                        "id": "1d78395c-3a06-4ffe-abb0-384d27692e3a",
                        "name": "Бренд DeviceEmulationControllerTests2145508",
                        "images": []
                    },
                    {
                        "id": "ed7c7c20-4b19-4608-b387-ee4b4943083e",
                        "name": "Бренд DeviceEmulationControllerTests4145508",
                        "images": []
                    },
                    {
                        "id": "880ed950-9f7a-41df-89d1-62dcf9d64b8b",
                        "name": "Бренд DeviceEmulationControllerTests0145509",
                        "images": []
                    },
                    {
                        "id": "62846543-0b4c-4f74-b1c0-1609d6170e8b",
                        "name": "Бренд DeviceEmulationControllerTests1145509",
                        "images": []
                    },
                    {
                        "id": "68bfb082-b308-4e8f-be2b-475f2e5169dd",
                        "name": "Бренд DeviceEmulationControllerTests2145509",
                        "images": []
                    },
                    {
                        "id": "b99826b3-cb6b-48ff-953a-e426baaafa26",
                        "name": "Бренд DeviceEmulationControllerTests4145509",
                        "images": []
                    },
                    {
                        "id": "8bc2045e-9f7a-404b-ac59-df531f13770c",
                        "name": "Бренд DeviceEmulationControllerTests0145510",
                        "images": []
                    },
                    {
                        "id": "3b51e1e6-9a11-42a0-ac70-405f8e39328d",
                        "name": "Бренд DeviceEmulationControllerTests1145510",
                        "images": []
                    },
                    {
                        "id": "376203bd-7455-4d9f-9af5-3506e376a688",
                        "name": "Бренд DeviceEmulationControllerTests2145510",
                        "images": []
                    },
                    {
                        "id": "3ae4cc80-144e-4ff9-8b05-ff8f40ef2b23",
                        "name": "Бренд DeviceEmulationControllerTests4145510",
                        "images": []
                    },
                    {
                        "id": "78d11410-225d-4e11-8d50-5bd7a07875c8",
                        "name": "Бренд DeviceEmulationControllerTests0145511",
                        "images": []
                    },
                    {
                        "id": "219a2bed-d32d-471f-8312-66281d8cf56d",
                        "name": "Бренд DeviceEmulationControllerTests1145511",
                        "images": []
                    },
                    {
                        "id": "c9be45c5-5034-4bb4-bbc8-1d7832192289",
                        "name": "Бренд DeviceEmulationControllerTests2145511",
                        "images": []
                    },
                    {
                        "id": "9480539f-3675-4903-9087-7aed8e5faa71",
                        "name": "Бренд DeviceEmulationControllerTests4145511",
                        "images": []
                    }
                ],
                "brandId": "3e51b3a3-db7a-454f-8328-e9454c1450a0",
                "images": [],
                "instructions": [],
                "merchantsCount": 17
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1917,
                    "startFillDate": "2018-04-01T00:00:00Z",
                    "endFillDate": "2018-04-30T00:00:00Z",
                    "maxGoodsCount": 33,
                    "goodsGroup": {
                        "id": 306474,
                        "name": "7878787887878"
                    }
                },
                "canSelectGoods": false,
                "id": 369,
                "title": "Я абрикос, на солнце рос",
                "description": "оооооооооооооооооооооопрпа  папапвап апапапвап уеук пввав вававав ыаыаы аыаыа ыаыаы аыаы нкнке пвпвп впввава аааыа аыа ыаыаы аыаыа ыаыа кнкнн уеуееу пввав вавава ваваупеуп авав пвпвп пвпвпвав",
                "begin": "2018-05-17T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "bdff376a-d33c-454f-ae73-cbe702a42bd4",
                    "afa7d36c-b78e-4ff2-9c19-e979e12a5c85",
                    "5b66a8e8-a1f4-468c-a1cb-9c8d3f811ea2",
                    "51ab767b-bce2-4d99-9530-4a6c3bf125bb",
                    "853e4ad2-a974-4f0b-b73a-47b40b642e16",
                    "84153d23-74b7-44f4-8e22-7607ef2c5dde",
                    "012da5a4-e2c4-47b7-8b92-f137c6af4e4e",
                    "b921d8de-d805-4733-9c70-4c41483cee96",
                    "28ce1ea2-e186-4477-9252-74c308de510c",
                    "34427892-0656-43de-b26b-2a4300f922ef",
                    "1312f933-21de-473d-8256-46f6d7212c22",
                    "12de1b6d-ed4a-4902-a459-ecd798471de5",
                    "52afc2f9-db7c-4cb8-8300-d7c33236ee3c",
                    "5b7f31fe-ade9-45ad-90f4-fee75d358904",
                    "5e88d6c6-732b-4c43-9824-dc2ae6ecc64a",
                    "ba742ead-f1c2-4efd-9d47-57f13bed26ed",
                    "da90d9f4-44e4-40af-95a8-17521882951c",
                    "d190ad7d-5081-438c-91a7-90f4771fcc13",
                    "2a978d2e-42ad-4e31-a124-74d7ce0e3dc8",
                    "5a933951-f7b3-4ece-88bd-6cf700e96b9a",
                    "f2b56386-3a40-4f59-9a75-17da23eb7a1d",
                    "795b09d9-4853-4105-8d95-219ac5e088ed",
                    "96f12bfa-ab6b-4a1c-b37e-fb26903addc9",
                    "fc6361d8-5c7a-4079-a5c3-5840392cdd26",
                    "49e21ccc-2843-4231-b4ea-f219abde545b",
                    "c788b5e7-475a-413d-a560-a39c3696d624",
                    "80704696-1989-4861-9e37-929f9be990bf",
                    "cded9294-20bb-4cfc-803c-7e9ec162835f",
                    "22b47afd-bab2-410a-b8d6-e32b80fc8fae",
                    "a96abc73-4ce9-41bb-9da1-bb4723c2dcd5",
                    "296b49e1-d927-4af5-9485-2c2e41033ad9",
                    "39761a69-4528-44db-9302-06a2257aa5c8",
                    "683dec24-1843-4b8b-9216-94d8e314bb6a",
                    "0b1f91fe-6e79-4010-885b-5aacc4a3fc07",
                    "322f7a80-bcef-43e9-b945-27420f9bf4ee",
                    "96a3e4b0-4cd6-44cd-a592-57422f1a0ee1",
                    "01649c79-fb19-44e4-85df-d75550e0ea91",
                    "181aa419-9d34-4002-9336-e3680b022387",
                    "91339aa8-5b16-4a8a-a518-9868445d3d24",
                    "e92ab0e5-b664-4e2c-a178-5e1ec5685412",
                    "f6216708-e425-4230-ab08-d2634d86b661",
                    "18fccd7b-44fc-46bf-9829-a19eeeb53b92",
                    "4cb83f73-4c91-4517-81ed-deccbaac70c5",
                    "a4b92ba3-e26b-4957-8348-ae6aaebb2b43",
                    "b7fadc38-91ce-4810-9be1-868ed4325e90",
                    "ba751e9b-2254-4507-91fc-9450453349c8",
                    "f7572041-76f7-4690-9152-d11959c9d592",
                    "120c3545-28a1-4b06-83cd-44216c2a1063",
                    "3481ede5-ead3-46b6-9fde-1d859915f97e",
                    "00d4d25f-bffd-4447-98bd-c54d96dd88f8",
                    "3f24d17b-620a-468f-8bbd-2bdd34864859",
                    "3e51b3a3-db7a-454f-8328-e9454c1450a0",
                    "219a2bed-d32d-471f-8312-66281d8cf56d",
                    "c9be45c5-5034-4bb4-bbc8-1d7832192289",
                    "9480539f-3675-4903-9087-7aed8e5faa71",
                    "e8c270ad-d885-4e00-8ca3-2582bb67cfe0",
                    "ff3d1337-d0ca-41ff-867d-89c0a621dff8",
                    "4bc3a407-4bdc-46a1-a952-2540bb9b206a",
                    "0693677f-19dc-4e91-889f-df54de7cceb7"
                ],
                "brands": [
                    {
                        "id": "bdff376a-d33c-454f-ae73-cbe702a42bd4",
                        "name": "50d5a322416d401c9c77d681adbd2945",
                        "images": []
                    },
                    {
                        "id": "afa7d36c-b78e-4ff2-9c19-e979e12a5c85",
                        "name": "7f714cee449346c8a863a276cebbdbfc",
                        "images": []
                    },
                    {
                        "id": "5b66a8e8-a1f4-468c-a1cb-9c8d3f811ea2",
                        "name": "0d5a2409c75142c088e18b5fa1c49690",
                        "images": []
                    },
                    {
                        "id": "51ab767b-bce2-4d99-9530-4a6c3bf125bb",
                        "name": "be397a939a214b5b859cfbb73586c1da",
                        "images": []
                    },
                    {
                        "id": "853e4ad2-a974-4f0b-b73a-47b40b642e16",
                        "name": "67c96c4bdb8248228fe13a7147696ef8",
                        "images": []
                    },
                    {
                        "id": "84153d23-74b7-44f4-8e22-7607ef2c5dde",
                        "name": "bae9487ec4744cf7b723da958acb617e",
                        "images": []
                    },
                    {
                        "id": "012da5a4-e2c4-47b7-8b92-f137c6af4e4e",
                        "name": "1c1c4b290cbf49db85eeb92356fde085",
                        "images": []
                    },
                    {
                        "id": "b921d8de-d805-4733-9c70-4c41483cee96",
                        "name": "267413f2c281407bbdb9c1baaa573c55",
                        "images": []
                    },
                    {
                        "id": "28ce1ea2-e186-4477-9252-74c308de510c",
                        "name": "94aa781a7f2d4621942546828d32a2dd",
                        "images": []
                    },
                    {
                        "id": "34427892-0656-43de-b26b-2a4300f922ef",
                        "name": "7969684c90da41e8aa0f3aaaf6889bac",
                        "images": []
                    },
                    {
                        "id": "1312f933-21de-473d-8256-46f6d7212c22",
                        "name": "f67c3b04f29a473c8a582ca8f337d728",
                        "images": []
                    },
                    {
                        "id": "12de1b6d-ed4a-4902-a459-ecd798471de5",
                        "name": "9a912bc958a54d95bea2b8c01a3090f0",
                        "images": []
                    },
                    {
                        "id": "52afc2f9-db7c-4cb8-8300-d7c33236ee3c",
                        "name": "9f80cb3852bc43fc8be7d531f6d541af",
                        "images": []
                    },
                    {
                        "id": "5b7f31fe-ade9-45ad-90f4-fee75d358904",
                        "name": "3f2b748905e94581ba1a259d9afe6287",
                        "images": []
                    },
                    {
                        "id": "5e88d6c6-732b-4c43-9824-dc2ae6ecc64a",
                        "name": "747913272d764bc798ac0bb11718ccf4",
                        "images": []
                    },
                    {
                        "id": "ba742ead-f1c2-4efd-9d47-57f13bed26ed",
                        "name": "816cc7092986441a9c5914e471f08359",
                        "images": []
                    },
                    {
                        "id": "da90d9f4-44e4-40af-95a8-17521882951c",
                        "name": "64f87c4c8f904cc99a0be3682b8f513b",
                        "images": []
                    },
                    {
                        "id": "d190ad7d-5081-438c-91a7-90f4771fcc13",
                        "name": "8e2f8d64400a4e09a027c83bc09c331c",
                        "images": []
                    },
                    {
                        "id": "2a978d2e-42ad-4e31-a124-74d7ce0e3dc8",
                        "name": "32e072ee3b98451e9db60c7296ae7f72",
                        "images": []
                    },
                    {
                        "id": "5a933951-f7b3-4ece-88bd-6cf700e96b9a",
                        "name": "119e3c79018943c58bae5991e296c41b",
                        "images": []
                    },
                    {
                        "id": "f2b56386-3a40-4f59-9a75-17da23eb7a1d",
                        "name": "c935fc085a594fbfae7dd082679b65cf",
                        "images": []
                    },
                    {
                        "id": "795b09d9-4853-4105-8d95-219ac5e088ed",
                        "name": "40cbcff483214117bc956bda068a9935",
                        "images": []
                    },
                    {
                        "id": "96f12bfa-ab6b-4a1c-b37e-fb26903addc9",
                        "name": "582d31df9e774117aec703fdc828bc4c",
                        "images": []
                    },
                    {
                        "id": "fc6361d8-5c7a-4079-a5c3-5840392cdd26",
                        "name": "a1e12a80b81c45658cefe4950daa0045",
                        "images": []
                    },
                    {
                        "id": "49e21ccc-2843-4231-b4ea-f219abde545b",
                        "name": "f926e00ef9694013baccbf7f2f3f85ff",
                        "images": []
                    },
                    {
                        "id": "c788b5e7-475a-413d-a560-a39c3696d624",
                        "name": "3f9216d54e6e4b97baa507d42b9a9623",
                        "images": []
                    },
                    {
                        "id": "80704696-1989-4861-9e37-929f9be990bf",
                        "name": "260d24619229435ca51b2de3353aa6d4",
                        "images": []
                    },
                    {
                        "id": "cded9294-20bb-4cfc-803c-7e9ec162835f",
                        "name": "cceda1913ddb4b4b953e9c13bb7ed7b3",
                        "images": []
                    },
                    {
                        "id": "22b47afd-bab2-410a-b8d6-e32b80fc8fae",
                        "name": "9960aba79c01499db2e81437628edb8c",
                        "images": []
                    },
                    {
                        "id": "a96abc73-4ce9-41bb-9da1-bb4723c2dcd5",
                        "name": "b734005e9f95489492da3859da2abbef",
                        "images": []
                    },
                    {
                        "id": "296b49e1-d927-4af5-9485-2c2e41033ad9",
                        "name": "b7f3fdb4bee04213a426970f6a7ce3d9",
                        "images": []
                    },
                    {
                        "id": "39761a69-4528-44db-9302-06a2257aa5c8",
                        "name": "bfeea14a987546bbae15baeaa22c3ffb",
                        "images": []
                    },
                    {
                        "id": "683dec24-1843-4b8b-9216-94d8e314bb6a",
                        "name": "258ca758a33f4430b35d8ceb98ae0267",
                        "images": []
                    },
                    {
                        "id": "0b1f91fe-6e79-4010-885b-5aacc4a3fc07",
                        "name": "979693b37b29486abb74b7fe9bbb11cd",
                        "images": []
                    },
                    {
                        "id": "322f7a80-bcef-43e9-b945-27420f9bf4ee",
                        "name": "5834ab810c244cb1a7170237579580f6",
                        "images": []
                    },
                    {
                        "id": "96a3e4b0-4cd6-44cd-a592-57422f1a0ee1",
                        "name": "04403b407f9847f7ba1d43386a0f89bb",
                        "images": []
                    },
                    {
                        "id": "01649c79-fb19-44e4-85df-d75550e0ea91",
                        "name": "e8169bc587e9416b83e8a3fa0c1e47b6",
                        "images": []
                    },
                    {
                        "id": "181aa419-9d34-4002-9336-e3680b022387",
                        "name": "07009e324fc6488c94ffaffa93b3e929",
                        "images": []
                    },
                    {
                        "id": "91339aa8-5b16-4a8a-a518-9868445d3d24",
                        "name": "60a49d0173194cf4a15509af0efefd74",
                        "images": []
                    },
                    {
                        "id": "e92ab0e5-b664-4e2c-a178-5e1ec5685412",
                        "name": "5595d24f429a45e9b399ce7c6e7b9f6c",
                        "images": []
                    },
                    {
                        "id": "f6216708-e425-4230-ab08-d2634d86b661",
                        "name": "3ed2eb6d911f4f179933bbd35a05f7a7",
                        "images": []
                    },
                    {
                        "id": "18fccd7b-44fc-46bf-9829-a19eeeb53b92",
                        "name": "a54570c0ff68422ca3746a54f734b7d4",
                        "images": []
                    },
                    {
                        "id": "4cb83f73-4c91-4517-81ed-deccbaac70c5",
                        "name": "9c96049ebda943dba2fb6c09f29ea204",
                        "images": []
                    },
                    {
                        "id": "a4b92ba3-e26b-4957-8348-ae6aaebb2b43",
                        "name": "7aa16e10006f4ff59512cf47fa824ca1",
                        "images": []
                    },
                    {
                        "id": "b7fadc38-91ce-4810-9be1-868ed4325e90",
                        "name": "8014ebdbbc9340a3b4ac3b63e2ec3a94",
                        "images": []
                    },
                    {
                        "id": "ba751e9b-2254-4507-91fc-9450453349c8",
                        "name": "e1c3b766498145d5a2e3413ac50f2f4c",
                        "images": []
                    },
                    {
                        "id": "f7572041-76f7-4690-9152-d11959c9d592",
                        "name": "5ed5572eb49a4d1ca37679003342933c",
                        "images": []
                    },
                    {
                        "id": "120c3545-28a1-4b06-83cd-44216c2a1063",
                        "name": "131e765cbc00499cb129955563868b17",
                        "images": []
                    },
                    {
                        "id": "3481ede5-ead3-46b6-9fde-1d859915f97e",
                        "name": "Бренд DeviceEmulationControllerTests0145507",
                        "images": []
                    },
                    {
                        "id": "00d4d25f-bffd-4447-98bd-c54d96dd88f8",
                        "name": "Бренд DeviceEmulationControllerTests1145507",
                        "images": []
                    },
                    {
                        "id": "3f24d17b-620a-468f-8bbd-2bdd34864859",
                        "name": "Бренд DeviceEmulationControllerTests2145507",
                        "images": []
                    },
                    {
                        "id": "3e51b3a3-db7a-454f-8328-e9454c1450a0",
                        "name": "Бренд DeviceEmulationControllerTests4145507",
                        "images": []
                    },
                    {
                        "id": "219a2bed-d32d-471f-8312-66281d8cf56d",
                        "name": "Бренд DeviceEmulationControllerTests1145511",
                        "images": []
                    },
                    {
                        "id": "c9be45c5-5034-4bb4-bbc8-1d7832192289",
                        "name": "Бренд DeviceEmulationControllerTests2145511",
                        "images": []
                    },
                    {
                        "id": "9480539f-3675-4903-9087-7aed8e5faa71",
                        "name": "Бренд DeviceEmulationControllerTests4145511",
                        "images": []
                    },
                    {
                        "id": "e8c270ad-d885-4e00-8ca3-2582bb67cfe0",
                        "name": "4a03c81130354f389d5574b9f55099ef",
                        "images": []
                    },
                    {
                        "id": "ff3d1337-d0ca-41ff-867d-89c0a621dff8",
                        "name": "Brand_TestPartner_77e3e31b773d472e8675ae9da98d42e2",
                        "images": []
                    },
                    {
                        "id": "4bc3a407-4bdc-46a1-a952-2540bb9b206a",
                        "name": "Brand_TestPartner_cf2196ec8b1741d480476b02ac170ecc",
                        "images": []
                    },
                    {
                        "id": "0693677f-19dc-4e91-889f-df54de7cceb7",
                        "name": "Brand_TestPartner_b95ddca7d2a242bab8d3ef5d96e91c37",
                        "images": []
                    }
                ],
                "brandId": "bdff376a-d33c-454f-ae73-cbe702a42bd4",
                "images": [
                    {
                        "fileId": "a32dbb7a-be7d-445f-956e-eeff328d9237",
                        "description": "banner"
                    }
                ],
                "instructions": [],
                "merchantsCount": 59
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1754,
                    "startFillDate": "2017-02-13T00:00:00Z",
                    "endFillDate": "2017-02-19T00:00:00Z",
                    "maxGoodsCount": 5,
                    "goodsGroup": {
                        "id": 190699,
                        "name": "для мобилки"
                    }
                },
                "canSelectGoods": false,
                "id": 151,
                "title": "Перс предлож",
                "description": "описание тра-та-та",
                "begin": "2018-05-13T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                    "9848b201-90ac-4714-aa63-f9232485becc"
                ],
                "brands": [
                    {
                        "id": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                        "name": "Лама",
                        "images": [
                            {
                                "fileId": "3a4570d8-ad55-4e4f-85b0-1882d8ef868f",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "f4b21676-e710-4fcd-9c56-48073937d39d",
                                "description": "marker"
                            }
                        ]
                    },
                    {
                        "id": "9848b201-90ac-4714-aa63-f9232485becc",
                        "name": "Фудсити",
                        "images": [
                            {
                                "fileId": "2a03f2fa-1af6-4564-9eca-3270b25b4eee",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "9d1491d5-c43f-4a1a-801f-f2ca92a09bba",
                                "description": "marker"
                            }
                        ]
                    }
                ],
                "brandId": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                "images": [],
                "instructions": [],
                "merchantsCount": 10
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1921,
                    "startFillDate": "2016-11-15T00:00:00Z",
                    "endFillDate": "2018-07-06T00:00:00Z",
                    "maxGoodsCount": 4,
                    "goodsGroup": {
                        "id": 306493,
                        "name": "мегаакция2"
                    }
                },
                "canSelectGoods": true,
                "id": 368,
                "title": "персональные товары вапвп впвпв пвпвпв вава ыаыаа аыаы пвпв аваавааваа ыыаыа",
                "description": "аа вавав вава ваыа авава вавава ыаыа вава кпвпвпвп пвпвп впвпвпвп впвпвп впвпвп пвпвпп впвпв впвпввп пвппв впвпв",
                "begin": "2018-05-08T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                    "27812daa-fa4d-4564-abab-3305e8355017",
                    "5198c924-2459-4a06-8200-272ff70cdab4"
                ],
                "brands": [
                    {
                        "id": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                        "name": "Лама",
                        "images": [
                            {
                                "fileId": "3a4570d8-ad55-4e4f-85b0-1882d8ef868f",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "f4b21676-e710-4fcd-9c56-48073937d39d",
                                "description": "marker"
                            }
                        ]
                    },
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    {
                        "id": "5198c924-2459-4a06-8200-272ff70cdab4",
                        "name": "Brand_ТЕСТ",
                        "images": []
                    }
                ],
                "brandId": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                "images": [],
                "instructions": [],
                "merchantsCount": 231
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1913,
                    "startFillDate": "2017-11-16T00:00:00Z",
                    "endFillDate": "2018-12-02T00:00:00Z",
                    "maxGoodsCount": 3,
                    "goodsGroup": {
                        "id": 253276,
                        "name": "Проверка перс товаров"
                    }
                },
                "canSelectGoods": true,
                "id": 362,
                "title": "2 вещи по цене 3-ёх",
                "description": "выбери 2 вещи и получи 3-ю в подарок!!!",
                "begin": "2018-05-04T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                    "9d5b66e5-cbca-4b7d-a596-7e3c6497e615"
                ],
                "brands": [
                    {
                        "id": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                        "name": "Лама",
                        "images": [
                            {
                                "fileId": "3a4570d8-ad55-4e4f-85b0-1882d8ef868f",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "f4b21676-e710-4fcd-9c56-48073937d39d",
                                "description": "marker"
                            }
                        ]
                    },
                    {
                        "id": "9d5b66e5-cbca-4b7d-a596-7e3c6497e615",
                        "name": "СпортСЕ",
                        "images": [
                            {
                                "fileId": "279eea5c-22be-4764-9693-868f0fbc28e3",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "44c574bf-d470-4f7b-ab1d-afc2d2acc1b5",
                                "description": "marker"
                            }
                        ]
                    }
                ],
                "brandId": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                "images": [],
                "instructions": [
                    "1. во-первых",
                    "2. во-вторых",
                    "3. в-третьих"
                ],
                "merchantsCount": 10
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1921,
                    "startFillDate": "2016-11-15T00:00:00Z",
                    "endFillDate": "2018-07-06T00:00:00Z",
                    "maxGoodsCount": 4,
                    "goodsGroup": {
                        "id": 306493,
                        "name": "мегаакция2"
                    }
                },
                "canSelectGoods": true,
                "id": 359,
                "title": "Суперакция 2",
                "description": "Суперакция 2",
                "begin": "2018-01-01T00:00:00Z",
                "end": "2018-09-28T11:00:00Z",
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "d78ac5e5-04b3-46af-807f-347926697b56",
                    "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "d78ac5e5-04b3-46af-807f-347926697b56",
                        "name": "Абрикос",
                        "images": [
                            {
                                "fileId": "678fc070-4164-4d6d-a82f-d93f21270e11",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "67f09151-60b4-417d-bfa8-22de4c987023",
                                "description": "marker"
                            }
                        ]
                    },
                    {
                        "id": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                        "name": "Лама",
                        "images": [
                            {
                                "fileId": "3a4570d8-ad55-4e4f-85b0-1882d8ef868f",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "f4b21676-e710-4fcd-9c56-48073937d39d",
                                "description": "marker"
                            }
                        ]
                    },
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "d78ac5e5-04b3-46af-807f-347926697b56",
                "images": [],
                "instructions": [],
                "merchantsCount": 263
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
                "attribute": {
                    "id": 1852,
                    "startFillDate": "2017-06-08T00:00:00Z",
                    "endFillDate": "2019-01-19T00:00:00Z",
                    "maxGoodsCount": 39,
                    "goodsGroup": {
                        "id": 3,
                        "name": "День рождения NEW"
                    }
                },
                "canSelectGoods": true,
                "id": 204,
                "title": "Для персонального предложения",
                "description": "1",
                "begin": "2017-06-01T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 225
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails357: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails365: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 79976,
                "id": "565",
                "rewardThumbnail": null,
                "title": "Гранат без косточек, ЦЕНА ЗА КГ",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 98614,
                "id": "566",
                "rewardThumbnail": null,
                "title": "Альбумин Молоко 2,5% 1л п/пак Мол.Река",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 106613,
                "id": "567",
                "rewardThumbnail": null,
                "title": "Апельсины импорт*, цена за кг",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 121846,
                "id": "568",
                "rewardThumbnail": null,
                "title": "Десерт Аппетиссимо вишня 250г пл/ст/12",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 134083,
                "id": "569",
                "rewardThumbnail": null,
                "title": "Слойка дрожжевая 100г, 2шт в упаковке",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 139845,
                "id": "570",
                "rewardThumbnail": null,
                "title": "Устрицы Португальские, цена за шт/Португалия",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails358: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159570,
                "id": "465",
                "rewardThumbnail": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.TwoLineText, Loymax.Mobile.Contract",
                    "firstLine": "100",
                    "secondLine": "рублей"
                },
                "title": "Aqva",
                "description": null,
                "picture": "2a4a43cd-6033-40c7-a181-0910179bd050"
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159571,
                "id": "466",
                "rewardThumbnail": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.TwoLineText, Loymax.Mobile.Contract",
                    "firstLine": "100",
                    "secondLine": "рублей"
                },
                "title": "WaakaWaka",
                "description": null,
                "picture": "0ce6c7cf-9fe3-48d8-90ce-bdbf1d72f87b"
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159572,
                "id": "467",
                "rewardThumbnail": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.TwoLineText, Loymax.Mobile.Contract",
                    "firstLine": "80",
                    "secondLine": "рублей"
                },
                "title": "Js",
                "description": null,
                "picture": "e439dfb0-19d4-4459-add9-a428fb9fe429"
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159573,
                "id": "468",
                "rewardThumbnail": null,
                "title": "7p",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327020,
                "id": "469",
                "rewardThumbnail": null,
                "title": "Supervolume mascara Secret message",
                "description": null,
                "picture": "890edee6-71cb-4dad-a8ea-c225038a9713"
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327021,
                "id": "470",
                "rewardThumbnail": null,
                "title": "Wet Dry Секрет великолепия",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327022,
                "id": "471",
                "rewardThumbnail": null,
                "title": "Foundation cream Double agent",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327023,
                "id": "472",
                "rewardThumbnail": null,
                "title": "Mascara Try out!",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1186444,
                "id": "473",
                "rewardThumbnail": null,
                "title": "Водка Медофф люкс 40% 0,5л",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1361176,
                "id": "474",
                "rewardThumbnail": null,
                "title": "Пирог Лоранский с ветчиной вес. Собственное пр-во",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails369: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails366: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 15497,
                "id": "571",
                "rewardThumbnail": null,
                "title": "Кофе CAVALIERE 100% Арабика красная банка 250г/12",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 57885,
                "id": "572",
                "rewardThumbnail": null,
                "title": "Горячий шоколад LUKER Panela 700г пл/б/12",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 92552,
                "id": "573",
                "rewardThumbnail": null,
                "title": "Соус Хайнц Сырный 230гр ДП/20",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 100282,
                "id": "574",
                "rewardThumbnail": null,
                "title": "Анж сметана 10%  200гр  стакан  (нет ш/к)",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 143234,
                "id": "575",
                "rewardThumbnail": null,
                "title": "Мидии дальневосточные, цена за кг/Россия",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 145650,
                "id": "576",
                "rewardThumbnail": null,
                "title": "Майонез",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 151510,
                "id": "577",
                "rewardThumbnail": null,
                "title": "Уксус 750мл Гундельсхайм столовый 5%",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails151: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159563,
                "id": "285",
                "rewardThumbnail": null,
                "title": "Albeni",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159565,
                "id": "286",
                "rewardThumbnail": null,
                "title": "Snikers",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159566,
                "id": "287",
                "rewardThumbnail": null,
                "title": "Mars",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159568,
                "id": "288",
                "rewardThumbnail": null,
                "title": "Luna",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159575,
                "id": "289",
                "rewardThumbnail": null,
                "title": "BigBurger",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159577,
                "id": "290",
                "rewardThumbnail": null,
                "title": "SmallBurger",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159578,
                "id": "291",
                "rewardThumbnail": null,
                "title": "DoubleBurger",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159579,
                "id": "292",
                "rewardThumbnail": null,
                "title": "TripleChicken",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 325428,
                "id": "293",
                "rewardThumbnail": null,
                "title": "Просто пицца",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327020,
                "id": "294",
                "rewardThumbnail": null,
                "title": "Supervolume mascara Secret message",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327021,
                "id": "295",
                "rewardThumbnail": null,
                "title": "Wet Dry Секрет великолепия",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327022,
                "id": "296",
                "rewardThumbnail": null,
                "title": "Foundation cream Double agent",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327023,
                "id": "297",
                "rewardThumbnail": null,
                "title": "Mascara Try out!",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159570,
                "id": "298",
                "rewardThumbnail": null,
                "title": "Aqva",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159571,
                "id": "299",
                "rewardThumbnail": null,
                "title": "WaakaWaka",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159572,
                "id": "300",
                "rewardThumbnail": null,
                "title": "Js",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159573,
                "id": "301",
                "rewardThumbnail": null,
                "title": "7p",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 325399,
                "id": "302",
                "rewardThumbnail": null,
                "title": "Дагестанский 3*",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327006,
                "id": "303",
                "rewardThumbnail": null,
                "title": "Аратат 5*",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327007,
                "id": "304",
                "rewardThumbnail": null,
                "title": "Хеннесси VSOP",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327008,
                "id": "305",
                "rewardThumbnail": null,
                "title": "Хеннесси VS",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1186444,
                "id": "306",
                "rewardThumbnail": null,
                "title": "Водка Медофф люкс 40% 0,5л",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1316053,
                "id": "307",
                "rewardThumbnail": null,
                "title": "Товар МРЦ",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails368: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159570,
                "id": "620",
                "rewardThumbnail": null,
                "title": "Aqva",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159571,
                "id": "621",
                "rewardThumbnail": null,
                "title": "WaakaWaka",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159572,
                "id": "622",
                "rewardThumbnail": null,
                "title": "Js",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159573,
                "id": "623",
                "rewardThumbnail": null,
                "title": "7p",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327020,
                "id": "624",
                "rewardThumbnail": null,
                "title": "Supervolume mascara Secret message",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327021,
                "id": "625",
                "rewardThumbnail": null,
                "title": "Wet Dry Секрет великолепия",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327022,
                "id": "626",
                "rewardThumbnail": null,
                "title": "Foundation cream Double agent",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327023,
                "id": "627",
                "rewardThumbnail": null,
                "title": "Mascara Try out!",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1186444,
                "id": "628",
                "rewardThumbnail": null,
                "title": "Водка Медофф люкс 40% 0,5л",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1361176,
                "id": "629",
                "rewardThumbnail": null,
                "title": "Пирог Лоранский с ветчиной вес. Собственное пр-во",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails362: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": true,
                "goodsId": 159563,
                "id": "514",
                "rewardThumbnail": null,
                "title": "Albeni",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159565,
                "id": "515",
                "rewardThumbnail": null,
                "title": "Snikers",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": true,
                "goodsId": 159566,
                "id": "516",
                "rewardThumbnail": null,
                "title": "Mars",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159568,
                "id": "517",
                "rewardThumbnail": null,
                "title": "Luna",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": true,
                "goodsId": 159575,
                "id": "518",
                "rewardThumbnail": null,
                "title": "BigBurger",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails359: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159570,
                "id": "475",
                "rewardThumbnail": null,
                "title": "Aqva",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159571,
                "id": "476",
                "rewardThumbnail": null,
                "title": "WaakaWaka",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159572,
                "id": "477",
                "rewardThumbnail": null,
                "title": "Js",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159573,
                "id": "478",
                "rewardThumbnail": null,
                "title": "7p",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327020,
                "id": "479",
                "rewardThumbnail": null,
                "title": "Supervolume mascara Secret message",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327021,
                "id": "480",
                "rewardThumbnail": null,
                "title": "Wet Dry Секрет великолепия",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327022,
                "id": "481",
                "rewardThumbnail": null,
                "title": "Foundation cream Double agent",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 327023,
                "id": "482",
                "rewardThumbnail": null,
                "title": "Mascara Try out!",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1186444,
                "id": "483",
                "rewardThumbnail": null,
                "title": "Водка Медофф люкс 40% 0,5л",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 1361176,
                "id": "484",
                "rewardThumbnail": null,
                "title": "Пирог Лоранский с ветчиной вес. Собственное пр-во",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails204: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159575,
                "id": "407",
                "rewardThumbnail": null,
                "title": "BigBurger",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159577,
                "id": "408",
                "rewardThumbnail": null,
                "title": "SmallBurger",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159578,
                "id": "409",
                "rewardThumbnail": null,
                "title": "DoubleBurger",
                "description": null,
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalGoodsDetailItem, Loymax.Mobile.Contract",
                "selected": false,
                "goodsId": 159579,
                "id": "410",
                "rewardThumbnail": null,
                "title": "TripleChicken",
                "description": null,
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offer362: {
        "data": {
            "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
            "attribute": {
                "id": 1913,
                "startFillDate": "2017-11-16T00:00:00Z",
                "endFillDate": "2018-12-02T00:00:00Z",
                "maxGoodsCount": 3,
                "goodsGroup": {
                    "id": 253276,
                    "name": "Проверка перс товаров"
                }
            },
            "canSelectGoods": true,
            "id": 362,
            "title": "2 вещи по цене 3-ёх",
            "description": "выбери 2 вещи и получи 3-ю в подарок!!!",
            "begin": "2018-05-04T00:00:00Z",
            "end": null,
            "rewardThumbnail": null,
            "rewardImageId": null,
            "brandIds": [
                "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                "9d5b66e5-cbca-4b7d-a596-7e3c6497e615"
            ],
            "brands": [
                {
                    "id": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                    "name": "Лама",
                    "images": [
                        {
                            "fileId": "3a4570d8-ad55-4e4f-85b0-1882d8ef868f",
                            "description": "user_portal"
                        },
                        {
                            "fileId": "f4b21676-e710-4fcd-9c56-48073937d39d",
                            "description": "marker"
                        }
                    ]
                },
                {
                    "id": "9d5b66e5-cbca-4b7d-a596-7e3c6497e615",
                    "name": "СпортСЕ",
                    "images": [
                        {
                            "fileId": "279eea5c-22be-4764-9693-868f0fbc28e3",
                            "description": "user_portal"
                        },
                        {
                            "fileId": "44c574bf-d470-4f7b-ab1d-afc2d2acc1b5",
                            "description": "marker"
                        }
                    ]
                }
            ],
            "brandId": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
            "images": [],
            "instructions": [
                "1. во-первых",
                "2. во-вторых",
                "3. в-третьих"
            ],
            "merchantsCount": 10
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offer151: {
        "data": {
            "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
            "attribute": {
                "id": 1754,
                "startFillDate": "2017-02-13T00:00:00Z",
                "endFillDate": "2017-02-19T00:00:00Z",
                "maxGoodsCount": 5,
                "goodsGroup": {
                    "id": 190699,
                    "name": "для мобилки"
                }
            },
            "canSelectGoods": false,
            "id": 151,
            "title": "Перс предлож",
            "description": "описание тра-та-та",
            "begin": "2018-05-13T00:00:00Z",
            "end": null,
            "rewardThumbnail": null,
            "rewardImageId": null,
            "brandIds": [
                "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                "9848b201-90ac-4714-aa63-f9232485becc"
            ],
            "brands": [
                {
                    "id": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
                    "name": "Лама",
                    "images": [
                        {
                            "fileId": "3a4570d8-ad55-4e4f-85b0-1882d8ef868f",
                            "description": "user_portal"
                        },
                        {
                            "fileId": "f4b21676-e710-4fcd-9c56-48073937d39d",
                            "description": "marker"
                        }
                    ]
                },
                {
                    "id": "9848b201-90ac-4714-aa63-f9232485becc",
                    "name": "Фудсити",
                    "images": [
                        {
                            "fileId": "2a03f2fa-1af6-4564-9eca-3270b25b4eee",
                            "description": "user_portal"
                        },
                        {
                            "fileId": "9d1491d5-c43f-4a1a-801f-f2ca92a09bba",
                            "description": "marker"
                        }
                    ]
                }
            ],
            "brandId": "a988b93b-71a9-43cc-8eb5-1a23e412f0a4",
            "images": [],
            "instructions": [],
            "merchantsCount": 10
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offer204: {
        "data": {
            "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOfferModel, Loymax.Mobile.Contract",
            "attribute": {
                "id": 1852,
                "startFillDate": "2017-06-08T00:00:00Z",
                "endFillDate": "2019-01-19T00:00:00Z",
                "maxGoodsCount": 39,
                "goodsGroup": {
                    "id": 3,
                    "name": "День рождения NEW"
                }
            },
            "canSelectGoods": true,
            "id": 204,
            "title": "Для персонального предложения",
            "description": "1",
            "begin": "2017-06-01T00:00:00Z",
            "end": null,
            "rewardThumbnail": null,
            "rewardImageId": null,
            "brandIds": [
                "27812daa-fa4d-4564-abab-3305e8355017"
            ],
            "brands": [
                {
                    "id": "27812daa-fa4d-4564-abab-3305e8355017",
                    "name": "ТЕСТ",
                    "images": [
                        {
                            "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                            "description": "user_portal"
                        },
                        {
                            "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                            "description": "marker"
                        },
                        {
                            "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                            "description": "testImage"
                        },
                        {
                            "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                            "description": "superimage"
                        }
                    ]
                }
            ],
            "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
            "images": [],
            "instructions": [],
            "merchantsCount": 225
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    personalOffer: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};
