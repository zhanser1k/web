const httpResponses = require('./index');

module.exports = {
    ...httpResponses,
    userLogins: {
        "data": {
            "identifiers": [
                {
                    "identifierType": "Login",
                    "value": "9070129401809630"
                },
                {
                    "identifierType": "Login",
                    "value": "9070557401011494"
                },
                {
                    "identifierType": "Login",
                    "value": "79999999999"
                },
                {
                    "identifierType": "Login",
                    "value": "wohebiju@yk20.com"
                }
            ],
            "socialIdentifiers": [
                {
                    "provider": "VKontakte",
                    "userId": "79266790",
                    "creationDate": "0001-01-01T00:00:00Z"
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};
