require('@loymax-solutions/protractor-helpers');
const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const mocks = require('./mocks');
const httpResponsesWithoutSocial = require('./httpResponses/withoutSocial');
const httpResponsesWithSocial = require('./httpResponses/withSocial');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();
    const updateOptions = `
        return angular.element(document.body).injector().get('$rootScope').$apply(function() {
            angular.element(document.body).injector().get('$rootScope').newOptions = {
                "redirectUrlOnSocialBinding": "#settings",
            };
        });
    `;

    const test = (firstUserLoginsResponse, secondUserLoginResponse, expectedTest) => {
        $httpBackend.when('GET', /api\/User\/Logins$/).respond(() => {
            if (httpResponses) {
                return [200, httpResponses.userLogins];
            }
        });
        $httpBackend.passThrough();

        browser.get('/#');
        browser.executeScript(`return localStorage.setItem('ls.e2eHttpResponses', '${JSON.stringify(firstUserLoginsResponse)}');`);
        browser.get('/#/settings');
        browser.executeScript(updateOptions);

        po.items.get(1).element(by.css(".social-action")).click();

        if (expectedTest !== 'Привязать') {
            browser.get('http://127.0.0.1:8888/?providerType=VKontakte&code=123456&action=binding&response_type=code');
            browser.sleep(1000);
        }

        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        mocks('with social').forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        $httpBackend.when('GET', /api\/User\/Logins$/).respond(() => {
            if (httpResponses) {
                return [200, httpResponses.userLogins];
            }
        });
        $httpBackend.passThrough();

        browser.get('/#');
        browser.executeScript(`return localStorage.setItem('ls.e2eHttpResponses', '${JSON.stringify(secondUserLoginResponse)}');`);
        browser.get('/#/settings');

        expect(po.items.get(1).element(by.css(".social-action"))).toHaveText(expectedTest);
    };

    return {
        'Should successfully binded social network': () => { test(httpResponsesWithoutSocial, httpResponsesWithSocial, 'Отвязать'); },
        'Should successfully unbinded social network': () => { test(httpResponsesWithSocial, httpResponsesWithoutSocial, 'Привязать') },
    }
};
