const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/PhoneNumber\/$/,
        respond: httpResponses.userPhoneNumber,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/Email\/$/,
        respond: httpResponses.userEmail,
    }, {
        requestMethod: 'GET',
        url: /api\/Social\/Clients$/,
        respond: httpResponses.socialClients,
    }, {
        requestMethod: 'GET',
        url: /api\/History/,
        respond: httpResponses.history,
    }, {
        requestMethod: 'POST',
        url: /api\/User\/VKontakte\/Remove/,
        respond: httpResponses.userVKontakteRemove,
    }];

    return mocks;
};
