const pageObject = function() {
    return {
        attachCard: element(by.css('button[ng-click="attachCard()"]')),
        buttonSubmitAttachCard: element(by.css('form[name="attachCardForm"] button[type="submit"]')),
        buttonSubmitAttachCardConfirmForm: element(by.css('form[name="attachCardConfirmForm"] button[type="submit"]')),
        buttonSubmitBlockCard: element(by.css('form[name="blockCardForm"] button[type="submit"]')),
        helpBlockError: $$('.lmx-help-block__error'),
        cancelAttachCard: element(by.css('button[ng-click="cancelAttachCard()"]')),
        inputCardNumber: element(by.model('model.cardNumber')),
        inputPassword: element(by.model('model.password')),
        inputConfirmCode: element(by.model('model.confirmCode')),
        warning: element(by.css('.lmx-alert-warning')),
        modal: element(by.css('.modal-content')),
        block: element(by.css('.lmx-card-action__block')),
        unblock: element(by.css('.lmx-card-action__unblock')),
    }
};

module.exports = pageObject;
