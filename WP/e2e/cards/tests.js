require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should display an error message if input fields are empty on attach card step': () => {
            po.attachCard.click();
            po.buttonSubmitAttachCard.click();
            expect(po.helpBlockError.count()).toBe(2);
        },
        'Should display an error message if card number is incorrect on attach card step': () => {
            po.attachCard.click();
            po.inputCardNumber.sendKeys('0000000000000000000000000000000');
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitAttachCard.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if password is incorrect on attach card step': () => {
            po.attachCard.click();
            po.inputCardNumber.sendKeys('111111');
            po.inputPassword.sendKeys('000000');
            po.buttonSubmitAttachCard.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if exist error on back-end': () => {
            po.attachCard.click();
            po.inputCardNumber.sendKeys('111111');
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitAttachCard.click();
            expect(po.warning).toBePresent();
        },
        'Should display an error message if confirm code input field is empty': () => {
            po.attachCard.click();
            po.inputCardNumber.sendKeys('111111');
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitAttachCard.click();
            po.buttonSubmitAttachCardConfirmForm.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if confirm code is incorrect': () => {
            po.attachCard.click();
            po.inputCardNumber.sendKeys('111111');
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitAttachCard.click();
            po.inputConfirmCode.sendKeys('000000');
            po.buttonSubmitAttachCardConfirmForm.click();
            expect(po.warning).toBePresent();
        },
        'Should successfully cancel attach card': () => {
            po.attachCard.click();
            po.inputCardNumber.sendKeys('111111');
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitAttachCard.click();
            po.cancelAttachCard.click();
            expect(po.modal.isPresent()).toBe(false);
        },
        'Should successfully attach card': () => {
            po.attachCard.click();
            po.inputCardNumber.sendKeys('111111');
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitAttachCard.click();
            po.inputConfirmCode.sendKeys('111111');
            po.buttonSubmitAttachCardConfirmForm.click();
            expect(po.modal.isPresent()).toBe(false);
        },
        'Should display an error message if password input field is empty on block step': () => {
            po.block.click();
            po.buttonSubmitBlockCard.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if password is incorrect on block step': () => {
            po.block.click();
            po.inputPassword.sendKeys('000000');
            po.buttonSubmitBlockCard.click();
            expect(po.warning).toBePresent();
        },
        'Should successfully block card': () => {
            po.block.click();
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitBlockCard.click();
            expect(po.modal.isPresent()).toBe(false);
        },
        'Should display an error message if password input field is empty on unblock step': () => {
            po.unblock.click();
            po.buttonSubmitBlockCard.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if password is incorrect on unblock step': () => {
            po.unblock.click();
            po.inputPassword.sendKeys('000000');
            po.buttonSubmitBlockCard.click();
            expect(po.warning).toBePresent();
        },
        'Should successfully unblock card': () => {
            po.unblock.click();
            po.inputPassword.sendKeys('111111');
            po.buttonSubmitBlockCard.click();
            expect(po.modal.isPresent()).toBe(false);
        },
    }
};
