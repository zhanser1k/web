const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/Attach$/,
        respond: httpResponses.cardsAttachGet,
    }, {
        requestMethod: 'POST',
        url: /api\/Cards\/Attach\/Cancel$/,
        respond: httpResponses.cardsAttachCancel,
    }];

    const answerCardsAttach = {
        'Should display an error message if card number is incorrect on attach card step': httpResponses.cardsAttachErrorCard,
        'Should display an error message if password is incorrect on attach card step': httpResponses.cardsAttachErrorPassword,
        'Should display an error message if exist error on back-end': httpResponses.cardsAttachErrorBackend,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/Cards\/Attach$/,
        respond: answerCardsAttach[description] || httpResponses.cardsAttachSuccess,
    });

    const answerCardsAttachConfirm = {
        'Should display an error message if confirm code is incorrect': httpResponses.cardsAttachConfirmIncorrect,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/Cards\/Attach\/Confirm$/,
        respond: answerCardsAttachConfirm[description] || httpResponses.cardsAttachConfirm,
    });

    const answerCardsChangeBlockState = {
        'Should display an error message if password is incorrect on block step': httpResponses.cardsChangeBlockStateError,
        'Should display an error message if password is incorrect on unblock step': httpResponses.cardsChangeBlockStateError,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/Cards\/\d+\/ChangeBlockState$/,
        respond: answerCardsChangeBlockState[description] || httpResponses.cardsChangeBlockState,
    });

    return mocks;
};


