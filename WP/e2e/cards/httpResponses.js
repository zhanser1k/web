module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 98.3300,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "accumulatedInfo": [
                    {
                        "amount": 98.3300,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "paidInfo": [
                    {
                        "amount": 0.0000,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "cardActionAccessInfo": {
                    "canBlock": false,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "8c557cac-1219-4da4-9b15-163179653bc4",
                            "description": "1"
                        }
                    ],
                    "cardCount": 321223
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***4922",
                    "email": null,
                    "id": 1066804,
                    "personUid": "7e7216d6-ff81-46a2-92e2-b10588d79ea8",
                    "firstName": "Fjh",
                    "lastName": "Fsd",
                    "patronymicName": "Ssf"
                },
                "id": 613545,
                "state": "Activated",
                "number": "9070953900639382",
                "barCode": "9070953900639382",
                "block": false
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Cards.VirtualCardInfo, Loymax.Mobile.Contract",
                "qrContent": "https://loymax.ru/join/2660000015022",
                "accumulated": {
                    "amount": 98.3300,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "accumulatedInfo": [
                    {
                        "amount": 98.3300,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "paidInfo": [
                    {
                        "amount": 0.0000,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "cardActionAccessInfo": {
                    "canBlock": false,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 27540,
                    "title": "Виртуальная карта",
                    "logicalName": "VirtualCard",
                    "images": [],
                    "cardCount": 3102
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***4922",
                    "email": null,
                    "id": 1066804,
                    "personUid": "7e7216d6-ff81-46a2-92e2-b10588d79ea8",
                    "firstName": "Fjh",
                    "lastName": "Fsd",
                    "patronymicName": "Ssf"
                },
                "id": 1395358,
                "state": "Activated",
                "number": "000001502",
                "barCode": "2660000015022",
                "block": false
            },
            {
                "accumulated": {
                    "amount": 0.0,
                    "currency": "бнс.",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "paid": {
                    "amount": 0.0,
                    "currency": "бнс.",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "accumulatedInfo": [],
                "paidInfo": [],
                "cardActionAccessInfo": {
                    "canBlock": false,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "8c557cac-1219-4da4-9b15-163179653bc4",
                            "description": "1"
                        }
                    ],
                    "cardCount": 321223
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***5557",
                    "email": null,
                    "id": 391890,
                    "personUid": "98a3222c-bb6e-e611-80df-00155d000205",
                    "firstName": "p97tRq",
                    "lastName": "p97tRq",
                    "patronymicName": "p97tRq"
                },
                "id": 611340,
                "state": "Activated",
                "number": "9070950300617331",
                "barCode": "9070950300617331",
                "block": false
            },
            {
                "accumulated": {
                    "amount": 3515.9300,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "paid": {
                    "amount": 449.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "accumulatedInfo": [
                    {
                        "amount": 3515.9300,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "paidInfo": [
                    {
                        "amount": 449.0000,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "grggd@dfgdfg.ru",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 3515.9300,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "paid": {
                    "amount": 449.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "accumulatedInfo": [
                    {
                        "amount": 3515.9300,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "paidInfo": [
                    {
                        "amount": 449.0000,
                        "currency": "бнс.",
                        "currencyInfo": {
                            "id": 1,
                            "name": "Бонусы",
                            "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                            "description": "Общая внутрисистемная валюта",
                            "isDeleted": false,
                            "nameCases": {
                                "nominative": "бонус",
                                "genitive": "бонуса",
                                "plural": "бонусов",
                                "abbreviation": "бнс."
                            }
                        }
                    }
                ],
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "8c557cac-1219-4da4-9b15-163179653bc4",
                            "description": "1"
                        }
                    ],
                    "cardCount": 321223
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "grggd@dfgdfg.ru",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsAttachCancel: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsAttachGet: {
        "data": {
            "cardNumber": null,
            "isStarted": false,
            "maxCountOfCards": 5,
            "currentCountOfAttachedCards": 5
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsAttachSuccess: {
        "data": {
            "confirmCodeLength": 6
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsAttachErrorCard: {
        "result": {
            "state": "ValidationError",
            "message": null,
            "validationErrors": [
                {
                    "field": "attachCardModel.CardNumber",
                    "errorMessages": [
                        "Некорректный номер карты"
                    ]
                }
            ]
        }
    },
    cardsAttachErrorPassword: {
        "result": {
            "state": "ValidationError",
            "message": null,
            "validationErrors": [
                {
                    "field": "attachCardModel.Password",
                    "errorMessages": [
                        "Неправильный пароль."
                    ]
                }
            ]
        }
    },
    cardsAttachErrorBackend: {
        "result": {
            "state": "Error",
            "message": "Не установлен способ оповещения.",
            "validationErrors": null
        }
    },
    cardsAttachConfirm: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsAttachConfirmIncorrect: {
        "result": {
            "state": "Error",
            "message": "Неправильный код подтверждения.",
            "validationErrors": null
        }
    },
    cardsChangeBlockStateError: {
        "result": {
            "state": "Error",
            "message": "Неправильный пароль.",
            "validationErrors": null
        }
    },
    cardsChangeBlockState: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};
