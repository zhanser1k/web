const pageObject = function() {
    return {
        items: element.all(by.repeater('merchant in merchants')),
        changeView: element(by.css('.lmx-pseudolink')),
    }
};

module.exports = pageObject;
