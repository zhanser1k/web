module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    merchants: {
        "data": [
            {
                "id": 86058,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "4504611e-ff42-4a67-870f-c7071fb38b2b",
                "location": {
                    "id": 82000,
                    "locationId": "8a65caab-9b72-48f5-8433-4a020bd6f740",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 86179,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "76672b5f-e1dd-4d99-9eb6-dc264b0dc011",
                "location": {
                    "id": 82121,
                    "locationId": "29bcaaa1-407e-4e41-91af-682571973943",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 86297,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "85ce924d-7729-41d7-b690-23acd1828bea",
                "location": {
                    "id": 82239,
                    "locationId": "e4b35ed3-1740-42c3-9bcd-789a23c18556",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 86415,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "4e3810aa-08bf-46bf-b2c4-5e0c8498f622",
                "location": {
                    "id": 82357,
                    "locationId": "81a48a71-b7ca-41ab-8ee8-a950cfad6c5f",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 86533,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "a47ee280-b157-4c46-ac5f-585163a56d40",
                "location": {
                    "id": 82475,
                    "locationId": "f00b42aa-7f2c-4499-9d54-93529bae4152",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 86651,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "36b5247c-5101-4203-8d4e-68224edf0241",
                "location": {
                    "id": 82593,
                    "locationId": "49ae7953-8355-4e4e-a079-838efcff6756",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 86769,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "b58cd221-9c85-412d-a85a-08b5e582c0b1",
                "location": {
                    "id": 82711,
                    "locationId": "b4a729f4-accb-4639-999c-be1592c0eade",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 86887,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "37a4a2dd-8b49-43a0-ad6f-371bbc5dcda0",
                "location": {
                    "id": 82829,
                    "locationId": "4c601e39-62ed-42ff-9dd0-b23435a478f0",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 87006,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "f9180c54-c513-4cc0-9c2f-696b97f0ec81",
                "location": {
                    "id": 82948,
                    "locationId": "855b5d6f-87d4-46ce-ac28-88807e552447",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 87124,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "9d99ae91-f0f0-4edf-b678-ac67179fdbac",
                "location": {
                    "id": 83066,
                    "locationId": "29e277b3-5e84-40bc-852d-c243a41197a0",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 87190,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "ca081274-ae41-4779-86a5-36decf1568b8",
                "location": {
                    "id": 83132,
                    "locationId": "85fd7787-3714-4cae-8005-8e24f936ccb2",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 87256,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "c7718ff1-4b62-4e36-ac6d-c33d9cc1c16f",
                "location": {
                    "id": 83198,
                    "locationId": "7ee8bed7-a74f-4f1b-8e0d-912b79be0bb5",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 87322,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "6083e139-5e46-4baf-a22c-f31a5ef98393",
                "location": {
                    "id": 83264,
                    "locationId": "d9692b1b-e258-4b29-a7f9-a2a4212f5631",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 119628,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "2bc9b32e-8954-46cf-9065-a403aa600e43",
                "location": {
                    "id": 115215,
                    "locationId": "22ceb1f9-2c2f-4629-b0e7-4a1e366ab62a",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 119629,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "e5d2ad20-d86e-411b-89a0-cf2dc3ea8906",
                "location": {
                    "id": 115216,
                    "locationId": "584e837a-29dc-4e40-9c04-c659a4a06566",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    }
};
