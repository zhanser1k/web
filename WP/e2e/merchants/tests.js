module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should change count of merchants': () => {
            po.changeView.click();
            expect(po.items.count()).toBe(15);
            po.changeView.click();
            expect(po.items.count()).toBe(10);
        },
    }
};
