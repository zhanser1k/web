const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/Email\/$/,
        respond: httpResponses.userEmail,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/PhoneNumber\/$/,
        respond: httpResponses.userPhoneNumber,
    }, {
        requestMethod: 'POST',
        url: /api\/User\/Email\/CancelChange$/,
        respond: httpResponses.userEmailCancelChange,
    }, {
        requestMethod: 'POST',
        url: /api\/User\/Email\/LinkConfirm/,
        respond: httpResponses.userEmailLinkConfirm,
    }];

    const answerChangeEmail = {
        'Should display an error message if a new e-mail address exist': httpResponses.userEmailChangeErrorExist,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Email\/$/,
        respond: answerChangeEmail[description] || httpResponses.userEmailChangeSuccess,
    });

    const answerConfirmCode = {
        'Should display an error message if a confirm code is incorrect': httpResponses.userEmailChangeErrorIncorrectConfirmCode,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Email\/Confirm$/,
        respond: answerConfirmCode[description] || httpResponses.userEmailChangeConfirmSuccess,
    });

    return mocks;
};
