const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const tests = require('../tests')('default');
const mocks = require('../mocks');
const pageObject = require('../pageObject/default');

describe('Default user e-mail', function() {
    const po = new pageObject();
    let descriptions;

    beforeEach(() => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions) {
            descriptions = this.children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks(descriptions[0]).forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        $httpBackend.passThrough();

        browser.get('/#/contacts');
        po.openForm.click();
    });

    afterEach(() => {
        ngMockE2E.clearMockModules();
    });

    for (const name in tests) {
        if (tests.hasOwnProperty(name)) {
            it(name, tests[name]);
        }
    }
});
