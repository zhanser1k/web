const pageObject = function() {
    return {
        openForm: element(by.id('lmx-changeEmail-openForm')),
        inputNewEmail: element(by.model('model.newEmail')),
        inputCodeConfirm: element(by.model('model.confirmationCode')),
        buttonContinue: element(by.id('lmx-changeEmail-continue')),
        buttonCancel: element(by.id('lmx-changeEmail-cancel')),
        helpBlockError: $$('.lmx-help-block__error'),
        message: $$('.lmx-alert-message'),
        warning: $$('.lmx-alert-warning'),
        resend: element(by.id('lmx-changeEmail-resend')),
        reenter: element(by.id('lmx-changeEmail-reenter')),
    }
};

module.exports = pageObject;
