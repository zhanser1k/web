require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should successfully change e-mail address': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonContinue.click();
            po.inputCodeConfirm.sendKeys('111111');
            po.buttonContinue.click();
            expect(po.message.count()).toBe(1);
        },
        'Should cancel e-mail address form changes correctly before sending a new e-mail address': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonCancel.click();
            expect(po.openForm).toBePresent();
            po.openForm.click();
            expect(po.inputNewEmail).toHaveValue('');
        },
        'Should cancel e-mail address form changes correctly before sending a confirm code': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonContinue.click();
            po.inputCodeConfirm.sendKeys('111111');
            po.buttonCancel.click();
            po.openForm.click();
            po.inputNewEmail.sendKeys('pijariveyo@fxprix11111111111.com');
            po.buttonContinue.click();
            expect(po.inputCodeConfirm).toHaveValue('');
        },
        'Should display an error message if a new e-mail address field is empty': () => {
            po.buttonContinue.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if a new e-mail address is incorrect': () => {
            po.inputNewEmail.sendKeys('91491791917987198179');
            po.buttonContinue.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if a new e-mail address exist': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonContinue.click();
            expect(po.warning.count()).toBe(1);
        },
        'Should display an error message if a confirm code field is empty': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonContinue.click();
            po.buttonContinue.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if a confirm code is incorrect': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonContinue.click();
            po.inputCodeConfirm.sendKeys('000000000');
            po.buttonContinue.click();
            expect(po.warning.count()).toBe(1);
        },
        'Should successfully reenter an e-mail address': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonContinue.click();
            po.reenter.click();
            expect(po.inputNewEmail).toHaveValue('pijariveyo@fxprix.com');
        },
        'Should successfully change e-mail address from e-mail message': () => {
            po.inputNewEmail.sendKeys('pijariveyo@fxprix.com');
            po.buttonContinue.click();
            browser.setLocation('confirmEmail?code=254052&id=376105');
            expect(po.message.count()).toBe(1);
        },
    }
};
