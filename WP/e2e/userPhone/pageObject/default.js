const pageObject = function() {
    return {
        openForm: element(by.id('lmx-changePhone-openForm')),
        inputNewPhone: element(by.model('model.newPhone')),
        inputConfirmationCode: element(by.model('model.confirmationCode')),
        buttonCancel: element(by.id('lmx-changePhone-cancel')),
        buttonSubmit: element(by.id('lmx-changePhone-submit')),
        resendCode: element(by.id('lmx-changePhone-resendCode')),
        reenterPhone: element(by.id('lmx-changePhone-reenterPhone')),
        message: $$('.lmx-alert-message'),
        warning: $$('.lmx-alert-warning'),
        helpBlockError: $$('.lmx-help-block__error'),
    }
};

module.exports = pageObject;
