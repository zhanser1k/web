const helpers = require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should save form changes': () => {
            po.inputNewPhone.sendKeys('1234567890');
            po.buttonSubmit.click();
            po.inputConfirmationCode.sendKeys('111111');
            po.buttonSubmit.click();
            expect(po.message.count()).toEqual(1);
        },
        'Should cancel form changes on new phone input': () => {
            po.inputNewPhone.sendKeys('1234567890');
            po.buttonCancel.click();
            po.openForm.click();
            expect(po.inputNewPhone).toHaveValue('');
        },
        'Should display an error message if a new phone number is incorrect or exists': () => {
            po.inputNewPhone.sendKeys('111');
            po.buttonSubmit.click();
            expect(po.warning.count()).toBe(1);
        },
        'Should display an error message if a new phone number input field is empty': () => {
            po.buttonSubmit.click();
            expect(po.helpBlockError.count()).toEqual(1);
        },
        'Should display an error message if confirm code input field is empty': () => {
            po.inputNewPhone.sendKeys('9999999999');
            po.buttonSubmit.click();
            po.buttonSubmit.click();
            expect(po.helpBlockError.count()).toEqual(1);
        },
        'Should display an error message if confirm code is incorrect': () => {
            po.inputNewPhone.sendKeys('9999999999');
            po.buttonSubmit.click();
            po.inputConfirmationCode.sendKeys('000000');
            po.buttonSubmit.click();
            expect(po.warning.count()).toBe(1);
        },
        'Should cancel form changes on confirm code input': () => {
            po.inputNewPhone.sendKeys('1234567890');
            po.buttonSubmit.click();
            po.buttonCancel.click();
            po.openForm.click();
            expect(po.inputNewPhone).toHaveValue('');
        },
        'Should reenter a new phone number successfully': () => {
            po.inputNewPhone.sendKeys('1234567890');
            po.buttonSubmit.click();
            po.reenterPhone.click();
            expect(po.inputNewPhone).toHaveValue('(123) 456-7890');
        },
    }
};
