const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/Email\/$/,
        respond: httpResponses.userEmail,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/PhoneNumber\/$/,
        respond: httpResponses.userPhoneNumber,
    }, {
        requestMethod: 'POST',
        url: /api\/User\/PhoneNumber\/CancelChange$/,
        respond: httpResponses.userPhoneCancelChange,
    }];

    const answerUserPhoneNumber = {
        'Should display an error message if a new phone number is incorrect or exists': httpResponses.userPhoneErrorIncorrect,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/PhoneNumber\/$/,
        respond: answerUserPhoneNumber[description] || httpResponses.userPhoneSuccess,
    });

    const answerUserPhoneNumberConfirm = {
        'Should display an error message if confirm code is incorrect': httpResponses.userPhoneConfirmErrorIncorrect
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/PhoneNumber\/Confirm$/,
        respond: answerUserPhoneNumberConfirm[description] || httpResponses.userPhoneConfirmSuccess,
    });

    return mocks;
};


