module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();
    const updateOptions = `
        return angular.element(document.body).injector().get('$rootScope').$apply(function() {
            angular.element(document.body).injector().get('$rootScope').newOptions = {
                "redirectUrlOnLogin": "#news",
            };
        });
    `;

    return {
        'Should display error message if input fields are empty': () => {
            po.buttonSubmit.click();
            expect(po.errorMessage.count()).toEqual(2);
        },
        'Should display error message if login or password is incorrect': () => {
            po.inputLogin.sendKeys('9999999999');
            po.inputPassword.sendKeys('9999999999');
            po.buttonSubmit.click();
            expect(po.warning.count()).toEqual(1);
        },
        'Should authenticate successfully': () => {
            browser.executeScript(updateOptions);
            po.inputLogin.sendKeys('9999999999');
            po.inputPassword.sendKeys('111111');
            po.buttonSubmit.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/news`);
        },
    }
};
