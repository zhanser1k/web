module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    announcements: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    socialClients: {
        "data": [
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Facebook/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Facebook/Login",
                "providerType": "Facebook",
                "id": 1,
                "clientId": "1750162501880648",
                "clientSecret": null,
                "token": null,
                "name": "Facebook",
                "isActive": false,
                "protocol": "OAuth2",
                "scope": "public_profile",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "https://m.facebook.com/dialog/oauth",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "https://graph.facebook.com/oauth/access_token",
                    "method": "GET",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            },
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/VKontakte/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/VKontakte/Login",
                "providerType": "VKontakte",
                "id": 2,
                "clientId": "6354952",
                "clientSecret": null,
                "token": null,
                "name": "VKontakte",
                "isActive": false,
                "protocol": "OAuth2",
                "scope": "offline",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "https://oauth.vk.com/authorize",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "https://oauth.vk.com/access_token",
                    "method": "GET",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            },
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Odnoklassniki/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Odnoklassniki/Login",
                "providerType": "Odnoklassniki",
                "id": 3,
                "clientId": "1262202880",
                "clientSecret": null,
                "token": null,
                "name": "Odnoklassniki",
                "isActive": false,
                "protocol": "OAuth2",
                "scope": "VALUABLE_ACCESS",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "https://connect.ok.ru/oauth/authorize?layout=m",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "https://api.ok.ru/oauth/token.do",
                    "method": "POST",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            },
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Instagram/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Instagram/Login",
                "providerType": "Instagram",
                "id": 4,
                "clientId": "80--808",
                "clientSecret": null,
                "token": null,
                "name": "Instagram",
                "isActive": false,
                "protocol": "OAuth1",
                "scope": "09",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "http://www.google/",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "http://www.google.com/",
                    "method": "GET",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },

};