const httpResponses = require('./httpResponses');

module.exports = () => {
    return [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/Announcements\/.*$/gi,
        respond: httpResponses.announcements,
    }, {
        requestMethod: 'GET',
        url: /api\/Social\/Clients$/,
        respond: httpResponses.socialClients,
    }];
};
