const pageObject = function() {
    return {
        inputLogin: element(by.model('loginData.login')),
        inputPassword: element(by.model('loginData.password')),
        buttonSubmit: element(by.css('form[name="loginForm"] button[type="submit"]')),
        errorMessage: element.all(by.css('.lmx-help-block')),
        warning: element.all(by.css('.lmx-alert-warning')),
    }
};

module.exports = pageObject;
