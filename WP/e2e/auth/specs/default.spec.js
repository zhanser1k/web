const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const tests = require('../tests')('default');
const mocks = require('../mocks');

describe('Default auth', function() {
    let descriptions;

    beforeEach(() => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions) {
            descriptions = this.children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks().forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        if (descriptions[0] === 'Should authenticate successfully') {
            $httpBackend.when("POST", /api\/token/).respond(200, {"access_token":"e2e_tests_token","token_type":"bearer","expires_in":2399999,"refresh_token":"e2e_tests_token"});
            $httpBackend.when("POST", /api\/Registration\/TryFinishRegistration/).respond(200, {registrationCompleted: true, authToken: "e2e_tests_token"});
        } else {
            $httpBackend.when("POST", /api\/token/).respond(400, {"error":"IncorrectLoginOrPassword","error_description":"Неправильный пароль."});
        }

        $httpBackend.passThrough();

        browser.get('/#/login');
    });

    afterEach(() => {
        ngMockE2E.clearMockModules();
    });

    for (const name in tests) {
        if (tests.hasOwnProperty(name)) {
            it(name, tests[name]);
        }
    }
});
