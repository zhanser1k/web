module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    announcements: {
        "data": [
            {
                "spaceId": 27915,
                "partnerId": 5,
                "authorId": 349482,
                "relatedEntityId": null,
                "id": 3101,
                "isDeleted": false,
                "beginDate": "2018-05-01T00:00:00Z",
                "endDate": null,
                "creationDate": "2018-05-14T08:25:16Z",
                "state": "Published",
                "attributes": [
                    {
                        "$type": "Loymax.Business.Announcement.Model.StringAttributeValue, Loymax.Business.Announcement.Model",
                        "value": "14324",
                        "attributeId": 63785,
                        "attributeLogicalName": "Description",
                        "attributeName": "Описание",
                        "isRelated": false
                    },
                    {
                        "$type": "Loymax.Business.Announcement.Model.StringAttributeValue, Loymax.Business.Announcement.Model",
                        "value": "2345",
                        "attributeId": 63786,
                        "attributeLogicalName": "Title",
                        "attributeName": "Название",
                        "isRelated": false
                    },
                    {
                        "$type": "Loymax.Business.Announcement.Model.GuidAttributeValue, Loymax.Business.Announcement.Model",
                        "value": "e415e65d-bd1f-4d2d-924f-3fe169f17a1c",
                        "attributeId": 63787,
                        "attributeLogicalName": "Image",
                        "attributeName": "Изображение",
                        "isRelated": false
                    },
                    {
                        "$type": "Loymax.Business.Announcement.Model.StringAttributeValue, Loymax.Business.Announcement.Model",
                        "value": "<p>234234</p>",
                        "attributeId": 63846,
                        "attributeLogicalName": "HtmlContent",
                        "attributeName": "HTML контект для ЛК",
                        "isRelated": false
                    }
                ],
                "relatedAnnouncementType": null,
                "permissions": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    announcement: {
        "data": {
            "spaceId": 27915,
            "partnerId": 5,
            "authorId": 349482,
            "relatedEntityId": null,
            "id": 3101,
            "isDeleted": false,
            "beginDate": "2018-05-01T00:00:00Z",
            "endDate": null,
            "creationDate": "2018-05-14T08:25:16Z",
            "state": "Published",
            "attributes": [
                {
                    "$type": "Loymax.Business.Announcement.Model.StringAttributeValue, Loymax.Business.Announcement.Model",
                    "value": "14324",
                    "attributeId": 63785,
                    "attributeLogicalName": "Description",
                    "attributeName": "Описание",
                    "isRelated": false
                },
                {
                    "$type": "Loymax.Business.Announcement.Model.StringAttributeValue, Loymax.Business.Announcement.Model",
                    "value": "2345",
                    "attributeId": 63786,
                    "attributeLogicalName": "Title",
                    "attributeName": "Название",
                    "isRelated": false
                },
                {
                    "$type": "Loymax.Business.Announcement.Model.GuidAttributeValue, Loymax.Business.Announcement.Model",
                    "value": "e415e65d-bd1f-4d2d-924f-3fe169f17a1c",
                    "attributeId": 63787,
                    "attributeLogicalName": "Image",
                    "attributeName": "Изображение",
                    "isRelated": false
                },
                {
                    "$type": "Loymax.Business.Announcement.Model.StringAttributeValue, Loymax.Business.Announcement.Model",
                    "value": "<p>234234</p>",
                    "attributeId": 63846,
                    "attributeLogicalName": "HtmlContent",
                    "attributeName": "HTML контект для ЛК",
                    "isRelated": false
                }
            ],
            "relatedAnnouncementType": null,
            "permissions": null
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};
