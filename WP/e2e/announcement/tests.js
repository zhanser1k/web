module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should display announcement': () => {
            po.items.first().click();
            expect(po.details.count()).toBe(1);
        },
        'Should back to announcements': () => {
            po.items.first().click();
            po.back.click();
            expect(po.items.count()).toBe(1);
        },
    }
};
