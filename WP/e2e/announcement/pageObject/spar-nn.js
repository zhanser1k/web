const pageObject = function() {
    return {
        items: $$('.lmx-announcement'),
        back: element(by.css('a[href="#news"]')),
        details: $$(".lmx-announcement-details"),
    }
};

module.exports = pageObject;
