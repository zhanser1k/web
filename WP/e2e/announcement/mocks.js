const httpResponses = require('./httpResponses');

module.exports = () => {
    return [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/Announcements\?filter/,
        respond: httpResponses.announcements,
    }, {
        requestMethod: 'GET',
        url: /api\/Announcements\/3101/,
        respond: httpResponses.announcement,
    }];
};
