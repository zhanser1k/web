const pageObject = function() {
    return {
        acceptTenderOffer: {
            buttonCancel: element(by.id('lmx-registration-offer__cancel')),
            buttonSubmit: element(by.id('lmx-registration-offer__submit')),
            userAgreesWithOffer: element(by.css('[for="lmx-userAgreesWithOffer"]')),
            userAgreesWithPDP: element(by.css('[for="lmx-userAgreesWithPDP"]')),
            offer: element(by.css('.lmx-AcceptTenderOffer')),
        },
        assignCard: {
            form: element(by.css('[name="assignCardForm"]')),
            inputCardNumber: element(by.model('cardModel.cardNumber')),
            buttonSubmit: element(by.id('lmx-registration-assignCard__submit')),
            doNotHaveCard: element(by.id('lmx-registration-assignCard__doNotHave')),
        },
        changeEmail: {
            form: element(by.css('.lmx-ChangeEmail')),
            inputEmail: element(by.model('model.newEmail')),
            inputConfirmCode: element(by.model('model.confirmationCode')),
            buttonCancel: element(by.id('lmx-changeEmail-cancel')),
            buttonSubmit: element(by.id('lmx-changeEmail-continue')),
            resendCode: element(by.id('lmx-changeEmail-resend')),
            reenterCode: element(by.id('lmx-changeEmail-reenter')),
        },
        changePhone: {
            confirmCodeEntryForm: element(by.css('.lmx-user-phone')),
            phoneEntryForm: element(by.id('lmx-registrationForm')),
            inputConfirmCode: element(by.model('model.confirmationCode')),
            inputNewPhone: element(by.model('registrationData.login')),
            inputReenterPhone: element(by.model('model.newPhone')),
            buttonCancel: element(by.id('lmx-changePhone-cancel')),
            buttonSubmitPhone: element(by.id('lmx-registration-phone__submit')),
            buttonSubmitConfirmCode: element(by.id('lmx-changePhone-submit')),
            resendCode: element(by.id('lmx-changePhone-resendCode')),
            reenterPhone: element(by.id('lmx-changePhone-reenterPhone')),
        },
        passwordRequired: {
            form: element(by.css('[name="setPasswordForm"]')),
            inputNewPassword: element(by.model('model.currentPassword')),
            inputRepeatedPassword: element(by.model('model.repeatPassword')),
            buttonCancel: element(by.id('lmx-registration-password__cancel')),
            buttonSubmit: element(by.id('lmx-registration-password__submit')),
        },
        questions: {
            form: element(by.css('.lmx-Questions')),
            lastName: element(by.id('lmx-_2')),
            firstName: element(by.id('lmx-_1')),
            patronymic: element(by.id('lmx-_16')),
            day: $$('.sb-date-select-wrapper').get(0),
            month: $$('.sb-date-select-wrapper').get(1),
            year: $$('.sb-date-select-wrapper').get(2),
            dayForSelect: $$('.customSelect-list').get(0).$$('.customSelect-item').first(),
            monthForSelect: $$('.customSelect-list').get(1).$$('.customSelect-item').first(),
            yearForSelect: $$('.customSelect-list').get(2).$$('.customSelect-item').get(19),
            currentYear: $$('.customSelect-list').last().$$('.customSelect-item').first(),
            house: element(by.id('lmx-_4')),
            street: element(by.id('lmx-_3')),
            city: element(by.id('lmx-_17')),
            male: element(by.css('[for="lmx-id_Sex_1"]')),
            dropdownMenu: element(by.css('.dropdown-menu')),
            buttonCancel: element(by.id('lmx-registration-questionnaire__cancel')),
            buttonSubmit: element(by.id('lmx-registration-questionnaire__submit')),
        },
        common: {
            success: element(by.css('.lmx-registration-finish h2')),
            helpBlockError: $$('.lmx-help-block__error'),
            warning: $$('.lmx-alert-warning'),
        }
    }
};

module.exports = pageObject;
