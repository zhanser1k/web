module.exports = theme => {
    const pageObject = require(`../pageObject/${theme}`);
    const {
        acceptTenderOffer,
        changePhone,
        common,
    } = new pageObject();

    return {
        'Should cancel registration process': () => {
            acceptTenderOffer.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/login`);
        },
        'Should display an error message if agreements are not accepted': () => {
            acceptTenderOffer.buttonSubmit.click();
            expect(common.helpBlockError.count()).toEqual(2);
        },
        'Should display an error message if some agreements are not accepted': () => {
            acceptTenderOffer.userAgreesWithOffer.click();
            acceptTenderOffer.buttonSubmit.click();
            expect(common.helpBlockError.count()).toEqual(1);
        },
        'Should successfully accept required fields': () => {
            acceptTenderOffer.userAgreesWithOffer.click();
            acceptTenderOffer.userAgreesWithPDP.click();
            acceptTenderOffer.buttonSubmit.click();
            expect(changePhone.confirmCodeEntryForm.isDisplayed()).toBe(true);
        },
    }
};
