module.exports = theme => {
    const pageObject = require(`../pageObject/${theme}`);
    const {assignCard, common} = new pageObject();

    return {
        'Should display an error message for empty card number field': () => {
            assignCard.buttonSubmit.click();
            expect(common.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if card number is incorrect': () => {
            assignCard.inputCardNumber.sendKeys('000');
            assignCard.buttonSubmit.click();
            expect(common.warning.count()).toBe(1);
        },
        'Should successfully complete registration if user do not have card': () => {
            assignCard.doNotHaveCard.click();
            expect(common.success.isDisplayed()).toBe(true);
        },
        'Should successfully assign card': () => {
            assignCard.inputCardNumber.sendKeys('111111111');
            assignCard.buttonSubmit.click();
            expect(common.success.isDisplayed()).toBe(true);
        },
    }
};
