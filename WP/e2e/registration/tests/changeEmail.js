module.exports = theme => {
    const pageObject = require(`../pageObject/${theme}`);
    const {
        changeEmail,
        passwordRequired,
        common,
    } = new pageObject();

    return {
        'Should cancel registration process before sending a new e-mail address': () => {
            changeEmail.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/login`);
        },
        'Should cancel registration process before sending a confirm code': () => {
            changeEmail.inputEmail.sendKeys('test@test.ru');
            changeEmail.buttonSubmit.click();
            changeEmail.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/login`);
        },
        'Should display an error message if a new e-mail address field is empty': () => {
            changeEmail.buttonSubmit.click();
            expect(common.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if a new e-mail address exist': () => {
            changeEmail.inputEmail.sendKeys('pijariveyo@fxprix.com');
            changeEmail.buttonSubmit.click();
            expect(common.warning.count()).toBe(1);
        },
        'Should display an error message if a confirm code field is empty': () => {
            changeEmail.inputEmail.sendKeys('test@test.ru');
            changeEmail.buttonSubmit.click();
            changeEmail.buttonSubmit.click();
            expect(common.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if a confirm code is incorrect': () => {
            changeEmail.inputEmail.sendKeys('pijariveyo@fxprix.com');
            changeEmail.buttonSubmit.click();
            changeEmail.inputConfirmCode.sendKeys('000000000');
            changeEmail.buttonSubmit.click();
            expect(common.warning.count()).toBe(1);
        },
        'Should successfully save e-mail address': () => {
            changeEmail.inputEmail.sendKeys('test@test.ru');
            changeEmail.buttonSubmit.click();
            changeEmail.inputConfirmCode.sendKeys('111111');
            changeEmail.buttonSubmit.click();
            expect(passwordRequired.form.isDisplayed()).toBe(true);
        },
        'Should successfully save e-mail address from e-mail message': () => {
            changeEmail.inputEmail.sendKeys('test@test.ru');
            changeEmail.buttonSubmit.click();
            browser.setLocation('confirmEmail?code=480123&id=2102888');
            expect(passwordRequired.form.isDisplayed()).toBe(true);
        }
    }
};
