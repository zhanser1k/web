module.exports = theme => {
    const pageObject = require(`../pageObject/${theme}`);
    const {
        acceptTenderOffer,
        changePhone,
        common,
    } = new pageObject();

    return {
        'Should display an error message if a new phone number is incorrect or exists': () => {
            changePhone.inputNewPhone.sendKeys('111');
            changePhone.buttonSubmitPhone.click();
            expect(common.warning.count()).toBe(1);
        },
        'Should display an error message if a new phone number input field is empty': () => {
            changePhone.buttonSubmitPhone.click();
            expect(common.helpBlockError.count()).toEqual(1);
        },
        'Should successfully save form changes': () => {
            changePhone.inputNewPhone.sendKeys('9999999999');
            changePhone.buttonSubmitPhone.click();
            expect(acceptTenderOffer.offer.isDisplayed()).toBe(true);
        },
    }
};
