const helpers = require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`../pageObject/${theme}`);
    const {
        changePhone,
        changeEmail,
        common,
    } = new pageObject();

    return {
        'Should cancel registration process': () => {
            changePhone.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/login`);
        },
        'Should reenter a new phone number successfully': () => {
            changePhone.reenterPhone.click();
            expect(changePhone.inputReenterPhone).toHaveValue('');
        },
        'Should display an error message if confirm code input field is empty': () => {
            changePhone.buttonSubmitConfirmCode.click();
            expect(common.helpBlockError.count()).toEqual(1);
        },
        'Should display an error message if confirm code is incorrect': () => {
            changePhone.inputConfirmCode.sendKeys('000000');
            changePhone.buttonSubmitConfirmCode.click();
            expect(common.warning.count()).toBe(1);
        },
        'Should successfully save phone': () => {
            changePhone.inputConfirmCode.sendKeys('111111');
            changePhone.buttonSubmitConfirmCode.click();
            expect(changeEmail.form.isDisplayed()).toBe(true);
        },
    }
};
