const helpers = require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`../pageObject/${theme}`);
    const {
        questions,
        assignCard,
        common,
    } = new pageObject();

    return {
        'Should correctly cancel registration process': () => {
            questions.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/login`);
        },
        'Should not save the questionnaire without required fields filled in': () => {
            questions.buttonSubmit.click();
            expect(common.helpBlockError.count()).toBe(8);
        },
        'Should display drop-down lists for autofill city field': () => {
            questions.city.click();
            expect(questions.dropdownMenu).toBePresent();
        },
        'Should display drop-down lists for autofill house field': () => {
            questions.house.click();
            expect(questions.dropdownMenu).toBePresent();
        },
        'Should display drop-down lists for autofill street field': () => {
            questions.street.click();
            expect(questions.dropdownMenu).toBePresent();
        },
        'Should display an error message for incorrect birthday': () => {
            questions.lastName.sendKeys('Фамилия');
            questions.firstName.sendKeys('Имя');
            questions.patronymic.sendKeys('Отчество');
            questions.day.click();
            questions.dayForSelect.click();
            questions.month.click();
            questions.monthForSelect.click();
            questions.year.click();
            questions.currentYear.click();
            questions.city.sendKeys('Город');
            questions.street.sendKeys('Улица');
            questions.house.sendKeys('1');
            questions.male.click();
            questions.buttonSubmit.click();
            expect(common.warning.count()).toBe(1);
        },
        'Should display an error message for fields with long values': () => {
            helpers.clearAndSetValue(questions.lastName, Array(257).fill(9).join(""));
            questions.firstName.sendKeys('Имя');
            questions.patronymic.sendKeys('Отчество');
            questions.day.click();
            questions.dayForSelect.click();
            questions.month.click();
            questions.monthForSelect.click();
            questions.year.click();
            questions.yearForSelect.click();
            questions.city.sendKeys('Город');
            questions.street.sendKeys('Улица');
            questions.house.sendKeys('1');
            questions.male.click();
            questions.buttonSubmit.click();
            expect(common.warning.count()).toBe(1);
        },
        'Should successfully save questions': () => {
            questions.lastName.sendKeys('Фамилия');
            questions.firstName.sendKeys('Имя');
            questions.patronymic.sendKeys('Отчество');
            questions.day.click();
            questions.dayForSelect.click();
            questions.month.click();
            questions.monthForSelect.click();
            questions.year.click();
            questions.yearForSelect.click();
            questions.city.sendKeys('Город');
            questions.street.sendKeys('Улица');
            questions.house.sendKeys('1');
            questions.male.click();
            questions.buttonSubmit.click();
            expect(assignCard.form.isDisplayed()).toBe(true);
        },
    }
};
