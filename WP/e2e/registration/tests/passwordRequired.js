module.exports = theme => {
    const pageObject = require(`../pageObject/${theme}`);
    const {
        passwordRequired,
        questions,
        common,
    } = new pageObject();

    return {
        'Should correctly cancel registration process': () => {
            passwordRequired.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/login`);
        },
        'Should display an error message if all password fields are empty': () => {
            passwordRequired.buttonSubmit.click();
            expect(common.helpBlockError.count()).toBe(2);
        },
        'Should display an error message if a new password has incorrect length': () => {
            passwordRequired.inputNewPassword.sendKeys('1');
            passwordRequired.inputRepeatedPassword.sendKeys('1');
            passwordRequired.buttonSubmit.click();
            expect(common.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if new password and repeated password are different': () => {
            passwordRequired.inputNewPassword.sendKeys(Array(6).fill(1).join(""));
            passwordRequired.inputRepeatedPassword.sendKeys(Array(7).fill(1).join(""));
            passwordRequired.buttonSubmit.click();
            expect(common.helpBlockError.count()).toBe(1);
        },
        'Should successfully save password': () => {
            passwordRequired.inputNewPassword.sendKeys('111111');
            passwordRequired.inputRepeatedPassword.sendKeys('111111');
            passwordRequired.buttonSubmit.click();
            expect(questions.form.isDisplayed()).toBe(true);
        },
    }
};
