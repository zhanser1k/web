const acceptTenderOffer = require('./acceptTenderOffer');
const assignCard = require('./assignCard');
const changeEmail = require('./changeEmail');
const changePhoneStep1 = require('./changePhoneStep1');
const changePhoneStep2 = require('./changePhoneStep2');
const passwordRequired = require('./passwordRequired');
const questions = require('./questions');

module.exports = theme => {
    return {
        acceptTenderOffer: acceptTenderOffer(theme),
        assignCard: assignCard(theme),
        changeEmail: changeEmail(theme),
        changePhoneStep1: changePhoneStep1(theme),
        changePhoneStep2: changePhoneStep2(theme),
        passwordRequired: passwordRequired(theme),
        questions: questions(theme),
    }
};
