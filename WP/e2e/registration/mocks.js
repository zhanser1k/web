const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'POST',
        url: /api\/User\/AcceptTenderOffer$/,
        respond: httpResponses.userAcceptTenderOffer,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/PhoneNumber\/$/,
        respond: httpResponses.userPhoneNumber,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/Email\/$/,
        respond: httpResponses.userEmailGet,
    }, {
        requestMethod: 'GET',
        url: /api\/user\/questions.*$/gi,
        respond: httpResponses.userQuestionsFilter,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/EmitVirtual/,
        respond: httpResponses.cardsEmitVirtualGet,
    }, {
        requestMethod: 'PUT',
        url: /api\/Cards\/EmitVirtual/,
        respond: httpResponses.cardsEmitVirtualPut,
    }, {
        requestMethod: 'POST',
        url: /api\/Registration\/TryFinishRegistration/,
        respond: httpResponses.registrationTryFinishRegistration,
    }, {
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\//,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'POST',
        url: /api\/User\/Email\/LinkConfirm/,
        respond: httpResponses.userEmailLinkConfirm,
    }];

    const answerRegistrationBeginRegistration = {
        'Should display an error message if a new phone number is incorrect or exists': httpResponses.registrationBeginRegistrationError,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/Registration\/BeginRegistration$/,
        respond: answerRegistrationBeginRegistration[description] || httpResponses.registrationBeginRegistration,
    });

    const answerUserPhoneNumberConfirm = {
        'Should display an error message if confirm code is incorrect': httpResponses.userPhoneNumberConfirmError,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/PhoneNumber\/Confirm/,
        respond: answerUserPhoneNumberConfirm[description] || httpResponses.userPhoneNumberConfirm,
    });

    const answerUserEmailPost = {
        'Should display an error message if a new e-mail address exist': httpResponses.userEmailPostError,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Email\/$/,
        respond: answerUserEmailPost[description] || httpResponses.userEmailPost,
    });

    const answerUserEmailConfirm = {
        'Should display an error message if a confirm code is incorrect': httpResponses.userEmailConfirmError,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Email\/Confirm$/,
        respond: answerUserEmailConfirm[description] || httpResponses.userEmailConfirm,
    });

    const answerUserPasswordSet = {
        'Should display an error message if a new password has incorrect length': httpResponses.userPasswordSetError,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Password\/Set/,
        respond: answerUserPasswordSet[description] || httpResponses.userPasswordSet,
    });

    const answerUserAnswers = {
        'Should display an error message for incorrect birthday': httpResponses.userAnswersErrorDate,
        'Should display an error message for fields with long values': httpResponses.userAnswersErrorName,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Answers$/,
        respond: answerUserAnswers[description] || httpResponses.userAnswers,
    });

    const answerCardsSet = {
        'Should display an error message if card number is incorrect': httpResponses.cardsSetError,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/Cards\/Set/,
        respond: answerCardsSet[description] || httpResponses.cardsSet,
    });

    return mocks;
};
