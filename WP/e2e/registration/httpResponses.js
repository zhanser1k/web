module.exports = {
    actionsAcceptTenderOffer: {
        "data": {
            "actions": [
                {
                    "userActionType": "AcceptTenderOffer",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "ChangePhone",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "ChangeEmail",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "PasswordRequired",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "Questions",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "AssignCard",
                    "actionState": "Required",
                    "isDone": false
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    actionsAssignCard: {
        "data": {
            "actions": [
                {
                    "userActionType": "AcceptTenderOffer",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangePhone",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangeEmail",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "PasswordRequired",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "Questions",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "AssignCard",
                    "actionState": "Required",
                    "isDone": false
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    actionsChangeEmail: {
        "data": {
            "actions": [
                {
                    "userActionType": "AcceptTenderOffer",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangePhone",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangeEmail",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "PasswordRequired",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "Questions",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "AssignCard",
                    "actionState": "Required",
                    "isDone": false
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    actionsChangePhone: {
        "data": {
            "actions": [
                {
                    "userActionType": "AcceptTenderOffer",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangePhone",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "ChangeEmail",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "PasswordRequired",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "Questions",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "AssignCard",
                    "actionState": "Required",
                    "isDone": false
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    actionsPasswordRequired: {
        "data": {
            "actions": [
                {
                    "userActionType": "AcceptTenderOffer",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangePhone",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangeEmail",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "PasswordRequired",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "Questions",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "AssignCard",
                    "actionState": "Required",
                    "isDone": false
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    actionsQuestions: {
        "data": {
            "actions": [
                {
                    "userActionType": "AcceptTenderOffer",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangePhone",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangeEmail",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "PasswordRequired",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "Questions",
                    "actionState": "Required",
                    "isDone": false
                },
                {
                    "userActionType": "AssignCard",
                    "actionState": "Required",
                    "isDone": false
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    actionsComplete: {
        "data": {
            "actions": [
                {
                    "userActionType": "AcceptTenderOffer",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangePhone",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "ChangeEmail",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "PasswordRequired",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "Questions",
                    "actionState": "Required",
                    "isDone": true
                },
                {
                    "userActionType": "AssignCard",
                    "actionState": "Required",
                    "isDone": true
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    announcements: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cards: {
        "data": [
            {
                "accumulated": null,
                "paid": null,
                "accumulatedInfo": [],
                "paidInfo": [],
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "8c557cac-1219-4da4-9b15-163179653bc4",
                            "description": "1"
                        }
                    ],
                    "cardCount": 321223
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9819",
                    "email": "dfgdjf@dfbghdjufg.ru",
                    "id": 2102865,
                    "personUid": "4607b756-a94e-4f0d-8408-9d3241f69cea",
                    "firstName": "1",
                    "lastName": "1",
                    "patronymicName": "1"
                },
                "id": 619077,
                "state": "Activated",
                "number": "9070954900694708",
                "barCode": "9070954900694708",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsEmitVirtualGet: {
        "data": {
            "currentCountOfVirtualCards": 0,
            "isVirtualCardEmissionAllowed": true
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsEmitVirtualPut: {
        "data": 2418273,
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsSet: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cardsSetError: {
        "result": {
            "state": "Error",
            "message": "Не найдена карта.",
            "validationErrors": null
        }
    },
    filesOffer: '<p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p><p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p><p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p><p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p><p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p><p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p><p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p><p>Линеаризация мышления без оглядки на авторитеты иллюстрирует CTR. Дифференциальное уравнение притягивает интеграл по бесконечной области. Первое полустишие начинает символ. График функции поддерживает интеграл от функции комплексной переменной. Гиперцитата естественно отталкивает рыночный определитель системы линейных уравнений. Например, лес — для опытного лесника, охотника, просто внимательного грибника — неисчерпаемое природное семиотическое пространство — текст, поэтому диалектический характер сложен.</p><p>Символ представляет собой эпизодический генезис свободного стиха, не считаясь с затратами. Окрестность точки многопланово позиционирует строфоид, таким образом сбылась мечта идиота - утверждение полностью доказано. Разрыв функции обуславливает побочный PR-эффект, и это придает ему свое звучание, свой характер.</p><p>Механизм сочленений, очевидно, повсеместно поддерживает полином. Эпитет последовательно приводит тройной интеграл – это уже пятая стадия понимания по М.Бахтину. Привлечение аудитории восстанавливает SWOT-анализ. Маркетингово-ориентированное издание ускоряет строфоид.</p>',
    filesOpd: 'JVBERi0xLjMNCiXi48/TDQoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL091dGxpbmVzIDIgMCBSDQovUGFnZXMgMyAwIFINCj4+DQplbmRvYmoNCg0KMiAwIG9iag0KPDwNCi9UeXBlIC9PdXRsaW5lcw0KL0NvdW50IDANCj4+DQplbmRvYmoNCg0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0NvdW50IDINCi9LaWRzIFsgNCAwIFIgNiAwIFIgXSANCj4+DQplbmRvYmoNCg0KNCAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDw8DQovRm9udCA8PA0KL0YxIDkgMCBSIA0KPj4NCi9Qcm9jU2V0IDggMCBSDQo+Pg0KL01lZGlhQm94IFswIDAgNjEyLjAwMDAgNzkyLjAwMDBdDQovQ29udGVudHMgNSAwIFINCj4+DQplbmRvYmoNCg0KNSAwIG9iag0KPDwgL0xlbmd0aCAxMDc0ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBBIFNpbXBsZSBQREYgRmlsZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIFRoaXMgaXMgYSBzbWFsbCBkZW1vbnN0cmF0aW9uIC5wZGYgZmlsZSAtICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjY0LjcwNDAgVGQNCigganVzdCBmb3IgdXNlIGluIHRoZSBWaXJ0dWFsIE1lY2hhbmljcyB0dXRvcmlhbHMuIE1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NTIuNzUyMCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDYyOC44NDgwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjE2Ljg5NjAgVGQNCiggdGV4dC4gQW5kIG1vcmUgdGV4dC4gQm9yaW5nLCB6enp6ei4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjA0Ljk0NDAgVGQNCiggbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDU5Mi45OTIwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNTY5LjA4ODAgVGQNCiggQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA1NTcuMTM2MCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBFdmVuIG1vcmUuIENvbnRpbnVlZCBvbiBwYWdlIDIgLi4uKSBUag0KRVQNCmVuZHN0cmVhbQ0KZW5kb2JqDQoNCjYgMCBvYmoNCjw8DQovVHlwZSAvUGFnZQ0KL1BhcmVudCAzIDAgUg0KL1Jlc291cmNlcyA8PA0KL0ZvbnQgPDwNCi9GMSA5IDAgUiANCj4+DQovUHJvY1NldCA4IDAgUg0KPj4NCi9NZWRpYUJveCBbMCAwIDYxMi4wMDAwIDc5Mi4wMDAwXQ0KL0NvbnRlbnRzIDcgMCBSDQo+Pg0KZW5kb2JqDQoNCjcgMCBvYmoNCjw8IC9MZW5ndGggNjc2ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBTaW1wbGUgUERGIEZpbGUgMiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIC4uLmNvbnRpbnVlZCBmcm9tIHBhZ2UgMS4gWWV0IG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NzYuNjU2MCBUZA0KKCBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY2NC43MDQwIFRkDQooIHRleHQuIE9oLCBob3cgYm9yaW5nIHR5cGluZyB0aGlzIHN0dWZmLiBCdXQgbm90IGFzIGJvcmluZyBhcyB3YXRjaGluZyApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY1Mi43NTIwIFRkDQooIHBhaW50IGRyeS4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NDAuODAwMCBUZA0KKCBCb3JpbmcuICBNb3JlLCBhIGxpdHRsZSBtb3JlIHRleHQuIFRoZSBlbmQsIGFuZCBqdXN0IGFzIHdlbGwuICkgVGoNCkVUDQplbmRzdHJlYW0NCmVuZG9iag0KDQo4IDAgb2JqDQpbL1BERiAvVGV4dF0NCmVuZG9iag0KDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL05hbWUgL0YxDQovQmFzZUZvbnQgL0hlbHZldGljYQ0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCg0KMTAgMCBvYmoNCjw8DQovQ3JlYXRvciAoUmF2ZSBcKGh0dHA6Ly93d3cubmV2cm9uYS5jb20vcmF2ZVwpKQ0KL1Byb2R1Y2VyIChOZXZyb25hIERlc2lnbnMpDQovQ3JlYXRpb25EYXRlIChEOjIwMDYwMzAxMDcyODI2KQ0KPj4NCmVuZG9iag0KDQp4cmVmDQowIDExDQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMTkgMDAwMDAgbg0KMDAwMDAwMDA5MyAwMDAwMCBuDQowMDAwMDAwMTQ3IDAwMDAwIG4NCjAwMDAwMDAyMjIgMDAwMDAgbg0KMDAwMDAwMDM5MCAwMDAwMCBuDQowMDAwMDAxNTIyIDAwMDAwIG4NCjAwMDAwMDE2OTAgMDAwMDAgbg0KMDAwMDAwMjQyMyAwMDAwMCBuDQowMDAwMDAyNDU2IDAwMDAwIG4NCjAwMDAwMDI1NzQgMDAwMDAgbg0KDQp0cmFpbGVyDQo8PA0KL1NpemUgMTENCi9Sb290IDEgMCBSDQovSW5mbyAxMCAwIFINCj4+DQoNCnN0YXJ0eHJlZg0KMjcxNA0KJSVFT0YNCg==',
    locationCities: {
        "data": [
            "cityc1aa3da5a09f46388b79319c39af1298",
            "г. г.Пермь",
            "г. Москва",
            "г. Северск",
            "г. Томск",
            "г. Томск2",
            "g. Томский район"
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    locationHouses: {
        "data": [
            "0",
            "1",
            "1/1",
            "103",
            "13",
            "2",
            "8"
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    locationStreets: {
        "data": [
            "1 Аникинский пер.",
            "1 Водяной пер.",
            "1 Заречная ул.",
            "1 Рабочая ул.",
            "19 Гвардейской Дивизии ул.",
            "1905 года пер.",
            "2 Водяной пер.",
            "2 Ново-Деповская ул.",
            "2 Рабочая ул.",
            "3 Заречная ул."
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    registrationBeginRegistration: {
        "data": {
            "state": "Success",
            "errorMessage": null,
            "authToken": "e2e-tests-token"
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    registrationBeginRegistrationError: {
        "data": {
            "state": "InvalidLoginPassword",
            "errorMessage": "Неправильный логин/пароль.",
            "authToken": null
        },
        "result": {
            "state": "Error",
            "message": "Неправильный логин/пароль.",
            "validationErrors": null
        }
    },
    registrationTryFinishRegistration: {
        "data": {
            "registrationCompleted": true,
            "authToken": "e2e-tests-token"
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    user: {
        "data": {
            "birthDay": "1990-05-03T00:00:00Z",
            "addressInfo": null,
            "cardShortInfo": null,
            "balanceShortInfo": {
                "balance": {
                    "amount": 0.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "notActivated": {
                    "amount": 0.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "accumulated": {
                    "amount": 42.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS",
                    "currencyInfo": {
                        "id": 1,
                        "name": "Бонусы",
                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                        "description": "Общая внутрисистемная валюта",
                        "isDeleted": false,
                        "nameCases": {
                            "nominative": "бонус",
                            "genitive": "бонуса",
                            "plural": "бонусов",
                            "abbreviation": "бнс."
                        }
                    }
                }
            },
            "phoneNumber": "***9819",
            "email": "dfgdjf@dfbghdjufg.ru",
            "id": 2102865,
            "personUid": "4607b756-a94e-4f0d-8408-9d3241f69cea",
            "firstName": "1",
            "lastName": "1",
            "patronymicName": "1"
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userAcceptTenderOffer: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userAnswers: {
        "data": null,
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userAnswersErrorDate: {
        "data": {
            "errors": [
                {
                    "idQuestion": "7",
                    "errors": [
                        "Не допускается участие в программе лиц младше 18 лет"
                    ]
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userAnswersErrorName: {
        "data": {
            "errors": [
                {
                    "idQuestion": "2",
                    "errors": [
                        "Длина строки должна быть меньше 256 символов"
                    ]
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userEmailConfirm: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userEmailConfirmError: {
        "result": {
            "state": "Error",
            "message": "Неправильный код подтверждения.",
            "validationErrors": null
        }
    },
    userEmailGet: {
        "data": {
            "currentEmail": null,
            "newEmail": {
                "email": null
            },
            "confirmCodeLength": 6
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userEmailPost: {
        "data": {
            "codeLength": 6
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userEmailPostError: {
        "result": {
            "state": "Error",
            "message": "Указанный адрес электронной почты уже используется в системе",
            "validationErrors": null
        }
    },
    userEmailLinkConfirm: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userPasswordSet: {
        "data": "e2e-tests-token",
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userPasswordSetError: {
        "result": {
            "state": "ValidationError",
            "message": null,
            "validationErrors": [
                {
                    "field": "passwordModel.Password",
                    "errorMessages": [
                        "Пароль должен содержать не менее 6 и не более 25 символов"
                    ]
                }
            ]
        }
    },
    userPhoneNumber: {
        "data": {
            "currentPhoneNumber": "",
            "newPhoneNumber": {
                "phoneNumber": "***7919"
            },
            "confirmCodeLength": 6
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userPhoneNumberConfirm: {
        "data": "e2e-tests-token",
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userPhoneNumberConfirmError: {
        "result": {
            "state": "Error",
            "message": "Неправильный код подтверждения.",
            "validationErrors": null
        }
    },
    userPhoneNumberSendConfirmCode: {
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userQuestionsFilter: {
        "data": {
            "questions": [
                {
                    "nodeType": "Header",
                    "displayName": "Личные данные",
                    "description": null,
                    "logicalName": null,
                    "value": null,
                    "readOnly": false,
                    "required": null,
                    "id": null,
                    "groupID": null,
                    "selected": null,
                    "maxLength": null,
                    "fieldType": null,
                    "children": [
                        {
                            "nodeType": "Question",
                            "displayName": "Фамилия",
                            "description": null,
                            "logicalName": "LastName",
                            "value": null,
                            "readOnly": false,
                            "required": true,
                            "id": 2,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "String",
                            "children": null
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Имя",
                            "description": null,
                            "logicalName": "FirstName",
                            "value": null,
                            "readOnly": false,
                            "required": true,
                            "id": 1,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "String",
                            "children": null
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Отчество",
                            "description": null,
                            "logicalName": "PatronymicName",
                            "value": null,
                            "readOnly": false,
                            "required": true,
                            "id": 16,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "String",
                            "children": null
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Дата рождения",
                            "description": null,
                            "logicalName": "BirthDay",
                            "value": null,
                            "readOnly": false,
                            "required": true,
                            "id": 7,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "DateTime",
                            "children": null
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Место жительства",
                            "description": null,
                            "logicalName": null,
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Город",
                                    "description": null,
                                    "logicalName": "City",
                                    "value": null,
                                    "readOnly": false,
                                    "required": true,
                                    "id": 17,
                                    "groupID": null,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "String",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Улица",
                                    "description": null,
                                    "logicalName": "Street",
                                    "value": null,
                                    "readOnly": false,
                                    "required": true,
                                    "id": 3,
                                    "groupID": null,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "String",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Дом",
                                    "description": null,
                                    "logicalName": "House",
                                    "value": null,
                                    "readOnly": false,
                                    "required": true,
                                    "id": 4,
                                    "groupID": null,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "String",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Пол",
                            "description": null,
                            "logicalName": "Sex",
                            "value": null,
                            "readOnly": false,
                            "required": true,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Мужской",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 1,
                                    "groupID": 5,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Женский",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 2,
                                    "groupID": 5,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};
