const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const tests = require('../tests')('default');
const mocks = require('../mocks');
const httpResponses = require('../httpResponses');

describe('Default registration.', function() {
    const runTests = testName => {
        for (const name in tests[testName]) {
            if (tests[testName].hasOwnProperty(name)) {
                it(name, tests[testName][name]);
            }
        }
    };

    const goToRegistration = description => {
        browser.get('/#');
        browser.executeScript(`return localStorage.setItem('ls.isRegistrationToken', 'true');`);
        browser.executeScript(`return localStorage.setItem('ls.authorizationToken', 'e2e_tests_token');`);
        browser.executeScript(`return localStorage.setItem('ls.e2eHttpResponses', '${JSON.stringify(httpResponses)}');`);

        if (description === 'Accept tender offer') {
            browser.executeScript(`return localStorage.setItem('ls.newOptions', '{"offerHtmlFileId": "offer", "opdAgreementFileId": "opd"}');`);
        }

        browser.get('/#/registration');
    };

    let descriptions;

    const setMocks = children => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions || descriptions.length === 1) {
            descriptions = children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks(descriptions[0]).forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });
    };

    afterEach(() => {
        browser.executeScript(`return localStorage.removeItem('ls.authorizationToken');`);
        browser.executeScript(`return localStorage.removeItem('ls.isRegistrationToken');`);
        browser.executeScript(`return localStorage.removeItem('ls.e2eHttpResponses');`);
        ngMockE2E.clearMockModules();
    });

    describe('Change phone step 1', function() {
        beforeEach(() => {
            setMocks(this.children);

            $httpBackend.when('GET', /api\/user\/actions$/).respond(httpResponses.actionsAcceptTenderOffer);
            $httpBackend.passThrough();

            browser.get('/#/registration');
            browser.executeScript('window.httpResponses = ' + JSON.stringify(httpResponses));
        });

        runTests('changePhoneStep1');
    });

    describe('Accept tender offer', function() {
        beforeEach(() => {
            setMocks(this.children);

            $httpBackend.when('GET', /api\/user\/actions$/).respond(() => {
                if (httpResponses) {
                    window.actionsE2eResponses = window.actionsE2eResponses || false;
                    var data = window.actionsE2eResponses ? httpResponses.actionsChangePhone : httpResponses.actionsAcceptTenderOffer;

                    window.actionsE2eResponses = !window.actionsE2eResponses;

                    return [200, data];
                }
            });

            $httpBackend.when('GET', /api\/files\/offer$/).respond(() => {
                if (httpResponses) {
                    return [200, httpResponses.filesOffer, {'Content-Type': 'text/html'}];
                }
            });

            $httpBackend.when('GET', /api\/files\/opd$/).respond(() => {
                if (httpResponses) {
                    return [200, httpResponses.filesOpd, {'Content-Type': 'application/pdf'}];
                }
            });

            $httpBackend.passThrough();

            goToRegistration(this.description);
        });

        runTests('acceptTenderOffer');
    });

    describe('Change phone step 2', function() {
        beforeEach(() => {
            setMocks(this.children);

            $httpBackend.when('GET', /api\/user\/actions$/).respond(() => {
                if (httpResponses) {
                    window.actionsE2eResponses = window.actionsE2eResponses || false;
                    var data = window.actionsE2eResponses ? httpResponses.actionsChangeEmail : httpResponses.actionsChangePhone;

                    window.actionsE2eResponses = !window.actionsE2eResponses;

                    return [200, data];
                }
            });
            $httpBackend.passThrough();

            goToRegistration();
        });

        runTests('changePhoneStep2');
    });

    describe('Change email', function() {
        beforeEach(() => {
            setMocks(this.children);

            $httpBackend.when('GET', /api\/user\/actions$/).respond(() => {
                if (httpResponses) {
                    window.actionsE2eResponses = window.actionsE2eResponses || false;
                    var data = window.actionsE2eResponses ? httpResponses.actionsPasswordRequired : httpResponses.actionsChangeEmail;

                    window.actionsE2eResponses = !window.actionsE2eResponses;

                    return [200, data];
                }
            });
            $httpBackend.passThrough();

            goToRegistration();
        });

        runTests('changeEmail');
    });

    describe('Password required', function() {
        beforeEach(() => {
            setMocks(this.children);

            $httpBackend.when('GET', /api\/user\/actions$/).respond(() => {
                if (httpResponses) {
                    window.actionsE2eResponses = window.actionsE2eResponses || false;
                    var data = window.actionsE2eResponses ? httpResponses.actionsQuestions : httpResponses.actionsPasswordRequired;

                    window.actionsE2eResponses = !window.actionsE2eResponses;

                    return [200, data];
                }
            });
            $httpBackend.passThrough();

            goToRegistration();
        });

        runTests('passwordRequired');
    });

    describe('Questionnaire', function() {
        beforeEach(() => {
            setMocks(this.children);

            $httpBackend.when('GET', /api\/user\/actions$/).respond(() => {
                if (httpResponses) {
                    window.actionsE2eResponses = window.actionsE2eResponses || false;
                    var data = window.actionsE2eResponses ? httpResponses.actionsAssignCard : httpResponses.actionsQuestions;

                    window.actionsE2eResponses = !window.actionsE2eResponses;

                    return [200, data];
                }
            });
            $httpBackend.passThrough();

            goToRegistration();
        });

        runTests('questions');
    });

    describe('Assign card', function() {
        beforeEach(() => {
            setMocks(this.children);

            $httpBackend.when('GET', /api\/user\/actions$/).respond(() => {
                if (httpResponses) {
                    window.actionsE2eResponses = window.actionsE2eResponses || false;
                    var data = window.actionsE2eResponses ? httpResponses.actionsComplete : httpResponses.actionsAssignCard;

                    window.actionsE2eResponses = !window.actionsE2eResponses;

                    return [200, data];
                }
            });
            $httpBackend.passThrough();

            goToRegistration();
        });

        runTests('assignCard');
    });
});
