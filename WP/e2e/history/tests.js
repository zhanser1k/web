const helpers = require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should display last items on pagination change event': () => {
            const oldData = po.items.last().$$('td').first().getText();
            po.paginationLast.click();
            expect(oldData === po.items.last().$$('td').first().getText()).toEqual(false);
        },
        'Should display more information': () => {
            po.items.get(2).click();
            expect(helpers.hasClass(po.items.get(2), 'lmx-cheque-opened')).toEqual(true);
        },
        'Should display not found message': () => {
            expect(po.notFound).toBeDisplayed();
        },
        'Should do not display pagination': () => {
            expect(helpers.hasClass(po.pagination, 'ng-hide')).toEqual(true);
        },
    }
};
