const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/History\?filter.count=10&filter.from=240/,
        respond: httpResponses.historyManyLast,
    }, {
        requestMethod: 'GET',
        url: /api\/History\?filter.count=10&filter.from=10/,
        respond: httpResponses.historyManySecond,
    }];

    const answerHistory = {
        'Should display not found message': httpResponses.historyEmpty,
        'Should do not display pagination': httpResponses.historyLittle,
    };

    mocks.push({
        requestMethod: 'GET',
        url: /api\/History\?filter.count=10&filter.from=0/,
        respond: answerHistory[description] || httpResponses.historyMany,
    });

    return mocks;
};


