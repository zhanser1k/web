const pageObject = function() {
    return {
        paginationLast: element(by.css('.pagination-last')),
        items: element.all(by.repeater('item in history.rows')),
        notFound: element(by.css('.lmx-not-found')),
        togglerCalendar: element.all(by.css('.toggler.calendar')),
        calendarMonthYear: element(by.css('.btn-sm.uib-title')),
        calendarMonthYearRight: element(by.css('.btn-sm.uib-right')),
        calendarSelectDate: element(by.css('button[ng-click="select(dt.date)"]')),
        pagination: element(by.css('.pagination')),
        triggerPagination: element(by.id("lmx-history-non-pagination-trigger")),
        increase: element(by.id("lmx-history-increase")),
    }
};

module.exports = pageObject;
