module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    historyMany: {
        "data": {
            "allCount": 250,
            "rows": [
                {
                    "id": "f17ab1c5-f686-42dc-a107-e82b035b023c",
                    "dateTime": "2018-04-06T09:44:35Z",
                    "type": "RewardData",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "Выдача карты",
                    "location": null,
                    "partnerId": "a5ac5ccc-96fe-4197-a491-418b5d7d4c86",
                    "brandId": null,
                    "brand": null,
                    "data": {
                        "$type": "Loymax.History.UI.Model.RewardDataModel, Loymax.History.UI.Model",
                        "offerExternalId": "6b455e6e-4876-43f6-9b6e-24642ea5f15d",
                        "rewardType": "Bonus",
                        "description": null,
                        "amount": {
                            "amount": 50.0,
                            "currency": "BNS",
                            "currencyInfo": {
                                "id": 1,
                                "name": "Бонусы",
                                "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                "description": "Общая внутрисистемная валюта",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "бонус",
                                    "genitive": "бонуса",
                                    "plural": "бонусов",
                                    "abbreviation": "бнс."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "2133d16f-35fc-4c7b-b9b6-84ae271905f7",
                    "dateTime": "2017-11-17T10:20:30Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 59, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "98_665_33766_2221595_91270343",
                        "chequeItems": [
                            {
                                "description": "БАНАНЫ 1КГ",
                                "count": 0.175,
                                "amount": 100.0
                            },
                            {
                                "description": "ХЛЕБ БАТОН НАРЕЗНОЙ",
                                "count": 1.0,
                                "amount": 50.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "658817",
                        "amount": {
                            "amount": 150.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "ff01b637-1f6d-4497-ba64-c1afed4a6d8f",
                    "dateTime": "2017-11-17T04:38:09Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "a2512db256c301d37c0c",
                        "chequeItems": [
                            {
                                "description": "Рагу из баклажан, цена за кг",
                                "count": 14.05,
                                "amount": 140.5
                            },
                            {
                                "description": "Салат из спаржи Мистер Хе 150г пл/уп",
                                "count": 87.956,
                                "amount": 879.56
                            },
                            {
                                "description": "Салат Гнездо глухаря 150г, цена за шт",
                                "count": 93.947,
                                "amount": 939.47
                            },
                            {
                                "description": "Каша пшеничная Сила и бодрость, 0,2кг, цена за шт.",
                                "count": 76.288,
                                "amount": 762.88
                            },
                            {
                                "description": "РЕСТОРАТОР Салат Цезарь, цена за кг",
                                "count": 33.729,
                                "amount": 337.29
                            },
                            {
                                "description": "Салат Мистер Хе Себастьян, цена за кг",
                                "count": 78.369,
                                "amount": 783.69
                            },
                            {
                                "description": "Салат Гурман, цена за кг",
                                "count": 91.811,
                                "amount": 918.11
                            },
                            {
                                "description": "ЛАМА Цыплята табака, цена за кг",
                                "count": 77.427,
                                "amount": 774.27
                            },
                            {
                                "description": "Биточки Муромские, цена за кг",
                                "count": 8.432,
                                "amount": 84.32
                            },
                            {
                                "description": "ФУД Заливное из курицы 180г",
                                "count": 39.047,
                                "amount": 390.47
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 420.74,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "64455466",
                        "amount": {
                            "amount": 6010.56,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "5f69be26-08be-464b-9b2a-6ec8f4dc9df4",
                    "dateTime": "2017-11-17T04:38:09Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "cccbafafcaf608e3e426",
                        "chequeItems": [
                            {
                                "description": "Салат из капусты Мистер Хе 150г пл/уп",
                                "count": 12.473,
                                "amount": 124.73
                            },
                            {
                                "description": "Инжир ГРЕКО с сыром фета, цена за кг (пл/банка 3,1кг, сух/вес 1,8кг)",
                                "count": 63.85,
                                "amount": 638.5
                            },
                            {
                                "description": "Салат Успенский, цена за кг",
                                "count": 84.262,
                                "amount": 842.62
                            },
                            {
                                "description": "Свинина деликатесная, цена за кг",
                                "count": 7.732,
                                "amount": 77.32
                            },
                            {
                                "description": "Заправка для супов, цена за кг",
                                "count": 37.132,
                                "amount": 371.32
                            },
                            {
                                "description": "Суши с угрем Мистер Хе, цена за кг",
                                "count": 94.85,
                                "amount": 948.5
                            },
                            {
                                "description": "Салат Русский размер, цена за кг",
                                "count": 51.797,
                                "amount": 517.97
                            },
                            {
                                "description": "Салат Ярославна, цена за кг",
                                "count": 72.683,
                                "amount": 726.83
                            },
                            {
                                "description": "Салат Капель, цена за кг",
                                "count": 36.416,
                                "amount": 364.16
                            },
                            {
                                "description": "Домашний ресторан Салат Каприз 150г, цена за шт",
                                "count": 61.446,
                                "amount": 614.46
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 365.85,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "59876435",
                        "amount": {
                            "amount": 5226.41,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "09d65df6-eb02-4c3c-8e94-56361ed60c7e",
                    "dateTime": "2017-11-17T04:38:08Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "4a1c89e71c941567e45d",
                        "chequeItems": [
                            {
                                "description": "Капуста тушеная, цена за кг",
                                "count": 80.058,
                                "amount": 800.58
                            },
                            {
                                "description": "Баклажаны ИП Салиджанов, цена за кг",
                                "count": 17.627,
                                "amount": 176.27
                            },
                            {
                                "description": "Суши с икрой ИП Салиджанов 200г пл/уп",
                                "count": 14.091,
                                "amount": 140.91
                            },
                            {
                                "description": "Жар-Пицца Пирог с свининой/луком 100г",
                                "count": 48.02,
                                "amount": 480.2
                            },
                            {
                                "description": "Салат из свежей капусты Мистер Хе, цена за кг",
                                "count": 14.295,
                                "amount": 142.95
                            },
                            {
                                "description": "Смак  18",
                                "count": 2.41,
                                "amount": 24.1
                            },
                            {
                                "description": "Имбирь маринованый Мистер Хе 50г",
                                "count": 75.764,
                                "amount": 757.64
                            },
                            {
                                "description": "Перец фаршированный овощами и рисом, цена за кг",
                                "count": 64.023,
                                "amount": 640.23
                            },
                            {
                                "description": "Салат 5 Авеню, цена за кг",
                                "count": 58.092,
                                "amount": 580.92
                            },
                            {
                                "description": "Бутерброд с овощами, цена за шт.",
                                "count": 47.27,
                                "amount": 472.7
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 295.15,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "89756310",
                        "amount": {
                            "amount": 4216.5,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "6470f2f5-e054-4029-9721-966f8943118e",
                    "dateTime": "2017-11-17T04:38:07Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "76f59efce0fa1403cf99",
                        "chequeItems": [
                            {
                                "description": "Зеленый Коктейль Дачный 0,3 л , цена за шт.",
                                "count": 24.948,
                                "amount": 249.48
                            },
                            {
                                "description": "Свиные ушки Мистер Хе 150г пл/уп",
                                "count": 46.978,
                                "amount": 469.78
                            },
                            {
                                "description": "Суши Ассорти ИП Салиджанов 200г пл/уп",
                                "count": 67.565,
                                "amount": 675.65
                            },
                            {
                                "description": "Бифштекс рубленый, цена за кг",
                                "count": 91.007,
                                "amount": 910.07
                            },
                            {
                                "description": "Опята ИП Салиджанов 150г пл/уп",
                                "count": 65.497,
                                "amount": 654.97
                            },
                            {
                                "description": "Форель слабосоленая стейк, цена за кг",
                                "count": 44.717,
                                "amount": 447.17
                            },
                            {
                                "description": "Котлета рыбная, цена за кг",
                                "count": 1.506,
                                "amount": 15.06
                            },
                            {
                                "description": "ЛАМА Куриные желудки за 1кг",
                                "count": 81.722,
                                "amount": 817.22
                            },
                            {
                                "description": "Баклажаны с мясом Мистер Хе, цена за кг",
                                "count": 97.712,
                                "amount": 977.12
                            },
                            {
                                "description": "ЛАМА Салат Цезарь, цена за кг",
                                "count": 98.484,
                                "amount": 984.84
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 434.09,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "19687662",
                        "amount": {
                            "amount": 6201.36,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "d03dc62c-84f3-4856-ad79-4fff84960576",
                    "dateTime": "2017-11-17T04:38:06Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "5e8083d77990dc711edc",
                        "chequeItems": [
                            {
                                "description": "РЕСТОРАТОР Колбаски гриль, цена за кг",
                                "count": 90.345,
                                "amount": 903.45
                            },
                            {
                                "description": "Баклажаны ИП Салиджанов, цена за кг",
                                "count": 87.317,
                                "amount": 873.17
                            },
                            {
                                "description": "Жар-Пицца Пирог с свининой/луком 100г",
                                "count": 95.032,
                                "amount": 950.32
                            },
                            {
                                "description": "Ролл Нежный (семга охл, угорь копч.,кунжут, авокадо, васаби), кг",
                                "count": 31.986,
                                "amount": 319.86
                            },
                            {
                                "description": "Оливки Грека Халкидики с/к в маринаде, цена за кг (банка 3кг,сух/вес 1,8 кг.)",
                                "count": 26.502,
                                "amount": 265.02
                            },
                            {
                                "description": "Морковча ИП Салиджанов, цена за кг",
                                "count": 21.614,
                                "amount": 216.14
                            },
                            {
                                "description": "Желе из яблок 180г",
                                "count": 99.099,
                                "amount": 990.99
                            },
                            {
                                "description": "Рыба ФудФиш, цена за кг (Пр-во) 1",
                                "count": 62.149,
                                "amount": 621.49
                            },
                            {
                                "description": "Свинина запеченная с овощами, цена за кг",
                                "count": 64.751,
                                "amount": 647.51
                            },
                            {
                                "description": "Тарталетки с грибным жульеном, цена за кг",
                                "count": 86.754,
                                "amount": 867.54
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 465.88,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "20694053",
                        "amount": {
                            "amount": 6655.49,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "e67e0e5c-7abb-4a5f-99a6-cf5d78f161cb",
                    "dateTime": "2017-11-17T04:38:06Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "cc848779c815d004c0de",
                        "chequeItems": [
                            {
                                "description": "Морская капуста с авокадо и сладким перцем Мистер Хе, цена за кг",
                                "count": 7.709,
                                "amount": 77.09
                            },
                            {
                                "description": "Рождественская утка, цена за кг",
                                "count": 65.153,
                                "amount": 651.53
                            },
                            {
                                "description": "Рулетик куриный с грибами гриль, цена за кг",
                                "count": 63.749,
                                "amount": 637.49
                            },
                            {
                                "description": "Жар-Пицца Пирог с карт/мясом 100г",
                                "count": 56.637,
                                "amount": 566.37
                            },
                            {
                                "description": "Салат Молодость с проростками овса и рукколой, 0,2кг, цена за шт.",
                                "count": 8.783,
                                "amount": 87.83
                            },
                            {
                                "description": "Сыр Копченый Охотничий Золотые горы 200г в/у",
                                "count": 3.064,
                                "amount": 30.64
                            },
                            {
                                "description": "Грибы белые ИП Салиджанов 150г пл/уп",
                                "count": 43.437,
                                "amount": 434.37
                            },
                            {
                                "description": "Салат Бонжур 210г, цена за шт.",
                                "count": 39.804,
                                "amount": 398.04
                            },
                            {
                                "description": "Суши Текка маки (рол с лососем) 95г, цена за порцию",
                                "count": 75.199,
                                "amount": 751.99
                            },
                            {
                                "description": "Манты, цена за кг",
                                "count": 34.429,
                                "amount": 344.29
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 278.58,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "97429737",
                        "amount": {
                            "amount": 3979.64,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "056555d9-b229-45fd-9792-3958c5c5ba42",
                    "dateTime": "2017-11-17T04:38:05Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "9b5da4a7757ddd0cda41",
                        "chequeItems": [
                            {
                                "description": "Салат Грибная шуба, цена за кг",
                                "count": 36.35,
                                "amount": 363.5
                            },
                            {
                                "description": "Фунчеза ИП Салиджанов 150г пл/уп",
                                "count": 34.367,
                                "amount": 343.67
                            },
                            {
                                "description": "Салат Морячка, цена за кг",
                                "count": 79.825,
                                "amount": 798.25
                            },
                            {
                                "description": "Желудки куриные ИП Салиджанов, цена за кг",
                                "count": 48.48,
                                "amount": 484.8
                            },
                            {
                                "description": "Камбала жареная, цена за кг",
                                "count": 99.432,
                                "amount": 994.32
                            },
                            {
                                "description": "Салат Мистер Хе Шанхай 150г, цена за шт",
                                "count": 88.483,
                                "amount": 884.83
                            },
                            {
                                "description": "Морковча ИП Салиджанов, цена за кг",
                                "count": 95.104,
                                "amount": 951.04
                            },
                            {
                                "description": "Хе из баклажан Мистер Хе 150г пл/уп",
                                "count": 4.761,
                                "amount": 47.61
                            },
                            {
                                "description": "ФУД Пасхальные яйца, цена за штуку",
                                "count": 97.691,
                                "amount": 976.91
                            },
                            {
                                "description": "Квише с ветчиной, цена за кг",
                                "count": 12.348,
                                "amount": 123.48
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 417.78,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "92576733",
                        "amount": {
                            "amount": 5968.41,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "300c7e72-c25e-4ce6-9957-19b754242f89",
                    "dateTime": "2017-11-17T04:38:04Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "d1aebc900ee1a7a88a1f",
                        "chequeItems": [
                            {
                                "description": "Свекла с грецким орехом 150г, цена за шт",
                                "count": 8.016,
                                "amount": 80.16
                            },
                            {
                                "description": "Филе куриное Нежность Мистер Хе, цена за кг",
                                "count": 58.358,
                                "amount": 583.58
                            },
                            {
                                "description": "Салат Петровский 150г, цена за шт",
                                "count": 49.751,
                                "amount": 497.51
                            },
                            {
                                "description": "Салат с говядиной, цена за кг",
                                "count": 73.814,
                                "amount": 738.14
                            },
                            {
                                "description": "Голень куриная гриль, цена за кг",
                                "count": 82.002,
                                "amount": 820.02
                            },
                            {
                                "description": "Салат Гнездо глухаря, цена за кг",
                                "count": 54.873,
                                "amount": 548.73
                            },
                            {
                                "description": "Соус Особый, цена за кг",
                                "count": 61.892,
                                "amount": 618.92
                            },
                            {
                                "description": "Соус Пикантный, цена за кг",
                                "count": 23.382,
                                "amount": 233.82
                            },
                            {
                                "description": "ФУД Набор для винегрета, цена за кг",
                                "count": 90.688,
                                "amount": 906.88
                            },
                            {
                                "description": "Хе из терпуга ИП Салиджанов, цена за кг",
                                "count": 60.946,
                                "amount": 609.46
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 394.6,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "91911149",
                        "amount": {
                            "amount": 5637.22,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    historyManyLast: {
        "data": {
            "allCount": 250,
            "rows": [
                {
                    "id": "83ef4824-5849-4185-9faf-c55917993812",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "dd1772616c78c9274cce",
                        "chequeItems": [
                            {
                                "description": "Филе куриное Нежность Мистер Хе, цена за кг",
                                "count": 98.676,
                                "amount": 986.76
                            },
                            {
                                "description": "Салат Римские каникулы, цена за кг",
                                "count": 15.455,
                                "amount": 154.55
                            },
                            {
                                "description": "Салат Гурман, цена за кг",
                                "count": 97.134,
                                "amount": 971.34
                            },
                            {
                                "description": "Сок апельсиново-грейфрутовый фреш 0,5 л , цена за бут.",
                                "count": 71.188,
                                "amount": 711.88
                            },
                            {
                                "description": "Морковча с мясом Мистер Хе, цена за кг",
                                "count": 42.838,
                                "amount": 428.38
                            },
                            {
                                "description": "Морковь отварная, цена за кг",
                                "count": 50.929,
                                "amount": 509.29
                            },
                            {
                                "description": "Свинина по итальянски, цена за кг",
                                "count": 78.458,
                                "amount": 784.58
                            },
                            {
                                "description": "Салат Грибная шуба, цена за кг",
                                "count": 7.13,
                                "amount": 71.3
                            },
                            {
                                "description": "Фунчеза ИП Салиджанов 150г пл/уп",
                                "count": 12.187,
                                "amount": 121.87
                            },
                            {
                                "description": "Салат Морячка, цена за кг",
                                "count": 95.84,
                                "amount": 958.4
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 398.88,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "17675619",
                        "amount": {
                            "amount": 5698.35,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "dc3ecd28-90e2-48bb-a7e2-f8b3c4137b31",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "bb5d3d92bdd46e28b2c7",
                        "chequeItems": [
                            {
                                "description": "Желудки куриные ИП Салиджанов, цена за кг",
                                "count": 43.289,
                                "amount": 432.89
                            },
                            {
                                "description": "Камбала жареная, цена за кг",
                                "count": 10.597,
                                "amount": 105.97
                            },
                            {
                                "description": "Салат Мистер Хе Шанхай 150г, цена за шт",
                                "count": 38.667,
                                "amount": 386.67
                            },
                            {
                                "description": "Морковча ИП Салиджанов, цена за кг",
                                "count": 74.395,
                                "amount": 743.95
                            },
                            {
                                "description": "Хе из баклажан Мистер Хе 150г пл/уп",
                                "count": 8.395,
                                "amount": 83.95
                            },
                            {
                                "description": "ФУД Пасхальные яйца, цена за штуку",
                                "count": 96.163,
                                "amount": 961.63
                            },
                            {
                                "description": "Квише с ветчиной, цена за кг",
                                "count": 15.687,
                                "amount": 156.87
                            },
                            {
                                "description": "РЕСТОРАТОР Колбаски гриль, цена за кг",
                                "count": 8.864,
                                "amount": 88.64
                            },
                            {
                                "description": "Баклажаны ИП Салиджанов, цена за кг",
                                "count": 97.425,
                                "amount": 974.25
                            },
                            {
                                "description": "Жар-Пицца Пирог с свининой/луком 100г",
                                "count": 13.132,
                                "amount": 131.32
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 284.63,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "71364178",
                        "amount": {
                            "amount": 4066.14,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "ea82ed64-ab0d-4567-95c3-9f42f18b0de2",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "a632109e9f02ac65f153",
                        "chequeItems": [
                            {
                                "description": "Ролл Нежный (семга охл, угорь копч.,кунжут, авокадо, васаби), кг",
                                "count": 89.372,
                                "amount": 893.72
                            },
                            {
                                "description": "Оливки Грека Халкидики с/к в маринаде, цена за кг (банка 3кг,сух/вес 1,8 кг.)",
                                "count": 54.348,
                                "amount": 543.48
                            },
                            {
                                "description": "Морковча ИП Салиджанов, цена за кг",
                                "count": 21.656,
                                "amount": 216.56
                            },
                            {
                                "description": "Желе из яблок 180г",
                                "count": 60.606,
                                "amount": 606.06
                            },
                            {
                                "description": "Рыба ФудФиш, цена за кг (Пр-во) 1",
                                "count": 27.385,
                                "amount": 273.85
                            },
                            {
                                "description": "Свинина запеченная с овощами, цена за кг",
                                "count": 87.82,
                                "amount": 878.2
                            },
                            {
                                "description": "Тарталетки с грибным жульеном, цена за кг",
                                "count": 20.048,
                                "amount": 200.48
                            },
                            {
                                "description": "Морская капуста с авокадо и сладким перцем Мистер Хе, цена за кг",
                                "count": 68.784,
                                "amount": 687.84
                            },
                            {
                                "description": "Рождественская утка, цена за кг",
                                "count": 18.101,
                                "amount": 181.01
                            },
                            {
                                "description": "Рулетик куриный с грибами гриль, цена за кг",
                                "count": 46.292,
                                "amount": 462.92
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 346.07,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "97718704",
                        "amount": {
                            "amount": 4944.12,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "b53d4d8b-c74c-4d78-98d6-671495708339",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "33c30b43914b585b3c32",
                        "chequeItems": [
                            {
                                "description": "Жар-Пицца Пирог с карт/мясом 100г",
                                "count": 65.086,
                                "amount": 650.86
                            },
                            {
                                "description": "Салат Молодость с проростками овса и рукколой, 0,2кг, цена за шт.",
                                "count": 88.986,
                                "amount": 889.86
                            },
                            {
                                "description": "Сыр Копченый Охотничий Золотые горы 200г в/у",
                                "count": 56.569,
                                "amount": 565.69
                            },
                            {
                                "description": "Грибы белые ИП Салиджанов 150г пл/уп",
                                "count": 51.759,
                                "amount": 517.59
                            },
                            {
                                "description": "Салат Бонжур 210г, цена за шт.",
                                "count": 26.108,
                                "amount": 261.08
                            },
                            {
                                "description": "Суши Текка маки (рол с лососем) 95г, цена за порцию",
                                "count": 23.268,
                                "amount": 232.68
                            },
                            {
                                "description": "Манты, цена за кг",
                                "count": 88.827,
                                "amount": 888.27
                            },
                            {
                                "description": "Зеленый Коктейль Дачный 0,3 л , цена за шт.",
                                "count": 39.6,
                                "amount": 396.0
                            },
                            {
                                "description": "Свиные ушки Мистер Хе 150г пл/уп",
                                "count": 94.725,
                                "amount": 947.25
                            },
                            {
                                "description": "Суши Ассорти ИП Салиджанов 200г пл/уп",
                                "count": 32.945,
                                "amount": 329.45
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 397.52,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "61257312",
                        "amount": {
                            "amount": 5678.73,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "1d4c591a-0bdc-4d8f-b05a-7a94d2d86c19",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "fa09ef6031898e5675fa",
                        "chequeItems": [
                            {
                                "description": "Бифштекс рубленый, цена за кг",
                                "count": 71.306,
                                "amount": 713.06
                            },
                            {
                                "description": "Опята ИП Салиджанов 150г пл/уп",
                                "count": 94.3,
                                "amount": 943.0
                            },
                            {
                                "description": "Форель слабосоленая стейк, цена за кг",
                                "count": 20.125,
                                "amount": 201.25
                            },
                            {
                                "description": "Котлета рыбная, цена за кг",
                                "count": 2.313,
                                "amount": 23.13
                            },
                            {
                                "description": "ЛАМА Куриные желудки за 1кг",
                                "count": 0.594,
                                "amount": 5.94
                            },
                            {
                                "description": "Баклажаны с мясом Мистер Хе, цена за кг",
                                "count": 91.533,
                                "amount": 915.33
                            },
                            {
                                "description": "ЛАМА Салат Цезарь, цена за кг",
                                "count": 23.757,
                                "amount": 237.57
                            },
                            {
                                "description": "Капуста тушеная, цена за кг",
                                "count": 54.583,
                                "amount": 545.83
                            },
                            {
                                "description": "Баклажаны ИП Салиджанов, цена за кг",
                                "count": 55.736,
                                "amount": 557.36
                            },
                            {
                                "description": "Суши с икрой ИП Салиджанов 200г пл/уп",
                                "count": 86.342,
                                "amount": 863.42
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 350.42,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "80726218",
                        "amount": {
                            "amount": 5005.89,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "16764fab-d867-41aa-84f7-c8d9c773e2d2",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "bc477af5f39c496a74d1",
                        "chequeItems": [
                            {
                                "description": "Жар-Пицца Пирог с свининой/луком 100г",
                                "count": 93.669,
                                "amount": 936.69
                            },
                            {
                                "description": "Салат из свежей капусты Мистер Хе, цена за кг",
                                "count": 66.356,
                                "amount": 663.56
                            },
                            {
                                "description": "Смак  18",
                                "count": 97.444,
                                "amount": 974.44
                            },
                            {
                                "description": "Имбирь маринованый Мистер Хе 50г",
                                "count": 15.156,
                                "amount": 151.56
                            },
                            {
                                "description": "Перец фаршированный овощами и рисом, цена за кг",
                                "count": 20.142,
                                "amount": 201.42
                            },
                            {
                                "description": "Салат 5 Авеню, цена за кг",
                                "count": 47.163,
                                "amount": 471.63
                            },
                            {
                                "description": "Бутерброд с овощами, цена за шт.",
                                "count": 95.246,
                                "amount": 952.46
                            },
                            {
                                "description": "Рагу из баклажан, цена за кг",
                                "count": 85.653,
                                "amount": 856.53
                            },
                            {
                                "description": "Салат из спаржи Мистер Хе 150г пл/уп",
                                "count": 41.482,
                                "amount": 414.82
                            },
                            {
                                "description": "Салат Гнездо глухаря 150г, цена за шт",
                                "count": 58.641,
                                "amount": 586.41
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 434.67,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "27740137",
                        "amount": {
                            "amount": 6209.52,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "048d4598-0bca-4aa2-acd1-7ef9a95c03c8",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "bd2bcf13898430cc81d6",
                        "chequeItems": [
                            {
                                "description": "Каша пшеничная Сила и бодрость, 0,2кг, цена за шт.",
                                "count": 51.097,
                                "amount": 510.97
                            },
                            {
                                "description": "РЕСТОРАТОР Салат Цезарь, цена за кг",
                                "count": 88.987,
                                "amount": 889.87
                            },
                            {
                                "description": "Салат Мистер Хе Себастьян, цена за кг",
                                "count": 68.945,
                                "amount": 689.45
                            },
                            {
                                "description": "Салат Гурман, цена за кг",
                                "count": 94.996,
                                "amount": 949.96
                            },
                            {
                                "description": "ЛАМА Цыплята табака, цена за кг",
                                "count": 33.829,
                                "amount": 338.29
                            },
                            {
                                "description": "Биточки Муромские, цена за кг",
                                "count": 72.26,
                                "amount": 722.6
                            },
                            {
                                "description": "ФУД Заливное из курицы 180г",
                                "count": 4.86,
                                "amount": 48.6
                            },
                            {
                                "description": "Салат из капусты Мистер Хе 150г пл/уп",
                                "count": 47.398,
                                "amount": 473.98
                            },
                            {
                                "description": "Инжир ГРЕКО с сыром фета, цена за кг (пл/банка 3,1кг, сух/вес 1,8кг)",
                                "count": 4.38,
                                "amount": 43.8
                            },
                            {
                                "description": "Салат Успенский, цена за кг",
                                "count": 36.185,
                                "amount": 361.85
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 352.06,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "58080985",
                        "amount": {
                            "amount": 5029.37,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "f78ab96e-754a-417b-a314-0cc73fbecadc",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "50ef9e924bafa4db9d91",
                        "chequeItems": [
                            {
                                "description": "Свинина деликатесная, цена за кг",
                                "count": 72.357,
                                "amount": 723.57
                            },
                            {
                                "description": "Заправка для супов, цена за кг",
                                "count": 42.708,
                                "amount": 427.08
                            },
                            {
                                "description": "Суши с угрем Мистер Хе, цена за кг",
                                "count": 97.113,
                                "amount": 971.13
                            },
                            {
                                "description": "Салат Русский размер, цена за кг",
                                "count": 65.86,
                                "amount": 658.6
                            },
                            {
                                "description": "Салат Ярославна, цена за кг",
                                "count": 65.506,
                                "amount": 655.06
                            },
                            {
                                "description": "Салат Капель, цена за кг",
                                "count": 50.448,
                                "amount": 504.48
                            },
                            {
                                "description": "Домашний ресторан Салат Каприз 150г, цена за шт",
                                "count": 40.0,
                                "amount": 400.0
                            },
                            {
                                "description": "ЛАМА Набор для окрошки, цена за кг",
                                "count": 6.986,
                                "amount": 69.86
                            },
                            {
                                "description": "Жар-Пицца Пирог с яйцом/луком 100г",
                                "count": 83.153,
                                "amount": 831.53
                            },
                            {
                                "description": "ФУД Заливное из говядины 180г, цена за шт",
                                "count": 62.514,
                                "amount": 625.14
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 410.65,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "63182707",
                        "amount": {
                            "amount": 5866.45,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "0ff643af-f8c9-4e19-b017-76ecabda2b06",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "dfad26ac9797240e218a",
                        "chequeItems": [
                            {
                                "description": "Салат Постный, цена за кг",
                                "count": 0.76,
                                "amount": 7.6
                            },
                            {
                                "description": "Зеленый Коктейль Для мужчин 0,3 л , цена за шт.",
                                "count": 37.122,
                                "amount": 371.22
                            },
                            {
                                "description": "Спаржа ИП Салиджанов 150г пл/уп",
                                "count": 42.159,
                                "amount": 421.59
                            },
                            {
                                "description": "ЛАМА Свекла отварная, цена за кг",
                                "count": 25.821,
                                "amount": 258.21
                            },
                            {
                                "description": "Морковча с мясом ИП Салиджанов, цена за кг",
                                "count": 25.139,
                                "amount": 251.39
                            },
                            {
                                "description": "Жар-Пицца Курник 130г",
                                "count": 0.852,
                                "amount": 8.52
                            },
                            {
                                "description": "Оливки ГРЕКО Халкидики с сыром фета, цена за кг (пл/банка 3,1кг, сух/вес 1,8кг)",
                                "count": 85.646,
                                "amount": 856.46
                            },
                            {
                                "description": "Имбирь маринованый Мистер Хе 50г",
                                "count": 58.406,
                                "amount": 584.06
                            },
                            {
                                "description": "Салат с говядиной, цена за кг",
                                "count": 49.018,
                                "amount": 490.18
                            },
                            {
                                "description": "ФУД Крылья в фирм.соусе Подворье, цена за кг",
                                "count": 3.439,
                                "amount": 34.39
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 229.85,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "42047494",
                        "amount": {
                            "amount": 3283.62,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "12763529-5dae-4c15-98c9-b2cb582fae78",
                    "dateTime": "2017-06-25T07:25:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "447791947f1c9af02ef3",
                        "chequeItems": [
                            {
                                "description": "Цветная капуста в сухарях, цена за кг",
                                "count": 53.575,
                                "amount": 535.75
                            },
                            {
                                "description": "Форель слабосоленая стейк, цена за кг",
                                "count": 34.06,
                                "amount": 340.6
                            },
                            {
                                "description": "Жар-Пицца Курник 130г",
                                "count": 63.825,
                                "amount": 638.25
                            },
                            {
                                "description": "Сельдь по-русски, цена за кг",
                                "count": 12.397,
                                "amount": 123.97
                            },
                            {
                                "description": "Салат Крабовый 150г, цена за шт",
                                "count": 52.947,
                                "amount": 529.47
                            },
                            {
                                "description": "Торт печеночный Томичка, цена за кг",
                                "count": 81.873,
                                "amount": 818.73
                            },
                            {
                                "description": "Папоротник ИП Салиджанов 150г пл/уп",
                                "count": 96.954,
                                "amount": 969.54
                            },
                            {
                                "description": "Салат 5 Авеню, цена за кг",
                                "count": 23.129,
                                "amount": 231.29
                            },
                            {
                                "description": "Домашний ресторан Салат Столичный 150г, цена за шт",
                                "count": 46.69,
                                "amount": 466.9
                            },
                            {
                                "description": "Запеканка творожная с вишней, цена за кг",
                                "count": 88.175,
                                "amount": 881.75
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 387.53,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "16056189",
                        "amount": {
                            "amount": 5536.25,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    historyManySecond: {
        "data": {
            "allCount": 250,
            "rows": [
                {
                    "id": "12755cba-3d49-47bd-a7a2-204fc2af240a",
                    "dateTime": "2017-11-17T04:38:04Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "34ea359853bb234029cc",
                        "chequeItems": [
                            {
                                "description": "Ролл Сяке-маки (семга с\\с), кг",
                                "count": 10.856,
                                "amount": 108.56
                            },
                            {
                                "description": "Морковь отварная, цена за кг",
                                "count": 99.918,
                                "amount": 999.18
                            },
                            {
                                "description": "Салат Мимоза 150г, цена за шт",
                                "count": 32.357,
                                "amount": 323.57
                            },
                            {
                                "description": "Филе куриное Нежность Мистер Хе, цена за кг",
                                "count": 99.876,
                                "amount": 998.76
                            },
                            {
                                "description": "Салат Римские каникулы, цена за кг",
                                "count": 50.138,
                                "amount": 501.38
                            },
                            {
                                "description": "Салат Гурман, цена за кг",
                                "count": 84.576,
                                "amount": 845.76
                            },
                            {
                                "description": "Сок апельсиново-грейфрутовый фреш 0,5 л , цена за бут.",
                                "count": 57.99,
                                "amount": 579.9
                            },
                            {
                                "description": "Морковча с мясом Мистер Хе, цена за кг",
                                "count": 50.495,
                                "amount": 504.95
                            },
                            {
                                "description": "Морковь отварная, цена за кг",
                                "count": 28.193,
                                "amount": 281.93
                            },
                            {
                                "description": "Свинина по итальянски, цена за кг",
                                "count": 50.138,
                                "amount": 501.38
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 395.18,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "30999578",
                        "amount": {
                            "amount": 5645.37,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "97024c17-dc59-49c1-97a0-7322b5a077d6",
                    "dateTime": "2017-11-17T04:38:03Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "122fa74941b639086588",
                        "chequeItems": [
                            {
                                "description": "ЛАМА Рис с овощами цена за 1кг",
                                "count": 26.785,
                                "amount": 267.85
                            },
                            {
                                "description": "Хе из языка ИП Салиджанов 150г пл/уп",
                                "count": 45.338,
                                "amount": 453.38
                            },
                            {
                                "description": "Суши Унаги маки (рол с копченым угрем) 95г, цена за порцию",
                                "count": 13.629,
                                "amount": 136.29
                            },
                            {
                                "description": "Салат Ташкент, цена за кг",
                                "count": 54.706,
                                "amount": 547.06
                            },
                            {
                                "description": "Пигоди 2шт*150г Мистер Хе, цена за упаковку",
                                "count": 25.71,
                                "amount": 257.1
                            },
                            {
                                "description": "Салат Шведский, цена за кг",
                                "count": 53.686,
                                "amount": 536.86
                            },
                            {
                                "description": "Грибы Рыжики соленые, цена за кг",
                                "count": 42.169,
                                "amount": 421.69
                            },
                            {
                                "description": "ФУД Хе из курицы, цена за кг",
                                "count": 13.859,
                                "amount": 138.59
                            },
                            {
                                "description": "Ассорти из оливок ГРЕКО (халкидики,альфонсо,блондин), цена за кг  (банка 3,2кг,сух/вес 1,8кг)",
                                "count": 96.486,
                                "amount": 964.86
                            },
                            {
                                "description": "Язык с грибами под сыром, цена за кг",
                                "count": 34.93,
                                "amount": 349.3
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 285.11,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "54467580",
                        "amount": {
                            "amount": 4072.98,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "c1c35c86-8b4b-4cf3-a3a7-74daba91da95",
                    "dateTime": "2017-11-17T04:38:02Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "e5a8d36297d9159376ab",
                        "chequeItems": [
                            {
                                "description": "Жар-Пицца Пирог с рисом/мясом 100г",
                                "count": 96.893,
                                "amount": 968.93
                            },
                            {
                                "description": "Салат Атлантида, цена за кг",
                                "count": 26.05,
                                "amount": 260.5
                            },
                            {
                                "description": "Икра овощная, цена за кг",
                                "count": 61.839,
                                "amount": 618.39
                            },
                            {
                                "description": "Ролл с огурцом и перцем (огурец, перец болг., васаби), кг",
                                "count": 23.793,
                                "amount": 237.93
                            },
                            {
                                "description": "Жюльен с курицей и овощами 300г, цена за шт",
                                "count": 19.244,
                                "amount": 192.44
                            },
                            {
                                "description": "ЛАМА Картофель в укропе, цена за кг",
                                "count": 65.85,
                                "amount": 658.5
                            },
                            {
                                "description": "Соевый соус Мистер Хе 30г, цена за шт",
                                "count": 90.575,
                                "amount": 905.75
                            },
                            {
                                "description": "Холодец по-домашнему, цена за кг",
                                "count": 1.869,
                                "amount": 18.69
                            },
                            {
                                "description": "Баклажаны с мясом Мистер Хе, цена за кг",
                                "count": 11.828,
                                "amount": 118.28
                            },
                            {
                                "description": "Слойка с мясом и рисом 80г (франц.выпечка)",
                                "count": 60.356,
                                "amount": 603.56
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 320.83,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "97936476",
                        "amount": {
                            "amount": 4582.97,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "be8e3ae0-e773-4083-bb33-152b0617bcf5",
                    "dateTime": "2017-11-17T04:38:02Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "56b82524cd8413998702",
                        "chequeItems": [
                            {
                                "description": "ЕДОК Салат с печенью 150г",
                                "count": 22.691,
                                "amount": 226.91
                            },
                            {
                                "description": "Хе из языка ИП Салиджанов 150г пл/уп",
                                "count": 82.756,
                                "amount": 827.56
                            },
                            {
                                "description": "Картофельное пюре, цена за кг",
                                "count": 88.429,
                                "amount": 884.29
                            },
                            {
                                "description": "Баклажаны Берендей, цена за кг",
                                "count": 72.488,
                                "amount": 724.88
                            },
                            {
                                "description": "Ролл каппа маки (огурец, васаби), кг",
                                "count": 48.134,
                                "amount": 481.34
                            },
                            {
                                "description": "Салат Сухоревский, цена за кг",
                                "count": 16.153,
                                "amount": 161.53
                            },
                            {
                                "description": "Заливное из курицы 180г",
                                "count": 56.526,
                                "amount": 565.26
                            },
                            {
                                "description": "Суши Набор 7 (Якудза,Хаси,Шахматы) 560г Империя суши",
                                "count": 32.118,
                                "amount": 321.18
                            },
                            {
                                "description": "ФУД Заливное из говядины 180г, цена за штуку",
                                "count": 28.098,
                                "amount": 280.98
                            },
                            {
                                "description": "Баклажаны фаршированные, цена за кг",
                                "count": 35.32,
                                "amount": 353.2
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 337.89,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "13946841",
                        "amount": {
                            "amount": 4827.13,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "7a070e9f-60ad-434c-a8be-f95bb4941d53",
                    "dateTime": "2017-11-17T04:38:01Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "fb0999763a89f881b0b5",
                        "chequeItems": [
                            {
                                "description": "Патэ Карлиони, цена за кг",
                                "count": 22.065,
                                "amount": 220.65
                            },
                            {
                                "description": "Грибы соленые, цена за кг",
                                "count": 32.68,
                                "amount": 326.8
                            },
                            {
                                "description": "Салат Молодость с проростками овса и рукколой, 0,2кг, цена за шт.",
                                "count": 1.875,
                                "amount": 18.75
                            },
                            {
                                "description": "Пирожки жареные с курицей и грибами 75г, цена за штуку",
                                "count": 26.238,
                                "amount": 262.38
                            },
                            {
                                "description": "Бутерброд ФУРШЕТ с языком (огурец, лук, салат, майонез) 150г/24",
                                "count": 50.403,
                                "amount": 504.03
                            },
                            {
                                "description": "Говядина с черносливом, цена за кг",
                                "count": 55.83,
                                "amount": 558.3
                            },
                            {
                                "description": "Ролл Дальневосточный (угорь копч., кунжут, огурец), кг",
                                "count": 83.641,
                                "amount": 836.41
                            },
                            {
                                "description": "Перец фаршированный мясом, цена за кг",
                                "count": 66.97,
                                "amount": 669.7
                            },
                            {
                                "description": "Хе из курицы Мистер Хе, цена за кг",
                                "count": 60.541,
                                "amount": 605.41
                            },
                            {
                                "description": "Баклажаны ИП Салиджанов, цена за кг",
                                "count": 7.078,
                                "amount": 70.78
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 285.13,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "35663779",
                        "amount": {
                            "amount": 4073.21,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "e06ac75b-945f-4600-ba9a-193e73cb5f43",
                    "dateTime": "2017-11-17T04:38:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "4475e90dccef215f6f6b",
                        "chequeItems": [
                            {
                                "description": "Салат с говядиной, цена за кг",
                                "count": 24.633,
                                "amount": 246.33
                            },
                            {
                                "description": "ФУД Крылья в фирм.соусе Подворье, цена за кг",
                                "count": 7.879,
                                "amount": 78.79
                            },
                            {
                                "description": "Цветная капуста в сухарях, цена за кг",
                                "count": 70.275,
                                "amount": 702.75
                            },
                            {
                                "description": "Форель слабосоленая стейк, цена за кг",
                                "count": 90.13,
                                "amount": 901.3
                            },
                            {
                                "description": "Жар-Пицца Курник 130г",
                                "count": 42.251,
                                "amount": 422.51
                            },
                            {
                                "description": "Сельдь по-русски, цена за кг",
                                "count": 64.772,
                                "amount": 647.72
                            },
                            {
                                "description": "Салат Крабовый 150г, цена за шт",
                                "count": 69.511,
                                "amount": 695.11
                            },
                            {
                                "description": "Торт печеночный Томичка, цена за кг",
                                "count": 96.259,
                                "amount": 962.59
                            },
                            {
                                "description": "Папоротник ИП Салиджанов 150г пл/уп",
                                "count": 11.303,
                                "amount": 113.03
                            },
                            {
                                "description": "Салат 5 Авеню, цена за кг",
                                "count": 54.966,
                                "amount": 549.66
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 372.39,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "55010303",
                        "amount": {
                            "amount": 5319.79,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "33b30446-a825-443c-ae16-fbf07ea47787",
                    "dateTime": "2017-11-17T04:38:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "552ecf6540522a46c3c0",
                        "chequeItems": [
                            {
                                "description": "Домашний ресторан Салат Столичный 150г, цена за шт",
                                "count": 95.28,
                                "amount": 952.8
                            },
                            {
                                "description": "Запеканка творожная с вишней, цена за кг",
                                "count": 17.53,
                                "amount": 175.3
                            },
                            {
                                "description": "Домашний ресторан Салат Африка, цена за кг",
                                "count": 80.07,
                                "amount": 800.7
                            },
                            {
                                "description": "Зеленый Коктейль Сильный иммунитет 0,3л , цена за шт.",
                                "count": 84.046,
                                "amount": 840.46
                            },
                            {
                                "description": "Окорочка жареные, цена за кг",
                                "count": 84.348,
                                "amount": 843.48
                            },
                            {
                                "description": "Жар-Пицца Пирог с карт/печенью 100г",
                                "count": 77.323,
                                "amount": 773.23
                            },
                            {
                                "description": "Морская капуста с авокадо и сладким перцем Мистер Хе, цена за кг",
                                "count": 77.607,
                                "amount": 776.07
                            },
                            {
                                "description": "Шашлык из курицы, цена за кг",
                                "count": 12.5,
                                "amount": 125.0
                            },
                            {
                                "description": "Салат Северный остров, цена за кг",
                                "count": 22.283,
                                "amount": 222.83
                            },
                            {
                                "description": "Домашний ресторан Салат Юбилейный 150г, цена за шт",
                                "count": 33.923,
                                "amount": 339.23
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 409.44,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "65466302",
                        "amount": {
                            "amount": 5849.1,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "39a6a5de-d896-43e0-a7c4-54d6a2f68461",
                    "dateTime": "2017-11-17T04:37:59Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "2aaa61012c14e0ec8b00",
                        "chequeItems": [
                            {
                                "description": "Жар-Пицца Пирог с яйцом/луком 100г",
                                "count": 59.505,
                                "amount": 595.05
                            },
                            {
                                "description": "ФУД Заливное из говядины 180г, цена за шт",
                                "count": 27.878,
                                "amount": 278.78
                            },
                            {
                                "description": "Салат Постный, цена за кг",
                                "count": 99.208,
                                "amount": 992.08
                            },
                            {
                                "description": "Зеленый Коктейль Для мужчин 0,3 л , цена за шт.",
                                "count": 19.676,
                                "amount": 196.76
                            },
                            {
                                "description": "Спаржа ИП Салиджанов 150г пл/уп",
                                "count": 86.276,
                                "amount": 862.76
                            },
                            {
                                "description": "ЛАМА Свекла отварная, цена за кг",
                                "count": 62.641,
                                "amount": 626.41
                            },
                            {
                                "description": "Морковча с мясом ИП Салиджанов, цена за кг",
                                "count": 23.187,
                                "amount": 231.87
                            },
                            {
                                "description": "Жар-Пицца Курник 130г",
                                "count": 1.9,
                                "amount": 19.0
                            },
                            {
                                "description": "Оливки ГРЕКО Халкидики с сыром фета, цена за кг (пл/банка 3,1кг, сух/вес 1,8кг)",
                                "count": 46.732,
                                "amount": 467.32
                            },
                            {
                                "description": "Имбирь маринованый Мистер Хе 50г",
                                "count": 89.838,
                                "amount": 898.38
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 361.78,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "03926837",
                        "amount": {
                            "amount": 5168.41,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "d6849ca7-6abb-4643-83d6-548dc7646e15",
                    "dateTime": "2017-11-17T04:37:58Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "e601642eb0581d468cb6",
                        "chequeItems": [
                            {
                                "description": "Инжир ГРЕКО с сыром фета, цена за кг (пл/банка 3,1кг, сух/вес 1,8кг)",
                                "count": 99.036,
                                "amount": 990.36
                            },
                            {
                                "description": "Салат Успенский, цена за кг",
                                "count": 87.008,
                                "amount": 870.08
                            },
                            {
                                "description": "Свинина деликатесная, цена за кг",
                                "count": 66.987,
                                "amount": 669.87
                            },
                            {
                                "description": "Заправка для супов, цена за кг",
                                "count": 35.504,
                                "amount": 355.04
                            },
                            {
                                "description": "Суши с угрем Мистер Хе, цена за кг",
                                "count": 34.118,
                                "amount": 341.18
                            },
                            {
                                "description": "Салат Русский размер, цена за кг",
                                "count": 81.921,
                                "amount": 819.21
                            },
                            {
                                "description": "Салат Ярославна, цена за кг",
                                "count": 19.556,
                                "amount": 195.56
                            },
                            {
                                "description": "Салат Капель, цена за кг",
                                "count": 47.571,
                                "amount": 475.71
                            },
                            {
                                "description": "Домашний ресторан Салат Каприз 150г, цена за шт",
                                "count": 35.571,
                                "amount": 355.71
                            },
                            {
                                "description": "ЛАМА Набор для окрошки, цена за кг",
                                "count": 87.916,
                                "amount": 879.16
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 416.63,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "55208010",
                        "amount": {
                            "amount": 5951.88,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "d3231367-d39d-4918-8491-579cf385c88b",
                    "dateTime": "2017-11-17T04:37:57Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 198, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "a6b258335e0512ceccd6",
                        "chequeItems": [
                            {
                                "description": "Баклажаны ИП Салиджанов, цена за кг",
                                "count": 69.904,
                                "amount": 699.04
                            },
                            {
                                "description": "Суши с икрой ИП Салиджанов 200г пл/уп",
                                "count": 47.034,
                                "amount": 470.34
                            },
                            {
                                "description": "Жар-Пицца Пирог с свининой/луком 100г",
                                "count": 9.362,
                                "amount": 93.62
                            },
                            {
                                "description": "Салат из свежей капусты Мистер Хе, цена за кг",
                                "count": 74.97,
                                "amount": 749.7
                            },
                            {
                                "description": "Смак  18",
                                "count": 41.093,
                                "amount": 410.93
                            },
                            {
                                "description": "Имбирь маринованый Мистер Хе 50г",
                                "count": 95.836,
                                "amount": 958.36
                            },
                            {
                                "description": "Перец фаршированный овощами и рисом, цена за кг",
                                "count": 34.316,
                                "amount": 343.16
                            },
                            {
                                "description": "Салат 5 Авеню, цена за кг",
                                "count": 64.825,
                                "amount": 648.25
                            },
                            {
                                "description": "Бутерброд с овощами, цена за шт.",
                                "count": 10.027,
                                "amount": 100.27
                            },
                            {
                                "description": "Рагу из баклажан, цена за кг",
                                "count": 76.775,
                                "amount": 767.75
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "3ada1e23-2045-42ff-bbfc-d9e9527d9e34",
                                "rewardType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": 366.9,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "10474081",
                        "amount": {
                            "amount": 5241.42,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    historyLittle: {
        "data": {
            "allCount": 1,
            "rows": [
                {
                    "id": "57fc7d4f-8af8-46be-bb10-ba2e3036643c",
                    "dateTime": "2017-07-25T10:31:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070557401011494",
                    "description": "МАГАЗИН 98, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 11",
                        "latitude": 3.0,
                        "longitude": 3.0,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "11",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "8abb2d3a-634e-4c95-bee3-a2c9476f7f43",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "78d1f845331145422fc2ac52b4519aab",
                        "chequeItems": [
                            {
                                "description": "НАТУР Влаж.туал.бумага в виде салф.72шт.с экстр.ромашки",
                                "count": 1.0,
                                "amount": 100.0
                            }
                        ],
                        "withdraws": [
                            {
                                "withdrawType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": -5.0,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "0904",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    historyEmpty: {
        "data": {
            "allCount": 0,
            "rows": []
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    }
};
