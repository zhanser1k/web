const helpers = require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();
    const updateOptions = `
        return angular.element(document.body).injector().get('$rootScope').$apply(function() {
            angular.element(document.body).injector().get('$rootScope').newOptions = {
                "redirectUrlOnLogin": "#news",
            };
        });
    `;

    return {
        'Should successfully reset password': () => {
            browser.executeScript(updateOptions);
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.inputConfirmCode.sendKeys('111111');
            po.inputNewPassword.sendKeys('111111');
            po.inputRepeatedPassword.sendKeys('111111');
            po.buttonConfirm.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/news`);
        },
        'Should display error message if phone number or e-mail is incorrect': () => {
            po.inputPhoneOrEmail.sendKeys('999999999999999999999999999');
            po.buttonStart.click();
            expect(po.alert).toBePresent();
        },
        'Should display error message if phone number or e-mail input field is empty': () => {
            po.buttonStart.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display error message if confirmation code, password and new password are missing': () => {
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.inputConfirmCode.sendKeys('614981749814981498');
            po.inputNewPassword.sendKeys('111111');
            po.inputRepeatedPassword.sendKeys('111111');
            po.buttonConfirm.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display error message if new password has less than 6 characters': () => {
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.inputConfirmCode.sendKeys('111111');
            po.inputNewPassword.sendKeys('1');
            po.inputRepeatedPassword.sendKeys('1');
            po.buttonConfirm.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display error message if new password has more than 25 characters': () => {
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.inputConfirmCode.sendKeys('111111');
            po.inputNewPassword.sendKeys(Array(26).fill(1).join(""));
            po.inputRepeatedPassword.sendKeys(Array(26).fill(1).join(""));
            po.buttonConfirm.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display error message if new password and repeated password are different': () => {
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.inputConfirmCode.sendKeys('111111');
            po.inputNewPassword.sendKeys(Array(6).fill(1).join(""));
            po.inputRepeatedPassword.sendKeys(Array(7).fill(1).join(""));
            po.buttonConfirm.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should cancel password reset correctly': () => {
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/login`);
        },
    }
};
