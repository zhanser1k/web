const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const mocks = require('../mocks');
const pageObject = require('../pageObject/bin');
let tests = require('../tests')('bin');

describe('Bin resetPassword', function() {
    const updateOptions = `
        return angular.element(document.body).injector().get('$rootScope').$apply(function() {
            angular.element(document.body).injector().get('$rootScope').newOptions = {
                "redirectUrlOnLogin": "#news",
            };
        });
    `;
    const po = new pageObject();
    let descriptions;

    tests = {
        ...tests,
        'Should cancel password reset correctly': () => {
            po.inputPhoneOrEmail.sendKeys('9999999999');
            po.buttonStart.click();
            po.buttonCancel.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/`);
        },
        // TODO #16067 корректно работает только в Bin
        // TODO 2 перестало работать в Bin, возможно надо обновить на 1.5
        /*'Should successfully reset password from e-mail message': () => {
            browser.executeScript(updateOptions);
            po.inputPhoneOrEmail.sendKeys('test@loymax.ru');
            po.buttonStart.click();
            browser.setLocation('confirmResetPassword?email=test%40loymax.ru&code=446311');
            browser.sleep(90000);
            po.inputNewPassword.sendKeys('111111');
            po.inputRepeatedPassword.sendKeys('111111');
            po.buttonConfirm.click();
            expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}/#/news`);
        },*/
    };

    beforeEach(() => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions) {
            descriptions = this.children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks(descriptions[0]).forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        $httpBackend.passThrough();

        browser.get('/#/resetPassword');
    });

    afterEach(() => {
        browser.executeScript(`return localStorage.removeItem('ls.authorizationToken');`);
        ngMockE2E.clearMockModules();
    });

    for (const name in tests) {
        if (tests.hasOwnProperty(name)) {
            it(name, tests[name]);
        }
    }
});
