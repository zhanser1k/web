const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/Announcements.*$/gi,
        respond: httpResponses.announcements,
    }];

    const answerStartResetPassword = {
        'Should display error message if phone number or e-mail is incorrect': httpResponses.resetPasswordStartError
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/ResetPassword\/Start$/,
        respond: answerStartResetPassword[description] || httpResponses.resetPasswordStartSuccess
    });

    const answerConfirmResetPassword = {
        'Should display error message if confirmation code, password and new password are missing': httpResponses.resetPasswordConfirmErrorIncorrect,
        'Should display error message if new password has less than 6 characters': httpResponses.resetPasswordConfirmErrorLength,
        'Should display error message if new password has more than 25 characters': httpResponses.resetPasswordConfirmErrorLength,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/ResetPassword\/Confirm/,
        respond: answerConfirmResetPassword[description] || httpResponses.resetPasswordConfirmSuccess
    });

    return mocks;
};
