const pageObject = function() {
    return {
        inputPhoneOrEmail: element(by.model('model.identity')),
        inputConfirmCode: element(by.model('model.confirmCode')),
        inputNewPassword: element(by.model('model.newPassword')),
        inputRepeatedPassword: element(by.model('model.repeatPassword')),
        confirmCodeRepeated: element(by.id('lmx-confirm-code-repeated')),
        buttonStart: element(by.id('lmx-reset-password-submit')),
        buttonCancel: element(by.id('lmx-confirm-code-cancel')),
        buttonConfirm: element(by.id('lmx-confirm-code-submit')),
        alert: element(by.css('.lmx-alert-warning')),
        helpBlockError: $$('.lmx-help-block__error'),
    }
};

module.exports = pageObject;
