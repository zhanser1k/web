const defaultPageObject = require('./default')();

const pageObject = function() {
    return {
        ...defaultPageObject,
        confirmCodeRepeated: element(by.id('confirm-code-repeated')),
        buttonStart: element(by.css('.forgot-form button[type="submit"]')),
        buttonCancel: element(by.css('.forgot-form button[type="button"]')),
        buttonConfirm: element(by.css('.forgot-form button[type="submit"]')),
        alert: element(by.css('.alert-warning')),
        helpBlockError: $$('.help-block__error'),
    }
};

module.exports = pageObject;
