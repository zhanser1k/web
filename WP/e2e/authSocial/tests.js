require('@loymax-solutions/protractor-helpers');

const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const mocks = require('./mocks');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        /* TODO поправить тесты после изменений на регистрацию через соцсети */
        /*'Should redirect to registration': () => {
            po.firstSocialItemLogo.element(by.css(".social-logo")).click();
            browser.get('http://127.0.0.1:8888/?providerType=VKontakte&action=auth&code=69daa54c28388a18b0');
            browser.sleep(1000);
            expect(po.registrationForm).toBePresent();
        },*/
        'Should authenticate successfully': () => {
            po.firstSocialItemLogo.element(by.css(".social-logo")).click();
            browser.get('http://127.0.0.1:8888/?providerType=VKontakte&action=auth&code=69daa54c28388a18b0');
            browser.sleep(1000);

            ngMockE2E.addMockModule();
            ngMockE2E.addAsDependencyForModule('lmxApp');

            mocks().forEach(({requestMethod, url, respond}) => {
                $httpBackend.when(requestMethod, url).respond(respond);
            });

            $httpBackend.passThrough();

            browser.get('/#/history');
            expect(po.history).toBePresent();
        }
    }
};
