const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const tests = require('../tests')('bonusme');
const mocks = require('../mocks');

describe('Gulliver auth', function() {
    let descriptions;

    beforeEach(() => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions) {
            descriptions = this.children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks().forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        $httpBackend.when("GET", /api\/User\/VKontakte\/Login/).respond(200, {
            accessToken: "e2e-tests", refreshToken: null, expiryDate: null
        });

        if (descriptions[0] === 'Should redirect to registration') {
            $httpBackend.when("POST", /api\/Registration\/TryFinishRegistration/).respond(200, {
                registrationCompleted: false, authToken: null, token_type: "bearer"
            });
        } else if (descriptions[0] === 'Should authenticate successfully') {
            $httpBackend.when("POST", /api\/Registration\/TryFinishRegistration/).respond(200, {
                registrationCompleted: true, authToken: "e2e-tests", token_type: "bearer", access_token: "e2e-tests", refresh_token: "b1c02a29b3294affaa9de0e6a2fab251"
            });
        }

        $httpBackend.passThrough();

        browser.get('/#/login');
    });

    afterEach(() => {
        ngMockE2E.clearMockModules();
    });

    for (const name in tests) {
        if (tests.hasOwnProperty(name)) {
            it(name, tests[name]);
        }
    }
});
