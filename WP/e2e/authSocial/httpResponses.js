module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    announcements: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    socialClients: {
        "data": [
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Facebook/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Facebook/Login",
                "providerType": "Facebook",
                "id": 1,
                "clientId": "1750162501880648",
                "clientSecret": null,
                "token": null,
                "name": "Facebook",
                "isActive": false,
                "protocol": "OAuth2",
                "scope": "public_profile",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "https://m.facebook.com/dialog/oauth",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "https://graph.facebook.com/oauth/access_token",
                    "method": "GET",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            },
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/VKontakte/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/VKontakte/Login",
                "providerType": "VKontakte",
                "id": 2,
                "clientId": "6354952",
                "clientSecret": null,
                "token": null,
                "name": "VKontakte",
                "isActive": false,
                "protocol": "OAuth2",
                "scope": "offline",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "https://oauth.vk.com/authorize",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "https://oauth.vk.com/access_token",
                    "method": "GET",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            },
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Odnoklassniki/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Odnoklassniki/Login",
                "providerType": "Odnoklassniki",
                "id": 3,
                "clientId": "1262202880",
                "clientSecret": null,
                "token": null,
                "name": "Odnoklassniki",
                "isActive": false,
                "protocol": "OAuth2",
                "scope": "VALUABLE_ACCESS",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "https://connect.ok.ru/oauth/authorize?layout=m",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "https://api.ok.ru/oauth/token.do",
                    "method": "POST",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            },
            {
                "socialSetUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Instagram/Set",
                "socialLoginUri": "http://public.branch-for-e2e-tests-for-lk-from-rc.iis.local/api/v1/User/Instagram/Login",
                "providerType": "Instagram",
                "id": 4,
                "clientId": "80--808",
                "clientSecret": null,
                "token": null,
                "name": "Instagram",
                "isActive": false,
                "protocol": "OAuth1",
                "scope": "09",
                "requestTokenUri": null,
                "authorizeUri": {
                    "target": "http://www.google/",
                    "method": "GET",
                    "parameters": null
                },
                "accessTokenUri": {
                    "target": "http://www.google.com/",
                    "method": "GET",
                    "parameters": null
                },
                "redirectUri": null,
                "apiSettings": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    history: {
        "data": {
            "allCount": 53,
            "rows": [
                {
                    "id": "eeae81df-6f18-45ce-bce2-449d1107cc21",
                    "dateTime": "2018-06-27T00:00:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 193, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "167WR11C8683416RYC399B921978FW32",
                        "chequeItems": [
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            },
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "6456456456",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "6ca9b724-60d4-44f5-aa6b-7801db3f76b4",
                    "dateTime": "2018-06-27T00:00:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 193, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "167WR71C8683416RYC399B921978FW32",
                        "chequeItems": [
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            },
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "6456456456",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "a74ef7d1-6762-4c00-9f65-ce75e2db04e4",
                    "dateTime": "2018-06-27T00:00:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 193, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "168WR13C8683416EYC399B921978FW32",
                        "chequeItems": [
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            },
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "6456456456",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "646a231a-8983-4d8e-bcfd-52865a06973c",
                    "dateTime": "2018-06-27T00:00:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 193, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "168WR17C8683416EYC399B921978FW32",
                        "chequeItems": [
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            },
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "6456456456",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "ea341547-cf3d-4e04-86b5-7c1d44f2abdb",
                    "dateTime": "2018-06-27T00:00:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 193, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "168WR27C8683416EYC399B921978FW32",
                        "chequeItems": [
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            },
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "6456456456",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "57b5305a-3e40-4963-b634-e5b0db2a569c",
                    "dateTime": "2018-06-27T00:00:00Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 193, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "168WR67C8683416EYC399B921978FW32",
                        "chequeItems": [
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            },
                            {
                                "description": "Товар",
                                "count": 10.0,
                                "amount": 50.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "6456456456",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "9a389166-1a71-4eb1-b805-96925b9a81ca",
                    "dateTime": "2018-06-26T00:31:14Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070557401011494",
                    "description": "МАГАЗИН 200, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "5205",
                        "chequeItems": [
                            {
                                "description": "Mars",
                                "count": 2.0,
                                "amount": 200.0
                            },
                            {
                                "description": "Пакет майка",
                                "count": 99.0,
                                "amount": 99.0
                            },
                            {
                                "description": "Luna",
                                "count": 2.0,
                                "amount": 200.0
                            }
                        ],
                        "withdraws": [
                            {
                                "withdrawType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": -50.0,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "123456",
                        "amount": {
                            "amount": 499.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "120c87d4-67ed-4297-9cb8-a08534e1b264",
                    "dateTime": "2018-06-26T00:31:14Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070557401011494",
                    "description": "МАГАЗИН 200, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "5212",
                        "chequeItems": [
                            {
                                "description": "Mars",
                                "count": 2.0,
                                "amount": 200.0
                            },
                            {
                                "description": "Пакет майка",
                                "count": 99.0,
                                "amount": 99.0
                            },
                            {
                                "description": "Luna",
                                "count": 2.0,
                                "amount": 200.0
                            }
                        ],
                        "withdraws": [
                            {
                                "withdrawType": "Bonus",
                                "description": null,
                                "amount": {
                                    "amount": -499.0,
                                    "currency": "BNS",
                                    "currencyInfo": {
                                        "id": 1,
                                        "name": "Бонусы",
                                        "externalId": "1f24174f-bfdb-4019-a3e7-4fb088b4a7a7",
                                        "description": "Общая внутрисистемная валюта",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "бонус",
                                            "genitive": "бонуса",
                                            "plural": "бонусов",
                                            "abbreviation": "бнс."
                                        }
                                    }
                                }
                            }
                        ],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "123456",
                        "amount": {
                            "amount": 499.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "31429f43-68e3-4d44-ab80-3c49572e168e",
                    "dateTime": "2018-06-20T00:36:29Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 59, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "15123112",
                        "chequeItems": [
                            {
                                "description": "Аи-92-5",
                                "count": 10.0,
                                "amount": 100.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [],
                        "isRefund": false,
                        "chequeNumber": "223322",
                        "amount": {
                            "amount": 100.0,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                },
                {
                    "id": "7a113547-ebed-4266-9acb-2ea3069061b8",
                    "dateTime": "2018-06-20T00:31:14Z",
                    "type": "Purchase",
                    "userId": 376105,
                    "identity": "9070129401809630",
                    "description": "МАГАЗИН 200, пр. Ленина, 10",
                    "location": {
                        "id": 996,
                        "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                        "description": "Томск, пр. Ленина, 10",
                        "latitude": 84.945384,
                        "longitude": 56.485249,
                        "region": null,
                        "city": {
                            "id": 1,
                            "regionId": null,
                            "name": "Томск",
                            "prefix": "г."
                        },
                        "street": "пр. Ленина",
                        "house": "10",
                        "building": null,
                        "office": null
                    },
                    "partnerId": "016e8025-7068-43e5-c689-8b01e638c43a",
                    "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                    "brand": {
                        "externalId": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    },
                    "data": {
                        "$type": "Loymax.History.UI.Model.HistoryPurchaseDataModel, Loymax.History.UI.Model",
                        "externalPurchaseId": "1326",
                        "chequeItems": [
                            {
                                "description": "Пирог Лоранский с ветчиной вес. Собственное пр-во",
                                "count": 2.0,
                                "amount": 102.5
                            },
                            {
                                "description": "Пакет майка",
                                "count": 99.0,
                                "amount": 99.0
                            },
                            {
                                "description": "Luna",
                                "count": 2.0,
                                "amount": 200.0
                            }
                        ],
                        "withdraws": [],
                        "rewards": [
                            {
                                "offerExternalId": "d9da9aff-b813-42be-aced-3b981c4c4e36",
                                "rewardType": "Discount",
                                "description": null,
                                "amount": {
                                    "amount": 97.5,
                                    "currency": "RUB",
                                    "currencyInfo": {
                                        "id": 2,
                                        "name": "Рубли",
                                        "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                        "description": "Денежная единица Российской Федерации",
                                        "isDeleted": false,
                                        "nameCases": {
                                            "nominative": "рубль",
                                            "genitive": "рубля",
                                            "plural": "рублей",
                                            "abbreviation": "руб."
                                        }
                                    }
                                }
                            }
                        ],
                        "isRefund": false,
                        "chequeNumber": "123456",
                        "amount": {
                            "amount": 401.5,
                            "currency": "RUB",
                            "currencyInfo": {
                                "id": 2,
                                "name": "Рубли",
                                "externalId": "718ae69b-76be-413f-ad19-7b7e02e4a438",
                                "description": "Денежная единица Российской Федерации",
                                "isDeleted": false,
                                "nameCases": {
                                    "nominative": "рубль",
                                    "genitive": "рубля",
                                    "plural": "рублей",
                                    "abbreviation": "руб."
                                }
                            }
                        }
                    }
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};