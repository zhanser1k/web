const pageObject = function() {
    return {
        registrationForm: element(by.css('.registration-form')),
        firstSocialItemLogo: $$(".social-item").get(1),
        history: element(by.css('.history-table')),
    }
};

module.exports = pageObject;
