const httpResponses = require('./httpResponses');

module.exports = () => {
    return [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\?filter\.count=\d+/,
        respond: httpResponses.offers,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\d+\/Merchants/,
        respond: httpResponses.offers,
    }];
};


