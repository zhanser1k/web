module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offers: {
        "data": [
            {
                "id": 351,
                "title": "gggggggggggggggggggggg",
                "description": "gggggg",
                "begin": "2018-05-09T19:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "e84d996a-0dcd-49f7-877f-2f24dd00db88",
                    "88d4cd60-fcb9-4b7d-9b69-70d6ca0a82ed"
                ],
                "brands": [
                    {
                        "id": "e84d996a-0dcd-49f7-877f-2f24dd00db88",
                        "name": "d2219b8cc786448091b3d2c624432e1e",
                        "images": []
                    },
                    {
                        "id": "88d4cd60-fcb9-4b7d-9b69-70d6ca0a82ed",
                        "name": "f443b8a2f3be4adc803bc8de57db4e13",
                        "images": []
                    }
                ],
                "brandId": "e84d996a-0dcd-49f7-877f-2f24dd00db88",
                "images": [],
                "instructions": [],
                "merchantsCount": 4
            },
            {
                "id": 353,
                "title": "1",
                "description": "",
                "begin": "2018-05-02T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "c183c62a-7441-4171-be20-d6941472ea5b",
                    "e84d996a-0dcd-49f7-877f-2f24dd00db88"
                ],
                "brands": [
                    {
                        "id": "c183c62a-7441-4171-be20-d6941472ea5b",
                        "name": "57481a33dddd4eb8ad9ada8764d6af59",
                        "images": []
                    },
                    {
                        "id": "e84d996a-0dcd-49f7-877f-2f24dd00db88",
                        "name": "d2219b8cc786448091b3d2c624432e1e",
                        "images": []
                    }
                ],
                "brandId": "c183c62a-7441-4171-be20-d6941472ea5b",
                "images": [
                    {
                        "fileId": "096438ee-9499-4cba-a3dc-18f6b50b256d",
                        "description": "700x350"
                    }
                ],
                "instructions": [],
                "merchantsCount": 3
            },
            {
                "id": 348,
                "title": "Уныние всем",
                "description": "Обыденность и безнадега",
                "begin": "2018-04-30T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [
                    {
                        "fileId": "1fe57c7b-1c4a-4089-9fde-f9ab7e527793",
                        "description": "user_portal"
                    }
                ],
                "instructions": [],
                "merchantsCount": 1
            },
            {
                "id": 349,
                "title": "Проверка отображения рекламных материалов на главной",
                "description": "Здесь пишется информация о том, что за нереальные преференции может получить пользователь!)",
                "begin": "2018-04-30T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [
                    {
                        "fileId": "71aebd08-f244-430d-8a11-e8bfd552e31c",
                        "description": "user_portal"
                    }
                ],
                "instructions": [],
                "merchantsCount": 225
            },
            {
                "id": 352,
                "title": "иииии",
                "description": "ииии",
                "begin": "2018-01-10T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 225
            },
            {
                "id": 196,
                "title": "234",
                "description": "234",
                "begin": "2017-06-08T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [
                    "1.",
                    "2.",
                    "3."
                ],
                "merchantsCount": 222
            },
            {
                "id": 201,
                "title": "тест",
                "description": "тест",
                "begin": "2017-06-08T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 225
            },
            {
                "id": 195,
                "title": "2",
                "description": "1",
                "begin": "2017-06-07T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 225
            },
            {
                "id": 200,
                "title": "проверка",
                "description": "проверка",
                "begin": "2017-05-31T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 225
            },
            {
                "id": 189,
                "title": "1",
                "description": "1",
                "begin": "2017-05-24T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 1
            },
            {
                "id": 190,
                "title": "12",
                "description": "12",
                "begin": "2017-05-17T00:00:00Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 3
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    merchants: {
        "data": [
            {
                "id": 120405,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "e84d996a-0dcd-49f7-877f-2f24dd00db88",
                "location": {
                    "id": 115991,
                    "locationId": "e7bf32f2-bf17-4181-9ba0-01ba619806d2",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 120406,
                "title": "newsuper",
                "internalTitle": "newsuper",
                "brandId": "e84d996a-0dcd-49f7-877f-2f24dd00db88",
                "location": {
                    "id": 115992,
                    "locationId": "34742ed7-6737-40a7-549a-e5e7c15e04ee",
                    "description": "Томск, Дербышевский пер., 26",
                    "latitude": 56.0,
                    "longitude": 56.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "Дербышевский пер.",
                    "house": "26",
                    "building": null,
                    "office": "56"
                },
                "scheduleModel": null,
                "contacts": "newsuper",
                "description": "newsuper"
            },
            {
                "id": 120407,
                "title": "Универсам",
                "internalTitle": "Универсам",
                "brandId": "88d4cd60-fcb9-4b7d-9b69-70d6ca0a82ed",
                "location": {
                    "id": 115993,
                    "locationId": "1a530931-a7dd-4a6f-a510-691034e1c42c",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 0.0,
                    "longitude": 0.0,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            },
            {
                "id": 120408,
                "title": "magazik",
                "internalTitle": "magazik",
                "brandId": "88d4cd60-fcb9-4b7d-9b69-70d6ca0a82ed",
                "location": null,
                "scheduleModel": null,
                "contacts": null,
                "description": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};
