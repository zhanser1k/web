require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should display list offers': () => {
            expect(po.items.count()).toBe(11);
        },
        'Should successfully display and close modal window': () => {
            po.items.first().click();
            expect(po.modal).toBePresent();
            po.close.click();
            expect(po.modal.isPresent()).toBe(false);
        },
    }
};
