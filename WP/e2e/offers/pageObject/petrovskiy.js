const pageObject = function() {
    return {
        items: element.all(by.repeater('offer in offers')),
        modal: element(by.css('.modal-content')),
        close: element(by.css('.modal-close')),
    }
};

module.exports = pageObject;
