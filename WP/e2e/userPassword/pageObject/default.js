const pageObject = function() {
    return {
        openForm: element(by.id('lmx-settings-openForm')),
        inputCurrentPassword: element(by.model('model.currentPassword')),
        inputNewPassword: element(by.model('model.newPassword')),
        inputRepeatedPassword: element(by.model('model.repeatPassword')),
        buttonCancel: element(by.id('lmx-settings-cancel')),
        buttonSubmit: element(by.id('lmx-settings-submit')),
        message: $$('.lmx-alert-message'),
        warning: $$('.lmx-alert-warning'),
        helpBlockError: $$('.lmx-help-block__error'),
    }
};

module.exports = pageObject;
