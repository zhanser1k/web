module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    passwordChangeSuccess: {
        "data": "e2e_tests_token",
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    passwordChangeErrorIncorrect: {
        "result": {
            "state": "Error",
            "message": "Неправильный пароль.",
            "validationErrors": null
        }
    },
    passwordChangeErrorLength: {
        "result": {
            "state": "ValidationError",
            "message": null,
            "validationErrors": [
                {
                    "field": "model.NewPassword",
                    "errorMessages": [
                        "Пароль должен содержать не менее 6 и не более 25 символов"
                    ]
                }
            ]
        }
    },
    logins: {
        "data": {
            "identifiers": [
                {
                    "identifierType": "Login",
                    "value": "9070129401809630"
                },
                {
                    "identifierType": "Login",
                    "value": "9070557401011494"
                },
                {
                    "identifierType": "Login",
                    "value": "79999999999"
                },
                {
                    "identifierType": "Login",
                    "value": "wohebiju@yk20.com"
                }
            ],
            "socialIdentifiers": []
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
};
