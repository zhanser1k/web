const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/User\/Logins$/,
        respond: httpResponses.logins,
    }];

    const answerUserPasswordChange = {
        'Should display an error message if a current password is incorrect': httpResponses.passwordChangeErrorIncorrect,
        'Should display an error message if a new password has incorrect length': httpResponses.passwordChangeErrorLength,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Password\/Change$/,
        respond: answerUserPasswordChange[description] || httpResponses.passwordChangeSuccess,
    });

    return mocks;
};
