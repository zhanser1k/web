require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should successfully change password': () => {
            po.inputCurrentPassword.sendKeys('111111');
            po.inputNewPassword.sendKeys('111111');
            po.inputRepeatedPassword.sendKeys('111111');
            po.buttonSubmit.click();
            expect(po.message.count()).toBe(1);
        },
        'Should correctly cancel password change': () => {
            expect(po.openForm.isPresent()).toBe(false);
            po.inputCurrentPassword.sendKeys('111111');
            po.inputNewPassword.sendKeys('111111');
            po.inputRepeatedPassword.sendKeys('111111');
            po.buttonCancel.click();
            expect(po.openForm).toBePresent();
        },
        'Should display an error message if all password fields are empty': () => {
            po.buttonSubmit.click();
            expect(po.helpBlockError.count()).toBe(3);
        },
        'Should display an error message if a current password is incorrect': () => {
            po.inputCurrentPassword.sendKeys('7777777777');
            po.inputNewPassword.sendKeys('111111');
            po.inputRepeatedPassword.sendKeys('111111');
            po.buttonSubmit.click();
            expect(po.warning.count()).toBe(1);
        },
        'Should display an error message if new password and repeated password are different': () => {
            po.inputCurrentPassword.sendKeys('111111');
            po.inputNewPassword.sendKeys(Array(6).fill(1).join(""));
            po.inputRepeatedPassword.sendKeys(Array(7).fill(1).join(""));
            po.buttonSubmit.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display an error message if a new password has incorrect length': () => {
            po.inputCurrentPassword.sendKeys('111111');
            po.inputNewPassword.sendKeys(Array(26).fill(1).join(""));
            po.inputRepeatedPassword.sendKeys(Array(26).fill(1).join(""));
            po.buttonSubmit.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
    }
};
