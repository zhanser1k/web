module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should display map from list of offers': () => {
            po.showOnMap.first().click();
            expect(po.map.isDisplayed()).toBe(true);
        },
        'Should display map from one offer': () => {
            po.more.first().click();
            po.showOnMap.first().click();
            expect(po.map.isDisplayed()).toBe(true);
        },
        'Should display map from detailed info': () => {
            po.more.first().click();
            po.detailedItems.first().click();
            po.showOnMapFromDetailedInfo.first().click();
            expect(po.map.isDisplayed()).toBe(true);
        },
        'Should display more info': () => {
            po.more.first().click();
            expect(po.detailedItems.count()).toEqual(10);
        },
        'Should back to list of offers': () => {
            po.more.first().click();
            po.back.click();
            expect(po.more.count()).toEqual(3);
        },
    }
};
