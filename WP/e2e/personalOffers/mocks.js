const httpResponses = require('./httpResponses');

module.exports = () => {
    return [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/Announcements\?filter/,
        respond: httpResponses.announcements,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\?filter/,
        respond: httpResponses.offers,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\d+$/,
        respond: httpResponses.offer,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\d+\/details$/,
        respond: httpResponses.offerDetails,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\d+\/details\/\d+-\d+\/merchants$/,
        respond: httpResponses.merchantsDetails,
    }, {
        requestMethod: 'GET',
        url: /api\/offer\/\d+\/Merchants$/,
        respond: httpResponses.merchants,
    }];
};
