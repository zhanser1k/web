const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const tests = require('../tests')('bin');
const mocks = require('../mocks');

// TODO после перевода Bin на версию 1.5 нужно проверить тесты
xdescribe('Bin personal offers', function() {
    let descriptions;

    beforeEach(() => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions) {
            descriptions = this.children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks(descriptions[0]).forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        $httpBackend.passThrough();

        browser.get('/#/personalOffers');
    });

    afterEach(() => {
        if (descriptions.length === 1) {
            browser.executeScript(`return localStorage.removeItem('ls.authorizationToken');`);
        }
        ngMockE2E.clearMockModules();
    });

    for (const name in tests) {
        if (tests.hasOwnProperty(name)) {
            it(name, tests[name]);
        }
    }
});
