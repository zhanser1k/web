module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    announcements: {
        "data": [],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offers: {
        "data": [
            {
                "id": 357,
                "title": "Только для Вас!",
                "description": null,
                "begin": "2018-06-11T22:52:26Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 3
            },
            {
                "id": 358,
                "title": "ац",
                "description": "цаф",
                "begin": "2018-06-11T22:52:26Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 3
            },
            {
                "id": 359,
                "title": "fsdf",
                "description": "ds",
                "begin": "2018-06-11T22:52:26Z",
                "end": null,
                "rewardThumbnail": null,
                "rewardImageId": null,
                "brandIds": [
                    "27812daa-fa4d-4564-abab-3305e8355017"
                ],
                "brands": [
                    {
                        "id": "27812daa-fa4d-4564-abab-3305e8355017",
                        "name": "ТЕСТ",
                        "images": [
                            {
                                "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                                "description": "user_portal"
                            },
                            {
                                "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                                "description": "marker"
                            },
                            {
                                "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                                "description": "testImage"
                            },
                            {
                                "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                                "description": "superimage"
                            }
                        ]
                    }
                ],
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "images": [],
                "instructions": [],
                "merchantsCount": 3
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offer: {
        "data": {
            "id": 357,
            "title": "Только для Вас!",
            "description": null,
            "begin": "2018-06-11T22:52:26Z",
            "end": null,
            "rewardThumbnail": null,
            "rewardImageId": null,
            "brandIds": [
                "27812daa-fa4d-4564-abab-3305e8355017"
            ],
            "brands": [
                {
                    "id": "27812daa-fa4d-4564-abab-3305e8355017",
                    "name": "ТЕСТ",
                    "images": [
                        {
                            "fileId": "0394f3e0-0275-4d3c-916d-bb134f9e3c91",
                            "description": "user_portal"
                        },
                        {
                            "fileId": "49e12497-33fb-4b01-9ce5-3958139499e7",
                            "description": "marker"
                        },
                        {
                            "fileId": "aeab841b-04c8-40e1-bfdc-203441d300d3",
                            "description": "testImage"
                        },
                        {
                            "fileId": "7b9e09e8-a7dd-4f43-8047-e75b74c95020",
                            "description": "superimage"
                        }
                    ]
                }
            ],
            "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
            "images": [],
            "instructions": [],
            "merchantsCount": 3
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    offerDetails: {
        "data": [
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.1
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KARA010"
                    }
                ],
                "annotation": "Количество не меньше 5",
                "hasMerchants": true,
                "detailBeginLocalDate": "2018-06-30T00:00:00Z",
                "detailEndLocalDate": null,
                "goodsId": 327020,
                "id": "327020-394841",
                "rewardThumbnail": null,
                "title": "Supervolume mascara Secret message",
                "description": "супер предложение",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.15
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KARA010"
                    }
                ],
                "annotation": "",
                "hasMerchants": false,
                "detailBeginLocalDate": null,
                "detailEndLocalDate": null,
                "goodsId": 327020,
                "id": "327020-394842",
                "rewardThumbnail": null,
                "title": "Supervolume mascara Secret message",
                "description": "мега акция",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.1
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KHEN010"
                    }
                ],
                "annotation": "Количество не меньше 5",
                "hasMerchants": true,
                "detailBeginLocalDate": "2018-06-30T00:00:00Z",
                "detailEndLocalDate": null,
                "goodsId": 327021,
                "id": "327021-394841",
                "rewardThumbnail": null,
                "title": "Wet Dry Секрет великолепия",
                "description": "супер предложение",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.15
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KHEN010"
                    }
                ],
                "annotation": "",
                "hasMerchants": false,
                "detailBeginLocalDate": null,
                "detailEndLocalDate": null,
                "goodsId": 327021,
                "id": "327021-394842",
                "rewardThumbnail": null,
                "title": "Wet Dry Секрет великолепия",
                "description": "мега акция",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.1
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KDAG0010"
                    }
                ],
                "annotation": "Количество не меньше 5",
                "hasMerchants": true,
                "detailBeginLocalDate": "2018-06-30T00:00:00Z",
                "detailEndLocalDate": null,
                "goodsId": 327022,
                "id": "327022-394841",
                "rewardThumbnail": null,
                "title": "Foundation cream Double agent",
                "description": "супер предложение",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.15
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KDAG0010"
                    }
                ],
                "annotation": "",
                "hasMerchants": false,
                "detailBeginLocalDate": null,
                "detailEndLocalDate": null,
                "goodsId": 327022,
                "id": "327022-394842",
                "rewardThumbnail": null,
                "title": "Foundation cream Double agent",
                "description": "мега акция",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.1
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KHEN020"
                    }
                ],
                "annotation": "Количество не меньше 5",
                "hasMerchants": true,
                "detailBeginLocalDate": "2018-06-30T00:00:00Z",
                "detailEndLocalDate": null,
                "goodsId": 327023,
                "id": "327023-394841",
                "rewardThumbnail": null,
                "title": "Mascara Try out!",
                "description": "супер предложение",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.15
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "_KHEN020"
                    }
                ],
                "annotation": "",
                "hasMerchants": false,
                "detailBeginLocalDate": null,
                "detailEndLocalDate": null,
                "goodsId": 327023,
                "id": "327023-394842",
                "rewardThumbnail": null,
                "title": "Mascara Try out!",
                "description": "мега акция",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.1
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "5112345112f451dfdf1234"
                    },
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsBarcodeViewModel, Loymax.Mobile.Contract",
                        "value": "4601921203677511234"
                    }
                ],
                "annotation": "Количество не меньше 5",
                "hasMerchants": true,
                "detailBeginLocalDate": "2018-06-30T00:00:00Z",
                "detailEndLocalDate": null,
                "goodsId": 1316053,
                "id": "1316053-394841",
                "rewardThumbnail": null,
                "title": "Товар МРЦ",
                "description": "супер предложение",
                "picture": null
            },
            {
                "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.PersonalOfferGoodsDetailItemViewModel, Loymax.Mobile.Contract",
                "discount": {
                    "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
                    "discountType": "Discount",
                    "value": 0.1
                },
                "codes": [
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsPartnerCodeViewModel, Loymax.Mobile.Contract",
                        "value": "45584"
                    },
                    {
                        "$type": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Goods.PersonalOfferGoodsBarcodeViewModel, Loymax.Mobile.Contract",
                        "value": "2186408"
                    }
                ],
                "annotation": "Количество не меньше 5",
                "hasMerchants": true,
                "detailBeginLocalDate": "2018-06-30T00:00:00Z",
                "detailEndLocalDate": null,
                "goodsId": 1361176,
                "id": "1361176-394841",
                "rewardThumbnail": null,
                "title": "Пирог Лоранский с ветчиной вес. Собственное пр-во",
                "description": "супер предложение",
                "picture": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    merchantsDetails: {
        "data": [
            {
                "id": 32060,
                "title": "МАГАЗИН 20",
                "internalTitle": "МАГАЗИН 20",
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "location": {
                    "id": 996,
                    "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 84.945384,
                    "longitude": 56.485249,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": {
                    "mon": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "tue": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "wed": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "thu": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "fri": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "sat": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "sun": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ]
                },
                "contacts": null,
                "description": null
            },
            {
                "id": 32172,
                "title": "МАГАЗИН 132",
                "internalTitle": "МАГАЗИН 132",
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "location": {
                    "id": 996,
                    "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 84.945384,
                    "longitude": 56.485249,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": {
                    "mon": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "tue": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "wed": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "thu": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "fri": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "sat": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "sun": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ]
                },
                "contacts": null,
                "description": null
            },
            {
                "id": 32194,
                "title": "МАГАЗИН 154",
                "internalTitle": "МАГАЗИН 154",
                "brandId": "27812daa-fa4d-4564-abab-3305e8355017",
                "location": {
                    "id": 996,
                    "locationId": "9eb0fa39-d066-48ec-8d5e-0e4630a9296e",
                    "description": "Томск, пр. Ленина, 10",
                    "latitude": 84.945384,
                    "longitude": 56.485249,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Ленина",
                    "house": "10",
                    "building": null,
                    "office": null
                },
                "scheduleModel": {
                    "mon": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "tue": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "wed": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "thu": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "fri": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "sat": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ],
                    "sun": [
                        {
                            "to": "00:00",
                            "from": "00:00"
                        }
                    ]
                },
                "contacts": null,
                "description": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    merchants: {
        "data": [
            {
                "id": 41,
                "title": "Гипермаркет Фудсити-ЭкспрессГипермаркет Фудсити-ЭкспрессГипермаркет Фудсити-Экспресс",
                "internalTitle": "Гипермаркет Фудсити-Экспресс",
                "brandId": "9848b201-90ac-4714-aa63-f9232485becc",
                "location": {
                    "id": 32,
                    "locationId": "cd860153-f3ac-45ed-a41c-b2ac90042b19",
                    "description": "Томск, пр. Кирова, 65",
                    "latitude": 55.865409,
                    "longitude": 37.603495,
                    "region": null,
                    "city": {
                        "id": 1,
                        "regionId": null,
                        "name": "Томск",
                        "prefix": "г."
                    },
                    "street": "пр. Кирова",
                    "house": "65",
                    "building": null,
                    "office": null
                },
                "scheduleModel": null,
                "contacts": null,
                "description": null
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    }
};
