const pageObject = function() {
    return {
        showOnMap: $$(".personal-offer__showOnMap"),
        showOnMapFromDetailedInfo: $$(".show-on-map__btn a"),
        more: $$(".personal-offers__more"),
        detailedItems: $$(".offer-details-wrap"),
        back: element(by.id('personal-offers__back')),
        map: element(by.css('.map')),
    }
};

module.exports = pageObject;
