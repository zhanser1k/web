const pageObject = function() {
    return {
        textarea: element(by.css('textarea')),
        buttonSubmit: element(by.css('button[type="submit"]')),
        inputFile: element(by.css('input[type="file"]')),
        removeFile: element(by.css('.form-file__remove')),
        helpBlockError: $$('.help-block__error'),
    }
};

module.exports = pageObject;
