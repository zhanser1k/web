const helpers = require('@loymax-solutions/protractor-helpers');
const path = require('path');
const remote = require('../../node_modules/selenium-webdriver/remote');
browser.setFileDetector(new remote.FileDetector());

const fileToUpload = './null.gif';
const absolutePath = path.resolve(__dirname, fileToUpload);

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should display an error message if message input field is empty': () => {
            po.buttonSubmit.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should successfully add and remove file': () => {
            po.inputFile.sendKeys(absolutePath);
            browser.driver.sleep(100);
            expect(po.removeFile).toBePresent();
            po.removeFile.click();
            expect(po.inputFile).toHaveValue('');
        },
        'Should successfully send feedback message': () => {
            po.textarea.sendKeys('test message');
            po.buttonSubmit.click();
            expect(po.textarea).toHaveValue('');;
        },
    }
};
