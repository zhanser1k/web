const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/Support\/Messages$/,
        respond: httpResponses.messagesGet,
    }, {
        requestMethod: 'POST',
        url: /api\/Support\/Messages$/,
        respond: httpResponses.messagesPost,
    }];

    return mocks;
};
