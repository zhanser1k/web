const helpers = require('@loymax-solutions/protractor-helpers');

module.exports = theme => {
    const pageObject = require(`./pageObject/${theme}`);
    const po = new pageObject();

    return {
        'Should save the questionnaire with all required fields filled in': () => {
            po.submit.click();
            expect(po.notification).toBePresent();
        },
        'Should not save the questionnaire without required fields filled in': () => {
            po.lastName.clear();
            po.firstName.clear();
            po.patronymic.clear();
            po.submit.click();
            expect(po.helpBlockError.count()).toBe(3);
        },
        'Should save changes': () => {
            helpers.clearAndSetValue(po.lastName, 'Мурзиков');
            helpers.clearAndSetValue(po.firstName, 'Арарат');
            helpers.clearAndSetValue(po.patronymic, 'Маркович');
            po.submit.click();
            expect(po.notification).toBePresent();
        },
        'Should display drop-down lists for autofill house field': () => {
            po.house.clear();
            po.house.click();
            expect(po.dropdownMenu).toBePresent();
        },
        'Should display drop-down lists for autofill street field': () => {
            po.street.clear();
            po.street.click();
            expect(po.dropdownMenu).toBePresent();
        },
        'Should display drop-down lists for autofill city field': () => {
            po.city.clear();
            po.city.click();
            expect(po.dropdownMenu).toBePresent();
        },
        'Should display an error message for incorrect birthday': () => {
            po.year.click();
            po.currentYear.click();
            po.submit.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        'Should display error message when not selecting radiobutton field in a set': () => {
            po.otherJob.click();
            po.submit.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
        // TODO #12727
        /*'Should display an error message for fields with long values': () => {
            helpers.clearAndSetValue(po.lastName, Array(257).fill(9).join(""));
            po.submit.click();
            expect(po.helpBlockError.count()).toBe(1);
        },*/
    }
};
