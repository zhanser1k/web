const pageObject = function() {
    return {
        form: element(by.css('.form')),
        datePreview: element(by.css('.questionnaire-date__preview')),
        cancel: element(by.css('.questionnaire-cancel')),
        notification: element(by.css('.lmx-notifications')),
        alert: element(by.css('.alert')),
        dropdownMenu: element(by.css('.dropdown-menu')),
        lastName: element(by.id('lmx-_2')),
        firstName: element(by.id('lmx-_1')),
        patronymic: element(by.id('lmx-_16')),
        house: element(by.id('lmx-_4')),
        street: element(by.id('lmx-_3')),
        city: element(by.id('lmx-_17')),
        helpBlockError: $$('.lmx-help-block__error'),
        day: $$('.sb-date-select-wrapper').get(0),
        month: $$('.sb-date-select-wrapper').get(1),
        year: $$('.sb-date-select-wrapper').get(2),
        currentYear: $$('.customSelect-list').last().$$('.customSelect-item').first(),
        otherJob: element(by.css('label[for="lmx-id_Job_8"]')),
        submit: element(by.css('button[type="button"]')),
        submitWrapper: element(by.css('.questionnaire-submit')),
    }
};

module.exports = pageObject;
