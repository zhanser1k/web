const defaultPageObject = require('./default')();

const pageObject = function() {
    return {
        ...defaultPageObject,
        lastName: element(by.id('_2')),
        firstName: element(by.id('_1')),
        patronymic: element(by.id('_16')),
        house: element(by.id('_4')),
        street: element(by.id('_3')),
        city: element(by.id('_17')),
        helpBlockError: $$('.help-block__error'),
        notification: element(by.css('.notifications')),
        otherJob: element(by.css('label[for="id_Job_8"]')),
        date: element(by.css('input[name="dateTime"]')),
        calendar: element(by.css('.datetime-picker-dropdown')),
        calendarMonthYear: element(by.css('.btn-sm.uib-title')),
        calendarMonthYearRight: element(by.css('.btn-sm.uib-right')),
        calendarSelectDate: $$('button[ng-click="select(dt.date)"]').get(0),
        submit: element(by.css('.questionnaire input[type="submit"]')),
    }
};

module.exports = pageObject;
