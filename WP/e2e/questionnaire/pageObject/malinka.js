const defaultPageObject = require('./default')();

const pageObject = function() {
    return {
        ...defaultPageObject,
        lastName: element(by.id('_2')),
        firstName: element(by.id('_1')),
        patronymic: element(by.id('_16')),
        house: element(by.id('_4')),
        street: element(by.id('_3')),
        city: element(by.id('_17')),
        helpBlockError: $$('.help-block__error'),
        notification: element(by.css('.notifications')),
        otherJob: element(by.css('label[for="id_Job_8"]')),
        submit: element(by.css('.questionnaire-submit button[type="submit"]')),
    }
};

module.exports = pageObject;
