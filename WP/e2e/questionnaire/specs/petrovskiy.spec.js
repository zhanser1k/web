const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;

const mocks = require('../mocks');
const pageObject = require('../pageObject/petrovskiy');
let tests = require('../tests')('petrovskiy');

describe('Petrovskiy questionnaire', function() {
    const po = new pageObject();
    let descriptions;

    tests = {
        ...tests,
        'Should display drop-down for autofill birthday field': () => {
            po.date.click();
            expect(po.calendar).toBePresent();
        },
        'Should display an error message for incorrect birthday': () => {
            po.date.click();
            po.calendarMonthYear.click();
            for (let i=0; i<30; i++) {
                po.calendarMonthYearRight.click();
            }
            po.calendarSelectDate.click();
            po.calendarSelectDate.click();
            po.submit.click();
            expect(po.helpBlockError.count()).toBe(1);
        },
    };

    beforeEach(() => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions) {
            descriptions = this.children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks(descriptions[0]).forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        $httpBackend.passThrough();

        browser.get('/#/personal');
    });

    afterEach(() => {
        ngMockE2E.clearMockModules();
    });

    for (const name in tests) {
        if (tests.hasOwnProperty(name)) {
            it(name, tests[name]);
        }
    }
});
