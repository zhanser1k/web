const ngMockE2E = require('ng-mock-e2e');
const {$httpBackend} = ngMockE2E;
const helpers = require('@loymax-solutions/protractor-helpers');

const mocks = require('../mocks');
const pageObject = require('../pageObject/gulliver');
let tests = require('../tests')('bonusme');

describe('Gulliver questionnaire', function() {
    const po = new pageObject();
    let descriptions;

    tests = {
        ...tests,
        'Should be transformed from view mode to edit mode': () => {
            expect(helpers.hasClass(po.form, '_disabled')).toEqual(false);
            expect(helpers.hasClass(po.edit, 'ng-hide')).toEqual(true);
            expect(helpers.hasClass(po.cancel, 'ng-hide')).toEqual(false);
            expect(po.datePreview.isDisplayed()).toBe(false);
            expect(po.submitWrapper.isDisplayed()).toBe(true);
        },
        'Should be transformed from edit mode to view mode if questionnaire changes are saved': () => {
            helpers.clearAndSetValue(po.lastName, 'Мурзиков');
            helpers.clearAndSetValue(po.firstName, 'Арарат');
            helpers.clearAndSetValue(po.patronymic, 'Маркович');
            po.submit.click();
            expect(helpers.hasClass(po.form, '_disabled')).toEqual(true);
            expect(helpers.hasClass(po.edit, 'ng-hide')).toEqual(false);
            expect(helpers.hasClass(po.cancel, 'ng-hide')).toEqual(true);
            expect(po.datePreview.isDisplayed()).toBe(true);
            expect(po.submitWrapper.isDisplayed()).toBe(false);
        },
        'Should cancel questionnaire changes': () => {
            helpers.clearAndSetValue(po.lastName, 'Мурзиков');
            helpers.clearAndSetValue(po.firstName, 'Арарат');
            helpers.clearAndSetValue(po.patronymic, 'Маркович');
            po.cancel.click();
            expect(po.lastName).toHaveValue('Ракитина123');
            expect(po.firstName).toHaveValue('Анна');
            expect(po.patronymic).toHaveValue('Анатольевна');
        },
    };

    beforeEach(() => {
        ngMockE2E.addMockModule();
        ngMockE2E.addAsDependencyForModule('lmxApp');

        if (!descriptions) {
            descriptions = this.children.map(({result}) => result.description);
        } else {
            descriptions.shift();
        }

        mocks(descriptions[0]).forEach(({requestMethod, url, respond}) => {
            $httpBackend.when(requestMethod, url).respond(respond);
        });

        $httpBackend.passThrough();

        browser.get('/#/questionnaire');

        po.edit.click();
    });

    afterEach(() => {
        ngMockE2E.clearMockModules();
    });

    for (const name in tests) {
        if (tests.hasOwnProperty(name)) {
            it(name, tests[name]);
        }
    }
});
