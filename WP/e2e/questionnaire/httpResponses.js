module.exports = {
    user: {
        "birthDay": "1988-04-05T00:00:00Z",
        "addressInfo": null,
        "cardShortInfo": null,
        "balanceShortInfo": {
            "balance": {
                "amount": 51349.2500,
                "currency": "BNS"
            },
            "notActivated": {
                "amount": 0.0000,
                "currency": "BNS"
            },
            "accumulated": {
                "amount": 2743.7000,
                "currency": "BNS"
            },
            "paid": {
                "amount": 0.0000,
                "currency": "BNS"
            }
        },
        "phoneNumber": "***9999",
        "email": "nono@mrjesus.nothere",
        "id": 376105,
        "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
        "firstName": "Анна",
        "lastName": "Ракитина123",
        "patronymicName": "Анатольевна"
    },
    questions: {
        "data": {
            "questions": [
                {
                    "nodeType": "Header",
                    "displayName": "Личные данные",
                    "description": null,
                    "logicalName": null,
                    "value": null,
                    "readOnly": false,
                    "required": null,
                    "id": null,
                    "groupID": null,
                    "selected": null,
                    "maxLength": null,
                    "fieldType": null,
                    "children": [
                        {
                            "nodeType": "Question",
                            "displayName": "Фамилия",
                            "description": null,
                            "logicalName": "LastName",
                            "value": "Ракитина123",
                            "readOnly": false,
                            "required": true,
                            "id": 2,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "String",
                            "children": null
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Имя",
                            "description": null,
                            "logicalName": "FirstName",
                            "value": "Анна",
                            "readOnly": false,
                            "required": true,
                            "id": 1,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "String",
                            "children": null
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Отчество",
                            "description": null,
                            "logicalName": "PatronymicName",
                            "value": "Анатольевна",
                            "readOnly": false,
                            "required": true,
                            "id": 16,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "String",
                            "children": null
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Дата рождения",
                            "description": null,
                            "logicalName": "BirthDay",
                            "value": "1988-04-05",
                            "readOnly": false,
                            "required": false,
                            "id": 7,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "DateTime",
                            "children": null
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Место жительства",
                            "description": null,
                            "logicalName": null,
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Город",
                                    "description": null,
                                    "logicalName": "City",
                                    "value": "г. Томск",
                                    "readOnly": false,
                                    "required": true,
                                    "id": 17,
                                    "groupID": null,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "String",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Улица",
                                    "description": null,
                                    "logicalName": "Street",
                                    "value": "Бирюкова ул.",
                                    "readOnly": false,
                                    "required": true,
                                    "id": 3,
                                    "groupID": null,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "String",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Дом",
                                    "description": null,
                                    "logicalName": "House",
                                    "value": "N9YCw1",
                                    "readOnly": false,
                                    "required": true,
                                    "id": 4,
                                    "groupID": null,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "String",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Пол",
                            "description": null,
                            "logicalName": "Sex",
                            "value": null,
                            "readOnly": false,
                            "required": true,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Мужской",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 1,
                                    "groupID": 5,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Женский",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 2,
                                    "groupID": 5,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Email",
                            "description": null,
                            "logicalName": "Email",
                            "value": "nono@m2222rjesus.nothere",
                            "readOnly": false,
                            "required": true,
                            "id": 18,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "String",
                            "children": null
                        }
                    ]
                },
                {
                    "nodeType": "Header",
                    "displayName": "Дополнительно",
                    "description": null,
                    "logicalName": null,
                    "value": null,
                    "readOnly": false,
                    "required": null,
                    "id": null,
                    "groupID": null,
                    "selected": null,
                    "maxLength": null,
                    "fieldType": null,
                    "children": [
                        {
                            "nodeType": "Header",
                            "displayName": "Семейное положение",
                            "description": null,
                            "logicalName": "FamilyType",
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Живу со спутником жизни",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 1,
                                    "groupID": 20,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Живу без спутника жизни",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 2,
                                    "groupID": 20,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Есть ли у Вас дети до 7 лет?",
                            "description": null,
                            "logicalName": "Children",
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Нет",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 1,
                                    "groupID": 6,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Да. Сколько детей",
                                    "description": null,
                                    "logicalName": null,
                                    "value": "333",
                                    "readOnly": false,
                                    "required": false,
                                    "id": 2,
                                    "groupID": 6,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "RadioButtonString",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Есть ли у Вас автомобиль?",
                            "description": null,
                            "logicalName": "Avto",
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Да",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 1,
                                    "groupID": 8,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Нет",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 2,
                                    "groupID": 8,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Род занятий",
                            "description": null,
                            "logicalName": "Job",
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Руководитель",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 1,
                                    "groupID": 9,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Специалист",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 2,
                                    "groupID": 9,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Рабочий",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 3,
                                    "groupID": 9,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Безработный",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 4,
                                    "groupID": 9,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Домохозяйка",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 5,
                                    "groupID": 9,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Студент",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 6,
                                    "groupID": 9,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Пенсионер",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 7,
                                    "groupID": 9,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Другое",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 8,
                                    "groupID": 9,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButtonString",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "На какие продукты/лекарства из списка Вам сложно найти деньги:",
                            "description": null,
                            "logicalName": null,
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Продукты/лекарства",
                                    "description": null,
                                    "logicalName": "PurchaseProducts",
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 10,
                                    "groupID": null,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "Checkbox",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Одежда/обувь",
                                    "description": null,
                                    "logicalName": "PurchaseClothes",
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 11,
                                    "groupID": null,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "Checkbox",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Мебель/бытовая техника",
                                    "description": null,
                                    "logicalName": "PurchaseTechnics",
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 12,
                                    "groupID": null,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "Checkbox",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Автомобиль",
                                    "description": null,
                                    "logicalName": "PurchaseAvto",
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 13,
                                    "groupID": null,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "Checkbox",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Квартира",
                                    "description": null,
                                    "logicalName": "PurchaseFlat",
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 14,
                                    "groupID": null,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "Checkbox",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Хватает на всё",
                                    "description": null,
                                    "logicalName": "PurchaseAnything",
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 15,
                                    "groupID": null,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "Checkbox",
                                    "children": null
                                }
                            ]
                        },
                        {
                            "nodeType": "Question",
                            "displayName": "Сколько человек проживает вместе с Вами (кроме Вас)?",
                            "description": null,
                            "logicalName": "FamilySize",
                            "value": "24",
                            "readOnly": false,
                            "required": false,
                            "id": 22,
                            "groupID": null,
                            "selected": false,
                            "maxLength": null,
                            "fieldType": "Int32",
                            "children": null
                        },
                        {
                            "nodeType": "Header",
                            "displayName": "Категория товаров",
                            "description": null,
                            "logicalName": "GoodsGroup",
                            "value": null,
                            "readOnly": false,
                            "required": null,
                            "id": null,
                            "groupID": null,
                            "selected": null,
                            "maxLength": null,
                            "fieldType": null,
                            "children": [
                                {
                                    "nodeType": "Question",
                                    "displayName": "Кондитерские изделия свежие",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 1,
                                    "groupID": 29,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Кулинария",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 2,
                                    "groupID": 29,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Напитки алкогольные",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 3,
                                    "groupID": 29,
                                    "selected": true,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Напитки безалкогольные",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 4,
                                    "groupID": 29,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                },
                                {
                                    "nodeType": "Question",
                                    "displayName": "Плоды и овощи",
                                    "description": null,
                                    "logicalName": null,
                                    "value": null,
                                    "readOnly": false,
                                    "required": false,
                                    "id": 5,
                                    "groupID": 29,
                                    "selected": false,
                                    "maxLength": null,
                                    "fieldType": "RadioButton",
                                    "children": null
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cards: {
        "data": [
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": true
                },
                "cardCategory": {
                    "id": 3,
                    "title": "Тест",
                    "logicalName": "Test",
                    "images": [
                        {
                            "fileId": "be57fa15-abb9-49cb-9549-c5c8f1a2a681",
                            "description": "1536x969"
                        },
                        {
                            "fileId": "3a957781-8bca-4f68-b157-80dc0103fa83",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 42067
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 584516,
                "state": "Activated",
                "number": "9070129401809630",
                "barCode": "9070129401809630",
                "block": true
            },
            {
                "accumulated": {
                    "amount": 2743.7000,
                    "currency": "BNS"
                },
                "paid": {
                    "amount": 0.0000,
                    "currency": "BNS"
                },
                "cardActionAccessInfo": {
                    "canBlock": true,
                    "canReplace": false
                },
                "cardCategory": {
                    "id": 1,
                    "title": "Стандартная",
                    "logicalName": "Standard",
                    "images": [
                        {
                            "fileId": "cecb3b9a-ac62-469a-9121-0dfdd7185d08",
                            "description": "user_portal"
                        }
                    ],
                    "cardCount": 321213
                },
                "cardOwnerInfo": {
                    "phoneNumber": "***9999",
                    "email": "nono@mrjesus.nothere",
                    "id": 376105,
                    "personUid": "eda1f02b-bb6e-e611-80df-00155d000205",
                    "firstName": "Анна",
                    "lastName": "Ракитина123",
                    "patronymicName": "Анатольевна"
                },
                "id": 600757,
                "state": "Activated",
                "number": "9070557401011494",
                "barCode": "9070557401011494",
                "block": false
            }
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userAnswers: {
        "data": null,
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userAnswersIncorrectBirthdate: {
        "data": {
            "errors": [
                {
                    "idQuestion": "7",
                    "errors": [
                        "Не допускается участие в программе лиц младше 18 лет"
                    ]
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    userAnswersIncorrectLastName: {
        "data": {
            "errors": [
                {
                    "idQuestion": "2",
                    "errors": [
                        "Длина строки должна быть меньше 256 символов"
                    ]
                }
            ]
        },
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    houses: {
        "data": [
            "10",
            "11",
            "12",
            "13",
            "15",
            "2",
            "22",
            "22/1",
            "24",
            "26"
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    streets: {
        "data": [
            "1 Аникинский пер.",
            "1 Водяной пер.",
            "1 Заречная ул.",
            "1 Рабочая ул.",
            "19 Гвардейской Дивизии ул.",
            "1905 года пер.",
            "2 Водяной пер.",
            "2 Ново-Деповская ул.",
            "2 Рабочая ул.",
            "3 Заречная ул."
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    },
    cities: {
        "data": [
            "cityc1aa3da5a09f46388b79319c39af1298",
            "г. г.Пермь",
            "г. Москва",
            "г. Северск",
            "г. Томск",
            "г. Томск2",
            "g. Томский район"
        ],
        "result": {
            "state": "Success",
            "message": null,
            "validationErrors": null
        }
    }
};