const httpResponses = require('./httpResponses');

module.exports = description => {
    let mocks = [{
        requestMethod: 'GET',
        url: /api\/user\/?$/gmi,
        respond: httpResponses.user,
    }, {
        requestMethod: 'GET',
        url: /api\/user\/questions.*$/gi,
        respond: httpResponses.questions,
    }, {
        requestMethod: 'GET',
        url: /api\/Cards\/$/,
        respond: httpResponses.cards,
    }, {
        requestMethod: 'GET',
        url: /api\/location\/houses.*$/gi,
        respond: httpResponses.houses,
    }, {
        requestMethod: 'GET',
        url: /api\/location\/streets.*$/gi,
        respond: httpResponses.streets,
    }, {
        requestMethod: 'GET',
        url: /api\/location\/cities.*$/gi,
        respond: httpResponses.cities,
    }];

    const userAnswers = {
        'Should display an error message for incorrect birthday': httpResponses.userAnswersIncorrectBirthdate,
        'Should display an error message for fields with long values': httpResponses.userAnswersIncorrectLastName,
    };

    mocks.push({
        requestMethod: 'POST',
        url: /api\/User\/Answers$/,
        respond: userAnswers[description] || httpResponses.userAnswers,
    });

    return mocks;
};