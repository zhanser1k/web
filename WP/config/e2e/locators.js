'use strict';

by.addLocator('modelValidation', function (name, using) {
    using = using || document;
    return using.querySelectorAll('form-group[label-text="' + name + '"] [ng-repeat="(error, value) in model.$error"]');
});