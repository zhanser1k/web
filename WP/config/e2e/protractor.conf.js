exports.config = {
    allScriptsTimeout: 11000,
    directConnect: true,
    chromeOnly: true,
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: ["--disable-gpu"]
        }
    },

    framework: 'jasmine2',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    },

    onPrepare: function () {

        by.addLocator('datePickerToggle', function (name, using) {
            using = using || document;
            var selector = 'input[datetime-picker][ng-model="' + name + '"] ~ .calendar';
            return using.querySelectorAll(selector);
        });

        by.addLocator('datePickerToday', function (name, using) {
            using = using || document;
            var selector = 'input[datetime-picker][ng-model="' + name + '"] ~ ul button.active';
            return using.querySelectorAll(selector);
        });

        var disableNgAnimate = function() {
            angular.module('disableNgAnimate', []).run(['$animate', function($animate) {
                $animate.enabled(false);
            }]);
        };

        var disableCssAnimate = function() {
            angular.module('disableCssAnimate', []).run(function() {
                var style = document.createElement('style');
                style.type = 'text/css';
                style.innerHTML = '* {' +
                    '-webkit-transition: none !important;' +
                    '-moz-transition: none !important' +
                    '-o-transition: none !important' +
                    '-ms-transition: none !important' +
                    'transition: none !important' +
                    '}';
                document.getElementsByTagName('head')[0].appendChild(style);
            });
        };

        browser.addMockModule('disableNgAnimate', disableNgAnimate);
        browser.addMockModule('disableCssAnimate', disableCssAnimate);

        browser.driver.manage().window().maximize();

        if (process.env.TEAMCITY_VERSION) {
            var reporter = require('jasmine-reporters');
            jasmine.getEnv().addReporter(new reporter.TeamCityReporter());
        }

        if (process.env.TF_BUILD) {
            var reporter = require('jasmine-reporters');
            jasmine.getEnv().addReporter(new reporter.JUnitXmlReporter({
                consolidateAll: false,
                savePath: 'e2eresults'
            }));
        }
    }
};