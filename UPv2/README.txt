==== Настройка окружения ====
Установить Node.js (LTS) https://nodejs.org/. Указать опцию добавления PATH
Установить git https://git-scm.com/. Указать опцию добавления PATH

	$: npm install gulp -g
	$: npm install bower -g
	$: npm install

==== Сборка проекта ====
production version:
	$: gulp build

Аргументы
--host "<url>" // Url адрес веб-приложения с WebApi
--theme "<projectName>" // Наименование проекта в системе
--templatesPath "<templatesPath>" // Путь к файлам шаблонов приложения
--version "<version>" // Версия build для отображения

Конфигурация формируется одним из следующийх источников (в порядке приоритетов):
- аргументы командной строки
- файл config\config.local.json
- файл config\config.default.json


==== Тесты ====
e2e
Для работы тестов нужно установить java (если не установлена).
    $: gulp e2e

unit
атоматический запуск тестов при изменениях
    $: npm run test
однократный запуск
    $: npm run test-single-run
однократный запуск с репортером для Team City
    $: npm run test-single-run-teamcity

==== Настройка teamcity ====
- Установить для компиляции 2013 студию: npm config set msvs_version 2013
- Перекинуть конфиг пользователю от которого компилируется: C:\Users\Teamcity\.npmrc
- Добавить в пути пользователя teamcity %path% C:\Users\teamcity\AppData\Roaming\npm
- Рестарт агента

==== FAQ ====

--- e2e ---
    Q: Не запускаются тесты с ошибкой "SessionNotCreatedError: session not created exception from unknown error: Runtime.executionContextCreated has invalid 'context':..."
    A: Нужно изменить версию chromedriver на 2.24 в файле /node_modules/protractor/config.json