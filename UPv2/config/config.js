(function(e) {
    'use strict';

    e.config = {
        host: '{{host}}',
        showcase: '{{showcase}}',
        templatesPath: '{{templatesPath}}',
        imagesPath: 'assets/images/',
        options: {
            acceptTenderOfferByCheck: false,
            authCookie: true,
            authorizeOnRegistrationComplete: true,
            forceRegistrationStartOnLoginAttempt: true,
            ignoreDocsFetchingErrors: false,
            map: {},
            offerHtmlFileId: 'd468ef8b-f7b6-4a7b-ba79-fd0a31f6b3f0',
            offerPdfFileId: 6926,
            opdAgreementFileId: 'c0b8ccb1-e25c-45f2-7900-aa8f271735a7',
            redirectUrlOnEmailConfirmForRegistration: '#registration',
            redirectUrlOnEmailConfirmForSettings: '#settings',
            redirectUrlOnEmailConfirmWithoutToken: '#login',
            redirectUrlOnLogin: '#history',
            redirectUrlOnLogout: '#home',
            redirectUrlOnRegistrationComplete: '#home',
            redirectUrlOnResetPasswordEmailConfirm: '#reset-password',
            redirectUrlOnSocialAuthFail: '#registration',
            redirectUrlOnSocialAuthSuccess: '#history',
            redirectUrlOnSocialBinding: '#social-binding',
            referralRegistration: false,
            requestUserAttributes: false,
            setRegistrationSmsCodeAsPassword: true,
            shareAuthLoginToRegistration: true,
            supportEmail: 'support@loymax.ru',
        },
    };
})(window);
