(function (angular) {
    'use strict';

    angular.module('bowser',[]).factory('bowser', ["$window" ,function ($window) {

        if ($window.bowser) {
            $window._thirdParty = $window._thirdParty || {};
            $window._thirdParty.bowser = $window.bowser;

            try { delete $window.bowser; }
            catch (e) { $window.bowser = undefined; } // < IE8
        }
        var bowser = $window._thirdParty.bowser;
        return bowser;
    }]);
})(angular);