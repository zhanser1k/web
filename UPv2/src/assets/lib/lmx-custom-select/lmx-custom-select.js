angular.module('lmxCustomSelect', [])

    .run(['$templateCache', function ($templateCache) {
        // TODO: К версии 1.5 заменить класс '_disabled' на, например, {'_empty': !model}
        var template = [
            '<div class="customSelect">',
            '<div outside-click="close()">',
            '<input class="customSelect-selected" ng-class="{\'_disabled\': !model || disabled, \'disabled\': disabled}" ng-click="!disabled && trigger()" ng-value="value" type="text" readonly>',
            '<div class="customSelect-scroller__wrapper" ng-class="{\'_hide\': !isVisible}">',
            '<div class="customSelect-scroller">',
            '<ul class="customSelect-list" ng-if="!subValue">',
            '<li class="customSelect-item _disabled" ng-if="placeholderAsCaption">{{placeholder}}</li>',
            '<li class="customSelect-item" ng-repeat="value in repeat" ng-click="select(value)">{{value}}</li>',
            '</ul>',
            '<ul class="customSelect-list" ng-if="subValue">',
            '<li class="customSelect-item _disabled" ng-if="placeholderAsCaption">{{placeholder}}</li>',
            '<li class="customSelect-item" ng-repeat="value in repeat" ng-click="select(value[subValue.value])">{{value[subValue.text]}}</li>',
            '</ul>',
            '</div>',
            '<div class="customSelect-scroller__track">',
            '<div class="customSelect-scroller__bar"></div>',
            '</div>',
            '</div>',
            '</div>',
            '<select class="class" ng-model="model" ng-options="{{options}}">',
            '</select>',
            '</div>'
        ];

        $templateCache.put('lmx-custom-select.html', template.join(''));
    }])

    .directive('lmxCustomSelect', function ($filter) {
        return {
            restrict: 'A',
            replace: true,
            require: 'ngModel',
            scope: { repeat: '=', options: '@', placeholder: '@', disabled: '<isDisabled' },
            templateUrl: function ($element, $attrs) {
                return $attrs.templateUrl || 'lmx-custom-select.html';
            },
            link: function($scope, elem, attrs, ngModelController) {
                $scope.options = attrs.options.replace(attrs.repeat, "repeat");

                var options = attrs.options.split(/\s+/);

                if (options[1] === 'as') {
                    $scope.subValue = {
                        value: options[0].split('.')[1],
                        text: options[2].split('.')[1]
                    };
                    $scope.$watch('repeat', function (newValue) {
                        if (newValue) {
                            setData({repeat: newValue});
                        }
                    });
                }

                $scope.$watch('model', function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        setData({model: newValue});
                    }
                });

                $scope.select = function (value) {
                    $scope.close();
                    $scope.model = value;
                    ngModelController.$setViewValue($scope.model);
                };

                $scope.close = function () {
                    $scope.isVisible = false;
                };

                $scope.trigger = function () {
                    $scope.isVisible = !$scope.isVisible;

                    if ($scope.isVisible) {
                        vscroll.update();
                    }
                };

                function setData(params) {
                    if (options[1] === 'as' && params) {
                        $scope.text = $filter('filter')(params.repeat || $scope.repeat, defineProperty({}, $scope.subValue.value, params.model || $scope.model))[0][$scope.subValue.text];
                        $scope.value = $scope.model ? $scope.text : $scope.placeholder;
                    } else {
                        $scope.value = $scope.model || $scope.placeholder;
                    }
                }

                // функция взята с babel из переделанного {[$scope.subValue.value]: $scope.model}
                function defineProperty(obj, key, value) {
                    if (key in obj) {
                        Object.defineProperty(obj, key, {
                            value: value,
                            enumerable: true,
                            configurable: true,
                            writable: true });
                    } else {
                        obj[key] = value;
                    }
                    return obj;
                }

                ngModelController.$render = function() {
                    $scope.model = ngModelController.$viewValue;
                    setData();
                };

                var vscroll = baron({
                    root: elem[0].querySelector('.customSelect-scroller__wrapper'),
                    scroller: '.customSelect-scroller',
                    barOnCls: '_scrollbar',
                    track: '.customSelect-scroller__track',
                    bar: '.customSelect-scroller__bar',
                    cssGuru: true,
                    direction: 'v'
                });
            }
        };
    });
