/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("changeEmailService", function ($http, configurationService) {

        var loader;
        var emailApiUrl = configurationService.host + 'api/User/Email/';

        var area;
        function setArea(serviceArea) {
            area = serviceArea;
        }

        function getState() {
            return showLoaderAndReturnData($http.get(emailApiUrl, {area: area}));
        }

        function sendCode(email){
            return sendPost(emailApiUrl, {email: email});
        }
        function reSendCode(){
            return sendPost(emailApiUrl + 'SendConfirmCode');
        }
        function confirmEmail(confirmCode){
            return sendPost(emailApiUrl + 'Confirm', {confirmCode: confirmCode});
        }
        function cancelChangeEmail(){
            return sendPost(emailApiUrl + 'CancelChange');
        }

        function sendPost(url, data) {
            return showLoaderAndReturnData($http.post(url, data, {area: area}));
        }

        function showLoaderAndReturnData(promise) {
            loader.inProgress = true;
            return promise.then(function(response){
                return response.data;
            }).finally(function(){
                loader.inProgress = false;
            });
        }

        function bindLoader(externalLoader) {
            loader = externalLoader;
        }

        return {
            setArea: setArea,
            getState: getState,
            sendCode: sendCode,
            reSendCode: reSendCode,
            confirmEmail: confirmEmail,
            cancelChangeEmail: cancelChangeEmail,
            bindLoader: bindLoader
        };

    });
})(angular);