/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("changePhoneService", function ($http, configurationService) {

        var loader;
        var phoneApiUrl = configurationService.host + 'api/User/PhoneNumber/';

        var area;
        function setArea(serviceArea) {
            area = serviceArea;
        }

        function getState() {
            return showLoaderAndReturnData($http.get(phoneApiUrl, {area: area}));
        }

        function sendCode(phoneNumber){
            return sendPost(phoneApiUrl, {phoneNumber: '7' + phoneNumber});
        }
        function reSendCode(){
            return sendPost(phoneApiUrl + 'SendConfirmCode');
        }
        function confirmPhone(confirmCode, setAsPassword){
            var params = {
                confirmCode: confirmCode
            };
            if (setAsPassword) {
                params.password = confirmCode;
            }
            return sendPost(phoneApiUrl + 'Confirm', params);
        }
        function cancelChangePhone(){
            return sendPost(phoneApiUrl + 'CancelChange');
        }

        function sendPost(url, data) {
            return showLoaderAndReturnData($http.post(url, data, {area: area}));
        }

        function showLoaderAndReturnData(promise) {
            loader.inProgress = true;
            return promise.then(function(response){
                return response.data;
            }).finally(function(){
                loader.inProgress = false;
            });
        }

        function bindLoader(externalLoader) {
            loader = externalLoader;
        }

        return {
            setArea: setArea,
            getState: getState,
            sendCode: sendCode,
            reSendCode: reSendCode,
            confirmPhone: confirmPhone,
            cancelChangePhone: cancelChangePhone,
            bindLoader: bindLoader
        };

    });
})(angular);