(function(angular) {
    angular.module('lmxApp', _.union([
        // angular
        'ngCookies',
        'ngMask',
        'ngResource',
        'ngRoute',

        // other vendors
        'LocalStorageModule',
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'bowser',
        'monospaced.qrcode',
        'angular-barcode',
        'ngSanitize',
        'naif.base64',
        'sbDateSelect',
        'lmxCustomSelect',
        'angular-toArrayFilter',
        'focus-if',

        // app
        'templateCache',
    ], window.dependencies || []));

    angular.module('lmxApp').config(function($cookiesProvider, $locationProvider, $qProvider) {
        // Избавляемся от лишних знаков в hash URL
        $locationProvider.hashPrefix('');

        // Время жизни куки авторизации - 1 месяц (как сейчас на BE)
        var expiryDate = new Date();
        expiryDate.setMonth(expiryDate.getMonth() + 1);
        $cookiesProvider.defaults.expires = expiryDate.toGMTString();

        // Отключаем ошибки вида possible unhandled rejection
        $qProvider.errorOnUnhandledRejections(false);
    });

    angular.module('lmxApp').run(function($window, e2eService) {
        var md = new MobileDetect(navigator.userAgent);
        var grade = md.mobileGrade();
        Modernizr.addTest({
            mobile: !!md.mobile(),
            phone: !!md.phone(),
            tablet: !!md.tablet(),
            mobilegradea: grade === 'A',
        });
        $window.mobileDetect = md;

        e2eService.init();
    });
})(angular);
