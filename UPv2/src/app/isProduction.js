/* eslint-disable */
(function (angular) {

	angular.module('lmxApp').config(['$compileProvider', function ($compileProvider) {
		$compileProvider.debugInfoEnabled(false);
	}]);
})(angular);