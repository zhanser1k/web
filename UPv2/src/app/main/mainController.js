(function(angular) {
    angular.module('lmxApp').controller('mainController', function(
        $locale,
        $q,
        $rootScope,
        $scope,
        authService,
        configurationService,
        emailConfirmationCheckerService,
        globalService,
        notification,
        routingService,
        socialActionCheckerService
    ) {
        $rootScope.locale = $locale;
        $rootScope.currentLocation = '';
        $rootScope.authInProcess = false;
        $scope.apiHost = configurationService.host;
        $scope.options = configurationService.options;
        $scope.allRequestsCompleted = true;

        $rootScope.isAuth = authService.loggedIn;

        $rootScope.changeLocation = globalService.changeLocation;

        $rootScope.logout = authService.logout;

        $rootScope.generateEvent = function(triggerEvent) {
            $scope.$broadcast(triggerEvent);
            return true;
        };

        function loadUserData(event) {
            var promises = [];
            var cards = [];
            var getCardInfoPromise = globalService.getCards().then(function(response) {
                cards = response;
            });
            promises.push(getCardInfoPromise);

            if (event.name !== 'loadUserInfo') {
                var getUserInfoPromise = globalService.getUserInfo().then(function(response) {
                    $rootScope.userInfo = response;
                });
                promises.push(getUserInfoPromise);
            }

            $q.all(promises).then(function() {
                cards.forEach(function(card) {
                    if (card.cardOwnerInfo.personUid === $rootScope.userInfo.personUid) {
                        $scope.cardInfo = card;
                    }
                });
            });
        }

        $scope.$on('$locationChangeStart', function() {
            routingService.checkOnUnavailablePageForAuthenticatedUser();

            $rootScope.currentLocation = globalService.getCurrentLocation();
            $rootScope.appLocationParts = globalService.getApplicationURLParts($scope.currentLocation);
            notification.clearAllNotifications();
        });

        $scope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl) {
            // Для отслеживания прямого копирования ссылки подтверждения, переданной по email, в адресную строку
            if (newUrl !== oldUrl && routingService.getSearchParams().params) {
                emailConfirmationCheckerService.parseEmailConfirmation();
            }
        });

        $scope.$on('loadUserInfo', loadUserData);
        $scope.$on('buyCoupon', loadUserData);
        $scope.$on('initialsUpdated', loadUserData);
        $scope.$on('cardReplaced', loadUserData);

        $scope.$on('requestsCompleted', function() {
            $scope.allRequestsCompleted = true;
        });

        $scope.$on('showGlobalLoader', function() {
            $scope.allRequestsCompleted = false;
        });

        $scope.$on('authInProcess', function() {
            $rootScope.authInProcess = true;
        });

        authService.loadAuth().finally(function() {
            emailConfirmationCheckerService.parseEmailConfirmation();
            socialActionCheckerService.parseSocialAction();
        });
    });
})(angular);
