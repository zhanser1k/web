(function(angular) {
    angular.module('lmxApp').factory('locationService', function($http, configurationService) {
        var locationApiUrl = configurationService.host + 'api/Location/';

        function getDataPromise(promise) {
            return promise.then(function(response) {
                return response.data;
            });
        }

        function getParams(startWith, count, inLocation) {
            return {
                params: angular.extend({
                    startWith: startWith || null,
                    count: count || 10,
                }, inLocation),
            };
        }

        function getCities(startWith, count) {
            return getDataPromise($http.get(locationApiUrl + 'Cities', getParams(startWith, count)));
        }
        function getStreets(startWith, city, count) {
            return getDataPromise($http.get(locationApiUrl + 'Streets', getParams(startWith, count, {city: city})));
        }
        function getHouses(startWith, street, count) {
            return getDataPromise($http.get(locationApiUrl + 'Houses', getParams(startWith, count, {street: street})));
        }

        return {
            getCities: getCities,
            getStreets: getStreets,
            getHouses: getHouses,
        };
    });
})(angular);
