(function(angular) {
    angular.module('lmxApp').factory('globalService', function(
        cardsService,
        configurationService,
        routingService,
        userService
    ) {
        function getCards() {
            return cardsService.getCards();
        }

        function getUserInfo() {
            return userService.getUserInfo();
        }

        function getCurrentLocation() {
            var currentLocation = routingService.getApplicationURL().replace(/\/?\?[^?]*$/, '');
            return currentLocation;
        }

        function getApplicationURLParts(location) {
            return routingService.getApplicationURLParts(location);
        }

        function changeLocation(path) {
            routingService.goTo(path);
        }

        function getUserBalance() {
            return userService.getBalance();
        }

        return {
            changeLocation: changeLocation,
            getApplicationURLParts: getApplicationURLParts,
            getCards: getCards,
            getCurrentLocation: getCurrentLocation,
            getUserBalance: getUserBalance,
            getUserInfo: getUserInfo,
        };
    });
})(angular);
