/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('permissionService', function($http, authService) {

        function hasPermission(permission){
            if (permission === undefined){
                throw new Error("permission is not defined");
            }

            if (typeof permission !== 'string'){
                throw new Error("permission is not string");
            }

            return authService.loggedIn() &&
                   angular.isArray(authService.authentication.permissions) &&
                   authService.authentication.permissions.contains(permission);
        }

        return {
            hasPermission: hasPermission
        };
    });
})(angular);