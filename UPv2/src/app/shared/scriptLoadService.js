/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('scriptLoadService', function () {
        var scriptLoadService = function () {
            function load(path, callback) {
                var el = document.createElement("script");
                el.onload = el.onreadystatechange = function () {
                    if (el.readyState && el.readyState !== "complete" &&
                        el.readyState !== "loaded") {
                        return;
                    }
                    el.onload = el.onreadystatechange = null;
                    if(angular.isFunction(callback)) {
                        callback();
                    }
                };
                el.async = true;
                el.src = path;
                document.getElementsByTagName('body')[0].appendChild(el);
            }

            return {
                load: load
            };
        };
        return scriptLoadService;
    });
})(angular);
