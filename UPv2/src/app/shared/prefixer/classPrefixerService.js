/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("classPrefixerService", function () {
        var prefix = 'lmx-';

        function getPrefixed(classString) {
            return classString.split(' ').reduce(function(filtered, item, index, array) {
                if (!item.startsWith(prefix) && item.length && array.indexOf(prefix + item) === -1) {
                    filtered.push(prefix + item);
                } else if (item.startsWith(prefix) || item === 'material-icons') {
                    filtered.push(item);
                }
                return filtered;
            }, []).join(' ');
        }

        return {
            prefix: prefix,
            getPrefixed: getPrefixed
        };
    });
})(angular);