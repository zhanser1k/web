/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("modalService", function ($uibModal, classPrefixerService) {

        var hasScrollWidth = false;

        function getScrollbarWidth() {
            var outer = document.createElement("div");
            outer.style.visibility = "hidden";
            outer.style.width = "100px";
            outer.style.msOverflowStyle = "scrollbar";

            document.body.appendChild(outer);

            var widthNoScroll = outer.offsetWidth;
            outer.style.overflow = "scroll";

            var inner = document.createElement("div");
            inner.style.width = "100%";
            outer.appendChild(inner);

            var widthWithScroll = inner.offsetWidth;

            outer.parentNode.removeChild(outer);

            return widthNoScroll - widthWithScroll;
        }

        function setPaddingForModal() {
            var style = document.createElement('style');
            style.type = 'text/css';
            style.innerHTML = 'body.modal-open { padding-right: ' + getScrollbarWidth() + 'px; }';
            document.getElementsByTagName('head')[0].appendChild(style);
        }

        function open(params) {
            if (!params.windowClass) {
                params.windowClass = classPrefixerService.prefix + 'modal';
            } else {
                params.windowClass = classPrefixerService.getPrefixed(params.windowClass + ' ' + classPrefixerService.prefix + 'modal');
            }

            if (!hasScrollWidth) {
                setPaddingForModal();
                hasScrollWidth = true;
            }

            return $uibModal.open(params);
        }

        return {
            open: open
        };
    });
})(angular);