/* eslint-disable */
(function (angular) {

    // При использовании внутри директивы для корректной работы необходимо элементу с ngSwitch добавить также ngIf, иначе scope - undefined
    // <div ng-switch="tempVar" ng-if=":: tempVar">
    angular.module('lmxApp').config(function($provide) {
        $provide.decorator('ngSwitchWhenDirective', function($delegate) {
            var directive = $delegate[0];
            var link = directive.link;

            directive.compile = function() {
                return function(scope, element, attrs) {
                    var expressionPattern = /^{{(.+)}}$/g;
                    var matchResult = expressionPattern.exec(attrs.ngSwitchWhen);

                    if (matchResult && matchResult[1]) {
                        var evalResult = scope.$eval(matchResult[1]);
                        if (angular.isDefined(evalResult)) {
                            attrs.$set('ngSwitchWhen', evalResult.toString());
                        }
                    }

                    link.apply(this, arguments);
                };
            };

            return $delegate;
        });
    });
})(angular);