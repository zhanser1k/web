/* eslint-disable */
(function () {

    Array.prototype.insertAt = function(index, items) {
        this.splice(index, 0, items);
        return this;
    };

    Array.prototype.remove = function(item) {
        var index = this.indexOf(item);
        this.removeAt(index);
        return this;
    };

    Array.prototype.removeAt = function(index) {
        if (index >= 0 && index < this.length){
            this.splice(index, 1);
        }

        return this;
    };

    Array.prototype.occurrencesCount = function(items) {

        var counter = 0;
        for (var i = items.length - 1; i >= 0; --i) {
            if (this.contains(items[i])) {
                counter++;
            }
        }
        return counter;
    };

    Array.prototype.contains = function (item) {
        return this.indexOf(item) !== -1;
    };

    Array.prototype.flatten = function () {
        var result = [];

        for (var i = 0; i < this.length; i++) {
            var item = this[i];
            if (angular.isArray(item)) {
                result = result.concat(item.flatten());
            } else {
                result.push(item);
            }
        }

        return result;
    };

    Array.prototype.fieldValues = function (field){
        var fieldList = [];
        for (var i = 0; i < this.length; i++) {
            fieldList.push(this[i][field]);
        }
        return fieldList;
    };

    Array.prototype.toDictionary = function (key, value) {
        if (this.length < 1) {
            return null;
        }

        if (arguments.length === 0) {
            key = 'id';
            value = 'name';
        }

        var dictionaryObject = {};

        for (var i = this.length - 1; i >= 0; --i) {
            dictionaryObject[this[i][key]] = angular.isDefined(value) ? this[i][value] : this[i];
        }
        return dictionaryObject;
    };

    Array.prototype.uniqueValues = function (field){
        var unique = [];
        if (field) {
            for (var i = 0; i < this.length; i++) {
                if(unique.contains(this[i][field])) {
                    continue;
                }
                unique.push(this[i][field]);
            }
        } else {
            unique = this.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
        }
        return unique;
    };

    Array.prototype.indexOfObjectByField = function (field, value){
        var index = null;
        for (var i = 0; i < this.length; i++) {
            if (this[i].hasOwnProperty(field)) {
                if (angular.isDefined(value)) {
                    if (this[i][field] === value) {
                        index = i;
                        break;
                    }
                } else {
                    index = i;
                    break;
                }
            }
        }
        return index;
    };

    Array.prototype.getObjectByField = function (field, value){
        return this[this.indexOfObjectByField(field, value)] || null;
    };

    Array.prototype.pushArray = function (concatArray){
        Array.prototype.push.apply(this, concatArray);
        return this;
    };

    if (!Array.prototype.find) {
        Array.prototype.find = function(predicate) {
            if (this === null) {
                throw new TypeError('Array.prototype.find called on null or undefined');
            }
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }
            var list = Object(this);
            var length = list.length >>> 0;
            var thisArg = arguments[1];
            var value;

            for (var i = 0; i < length; i++) {
                value = list[i];
                if (predicate.call(thisArg, value, i, list)) {
                    return value;
                }
            }
            return undefined;
        };
    }
})();