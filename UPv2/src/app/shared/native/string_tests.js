/* eslint-disable */

describe('string prototype', function() {
    it('should check occurrence', function () {
        var string = 'OneTwoThree';
        var containsFlag = string.contains('One');
        expect(containsFlag).toEqual(true);
    });
});