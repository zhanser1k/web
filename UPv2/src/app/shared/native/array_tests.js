/* eslint-disable */

describe('array prototype', function() {
    it('should insert an element at index', function() {
        var array = [1, 3];
        array.insertAt(1, 2);
        expect(array).toEqual([1, 2, 3]);
    });

    it('should remove an element', function() {
        var array = [1, 2, 3];
        array.remove(3);
        expect(array).toEqual([1, 2]);
    });

    it('should remove an element by index', function() {
        var array = [1, 2, 3];
        array.removeAt(1);
        expect(array).toEqual([1, 3]);
    });

    it('should count occurrences', function() {
        var array = [1, 2, 3];
        var occurrencesCount = array.occurrencesCount([1, 3]);
        expect(occurrencesCount).toEqual(2);
    });

    it('should check occurrence', function() {
        var array = [1, 2, 3];
        var containsFlag = array.contains(3);
        expect(containsFlag).toEqual(true);
    });

    it('should flattern an occurrence', function() {
        var array = [1, 2, [3, 4]];
        var flatArray = array.flatten();
        expect(flatArray).toEqual([1, 2, 3, 4]);
    });

    it('should extract an array of values by specified field from array of objects', function() {
        var array = [{
            "id": 1,
            "name": 2
        },{
            "id": 3,
            "name": 4
        }];
        expect(array.fieldValues('id')).toEqual([1, 3]);
    });

    it('should convert array of objects to object-dictionary', function() {
        var array = [{
            "id": 1,
            "name": 'name_1'
        },{
            "id": 3,
            "name": 'name_3'
        }];
        expect(array.toDictionary('id', 'name')).toEqual({1:'name_1', 3:'name_3'});
    });

    it('should extract an unique values from array', function() {
        var array = [1,5,2,3,5,1];
        expect(array.uniqueValues()).toEqual([1,5,2,3]);
    });

    it('should extract an unique values by field name from array of objects', function() {
        var array = [{
            "id": 1,
            "type": 'fisrtType'
        },{
            "id": 2,
            "type": 'fisrtType'
        },{
            "id": 3,
            "type": 'secondType'
        }];
        expect(array.uniqueValues('type')).toEqual(['fisrtType', 'secondType']);
    });

    it('should return an index of object in array by field name', function() {
        var array = [{
            "id": 1,
            "name": 'name_1'
        },{
            "id": 2,
            "type": 'fisrtType',
            "name": 'name_2'
        },{
            "id": 3,
            "type": 'secondType',
            "name": 'name_3'
        }];

        expect(array.indexOfObjectByField('type')).toEqual(1);
    });

    it('should return an index of object in array by field name with specific value', function() {
        var array = [{
            "id": 1,
            "name": 'name_1'
        },{
            "id": 2,
            "type": 'fisrtType',
            "name": 'name_2'
        },{
            "id": 3,
            "type": 'secondType',
            "name": 'name_3'
        }];

        expect(array.indexOfObjectByField('type', 'secondType')).toEqual(2);
    });

    it('should return an object in array by field name with specific value', function() {
        var array = [{
            "id": 1,
            "name": 'name_1'
        },{
            "id": 2,
            "type": 'fisrtType',
            "name": 'name_2'
        },{
            "id": 3,
            "type": 'secondType',
            "name": 'name_3'
        }];

        expect(array.getObjectByField('id', 2)).toEqual(array[1]);
    });

    it('should keep object binding after concat', function() {
        var array = [1,2,3];
        array.pushArray([4,5,6]);
        expect(array).toEqual([1,2,3,4,5,6]);
    });
});