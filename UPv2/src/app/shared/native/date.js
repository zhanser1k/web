/* eslint-disable */
(function () {

    Date.prototype.addDays = function (days) {
        var result = new Date(this);
        result.setDate(result.getDate() + days);
        return result;
    };

    Date.prototype.addMinutes = function (minutes) {
        return new Date(this.getTime() + minutes*60000);
    };

    Date.prototype.toUTCDate = function (unDo) {
        // метод взвращает значение даты после применения смещения времени
        // для обработки backend-методом, который не учитывает часовой пояс (использует только Томское время)
        var timeOffset = new Date().getTimezoneOffset();

        if (unDo) {
            timeOffset = -timeOffset;
        }
        return new Date(this.addMinutes(timeOffset));
    };


})(angular);