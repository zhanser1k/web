/* eslint-disable */
(function () {

    Number.maxIntValue = 2147483647;

    Number.isInteger = Number.isInteger || function(value){
            return (typeof value === 'number' && isFinite(value) && Math.floor(value) === value);
        };
})();