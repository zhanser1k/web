(function(angular) {
    angular.module('lmxApp').service('configurationService', function(
        $rootScope,
        $window,
        backendEnumsService,
        localStorageService,
        optionsService
    ) {
        var service = this;
        service.showcase = optionsService.get('showcaseHost') || ($window.config.showcase && $window.config.showcase.replace(/\/?$/, '/'));
        service.host = optionsService.get('host') || ($window.config.host && $window.config.host.replace(/\/?$/, '/'));
        service.options = optionsService.getOptions();
        service.templatesPath = $window.config.templatesPath;
        service.imagesPath = $window.config.imagesPath;

        // NOTE: значение enums формируется backend'ом в тесте ClientConstantGeneratorTest.GenerateEnums().
        service.enums = backendEnumsService.enums;

        // Кастомные параметры взаимодействия с АРМ
        service.userPortalDescriptionType = '700x350';

        if (localStorageService.get('e2e')) {
            $rootScope.$watch('$root.newOptions', function(newVal, oldVal) {
                if (newVal && newVal !== oldVal) {
                    service.options = Object.assign({}, service.options, typeof newVal === 'string' ? JSON.parse(newVal) : newVal);
                }
            });
        }

        if (localStorageService.get('newOptions')) {
            service.options = Object.assign({}, service.options, localStorageService.get('newOptions'));
        }
    });
})(angular);
