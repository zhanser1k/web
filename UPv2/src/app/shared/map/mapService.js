(function(angular) {
    angular.module('lmxApp').factory('mapService', function(
        $window,
        configurationService,
        scriptLoadService,
        toArrayFilter
    ) {
        var mapService = function() {
            var clusterer;
            var maxZoomForClusterer = 14;
            var myMap;
            var placemarks;
            var balloonTemplate;
            var scriptLoad = new scriptLoadService();

            var options = {
                version: '2.1.55',
                load: 'package.map,package.clusters',
            };
            var stateOptions = {
                center: [56.49771, 84.97437],
                zoom: 13,
                controls: [],
            };
            var markerOptions = {
                preset: 'islands#blueDotIcon',
                useCustomIcon: false,
                iconImageHref: '',
                iconImageSize: [35, 45],
                iconImageOffset: [-20, -47],
                useCustomBalloon: false,
                balloonCloseImage: 'assets/images/close.svg',
            };
            var clusterOptions = {
                preset: 'islands#invertedBlueClusterIcons',
                maxZoom: maxZoomForClusterer,
            };

            function updateData(data) {
                clusterer.removeAll();
                myMap.geoObjects.removeAll();

                var placemarkOptions = {
                    hideIconOnBalloonOpen: true,
                };

                if (markerOptions.useCustomIcon) {
                    placemarkOptions.iconLayout = 'default#image';
                    placemarkOptions.iconImageHref = markerOptions.iconImageHref;
                    placemarkOptions.iconImageSize = markerOptions.iconImageSize;
                    placemarkOptions.iconImageOffset = markerOptions.iconImageOffset;
                } else {
                    placemarkOptions.preset = markerOptions.preset;
                }

                if (markerOptions.useCustomBalloon) {
                    placemarkOptions.balloonLayout = balloonTemplate;
                }

                placemarks = {};
                data.forEach(function(item, index) {
                    if (item.location) {
                        var placemark = new ymaps.Placemark(
                            [item.location.latitude, item.location.longitude],
                            {
                                name: item.title,
                                location: item.location.description,
                                balloonContent: markerOptions.useCustomBalloon ? '' : ('<b>' + item.title + '</b><div>' + item.location.description + '</div>'),
                            },
                            placemarkOptions
                        );

                        placemarks[index] = placemark;
                    }
                });

                clusterer.add(toArrayFilter(placemarks, false));

                myMap.geoObjects.add(clusterer);

                myMap.setBounds(clusterer.getBounds(), {
                    checkZoomRange: true,
                    duration: 300,
                    preciseZoom: true,
                });
            }

            function mapInitialization(element, data) {
                var html = '<div class="lmx-balloon">' +
                    '<h2 class="lmx-title">{{ properties.name }}</h2>' +
                    '<img class="lmx-close" src="' + markerOptions.balloonCloseImage + '" alt="" />' +
                    '</div>';

                ymaps.ready(function() {
                    myMap = new ymaps.Map(element, stateOptions);

                    if (markerOptions.useCustomBalloon) {
                        balloonTemplate = ymaps.templateLayoutFactory.createClass(html, {
                            build: function() {
                                balloonTemplate.superclass.build.call(this);

                                var balloon = this.getParentElement().querySelector('.lmx-balloon');

                                balloon.style.marginTop = '-' + balloon.offsetHeight + 'px';
                                balloon.querySelector('.lmx-close').addEventListener('click', this.onCloseClick.bind(this));
                            },

                            clear: function() {
                                balloonTemplate.superclass.clear.call(this);
                            },

                            onCloseClick: function() {
                                this.events.fire('userclose');
                            },
                        });
                    }

                    if (clusterOptions.customClusterIconContentLayout) {
                        clusterOptions.clusterIconContentLayout = ymaps.templateLayoutFactory.createClass(clusterOptions.customClusterIconContentLayout);
                        delete clusterOptions.customClusterIconContentLayout;
                    }

                    clusterer = new ymaps.Clusterer(clusterOptions);

                    updateData(data);
                });
            }

            function setYmaps(element, data) {
                var mapOption = configurationService.options.map || {};

                angular.extend(options, mapOption.general);
                angular.extend(stateOptions, mapOption.state);
                angular.extend(markerOptions, mapOption.marker);
                angular.extend(clusterOptions, mapOption.cluster);

                if (!$window.ymaps) {
                    scriptLoad.load('https://api-maps.yandex.ru/' + options.version + '/?lang=ru_RU&load=' + options.load, function() {
                        mapInitialization(element, data);
                    });
                } else {
                    mapInitialization(element, data);
                }
            }

            function showBalloon(index) {
                if (placemarks[index]) {
                    var objectState = clusterer.getObjectState(placemarks[index]);

                    if (objectState.isClustered) {
                        myMap.setCenter(placemarks[index].geometry.getCoordinates(), maxZoomForClusterer + 1, {
                            checkZoomRange: true,
                        }).then(function() {
                            placemarks[index].balloon.open();
                        });
                    } else {
                        myMap.panTo(placemarks[index].geometry.getCoordinates(), {flying: false}).then(function() {
                            placemarks[index].balloon.open();
                        });
                    }
                }
            }

            return {
                setYmaps: setYmaps,
                showBalloon: showBalloon,
                updateData: updateData,
            };
        };
        return mapService;
    });
})(angular);
