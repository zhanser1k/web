/* eslint-disable */
(function (angular) {
	angular.module('lmxApp').service("backendEnumsService",function (){
	this.enums = {
  "permissionValue": {
    "authorized": "Authorized",
    "partner": "Partner",
    "siteAdmin": "SiteAdmin",
    "admin": "Admin",
    "activateCard": "ActivateCard",
    "cardEmission": "CardEmission",
    "cardPacking": "CardPacking",
    "manageUserPermissions": "ManageUserPermissions",
    "bankPayment": "BankPayment",
    "offerRead": "OfferRead",
    "offerEdit": "OfferEdit",
    "offerManage": "OfferManage",
    "answerManage": "AnswerManage",
    "partnerCatalogEdit": "PartnerCatalogEdit",
    "answersLoad": "AnswersLoad",
    "mailingEdit": "MailingEdit",
    "depositToCard": "DepositToCard",
    "targetGroupEdit": "TargetGroupEdit",
    "viewDepositsToCard": "ViewDepositsToCard",
    "sendCardPassword": "SendCardPassword",
    "blockAccount": "BlockAccount",
    "getHistoryAudit": "GetHistoryAudit",
    "getPurchaseHistory": "GetPurchaseHistory",
    "getCards": "GetCards",
    "getSmses": "GetSmses",
    "deleteCard": "DeleteCard",
    "recoverCard": "RecoverCard",
    "attachCard": "AttachCard",
    "getQuestions": "GetQuestions",
    "startMailingTask": "StartMailingTask",
    "cancelMailingTask": "CancelMailingTask",
    "changePhone": "ChangePhone",
    "browseHistoryAuditCodes": "BrowseHistoryAuditCodes",
    "viewReport": "ViewReport",
    "deviceEmulation": "DeviceEmulation",
    "cardIssuance": "CardIssuance",
    "replaceExternalCard": "ReplaceExternalCard",
    "blockCard": "BlockCard",
    "defaultOfferEdit": "DefaultOfferEdit",
    "defaultOfferManage": "DefaultOfferManage",
    "subscriptionsManage": "SubscriptionsManage",
    "managerPortalAuthorized": "ManagerPortalAuthorized",
    "partnerCatalogRead": "PartnerCatalogRead",
    "targetGroupRead": "TargetGroupRead",
    "partnerCatalogDecline": "PartnerCatalogDecline",
    "mailingRead": "MailingRead",
    "targetGroupCalculation": "TargetGroupCalculation",
    "operationConfirmCancel": "OperationConfirmCancel",
    "editReadonlyQuestions": "EditReadonlyQuestions",
    "editReadonlyAnswers": "EditReadonlyAnswers",
    "viewActiveOffersForCard": "ViewActiveOffersForCard",
    "announcementSpacesEdit": "AnnouncementSpacesEdit",
    "viewAnnouncements": "ViewAnnouncements",
    "editAnnouncements": "EditAnnouncements",
    "publishAnnouncements": "PublishAnnouncements",
    "registration": "Registration",
    "registerUsers": "RegisterUsers",
    "limitRulesView": "LimitRulesView",
    "limitRulesEdit": "LimitRulesEdit",
    "couponEmissionEdit": "CouponEmissionEdit",
    "couponEmissionView": "CouponEmissionView",
    "oAuthApplication": "OAuthApplication",
    "communicationOfferView": "CommunicationOfferView",
    "communicationOfferEdit": "CommunicationOfferEdit",
    "manageLifeTimeDefinitions": "ManageLifeTimeDefinitions",
    "getEMails": "GetEMails",
    "getPushes": "GetPushes",
    "partnerView": "PartnerView",
    "partnerEdit": "PartnerEdit",
    "manageGateways": "ManageGateways",
    "userAttributeView": "UserAttributeView",
    "userAttributeEdit": "UserAttributeEdit",
    "manageLocations": "ManageLocations",
    "showcaseCategoryEdit": "ShowcaseCategoryEdit",
    "showcaseCategoryView": "ShowcaseCategoryView",
    "socialNetworkEdit": "SocialNetworkEdit",
    "socialNetworkView": "SocialNetworkView",
    "showcaseCouponTypeEdit": "ShowcaseCouponTypeEdit",
    "showcaseCouponTypeView": "ShowcaseCouponTypeView",
    "showcasePartnerEdit": "ShowcasePartnerEdit",
    "showcasePartnerView": "ShowcasePartnerView",
    "couponEdit": "CouponEdit",
    "couponView": "CouponView",
    "announcementSpacesView": "AnnouncementSpacesView",
    "goodsGroupsView": "GoodsGroupsView",
    "goodsGroupsEdit": "GoodsGroupsEdit",
    "registrationCustomer": "RegistrationCustomer",
    "showcaseCouponView": "ShowcaseCouponView",
    "marketingCampaignsView": "MarketingCampaignsView",
    "marketingCampaignsEdit": "MarketingCampaignsEdit",
    "applicationUserAuthorize": "ApplicationUserAuthorize",
    "applicationProcessing": "ApplicationProcessing",
    "partnerPlanView": "PartnerPlanView",
    "partnerPlanEdit": "PartnerPlanEdit",
    "getPushableDevices": "GetPushableDevices",
    "getSocialAccounts": "GetSocialAccounts",
    "purchaseView": "PurchaseView",
    "marketingCampaignPlanView": "MarketingCampaignPlanView",
    "marketingCampaignPlanEdit": "MarketingCampaignPlanEdit"
  },
  "templateNodeType": {
    "header": "Header",
    "question": "Question"
  },
  "templateFieldType": {
    "string": "String",
    "dateTime": "DateTime",
    "radioButton": "RadioButton",
    "radioButtonString": "RadioButtonString",
    "checkbox": "Checkbox",
    "int32": "Int32"
  },
  "autoCompleteType": {
    "city": "City",
    "street": "Street",
    "house": "House"
  },
  "offerState": {
    "active": "Active",
    "commingSoon": "CommingSoon"
  },
  "communicationOfferTypeEnum": {
    "original": "Original",
    "personalGoods": "PersonalGoods",
    "personalOffer": "PersonalOffer"
  },
  "referrerCodeType": {
    "cardNumber": "CardNumber"
  }
};});
})(angular);