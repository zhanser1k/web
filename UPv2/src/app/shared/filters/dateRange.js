(function(angular) {
    /**
     * Форматирование периода времени "с ... [по ...]"
     */
    angular.module('lmxApp').filter('dateRange', function(
        dateFilter,
        translateFilter
    ) {
        return function(startDate, endDate) {
            if (angular.isUndefined(startDate)) {
                return startDate;
            }

            var result = [];
            var prepositions = {
                from: translateFilter('dateRange.from'),
                to: translateFilter('dateRange.to'),
                toTime: translateFilter('dateRange.toTime'),
                since: translateFilter('dateRange.since'),
            };

            if (!(startDate instanceof Date)) {
                startDate = new Date(startDate);
            }

            var startDay = startDate.getDate();
            var startDayString = dateFilter(startDate, 'd');

            var startMonth = startDate.getMonth();
            var startMonthString = dateFilter(startDate, 'MMMM');

            var startTime = dateFilter(startDate, 'HH:mm');

            if (endDate) {
                if (!(endDate instanceof Date)) {
                    endDate = new Date(endDate);
                }

                var endDay = endDate.getDate();
                var endDayString = dateFilter(endDate, 'd');

                var endMonth = endDate.getMonth();
                var endMonthString = dateFilter(endDate, 'MMMM');

                var endTime = dateFilter(endDate, 'HH:mm');

                if (startMonth === endMonth && startDay === endDay) {
                    result = [startDayString, startMonthString];

                    if (startTime !== '00:00' || endTime !== '23:59') {
                        result.pushArray([prepositions.from, startTime, prepositions.toTime, endTime]);
                    }
                } else {
                    result = [prepositions.from, startDayString, startMonthString, startTime, prepositions.to, endDayString, endMonthString, endTime];
                }
            } else {
                result = [prepositions.since, startDayString, startMonthString, startTime];
            }

            return result.join(' ');
        };
    });
})(angular);
