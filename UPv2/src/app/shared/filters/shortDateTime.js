/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('shortDateTime', function ($filter) {
        return function (value) {
            return $filter('date')(value, 'dd.MM.yyyy H:mm');
        };
    });

})(angular);