/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('lowerCamelCase', function () {
        return function(value) {
            //условие для совместимости с translateEnumFilter - переводит аббревиатуры в lowerCase
            if(value === value.toUpperCase()){
                return value.toLowerCase();
            }
            return value.charAt(0).toLowerCase() + value.slice(1);
        };
    });

})(angular);