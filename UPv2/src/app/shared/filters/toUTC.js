/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('toUTC', function () {
        return function (value, unDo) {
            var result = value;
            if (value instanceof Date) {
                result = value.toUTCDate(unDo);
            }
            return result;
        };
    });

})(angular);