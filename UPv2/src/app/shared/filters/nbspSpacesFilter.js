(function(angular) {
    angular.module('lmxApp').filter('nbspSpaces', function() {
        return function(input) {
            if (!input) {
                return input;
            }

            return input.replace(/ /g, '\u00A0');
        };
    });
})(angular);
