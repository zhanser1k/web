/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('shortDate', function ($filter) {
        return function (value) {
            return $filter('date')(value, 'dd.MM.yyyy');
        };
    });

})(angular);