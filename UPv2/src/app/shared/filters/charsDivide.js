(function(angular) {
    angular.module('lmxApp').filter('charsDivide', function() {
        return function(string, symbolsByGroup) {
            var regex = new RegExp('.{1,' + symbolsByGroup + '}', 'g');
            return string.toString().match(regex).join(' ');
        };
    });
})(angular);
