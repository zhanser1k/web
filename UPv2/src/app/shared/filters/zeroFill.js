/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('zeroFill', function () {
        //заполнение нулями пустых разрядов числа, общее количество разрядов указывается параметром фильтра
        return function (number, digits) {
            function getValidValue(value) {
                return isNaN(value) ? 0 : value;
            }

            var zerosString = '';
            var dot = '.';

            number = String(getValidValue(number));

            var digitsAfterComma = number.split(dot)[1] || dot;
            var emptyAfterComma = digitsAfterComma === dot;
            var zerosLength = emptyAfterComma ? 0 : digitsAfterComma.length;
            
            for(var i=1; i<=getValidValue(digits) - zerosLength; i++) {
                zerosString += 0;
            }
            return emptyAfterComma ?
                number + dot + zerosString :
                number + zerosString;
        };
    });

})(angular);