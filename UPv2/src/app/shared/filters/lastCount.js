/* eslint-disable */
(function (angular) {
    angular.module('lmxApp').filter('lastCount', function () {
        return function (string, show) {
            return string.substr(-show);
        };
    });
})(angular);