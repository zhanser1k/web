/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('shortTime', function ($filter) {
        return function (value) {
            return $filter('date')(value, 'H:mm');
        };
    });

})(angular);