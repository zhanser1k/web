/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('lastVisibleCount', function () {
        return function (string, show) {
            return string.replace(/.?/g, '*').slice(0, -show - 1) + string.slice(-show);
        };
    });

})(angular);