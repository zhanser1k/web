/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("userService", function ($http, configurationService) {

        var serviceUrl = configurationService.host + 'api/User/';

        function getUserInfo() {
            return $http.get(serviceUrl).then(function(response) {
                return response.data;
            });
        }

        function getBalance() {
            return $http.get(serviceUrl+ 'Balance').then(function (response) {
                return response.data;
            });
        }

        function getDetailedBalance() {
            return $http.get(serviceUrl + 'DetailedBalance').then(function(response) {
                return response.data;
            });
        }

        function getAttributes() {
            return $http.get(serviceUrl + 'Attributes').then(function(response) {
                var attributes = {};
                angular.forEach(response.data, function (attributeInfo) {
                    attributes[attributeInfo.info.logicalName] = attributeInfo;
                });

                return attributes;
            });
        }

        return {
            getUserInfo: getUserInfo,
            getDetailedBalance: getDetailedBalance,
            getBalance: getBalance,
            getAttributes: getAttributes
        };
    });
})(angular);