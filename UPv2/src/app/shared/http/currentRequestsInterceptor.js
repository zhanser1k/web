/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('currentRequestsInterceptor', function($q, $rootScope, $timeout) {
        var pendingRequests;

        function noPendingRequests() {
            return pendingRequests === 0;
        }

        function broadcastIfNoPendingRequests() {
            pendingRequests--;

            if (noPendingRequests()) {
                // Асинхронная проверка позволяет дожидаться выполнения всех запросов в цепочках запросов
                $timeout(function () {
                    if (noPendingRequests()) {
                        $rootScope.$broadcast('requestsCompleted');
                    }
                });
            }
        }
        return {
            request: function (config) {
                if (angular.isUndefined(pendingRequests)) {
                    $rootScope.$broadcast('showGlobalLoader');
                    pendingRequests = 0;
                }

                pendingRequests++;
                return config || $q.when(config);
            },
            response: function (response) {
                broadcastIfNoPendingRequests();
                return response || $q.when(response);
            },
            responseError: function (response) {
                broadcastIfNoPendingRequests();
                return $q.reject(response);
            }
        };
    });
})(angular);