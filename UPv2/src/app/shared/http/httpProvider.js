/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').config(function($httpProvider) {
        var isoDateRegex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/;

        function convertStringsToDates(data) {
            if (typeof data !== "object") {
                return data;
            }

            for (var key in data) {
                if (!data.hasOwnProperty(key)) {
                    continue;
                }

                var value = data[key];
                if (typeof value === "string") {
                    if (value.match(isoDateRegex)){
                        data[key] = new Date(value);
                    }
                } else {
                    convertStringsToDates(value);
                }
            }
        }

        $httpProvider.defaults.transformResponse.push(function (data) {
            if (data === "null") {
                data = null;
            }
            return data;
        });

        $httpProvider.defaults.transformResponse.push(function (data) {
            convertStringsToDates(data);
            return data;
        });

        $httpProvider.interceptors.push('notificationInterceptor');
        $httpProvider.interceptors.push('templateRequestInterceptor');
        $httpProvider.interceptors.push('authInterceptor');
        $httpProvider.interceptors.push('responseStateInterceptor');
        $httpProvider.interceptors.push('currentRequestsInterceptor');

        //disable IE ajax request caching
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get.Pragma = 'no-cache';
    });

})(angular);