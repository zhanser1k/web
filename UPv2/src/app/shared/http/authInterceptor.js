(function(angular) {
    angular.module('lmxApp').factory('authInterceptor', function($q, $location, $injector) {
        var authInterceptor = {};

        function request(config) {
            config.headers = config.headers || {};

            var authService = $injector.get('authService');

            var token = authService.authentication.token;
            if (token) {
                config.headers.Authorization = 'Bearer ' + token;
            }

            return config;
        }

        function responseError(rejection) {
            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                var routingService = $injector.get('routingService');

                rejection.data.error_description = 'Необходима авторизация';

                authService.clearAuthData();
                routingService.goToLogin(true);
            }
            return $q.reject(rejection);
        }

        authInterceptor.request = request;
        authInterceptor.responseError = responseError;

        return authInterceptor;
    });
})(angular);
