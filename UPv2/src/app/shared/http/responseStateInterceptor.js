/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('responseStateInterceptor', function ($q) {
        return {
            response: function (response) {
                if (angular.isDefined(response.data) && angular.isDefined(response.data.result)&& angular.isDefined(response.data.result.state)){
                    if (response.data.result.state === "Success"){
                        response.data = response.data.data;
                    } else {
                        return $q.reject(response);
                    }
                }

                return response;
            }
        };
    });
})(angular);