/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('templateRequestInterceptor', function (configurationService) {
        return {
            request: function (config) {
                var customTplPath = configurationService.templatesPath;
                if (customTplPath && config.url.match(/^app\/.*\.html$/)) {
                    config.url = customTplPath + config.url.replace(/^app\//, '');
                }
                return config;
            }
        };
    });
})(angular);