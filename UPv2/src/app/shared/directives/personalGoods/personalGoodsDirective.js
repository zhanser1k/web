(function(angular) {
    /**
     * Персональные товары
     * @name PersonalGoods
     * @description Отображение списка персональных предложений
     * @example <personal-goods></personal-goods>
     */
    angular.module('lmxApp').directive('personalGoods', function(
        $q,
        backendEnumsService,
        configurationService,
        offerService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/personalGoods/personalGoods.html',
            link: function($scope) {
                $scope.inProgress = true;
                $scope.activePersonalOffers = [];
                $scope.approvedPersonalOffers = [];
                $scope.futurePersonalOffers = [];

                var offerStates = backendEnumsService.enums.offerState;
                var offerType = backendEnumsService.enums.communicationOfferTypeEnum.personalGoods;

                var currentOffersPromise = offerService.getOffers(offerStates.active, offerType)
                    .then(function(response) {
                        var detailsPromises = {};
                        var personalGoodsOffers = {};

                        personalGoodsOffers = {};
                        response.forEach(function(offer) {
                            personalGoodsOffers[offer.id] = offer;
                            offer.approvedGoods = [];

                            detailsPromises[offer.id] = offerService.getOfferDetails(offer.id);
                        });

                        return $q.allSettled(detailsPromises).then(function(offersDetails) {
                            angular.forEach(offersDetails, function(offerDetails, key) {
                                if (offerDetails.success) {
                                    var offer = personalGoodsOffers[key];
                                    var offerGoods = offerDetails.value;

                                    offerGoods.forEach(function(goods) {
                                        if (goods.selected) {
                                            goods.imgUrl = goods.picture ? (configurationService.host + 'api/Files/' + goods.picture) : 'undefined';
                                            offer.approvedGoods.push(goods);
                                        }
                                    });
                                    offer.approvedGoodsCount = offer.approvedGoods.length;

                                    if (offer.approvedGoodsCount) {
                                        $scope.approvedPersonalOffers.push(offer);
                                    } else {
                                        $scope.activePersonalOffers.push(offer);
                                    }
                                }
                            });
                        });
                    });
                var futureOffersPromise = offerService.getOffers(offerStates.commingSoon, offerType)
                    .then(function(response) {
                        $scope.futurePersonalOffers = response;
                    });

                $q.all([currentOffersPromise, futureOffersPromise]).finally(function() {
                    $scope.inProgress = false;
                });
            },
        };
    });
})(angular);
