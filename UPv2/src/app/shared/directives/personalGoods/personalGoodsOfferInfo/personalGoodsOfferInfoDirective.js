(function(angular) {
    /**
     * Отображение информации о персональных товарах
     * @name PersonalGoodsOfferInfo
     * @description Отображение информации о персональных товарах. Используется как в общем списке перс. товаров, так и при просмотре конкретной акции.
     * @param {Object} offer - персональные товары
     * @param {html-attribute} [goods-preview] - признак отображения выбранных товаров
     * @example
     * <personal-goods-offer-info ng-repeat="personalGoodsOffer in personalGoods" offer="personalGoodsOffer"></personal-goods-offer-info>
     */
    angular.module('lmxApp').directive('personalGoodsOfferInfo', function($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                offer: '<',
            },
            templateUrl: 'app/shared/directives/personalGoods/personalGoodsOfferInfo/personalGoodsOfferInfo.html',

            link: function($scope, $element, $attrs) {
                $scope.isPreview = !$rootScope.appLocationParts[1];
                $scope.goodsPreview = angular.isDefined($attrs.goodsPreview);
            },
        };
    });
})(angular);
