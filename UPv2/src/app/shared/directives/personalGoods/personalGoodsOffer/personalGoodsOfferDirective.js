(function(angular) {
    /**
     * Отображение информации и списка товаров для акции персональных товаров
     * @name PersonalGoodsOffer
     * @description Отображение информации об акции персональных товаров с отображением включенных в акцию товарах и возможностью их выбора/подтверждения.
     * @param {int} offer-id - идентификатор акции персональных товаров
     * @example
     * <personal-goods-offer offer-id="personalGoodsOffers[0].id"></personal-goods-offer>
     */
    angular.module('lmxApp').directive('personalGoodsOffer', function(
        $q,
        offerService,
        configurationService,
        userAlertService,
        userConfirmationService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                offerId: '<',
            },
            templateUrl: 'app/shared/directives/personalGoods/personalGoodsOffer/personalGoodsOffer.html',
            link: function($scope) {
                $scope.offer = {};
                $scope.offerGoods = [];
                $scope.selectedGoods = [];
                $scope.availableGoodsCount = 0;
                $scope.selectableGoodsCount = 0;
                $scope.inProgress = true;
                $scope.offerDetailsInProgress = false;

                var approvedGoods = [];

                function processOfferDetails(offerGoods) {
                    $scope.offerGoods = offerGoods;

                    $scope.offerGoods.forEach(function(goods) {
                        if (goods.selected) {
                            approvedGoods.push(goods);
                        }

                        goods.imgUrl = goods.picture ? (configurationService.host + 'api/Files/' + goods.picture) : 'undefined';
                    });

                    $scope.availableGoodsCount = $scope.offer.attribute.maxGoodsCount - approvedGoods.length;
                    $scope.selectableGoodsCount = $scope.availableGoodsCount;
                }

                function loadOfferData() {
                    var offerPromises = {
                        offer: offerService.getOfferById($scope.offerId),
                        offerDetails: offerService.getOfferDetails($scope.offerId),
                    };

                    $scope.inProgress = true;

                    return $q.all(offerPromises).then(function(response) {
                        $scope.offer = response.offer;
                        processOfferDetails(response.offerDetails);
                    }).finally(function() {
                        $scope.inProgress = false;
                    });
                }

                $scope.changeSelectedGoods = function(goods) {
                    if (!goods.selected) {
                        if ($scope.selectedGoods.contains(goods)) {
                            $scope.selectedGoods.remove(goods);
                        } else if ($scope.selectableGoodsCount > 0) {
                            $scope.selectedGoods.push(goods);
                        } else {
                            userAlertService('Выбрано максимально допустимое количество товаров');
                        }
                    } else {
                        userAlertService('Изменить список подтверждённых товаров невозможно');
                    }
                };

                $scope.approveSelectedGoods = function() {
                    if ($scope.selectedGoods.length) {
                        userConfirmationService('Вы\u00A0уверены, что хотите подтвердить выбранные товары? Изменить список подтверждённых товаров невозможно', 'info')
                            .then(function() {
                                $scope.offerDetailsInProgress = true;

                                offerService.approvePersonalGoods($scope.offer.attribute.id, $scope.selectedGoods.fieldValues('goodsId'))
                                    .then(function() {
                                        $scope.selectedGoods.length = 0;
                                        approvedGoods.length = 0;
                                        return offerService.getOfferDetails($scope.offerId).then(processOfferDetails);
                                    }).finally(function() {
                                        $scope.offerDetailsInProgress = false;
                                    });
                            });
                    } else {
                        userAlertService('Не выбран ни один товар');
                    }
                };

                $scope.$watch('selectedGoods.length', function(newLength) {
                    $scope.selectableGoodsCount = $scope.availableGoodsCount - newLength;
                });

                loadOfferData();
            },
        };
    });
})(angular);
