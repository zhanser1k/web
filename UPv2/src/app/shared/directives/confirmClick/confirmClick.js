(function(angular) {
    angular.module('lmxApp').directive('confirmClick', function(translateFilter, userConfirmationService) {
        return {
            link: function(scope, element, attr) {
                var defaultMessage = translateFilter('areYouSure');
                var message = attr.confirmClick || defaultMessage;

                // Зачем добавлять свой атрибут для confirmedClick, а не использовать ngClick - http://stackoverflow.com/a/16211108 - читать комментарии.
                // директива не имеет возможности работать в режиме перекрывания других ng-click
                // исправляется использованием terminal: true; но тогда элемент игнорирует все прочие директивы.
                var clickAction = attr.confirmedClick;
                element.bind('click', function() {
                    userConfirmationService(message).then(function() {
                        scope.$eval(clickAction);
                    });
                });
            },
        };
    });
})(angular);
