/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('options', function (
        $window,
        classPrefixerService,
        configurationService,
        localStorageService,
        notification,
        userAlertService
    ){
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/options/options.html',

            link: function link($scope) {
                $scope.options = Object.assign({}, configurationService.options);
                $scope.api = configurationService.host;
                $scope.showcaseApi = configurationService.showcase;

                $scope.saveOptions = function () {
                    localStorageService.set("optionsCurrent", $scope.options);
                    configurationService.options = Object.assign({}, $scope.options);
                    notification.addMessage("Настройки успешно обновлены");
                };

                $scope.saveConfiguration = function(name, value) {
                    localStorageService.set(name, value);
                    userAlertService('Конфигурация ' + name + ' обновлена.<br>Cтраница будет перезагружена для применения настроек.', 'success')
                        .finally(function(){
                            $window.location.reload();
                    });
                };

                $scope.getType = function (value) {
                    switch(typeof value) {
                        case 'boolean':
                            return 'checkbox';
                        case 'object':
                            return 'textarea';
                        default:
                            return 'text';
                    }
                };

                $scope.getClass = function (value) {
                    return (typeof value === "boolean") ? classPrefixerService.prefix + "filled-in" : "";
                };
            }
        };
    });
})(angular);
