/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('optionsService', function (
        $window,
        localStorageService
    ) {
        function get(paramName) {
            return localStorageService.get(paramName);
        }

        function getOptions() {
            var options = $window.config.options;
            var optionsCurrent = localStorageService.get("optionsCurrent");
            var optionsOrigin = localStorageService.get("optionsOrigin");

            if (optionsOrigin === null || !_.isEqual(options, optionsOrigin)) {
                localStorageService.set("optionsCurrent", options);
                localStorageService.set("optionsOrigin", options);
                return options;
            } else {
                return Object.assign({}, options, optionsCurrent);
            }
        }

        return {
            get: get,
            getOptions: getOptions
        };
    });

})(angular);
