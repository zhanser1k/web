(function(angular) {
    angular.module('lmxApp').directive('tenderOffer', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/registration/tenderOffer.html',
        };
    });
})(angular);
