/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('registrationService', function (
        $http,
        $q,
        configurationService,
        authService,
        backendEnumsService
    ) {
        var apiHost = configurationService.host;
        
        var area;
        function setArea(serviceArea) {
            area = serviceArea;
        }

        function beginRegistration(regData) {
            authService.clearAuthData();
            regData.login = correctPhone(regData.login);
            return $http.post(apiHost + 'api/Registration/BeginRegistration', regData, {area:area}).then(function (response) {
                return response.data;
            });
        }

        function tryFinishRegistration() {
            return $http.post(apiHost + 'api/Registration/TryFinishRegistration', null, {area:area}).then(function (response){
                return response.data;
            });
        }

        function getActions() {
            return $http.get(apiHost + 'api/user/actions', {area:area}).then(function (response) {
                return response.data;
            });
        }

        function acceptTenderOffer() {
            return $http.post(apiHost + 'api/User/AcceptTenderOffer', null, {area:area});
        }

        function sendQuestions(answers) {
            var model = {
                answers: answers
            };
            return $http.post(apiHost + 'api/User/Answers', model, {area:area});
        }

        function getCityList(str) {
            var model = {
                startWith: str || null,
                count: 10
            };
            return $http.get(apiHost + 'api/Location/Cities', { params: model, area:area });
        }

        function getStreetList(city, str) {
            var model = {
                startWith: str || null,
                city: city || null,
                count: 10
            };
            return $http.get(apiHost + 'api/Location/Streets', { params: model, area:area });
        }

        function getHouseList(street, str) {
            var model = {
                startWith: str || 0,
                street: street || null,
                count: 10
            };
            return $http.get(apiHost + 'api/Location/Houses', { params: model, area:area });
        }

        function getTenderOffer(isHtml) {
            var fileId = isHtml ? configurationService.options.offerHtmlFileId : configurationService.options.offerPdfFileId;
            return $http.get(apiHost + 'api/files/' + fileId, {area:area}).then(function (response) {
                if (isHtml && !response.headers('content-type').contains('html')) {
                    return $q.reject({ incorrectFormat: true });
                }

                return response.data;
            });
        }

        function getAgreementPdf() {
            return $http.get(apiHost + 'api/files/' + configurationService.options.opdAgreementFileId, {area:area}).then(function (response) {
                return response.data;
            });
        }

        function getReferrerInfo() {
            return $http.get(apiHost + 'api/User/Referrer', {area: area}).then(function (response) {
                return response.data;
            });
        }

        function sendReferrerCardNumber(cardNumber) {
            var model = {
                type: backendEnumsService.enums.referrerCodeType.cardNumber,
                code: cardNumber
            };
            return $http.put(apiHost + 'api/User/Referrer', model, {area:area});
        }

        function correctPhone(phone) {
            // TODO написать директиву авторизации
            if (isNaN(phone.replace(/\s|\(|\)|\-/g,''))) {
                return phone;
            }
            var phoneRegex = new RegExp('^\\+7\\d{10}$');
            if (phoneRegex.test(phone)) {
                phone = phone.substring(1);
            }
            switch (phone.length) {
                case 11:
                    if (phone.charAt(0) === '8') {
                        phone = '7' + phone.slice(1);
                    }
                    break;
                case 10:
                    phone = '7' + phone;
                    break;
            }
            return phone;
        }

        return {
            setArea: setArea,
            acceptTenderOffer: acceptTenderOffer,
            beginRegistration: beginRegistration,
            correctPhone: correctPhone,
            getActions: getActions,
            getCityList: getCityList,
            getHouseList: getHouseList,
            getStreetList: getStreetList,
            getTenderOffer: getTenderOffer,
            sendQuestions: sendQuestions,
            tryFinishRegistration: tryFinishRegistration,
            getAgreementPdf: getAgreementPdf,
            getReferrerInfo: getReferrerInfo,
            sendReferrerCardNumber: sendReferrerCardNumber
        };
    });

})(angular);