(function(angular) {
    angular.module('lmxApp').directive('cardAssignment', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/registration/cardAssignment.html',
        };
    });
})(angular);
