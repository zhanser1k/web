(function(angular) {
    angular.module('lmxApp').directive('registrationStart', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/registration/registrationStart.html',
        };
    });
})(angular);
