(function(angular) {
    /**
     * Регистрация
     * @name Registration
     * @description Отображает форму регистрации пользователя.
     * @example
     * <registration></registration>
     * @version 1.1
     */
    angular.module('lmxApp').directive('registration', function(
        $q,
        $rootScope,
        authService,
        cardsService,
        configurationService,
        emailConfirmationCheckerService,
        localStorageService,
        notification,
        registrationService,
        routingService,
        shareAuthLoginToRegistration,
        translateFilter,
        userAlertService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/registration/registration.html',

            link: function link($scope, element, attrs) {
                var area = 'registration';
                registrationService.setArea(area);
                cardsService.setArea(area);

                // Признак нахождения внутри документа для отслеживания нажатия кнопки "Назад"
                var isInsideDocument = true;
                function setIsInside() {
                    isInsideDocument = true;
                }
                function setIsOutside() {
                    isInsideDocument = false;
                }
                document.addEventListener('mouseover', setIsInside);
                document.addEventListener('mouseleave', setIsOutside);

                $scope.apiHost = configurationService.host;
                $scope.opdAgreementFileId = configurationService.options.opdAgreementFileId;
                $scope.isRegistrationStarted = authService.isRegistrationStarted();
                $scope.acceptTenderOfferByCheck = configurationService.options.acceptTenderOfferByCheck;
                $scope.ignoreDocsFetchingErrors = configurationService.options.ignoreDocsFetchingErrors;
                $scope.offerPdfFileId = configurationService.options.offerPdfFileId;
                $scope.referralRegistration = configurationService.options.referralRegistration;
                $scope.isOfferAndAgreementLoaded = undefined;
                $scope.referrerCardNumber = {
                    value: '',
                };
                $scope.registrationCompleted = false;
                $scope.loader = {
                    cities: false,
                    streets: false,
                    houses: false,
                };
                $scope.emailSkippable = angular.isDefined(attrs.forceEmailStep);
                $scope.tokenForSocialRegistration = localStorageService.get('tokenForSocialRegistration');

                var actions = {};
                var displayForceEmailStep = false;

                function finishRegistration() {
                    var isRedirectToRegistration = configurationService.options.redirectUrlOnRegistrationComplete === '#registration';
                    $scope.inProgressFinishRegistration = true;

                    registrationService.tryFinishRegistration().then(function(response) {
                        if (isRedirectToRegistration) {
                            $scope.isRegistrationStarted = false;
                        }

                        authService.removeRegistrationToken();

                        if (configurationService.options.authorizeOnRegistrationComplete) {
                            authService.setAuth(response.authToken);
                        } else {
                            authService.clearAuthData();
                        }

                        $scope.registrationCompleted = true;
                    }).finally(function() {
                        $scope.inProgressFinishRegistration = false;
                    });
                }

                function loadActions() {
                    authService.loadAuth();

                    $scope.inProgress = true;

                    return registrationService.getActions().then(function(response) {
                        var currentStep;
                        actions = response.actions;

                        displayForceEmailStep = (
                            $scope.emailSkippable &&
                            !localStorageService.get('forceEmailStepSkipped') &&
                            !localStorageService.get('forceEmailStepHasCode')
                        );

                        for (var i = 0; i < actions.length; i++) {
                            if (!actions[i].isDone) {
                                if (actions[i].userActionType === 'Questions' && displayForceEmailStep) {
                                    currentStep = 'ChangeEmail';
                                } else {
                                    currentStep = actions[i].userActionType;
                                }
                                break;
                            }
                        }

                        $scope.currentStep = currentStep;
                        if (!$scope.currentStep) {
                            finishRegistration();
                        }
                    }).catch(function() {
                        $scope.currentStep = null;
                    }).finally(function() {
                        $scope.inProgress = false;
                    });
                }

                function loadTenderOfferAndAgreementFiles() {
                    var promises = {
                        tenderOffer: registrationService.getTenderOffer(!$scope.acceptTenderOfferByCheck),
                        agreement: registrationService.getAgreementPdf(),
                    };

                    $scope.inProgress = true;

                    $q.allSettled(promises).then(function(responses) {
                        if (responses.tenderOffer.success) {
                            $scope.tenderOfferFile = responses.tenderOffer.value;
                            if (!$scope.acceptTenderOfferByCheck) {
                                // TODO ломает скрипт, если на экране нет .scroller-wrapper > .scroller
                                var scrollerWrapper = element[0].querySelector('.scroller-wrapper');
                                if (scrollerWrapper && scrollerWrapper.querySelector('.scroller')) {
                                    baron({
                                        root: scrollerWrapper,
                                        scroller: '.scroller',
                                        barOnCls: '_scrollbar',
                                        track: '.scroller-track',
                                        bar: '.scroller-bar',
                                        cssGuru: true,
                                        direction: 'v',
                                    });
                                }
                            }
                        } else if (responses.tenderOffer.error.status !== 200) {
                            notification.addCritical('Ошибка получения файла оферты.', area);
                        } else if (responses.tenderOffer.error.incorrectFormat) {
                            notification.addCritical('Некорректный формат файла оферты.', area);
                        }

                        if (!responses.agreement.success && responses.agreement.error.status !== 200) {
                            notification.addCritical('Ошибка получения файла согласия на обработку персональных данных.', area);
                        }

                        $scope.isOfferAndAgreementLoaded = responses.tenderOffer.success && responses.agreement.success;
                    }).finally(function() {
                        if ($scope.ignoreDocsFetchingErrors) {
                            $scope.isOfferAndAgreementLoaded = true;
                        }
                        $scope.inProgress = false;
                    });
                }

                function loadStepsInfo() {
                    loadActions().then(function() {
                        if ($scope.currentStep === 'AcceptTenderOffer') {
                            loadTenderOfferAndAgreementFiles();
                        } else if ($scope.currentStep === 'AssignCard') {
                            $scope.inProgress = true;
                            cardsService.getVirtualCardInfo().then(function(response) {
                                $scope.isVirtualCardEmissionAllowed = response.isVirtualCardEmissionAllowed;
                            }).finally(function() {
                                $scope.inProgress = false;
                            });
                        }
                    });
                }

                if ($scope.isRegistrationStarted) {
                    loadStepsInfo();
                } else if ($scope.acceptTenderOfferByCheck) {
                    loadTenderOfferAndAgreementFiles();
                }

                $scope.continue = function() {
                    routingService.redirectTo(configurationService.options.redirectUrlOnRegistrationComplete);
                };

                $scope.acceptTenderOffer = function() {
                    $scope.stepInProgress = true;
                    return registrationService.acceptTenderOffer().then(function() {
                        loadActions();
                    }).finally(function() {
                        $scope.stepInProgress = false;
                    });
                };

                $scope.cancel = function() {
                    $scope.currentStep = null;
                    authService.clearAuthData();
                    routingService.goToLogin();
                };

                $scope.cardModel = {
                    cardNumber: '',
                };

                $scope.assignCard = function() {
                    $scope.stepInProgress = true;
                    cardsService.setCard($scope.cardModel.cardNumber).then(function() {
                        loadActions();
                    }).finally(function() {
                        $scope.stepInProgress = false;
                    });
                };

                $scope.emitVirtualCard = function() {
                    $scope.stepInProgress = true;
                    cardsService.emitVirtualCard().finally(function() {
                        loadActions();
                        $scope.stepInProgress = false;
                    });
                };

                $scope.$on('questionnaireIsSet', loadStepsInfo);
                $scope.$on('emailIsSet', loadStepsInfo);
                $scope.$on('phoneIsSet', loadStepsInfo);
                $scope.$on('passwordIsSet', loadStepsInfo);
                var locationChangeListener = $rootScope.$on('$locationChangeStart', function(event, newUrl) {
                    var isEmailConfirmation = (new RegExp('#/' + emailConfirmationCheckerService.confirmationKeys.email + '?')).test(newUrl);

                    // Если регистрация начата, и смена location инициирована вне документа -> переход на страницу входа
                    if ($scope.isRegistrationStarted && !isInsideDocument && !isEmailConfirmation) {
                        window.stop();

                        $scope.registrationCompleted = false;
                        $scope.isRegistrationStarted = false;
                        $scope.currentStep = null;

                        authService.removeRegistrationToken();
                        authService.clearAuthData();

                        routingService.goToLogin();
                    }
                });
                $scope.$on('$destroy', function() {
                    document.removeEventListener('mouseover', setIsInside);
                    document.removeEventListener('mouseleave', setIsOutside);
                    locationChangeListener();
                });

                function resetRegistrationForm() {
                    $scope.registrationData = {
                        password: '',
                    };

                    // Если установлен loginAuth и
                    // - одновременно установлены опции forceRegistrationStartOnLoginAttempt и acceptTenderOfferByCheck ->
                    //   не инициируем регистрацию (может не быть html-файла оферты, просто заполняем логин (независимо от наличия опции shareAuthLoginToRegistration)
                    // - установлена shareAuthLoginToRegistration -> заполняем логин
                    if ((configurationService.options.shareAuthLoginToRegistration || (configurationService.options.forceRegistrationStartOnLoginAttempt && $scope.acceptTenderOfferByCheck)) &&
                        shareAuthLoginToRegistration.loginAuth) {
                        $scope.registrationData.login = shareAuthLoginToRegistration.loginAuth;
                        shareAuthLoginToRegistration.clearLogin();
                    }
                }

                function startRegistration() {
                    $scope.isRegistrationStarted = true;
                    loadStepsInfo();
                    $scope.passwordRequired = false;
                }

                function checkOfferAndContinue() {
                    var acceptOfferPromise;
                    if ($scope.acceptTenderOfferByCheck) {
                        acceptOfferPromise = $scope.acceptTenderOffer();
                    }
                    return $q.when(acceptOfferPromise).then(function() {
                        startRegistration();
                    });
                }

                resetRegistrationForm();

                function checkOfferAndReferralSystem() {
                    var referrerPromise;

                    if ($scope.referralRegistration && $scope.referrerCardNumber.value) {
                        referrerPromise = registrationService.getReferrerInfo().then(function() {
                            return userAlertService(translateFilter('registration.referrerCodeMessage'), 'info');
                        }).catch(function() {
                            return registrationService.sendReferrerCardNumber($scope.referrerCardNumber.value);
                        });
                    }

                    return $q.when(referrerPromise).then(checkOfferAndContinue).catch(function() {
                        authService.removeRegistrationToken();
                        authService.clearAuthData();
                    });
                }

                $scope.registration = function(registrationForm) {
                    if ($scope.tokenForSocialRegistration) {
                        authService.setRegistrationToken($scope.tokenForSocialRegistration);
                        checkOfferAndReferralSystem();
                    } else {
                        $scope.authInProgress = true;
                        registrationService.beginRegistration(angular.copy($scope.registrationData)).then(function(response) {
                            authService.setRegistrationToken(response.authToken);
                            return checkOfferAndReferralSystem();
                        }, function(error) {
                            if (error.data.data.state === 'PasswordRequired') {
                                $scope.passwordRequired = true;
                                registrationForm.$submitted = false;
                            }
                        }).finally(function() {
                            $scope.authInProgress = false;
                        });
                    }
                };

                if ($scope.tokenForSocialRegistration && !$scope.referralRegistration && !$scope.acceptTenderOfferByCheck) {
                    authService.setRegistrationToken($scope.tokenForSocialRegistration);
                    startRegistration();
                }
            },
        };
    });
})(angular);
