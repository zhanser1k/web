(function(angular) {
    angular.module('lmxApp').directive('registrationFinish', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/registration/registrationFinish.html',
        };
    });
})(angular);
