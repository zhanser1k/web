/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("brandService", function ($http, configurationService) {
        var brandsApiUri = 'api/Brands/';
        function getBrands() {
            return $http.get(configurationService.host + brandsApiUri).then(function (response) {
                return response.data;
            });
        }

        function getBrandById(brandId) {
            return $http.get(configurationService.host + brandsApiUri + brandId).then(function (response) {
                populateImagesUrlFromDescription(response.data);
                return response.data;
            });
        }

        /**
         * Расширение объекта свойствами с url картинок на основе значении соответствующих полей description из ARM
         * @param brand
         */
        function populateImagesUrlFromDescription(brand) {
            var img = brand.images.getObjectByField("description", configurationService.userPortalDescriptionType);
            var marker = brand.images.getObjectByField("description", "marker");
            brand.imgUrl = img ? (configurationService.host + 'api/Files/' + img.fileId) : 'undefined';
            if (marker) {
                brand.markerUrl = configurationService.host + 'api/Files/' + marker.fileId;
            }
        }

        function getBrandMerchants(brandId) {
            return $http.get(configurationService.host + 'api/Merchants?brandId=' + brandId).then(function (response) {
                return response.data;
            });
        }

        return {
            getBrands: getBrands,
            getBrandById : getBrandById,
            getBrandMerchants: getBrandMerchants,
            populateImagesUrlFromDescription: populateImagesUrlFromDescription
        };
    });
})(angular);