/* eslint-disable */
(function (angular) {
    /**
     * Описание брендов
     * @name Brands
     * @description Отображает список брендов c описанием.
     * @example
     * <brands></brands>
     * @version 1.1
     */
    angular.module('lmxApp').directive('brands', function (
        brandService,
        configurationService,
        modalService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/brands/brands.html',

            link: function link($scope) {
                $scope.brands = [];
                $scope.inProgress = true;

                brandService.getBrands().then(function (response) {
                    $scope.brands = response;
                    $scope.brands.forEach(function (brand) {
                        brandService.populateImagesUrlFromDescription(brand);
                    });
                }).finally(function() {
                    $scope.inProgress = false;
                });

                $scope.openBrandViewModal = function (brand) {
                    modalService.open({
                        size: 'lg',
                        templateUrl: 'app/shared/directives/brands/modal/brandViewModal.html',
                        controller: 'brandViewModalController',
                        windowClass: 'modal-brand',
                        resolve: {
                            brand: brand
                        }
                    });
                };
            }
        };
    });
})(angular);