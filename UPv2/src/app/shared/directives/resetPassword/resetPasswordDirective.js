(function(angular) {
    /**
     * Сброс пароля
     * @name ResetPassword
     * @description Отображает форму сброса пароля от личного кабинета пользователя.
     * @example
     * <reset-password></reset-password>
     * @version 1.1
     */
    angular.module('lmxApp').directive('resetPassword', function(
        $http,
        $translate,
        authService,
        configurationService,
        resetPasswordService,
        routingService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/resetPassword/resetPassword.html',

            link: function link($scope) {
                resetPasswordService.setArea('resetPassword');

                $scope.forms = {};

                $scope.model = {
                    identity: '',
                    confirmCode: '',
                    newPassword: '',
                    repeatPassword: '',
                };

                $scope.steps = {
                    start: true,
                    confirm: false,
                };

                function nextStep() {
                    $scope.steps.start = false;
                    $scope.steps.confirm = true;
                }

                $scope.start = function() {
                    $scope.inProgress = true;
                    resetPasswordService.startResetPassword($scope.model.identity).then(function() {
                        nextStep();
                    }).finally(function() {
                        $scope.inProgress = false;
                    });
                };

                $scope.resetPasswordConfirmationData = resetPasswordService.getResetPasswordConfirmationData();
                if ($scope.resetPasswordConfirmationData) {
                    $scope.model.identity = $scope.resetPasswordConfirmationData.email;
                    $scope.start();
                    $scope.model.confirmCode = $scope.resetPasswordConfirmationData.code;
                }

                $scope.isResetPasswordConfirmationCodeHidden = function() {
                    var confirmCodeForm = $scope.forms.resetPasswordConfirm.confirmcode;
                    var isHidden = $scope.resetPasswordConfirmationData && !!confirmCodeForm && !Object.keys(confirmCodeForm.$error).length;

                    return isHidden;
                };

                $scope.confirm = function() {
                    $scope.inProgress = true;
                    resetPasswordService.confirmResetPassword($scope.model).then(function(response) {
                        authService.setAuth(response.data);
                        routingService.redirectTo(configurationService.options.redirectUrlOnLogin || '#history');
                    }, function() {
                        $scope.inProgress = false;
                    }).finally(function() {
                        $scope.resetPasswordConfirmationData = undefined;
                        resetPasswordService.setResetPasswordConfirmationData();
                    });
                };

                $scope.cancel = function() {
                    resetPasswordService.setResetPasswordConfirmationData();
                    routingService.goTo('login');
                };
            },
        };
    });
})(angular);
