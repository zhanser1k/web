(function(angular) {
    angular.module('lmxApp').factory('resetPasswordService', function($http, configurationService, registrationService) {
        var apiUrl = configurationService.host + 'api/ResetPassword/';

        var area;
        var resetPasswordData;

        function setArea(serviceArea) {
            area = serviceArea;
        }

        function startResetPassword(identity) {
            return $http.post(apiUrl + 'Start', {notifierIdentity: registrationService.correctPhone(identity)}, {area: area}).then(function(response) {
                return response;
            });
        }

        function confirmResetPassword(data) {
            return $http.post(apiUrl + 'Confirm', {
                notifierIdentity: registrationService.correctPhone(data.identity),
                confirmCode: data.confirmCode,
                newPassword: data.newPassword,
            }, {
                area: area,
            }).then(function(response) {
                return response;
            });
        }

        function setResetPasswordConfirmationData(data) {
            resetPasswordData = data;
        }

        function getResetPasswordConfirmationData() {
            return resetPasswordData;
        }

        return {
            setArea: setArea,
            startResetPassword: startResetPassword,
            confirmResetPassword: confirmResetPassword,
            getResetPasswordConfirmationData: getResetPasswordConfirmationData,
            setResetPasswordConfirmationData: setResetPasswordConfirmationData,

        };
    });
})(angular);
