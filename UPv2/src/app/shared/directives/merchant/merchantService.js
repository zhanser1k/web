/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('merchantService', function (
        $http,
        configurationService
    ) {
        var apiHost = configurationService.host + 'api/Merchants';

        /**
         * Получение магазинов
         */
        function getMerchants() {
            return $http.get(apiHost).then(function (response) {
                return response.data;
            });
        }

        return {
            getMerchants: getMerchants
        };
    });

})(angular);