/* eslint-disable */
(function (angular) {
    /**
     * Описание магазинов
     * @name Merchant
     * @description Отображает информацию о магазинах.
     * @example
     * <merchant></merchant>
     * @version 1.1
     */
    angular.module('lmxApp').directive('merchant', function (
        mapService,
        merchantService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/merchant/merchant.html',

            link: function link($scope, element) {

                $scope.isProgress = true;

                merchantService.getMerchants().then(function(response) {
                    $scope.merchants = response;
                }).finally(function() {
                    $scope.isProgress = false;
                });

                $scope.showMerchantOnMap = function(index) {
                    $scope.$broadcast('openBalloon', index);
                };
                var scrollerWrapper = element[0].querySelector('.scroller-wrapper');
                if (scrollerWrapper && scrollerWrapper.querySelector('.scroller')) {
                    baron({
                        root: scrollerWrapper,
                        scroller: '.scroller',
                        barOnCls: '_scrollbar',
                        track: '.scroller-track',
                        bar: '.scroller-bar',
                        cssGuru: true,
                        direction: 'v',
                    });
                }
            }
        };
    });
})(angular);