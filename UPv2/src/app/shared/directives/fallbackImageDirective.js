(function(angular) {
    /**
     * Замена атрибута src тега <img> при отсутствующем изображении
     * @name FallbackImage
     * @description Подменяет отсутствующее изображение-источник <img> на noimage-изображение
     * @param {string} [fallback-title] - Подпись при отсутствующем изображении
     * @param {string} [fallback-image = 'noimage.png'] - Название/путь до изображения-замены (относительно конфигурации imagesPath)
     * @example <img src="assets/images/broken_image.png" fallback-image fallback-title="Изображение отсутствует">
     */
    angular.module('lmxApp').directive('fallbackImage', function($log, configurationService) {
        return {
            restrict: 'A',
            scope: {
                fallbackTitle: '@',
                fallbackImage: '@',
            },
            link: function($scope, $element) {
                var imageElement = $element[0];

                if (imageElement.tagName.toLowerCase() !== 'img') {
                    $log.warn('fallbackImage directive can be used with <img> tag only');
                    return;
                }

                imageElement.onerror = function() {
                    this.onerror = '';
                    this.src = configurationService.imagesPath + ($scope.fallbackImage || 'noimage.png');
                    if ($scope.fallbackTitle) {
                        this.title = $scope.fallbackTitle;
                    }
                };
            },
        };
    });
})(angular);
