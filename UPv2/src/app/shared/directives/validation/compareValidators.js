/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('greaterThan', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                initValidator(scope, attrs, ngModel, 'greaterThan', true, false);
            }
        };
    });

    angular.module('lmxApp').directive('greaterThanEqual', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                initValidator(scope, attrs, ngModel, 'greaterThanEqual', true, true);
            }
        };
    });

    angular.module('lmxApp').directive('lessThan', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                initValidator(scope, attrs, ngModel, 'lessThan', false, false);
            }
        };
    });

    angular.module('lmxApp').directive('lessThanEqual', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                initValidator(scope, attrs, ngModel, 'lessThanEqual', false, true);
            }
        };
    });

    angular.module('lmxApp').directive('minLength', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                initValidator(scope, attrs, ngModel, 'minLength', true, false);
            }
        };
    });

    angular.module('lmxApp').directive('maxLength', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                initValidator(scope, attrs, ngModel, 'maxLength', false, false);
            }
        };
    });

    angular.module('lmxApp').directive('isEqualPasswords', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: {
                otherModelValue: "=isEqualPasswords"
            },
            link: function (scope, element, attrs, ngModel) {
                ngModel.$validators.isEqualPasswords = function(modelValue) {
                    return modelValue.length === 0 || modelValue === scope.otherModelValue;
                };

                scope.$watch('otherModelValue', function() {
                    ngModel.$validate();
                });
            }
        };
    });

    function initValidator(scope, attrs, ngModel, validatorName, isGreater, isEqual){

        function validator(modelValue){
            var validity = true;

            if (modelValue) {
                var comparedValue = scope.$eval(attrs[validatorName]);
                var value = modelValue;

                if (validatorName.contains('Length')) {
                    value = modelValue.length;
                    if (angular.isNumber(comparedValue)) {
                        if (isGreater) {
                            comparedValue--;
                        } else {
                            comparedValue++;
                        }
                    }
                }

                if(angular.isDefined(attrs.compareValidateIfCallback) && scope.$eval(attrs.compareValidateIfCallback)(value)){
                    validity = true;
                } else {
                    validity = validate(value, comparedValue, isGreater, isEqual);
                }
            }

            ngModel.$setValidity(validatorName, validity);
            return modelValue;

            function validate(value, comparedValue, isGreater, isEqual){
                if (!value || comparedValue === undefined){
                    return true;
                }

                if (isEqual && value === comparedValue){
                    return true;
                }

                return isGreater ? value > comparedValue : value < comparedValue;
            }
        }

        //refreshing model when use dynamic compared value
        scope.$watch(attrs[validatorName], function () {
            if (angular.isObject(ngModel.$viewValue)) {
                ngModel.$setViewValue(angular.copy(ngModel.$viewValue));
            } else {
                var value = ngModel.$viewValue;
                ngModel.$setViewValue(null);
                ngModel.$setViewValue(value);
            }
        });

        ngModel.$parsers.push(validator);
    }

})(angular);