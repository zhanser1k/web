/* eslint-disable */
(function (angular) {
    /**
     * Социальные сети
     * @name Social
     * @description Используется для отображения доступных для авторизации/привязки социальных сетей. Осуществляет
     * перенаправление  на страницу социальной сети для последующей авторизации. Также предоставляет возможность
     * привязывать и отвязывать социльную сеть.
     * @param {html-attribute} [is-binding] - Привязка социальной сети (параметр объявлен), авторизация через социальную
     * сеть
     * (параметр не объявлен)
     * @example
     * <lmx-social is-binding></lmx-social>
     * @version 1.1
     */
    angular.module('lmxApp').directive('lmxSocial', function (
        $http,
        $q,
        $window,
        configurationService,
        socialService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/social/social.html',

            link: function link($scope, elem, attrs) {
                $scope.isBinding = angular.isDefined(attrs.isBinding);
                $scope.socialImagesPath = configurationService.imagesPath ? (configurationService.imagesPath + 'social') : '';

                $scope.isProgress = {
                    getSocialInfo: true
                };

                $scope.isBinded = function(client) {
                    return socialIdentifiers.contains(client.providerType);
                };

                $scope.toggleBind = function(client) {
                    ($scope.isBinded(client) ? remove : redirectToProvider)(client.providerType);
                };

                $scope.authHandler = function(client) {
                    if (!$scope.isBinding) {
                        redirectToProvider(client.providerType);
                    }
                };

                var socialIdentifiers = [];

                /**
                 * Перенаправление на страницу соцсети
                 * @param {string} provider - соцсеть
                 */
                function redirectToProvider(provider) {
                    $window.location.replace(socialService.getAuthorizeURL(provider, $scope.clients, $scope.isBinding ? 'binding' : 'auth'));
                }

                /**
                 * Отвязка соцсети от профиля
                 * @param {string} provider - соцсеть
                 */
                function remove(provider) {
                    $scope.isProgress.remove = true;

                    socialService.removeClient(provider).then(function() {
                        getSocialIdentifiers();
                    }).finally(function() {
                        $scope.isProgress.remove = false;
                    });
                }

                /**
                 * Получение списка соцсетей, к которым привязан пользователь
                 */
                function getSocialIdentifiers() {
                    socialService.getSocialIdentifiers().then(function(response) {
                        socialIdentifiers = response;
                    });
                }

                /**
                 * Получение списка социальных сетей
                 */
                function getClients() {
                    socialService.getClients().then(function(response) {
                        $scope.clients = response;
                    });
                }

                $q.all([getClients(), $scope.isBinding ? getSocialIdentifiers() : null]).then(function() {
                    $scope.isProgress.getSocialInfo = false;
                });
            }
        };
    });
})(angular);
