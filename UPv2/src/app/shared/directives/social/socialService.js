(function(angular) {
    angular.module('lmxApp').factory('socialService', function(
        $http,
        $window,
        configurationService,
        routingService
    ) {
        var apiHost = configurationService.host + 'api/';

        /**
         * Получение списка социальных сетей
         */
        function getClients() {
            return $http.get(apiHost + 'Social/Clients').then(function(response) {
                return response.data;
            });
        }

        /**
         * Авторизация пользователя
         * @param {string} provider - соцсеть
         * @param {string} code - код, полученный от соцсети
         * @param {string} redirectUri - url, на который перенаправлялась соцсеть в случае успеха
         */
        function login(provider, code, redirectUri) {
            return $http.get(apiHost + 'User/' + provider + '/Login', {
                params: {
                    code: code,
                    redirect_uri: redirectUri,
                },
            }).then(function(response) {
                return response.data;
            });
        }

        /**
         * Привязка соцсети к профилю пользователя
         * @param {string} provider - соцсеть
         * @param {string} code - код, полученный от соцсети
         * @param {string} redirectUri - url, на который перенаправлялась соцсеть в случае успеха
         */
        function setClient(provider, code, redirectUri) {
            return $http.get(apiHost + 'User/' + provider + '/Set', {
                params: {
                    code: code,
                    redirect_uri: redirectUri,
                },
            });
        }

        /**
         * Отвязка соцсети от профиля
         * @param {string} provider - соцсеть
         */
        function removeClient(provider) {
            return $http.post(apiHost + 'User/' + provider + '/Remove');
        }

        /**
         * Получение списка соцсетей, к которым привязан пользователь
         */
        function getSocialIdentifiers() {
            return $http.get(apiHost + 'User/Logins').then(function(response) {
                return response.data.socialIdentifiers.map(function(item) {
                    return item.provider;
                });
            });
        }

        /**
         * Получение URL соцсети, на который надо переадресовать пользователя для авторизации в ней
         * @param provider - соцсеть
         * @param clients - список соцсетей
         * @param action - привязка или авторизация
         * @returns {string} - URL
         */
        function getAuthorizeURL(provider, clients, action) {
            var client = clients.find(function(item) {
                return item.providerType === provider;
            });

            var redirectUri = routingService.getLocationURL() + $window.location.pathname + '?providerType=' + provider + '&action=' + action;

            return (
                client.authorizeUri.target +
                (/[?&]/.test(client.authorizeUri.target) ? '&' : '?') + 'client_id=' + client.clientId +
                '&redirect_uri=' + encodeURIComponent(redirectUri) +
                '&response_type=code'
            );
        }

        return {
            getAuthorizeURL: getAuthorizeURL,
            getClients: getClients,
            getSocialIdentifiers: getSocialIdentifiers,
            login: login,
            removeClient: removeClient,
            setClient: setClient,
        };
    });
})(angular);
