/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('menuItem', function () {
        return {
            restrict: 'E',
            replace: true,
            template: function ($element, $attrs) {
                var currentClass = 'current-menu-item';
                var innerText = $element[0].innerText;
                var action;
                if (angular.isDefined($attrs.href)) {
                    action = 'ng-href="' + $attrs.href + '"';
                } else {
                    action = angular.isDefined($attrs.callback) ?
                        ('ng-click="' + $attrs.callback + '"') :
                        ('href="/#' +  $attrs.targetLocation + '"');
                }
                return '<li class="menu-item" ng-class="{\'' + currentClass + '\': currentLocation === \'' +  $attrs.targetLocation + '\'}">' +
                           '<a ' + action + '>' + innerText + '</a>' +
                           '<span>' + innerText + '</span>' +
                       '</li>';
            }
        };
    });
})(angular);