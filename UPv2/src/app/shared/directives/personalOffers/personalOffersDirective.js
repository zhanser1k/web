/* eslint-disable */
(function (angular) {
    /**
     * Персональные предложения
     * @name PersonalOffers
     * @description - Отображает список персональных предложений.
     * @example
     * <personal-offers></personal-offers>
     * @version 1.1
     */
    angular.module('lmxApp').directive('personalOffers', function (
        backendTypesService,
        brandService,
        configurationService,
        offerService
    ){
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/personalOffers/personalOffers.html',

            link: function link($scope) {
                var enums = configurationService.enums;
                $scope.inProgress = true;

                offerService.getOffers(
                    enums.offerState.active,
                    enums.communicationOfferTypeEnum.personalOffer
                ).then(function (response) {
                    $scope.personalOffers = response;
                    $scope.personalOffers.forEach(function (item) {
                        brandService.populateImagesUrlFromDescription(item.brands[0]);
                    });
                }).finally(function () {
                    $scope.inProgress = false;
                });
            }
        };
    });
})(angular);