/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('anonymousFeedback', function (
        $window,
        configurationService,
        userAlertService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/anonymousFeedback/anonymousFeedback.html',

            link: function link($scope) {

                var supportEmail = configurationService.options.supportEmail;

                $scope.messageModel = {
                    message: '',
                    subject: ''
                };

                $scope.sendMail = function(form) {
                    form.$submitted = false;
                    $window.location.href = 'mailto:' + supportEmail + '?subject=' + $scope.messageModel.subject +'&body='+ $scope.messageModel.message;
                    userAlertService('Происходит запуск почтового клиента. В\u00A0случае, если по\u00A0истечению некоторого времени ничего не\u00A0произошло, отправьте письмо непосредственно со\u00A0своей электронной почты на\u00A0адрес ' + supportEmail, 'success');
                };
            }
        };
    });
})(angular);