/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive("outsideClick", function( $document ){
        return {
            link: function( $scope, $element, $attributes ){
                var scopeExpression = $attributes.outsideClick,
                    onDocumentClick = function(event){
                        var isChild = $element[0].contains(event.target);
                        var isSelf = ($element[0] === event.target);

                        if (!isSelf && !isChild) {
                            $scope.$apply(scopeExpression);
                        }
                    };

                $document.on("click", onDocumentClick);

                $element.on('$destroy', function() {
                    $document.off("click", onDocumentClick);
                });
            }
        };
    });

})(angular);