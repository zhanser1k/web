/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service('feedbackService', function ($http, configurationService, backendTypesService) {
        var feedbackApiUri = configurationService.host + 'api/Support/Messages';
        var types = backendTypesService.enums;

        function getMessages() {
            return $http.get(feedbackApiUri).then(function (response) {
                return response.data;
            });
        }

        function sendMessage(message) {
            var messageModel = {
                $type: types.supportMessages.comment,
                message: message
            };
            return send(messageModel);
        }

        function sendAttachment(attachment) {
            var attachmentModel = {
                $type: types.supportMessages.attachments,
                attachments: {
                    $values: [
                        {
                            $type: types.support.attachment,
                            FileName: attachment.filename,
                            contentType: attachment.filetype,
                            Content: {
                                $type: 'System.Byte[], mscorlib',
                                $value: attachment.base64
                            }
                        }
                    ]
                }
            };
            return send(attachmentModel);
        }

        function send(model) {
            return $http.post(feedbackApiUri, model).then(function (response) {
                return response.data;
            });
        }

        return {
            getMessages: getMessages,
            sendMessage: sendMessage,
            sendAttachment: sendAttachment
        };
    });
})(angular);