/* eslint-disable */
(function (angular) {
    /**
     * Чат пользователя и службы поддержки
     * @name Feedback
     * @description Осуществляет связь в виде чата между пользователем и службой поддержки.
     * @example
     * <feedback></feedback>
     * @version 1.1
     */
    angular.module('lmxApp').directive('feedback', function (
        $q,
        feedbackService,
        $interval
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/feedback/feedback.html',

            link: function link($scope, element) {
                var scroller = false;
                $scope.inProgress = true;

                function resetFormModels() {
                    $scope.messageModel = {
                        text: '',
                        attachment: ''
                    };
                }
                resetFormModels();

                function loadMessages() {
                    return feedbackService.getMessages().then(function (response) {
                        $scope.messages = response;
                        setScroll();
                    }).finally(function () {
                        $scope.inProgress = false;
                    });
                }
                loadMessages();

                function setScroll() {
                    if (scroller) {
                        scroller.update();
                    } else {
                        scroller = baron({
                            root: element[0].querySelector('.scroller-wrapper'),
                            scroller: '.scroller',
                            barOnCls: '_scrollbar',
                            track: '.scroller-track',
                            bar: '.scroller-bar',
                            cssGuru: true,
                            direction: 'v'
                        });
                    }
                }

                var intervalGetMessagesPromise = $interval(function() {
                    loadMessages();
                }, 1000*60);

                $scope.$on('$destroy', function () {
                    $interval.cancel(intervalGetMessagesPromise);
                });

                $scope.send = function (feedbackForm) {
                    $scope.sendInProgress = true;
                    var promises = [];
                    promises.push(feedbackService.sendMessage($scope.messageModel.text));
                    if ($scope.messageModel.attachment) {
                        promises.push(feedbackService.sendAttachment($scope.messageModel.attachment));
                    }
                    feedbackForm.$submitted = false;
                    $q.all(promises).finally(function () {
                        $scope.sendInProgress = false;
                        $scope.inProgress = true;
                        resetFormModels();
                        loadMessages();
                    });
                };
            }
        };
    });
})(angular);