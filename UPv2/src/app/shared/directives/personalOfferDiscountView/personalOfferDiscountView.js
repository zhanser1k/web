/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('personalOfferDiscountView', function (
        $log,
        $translate,
        backendTypesService
    ){
        return {
            restrict: 'E',
            replace: true,
            scope: {
                value: '=',
                type: '=',
                measureAmount: '=',
                discountType: '='
            },
            templateUrl: 'app/shared/directives/personalOfferDiscountView/personalOfferDiscountView.html',
            link: function link($scope) {
                var error;
                if (!$scope.value) {
                    error = 'Должно быть указано значение скидки';
                } else if (!$scope.type) {
                    error = 'Должен быть указан тип скидки';
                }

                if (error) {
                    $log(error);
                    return;
                }

                $scope.discountEnums = _.invert(backendTypesService.enums.personalOfferDiscount);

                $scope.discountView = $translate('personalOffer.discountViewModel.' + $scope.discountEnums[$scope.type])
                    .replace('{TYPE}', $translate('personalOffer.discountTypes.' + $scope.discountType))
                    .replace('{VALUE}', $scope.value)
                    .replace('{STEP}', $scope.measureAmount)
                    .replace('{CUR}', $translate('personalOffer.discountTypesCurrency.' + $scope.discountType));

            }
        };
    });
})(angular);