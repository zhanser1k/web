/* eslint-disable */
(function (angular) {
    angular.module('lmxApp').directive('lmxModal', function (
        $log,
        configurationService,
        globalService,
        modalService
    ) {
        return {
            restrict: 'E',
            scope: {
                className: '@',
                closeOn: '@',
                openOn: '@',
                size: '@',
                templateUrl: '@',
                backdrop: '@',
                closeOnEsc: '@'
            },

            link: function link(scope, element, attrs) {

                if (scope.openOn) {
                    scope.$on(scope.openOn, modalOpen);
                } else {
                    modalOpen();
                }

                function modalOpen() {
                    var backdrop;
                    switch (scope.backdrop) {
                        case 'static':
                            backdrop = scope.backdrop;
                            break;
                        case 'false':
                            backdrop = false;
                            break;
                        default:
                            backdrop = true;
                            break;
                    }

                    var modal = modalService.open({
                        size: scope.size,
                        templateUrl: configurationService.templatesPath + scope.templateUrl,
                        controller: 'modalController',
                        windowTopClass: scope.className,
                        backdrop: backdrop,
                        keyboard: scope.closeOnEsc !== 'false',
                        resolve: {
                            closeEventName: function () {
                                return scope.closeOn;
                            }
                        }
                    });

                    if (angular.isDefined(attrs.resetLocationOnClose)) {
                        modal.closed.then(function () {
                            globalService.changeLocation('');
                        });
                    }

                    scope.$on('$destroy', modal.dismiss);
                }
            }
        };
    });
})(angular);