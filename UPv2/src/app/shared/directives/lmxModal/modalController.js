/* eslint-disable */
(function (angular) {
    angular.module('lmxApp').controller('modalController', function (
        $uibModalInstance,
        $scope,
        closeEventName
    ) {
        $scope.$on(closeEventName, $uibModalInstance.close);
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    });
})(angular);