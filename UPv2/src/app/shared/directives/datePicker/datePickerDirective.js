﻿/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('datePicker', function ($locale) {

        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                initDate: '=?',
                timezone: '@',
                inputClass: '@'
            },
            replace: true,
            link: {
                pre: function($scope, elem, attrs) {
                    var vendorElement = angular.element(elem[0].children[0]);

                    for (var key in attrs.$attr) {
                        if (attrs.$attr.hasOwnProperty(key)) {
                            vendorElement.attr(attrs.$attr[key], attrs[key]);
                        }
                    }

                    // todo сконфигурировать формат года для пикера yyyy вместо yy.
                    // как возможное решение - при загрузке файла локалей - расширять шаблоны, длбавляя своё свойство с расширенным годом
                    // после чего поискать по проекту костыли 'yyyy', так как в этих местах дата не локализуется

                    // todo добавить автоподстановку текущего формата в мессагу в зависимости от директивы (date-/dataTime-)
                    // var directiveElement = angular.element(elem).attr('date-message', "{{::'validationMessages.date' | translate}} в формате {{::locale.DATETIME_FORMATS.short}}");
                    // $compile(directiveElement)($scope);

                    var date = new Date();
                    $scope.modelOptions = {
                        allowInvalid: true
                    };
                    $scope.modelOptions.timezone = $scope.timezone || (date.getTimezoneOffset() * -1);
                    $scope.initDate = $scope.initDate || date;
                },
                post: function ($scope, elem, attrs, ngModelController) {

                    var customDateFormats = {
                        ru: 'dd.MM.yyyy'
                    };

                    // $scope.dateFormat = customDateFormats[$locale.id] || $locale.DATETIME_FORMATS.shortDate;
                    $scope.dateFormat = customDateFormats.ru;
                    $scope.options = {
                        showWeeks: false,
                        initDate: $scope.initDate,
                        formatMonth: 'LLLL',
                        formatDayTitle: 'LLLL yyyy'
                    };

                    $scope.isOpen = false;
                    $scope.toggleCalendar = function(e, toState) {
                        $scope.isOpen = angular.isDefined(toState) ? toState : !$scope.isOpen;
                        if (!$scope.isOpen) {
                            $scope.$$childHead.showPicker = 'date';
                        }
                    };

                    function isToUtcCorrectionNeeded(date) {
                        return angular.isDefined(attrs.asUtc) && angular.isDefined(date) && date instanceof Date;
                    }

                    $scope.$watch('model', function(newValue, oldValue) {
                        if (!angular.isDefined(oldValue) && angular.isDefined(newValue)) {
                            ngModelController.$setValidity('date', true);
                        }
                        if (isToUtcCorrectionNeeded(newValue)) {
                            newValue = newValue.toUTCDate(true);
                        }
                        ngModelController.$setViewValue(newValue);
                        $scope.validateModel();
                    });

                    // Дублирование вызова validateModel через on-keyup служит для корректной обработки валидатора 'date'
                    $scope.validateModel = function($event) {
                        var isValid = !$event || !$event.target.value.length || $scope.model instanceof Date;
                        ngModelController.$setValidity('date', isValid);
                        if (ngModelController.$validators.hasOwnProperty('required') && ngModelController.$$attr.required) {
                            ngModelController.$setValidity('required', ($event && $event.target.value.length) || !!$scope.model);
                        }
                    };

                    ngModelController.$render = function() {
                        var value = ngModelController.$viewValue;
                        if (isToUtcCorrectionNeeded(value)) {
                             value = value.toUTCDate();
                        }
                        $scope.model = value;
                    };
                }
            },
            templateUrl: 'app/shared/directives/datePicker/datePickerView.html'
        };
    });
})(angular);