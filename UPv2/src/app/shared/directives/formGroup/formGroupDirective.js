(function(angular) {
    /* ограничение, накладываемое ng-transclude - в качестве модели (ng-model) не использовать переменные примитивных типов,
    вместо этого должны использоваться свойства объектов

    не использовать ng-if на элементах ввода внутри директивы, теряется биндинг при валидации */

    angular.module('lmxApp').directive('formGroup', function(
        $filter,
        $translate,
        $rootScope,
        $timeout
    ) {
        var messages = {};
        // minlength и maxlength упразднены из-за обработки браузером без уведомления пользователя
        // браузерная обработка - обрезание при копировании из буфера обмена; блокировка ввода;
        var validators = [
            'defaultMsg',
            'email',
            'minLength',
            'maxLength',
            'min',
            'max',
            'required',
            'date',
            'pattern',
            'number',
            'url',
            'greaterThan',
            'greaterThanEqual',
            'lessThan',
            'lessThanEqual',
            'requiredItems',
            'digitsAfterComma',
            'integer',
            'notEmptyGuid',
            'isGuid',
            'isPhone',
            'isEmail',
            'isEqualPasswords',
        ];

        function localizeValidatorMessage(messageCode) {
            messages[messageCode] = $translate('validationMessages.' + messageCode);
        }

        for (var i = 0; i < validators.length; ++i) {
            localizeValidatorMessage(validators[i]);
        }

        return {
            restrict: 'E',
            require: '^form',
            transclude: true,
            scope: {
                labelText: '@',
                labelAlign: '@',
                viewMode: '=',
                viewModeValue: '=',
                hint: '@',
            },
            templateUrl: 'app/shared/directives/formGroup/formGroupTemplate.html',

            link: function(scope, element, attrs, formController) {
                scope.labelAlign = scope.labelAlign || 'left';

                /* TODO: СДЕЛАТЬ НОРМАЛЬНУЮ ВАЛИДАЦИЮ (без таймаутов) ДЛЯ CUSTOM-SELECT
                   $timeout стоит потому что аттрибут id не успевает интерполироваться в scope,
                   и записывается с фигурными скобками
                */
                $timeout(function() {
                    return angular.element(element[0].querySelector('[id]'));
                }).then(function(formItem) {
                    if (!formItem || formItem.length === 0 || formController.$name === '') {
                        return;
                    }

                    scope.itemId = formItem.attr('id');
                    scope.form = formController;

                    var modelWatcher = scope.$watch(function() {
                        return formController[formItem.attr('name')];
                    }, function(model) {
                        if (angular.isDefined(model)) {
                            scope.model = model;
                            modelWatcher();
                        }
                    }, true);

                    $rootScope.$on('validationErrors', function(event, error) {
                        var modelName = scope.model ? scope.model.$name.toLowerCase() : '';
                        if (error.hasOwnProperty(modelName)) {
                            scope.model.$error.validationErrors = error[modelName];
                            scope.model.$setValidity('validationError', false);
                        }

                        var viewValueWatcher = scope.$watch('model.$viewValue', function(newValue, oldValue) {
                            if (angular.isDefined(oldValue) && newValue !== oldValue) {
                                delete scope.model.$error.validationErrors;
                                scope.model.$setValidity('validationError', true);
                                viewValueWatcher();
                            }
                        });
                    });

                    function getAttribute(el, attribute) {
                        var attributeName = angular.unNormalizeDirective(attribute);
                        return el.attr('data-ng-' + attributeName) ||
                        el.attr('ng-' + attributeName) ||
                        el.attr(attributeName);
                    }

                    function getValue($scope, attribute) {
                        while ($scope !== null) {
                            var value = $scope.$eval(attribute);
                            if (value !== undefined) {
                                return value;
                            }

                            $scope = $scope.$parent;
                        }

                        return undefined;
                    }

                    scope.errorMessage = function(error, modelViewValue) {
                        var attribute = getAttribute(formItem, error);
                        var filter = getAttribute(formItem, error + 'Filter');
                        var customMessage = getAttribute(formItem, error + 'Message');
                        var value = getValue(scope, attribute);

                        var message = customMessage || messages[error];

                        if (filter) {
                            if (filter.match(/,/)) {
                                var filters = filter.split(',');
                                for (var j = 0; j < filters.length; j++) {
                                    value = $filter(filters[j])(value);
                                }
                            } else {
                                value = $filter(filter)(value);
                            }
                        }

                        var modelValue = [modelViewValue];
                        if (['minLength', 'maxLength'].contains(error)) {
                            modelValue = [modelViewValue.length];
                        }

                        return message.format([value], modelValue);
                    };
                });
            },
        };
    });
})(angular);
