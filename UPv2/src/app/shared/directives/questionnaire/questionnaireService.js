(function(angular) {
    angular.module('lmxApp').factory('questionnaireService', function(
        $http,
        $rootScope,
        configurationService,
        locationService
    ) {
        var editMode = false;
        var loader = {};
        var userApiUrl = configurationService.host + 'api/User/';

        var area;
        function setArea(serviceArea) {
            area = serviceArea;
        }

        function showLoaderAndReturnData(promise) {
            loader.inProgress = true;
            return promise.then(function(response) {
                return response.data;
            }).finally(function() {
                loader.inProgress = false;
            });
        }
        // "урезание" объектов для сохранения, оставляем тоьлко то что нужно BE
        function getSlimAnswers(answers) {
            var slimAnswers = [];
            var item;
            for (var i = 0; i < answers.length; i++) {
                item = answers[i];
                slimAnswers.push(angular.copyObjectFields(item, 'groupID', 'id', 'value', 'selected'));
            }
            return slimAnswers;
        }

        function registerAutocomplete(scope, method, currentValue, higherLevelValue, broadcastOnChangeEvent, clearOnEvent) {
            scope.browserAutocomplete = 'off';
            scope.autocomplete = true;

            if (currentValue) {
                scope.commitValue = function() {
                    currentValue.value = scope.question.value;
                };
                scope.commitValue();
            }

            scope.getAutocompleteValues = function(value) {
                var valueToSend = higherLevelValue ? higherLevelValue.value : null;
                return method(value, valueToSend);
            };

            if (editMode) {
                if (broadcastOnChangeEvent) {
                    scope.$watch('question.value', function(newValue) {
                        if (angular.isDefined(newValue)) {
                            $rootScope.$broadcast(broadcastOnChangeEvent);
                        }
                    });
                }
                if (clearOnEvent) {
                    scope.$on(clearOnEvent, function() {
                        scope.question.value = '';
                    });
                }
            }
        }

        function getQuestionnaire(onlyRequired) {
            return showLoaderAndReturnData($http.get(userApiUrl + 'Questions', {params: {'filter.onlyRequired': onlyRequired}, area: area})).then(function(response) {
                return response.questions;
            });
        }

        function save(answers) {
            var params = {answers: getSlimAnswers(answers)};

            return showLoaderAndReturnData($http.post(userApiUrl + 'Answers', params, {area: area})).then(function(response) {
                var errors = null;
                if (response && response.errors) {
                    errors = response.errors.toDictionary('idQuestion', 'errors');
                }
                return errors;
            });
        }

        function bindLoader(externalLoader) {
            loader = externalLoader;
        }

        function setEditMode(mode) {
            editMode = mode;
        }

        var geoData = {
            selectedCity: {value: ''},
            selectedStreet: {value: ''},
        };

        function registerCityAutocomplete(scope) {
            registerAutocomplete(scope, locationService.getCities, geoData.selectedCity, null, 'questionnaireCityChanged');
        }

        function registerStreetAutocomplete(scope) {
            registerAutocomplete(scope, locationService.getStreets, geoData.selectedStreet, geoData.selectedCity, 'questionnaireStreetChanged', 'questionnaireCityChanged');
        }

        function registerHouseAutocomplete(scope) {
            registerAutocomplete(scope, locationService.getHouses, null, geoData.selectedStreet, null, 'questionnaireStreetChanged');
        }

        return {
            editMode: editMode,
            setArea: setArea,
            bindLoader: bindLoader,
            getQuestionnaire: getQuestionnaire,
            save: save,
            setEditMode: setEditMode,
            registerCityAutocomplete: registerCityAutocomplete,
            registerStreetAutocomplete: registerStreetAutocomplete,
            registerHouseAutocomplete: registerHouseAutocomplete,
        };
    });
})(angular);
