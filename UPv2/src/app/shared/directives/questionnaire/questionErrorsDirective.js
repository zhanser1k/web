/* eslint-disable */
(function (angular) {
    // блок отображения ошибок вопроса анкеты

    angular.module('lmxApp').directive('questionErrors', function (){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/questionnaire/questionErrors.html'
        };
    });
})(angular);