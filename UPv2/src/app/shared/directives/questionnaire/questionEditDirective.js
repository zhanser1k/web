/* eslint-disable */
(function (angular) {
    // кнопка перехода в режим редактирования вопроса

    angular.module('lmxApp').directive('questionEdit', function (){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/questionnaire/questionEdit.html'
        };
    });
})(angular);