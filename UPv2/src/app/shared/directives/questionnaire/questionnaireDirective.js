(function(angular) {
    /**
     * Анкета
     * @name Questionnaire
     * @description Отображает анкету с информацией о пользователе.
     * @param {boolean} registration=false - Опрашивание анкетой производится в рамках регистрации
     * @param {html-attribute} [edit-mode] - Режим редактирования всей анкеты. Если не указать - редактирование вопросов по отдельности
     * @param {html-attribute} [only-required] - Отображать в акете только обязательные вопросы
     * @param {html-attribute} [is-disabled]- Анкета в режиме просмотра
     * @example
     * <!-- Оторбражение формы редактирования всей анкеты -->
     * <questionnaire edit-mode></questionnaire>
     *
     * <!-- Оторбражение формы заполнения только обязательных полей анкеты в рамках регистрации -->
     * <questionnaire registration="true" edit-mode only-required></questionnaire>
     *
     * <!-- Отображение анкеты без возможности редактирования -->
     * <questionnaire is-disabled></questionnaire>
     * @version 1.1
     */
    angular.module('lmxApp').directive('questionnaire', function(
        $rootScope,
        authService,
        notification,
        questionnaireService,
        routingService,
        userAlertService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                registration: '=',
            },
            templateUrl: 'app/shared/directives/questionnaire/questionnaire.html',
            link: function link($scope, elem, attrs) {
                var area = 'questionnaire';
                questionnaireService.setArea($scope.registration ? 'registration' : area);
                // запрашивать только required-вопросы для режима регистрации
                var onlyRequired = angular.isDefined(attrs.onlyRequired);

                // режим редактирования, в котором все вопросы анкеты доступны для редактирования и только одна кнопка сохранения
                $scope.editMode = angular.isDefined(attrs.editMode);
                $scope.loader = {
                    templatesInProgress: true,
                    saveInProgress: false,
                };
                $scope.isDisabled = angular.isDefined(attrs.isDisabled);

                var initialQuestionValues = {};
                var userInfoFields = ['LastName', 'FirstName', 'PatronymicName'];
                var successQuestionCount;
                var answers;
                var questionCount;

                if ($scope.editMode) {
                    answers = [];
                    questionCount = 0;
                    successQuestionCount = 0;

                    $scope.$on('questionInit', function(event, question) {
                        if (userInfoFields.contains(question.logicalName)) {
                            initialQuestionValues[question.logicalName] = question.value;
                        }
                        questionCount += 1;
                    });

                    questionnaireService.bindLoader($scope.loader);
                }

                questionnaireService.setEditMode($scope.editMode);
                questionnaireService.getQuestionnaire(onlyRequired).then(function(questionnaire) {
                    $scope.questionnaire = questionnaire;
                });

                // генерация события сохранения всей анкеты
                $scope.saveQuestionnaire = function() {
                    successQuestionCount = 0;
                    answers.length = 0;
                    $scope.$broadcast('saveQuestionnaire');
                };

                $scope.cancelForm = function() {
                    if ($scope.registration) {
                        authService.clearAuthData();
                        routingService.goToLogin();
                    } else {
                        $scope.$broadcast('cancelQuestionnaire');
                    }
                };

                $scope.changeEdit = function() {
                    $scope.isDisabled = !$scope.isDisabled;
                };

                var childrenLoaded = 0;
                $scope.$on('questionnaireItemTemplateLoaded', function(event, childBlock) {
                    if ($scope.questionnaire.contains(childBlock)) {
                        childrenLoaded += 1;

                        if (childrenLoaded === $scope.questionnaire.length) {
                            $scope.loader.templatesInProgress = false;
                        }
                    }
                });

                function save(answerItems) {
                    $scope.$broadcast('beforeSave');
                    $scope.loader.saveInProgress = true;
                    questionnaireService.save(answerItems).then(function(questionnaireErrors) {
                        if (onlyRequired && !questionnaireErrors) {
                            // оповещение успешного создания анкеты
                            $rootScope.$broadcast('questionnaireIsSet');
                        } else {
                            // оповещение завершения запроса сохранения анкеты
                            $scope.$broadcast('saveRequestComplete', answerItems, questionnaireErrors);

                            if (questionnaireErrors) {
                                notification.addError('Некоторые поля анкеты заполнены неверно', area);
                            } else {
                                if ($scope.registration) {
                                    notification.addMessage('Личные данные обновлены', area);
                                } else {
                                    userAlertService('Личные данные обновлены', 'success').finally(function() {
                                        document.documentElement.scrollTop = 0;
                                    });
                                }

                                $scope.isDisabled = true;

                                $scope.$broadcast('saveRequestSuccess');

                                answerItems.forEach(function(item) {
                                    if (userInfoFields.contains(item.logicalName)) {
                                        if (initialQuestionValues[item.logicalName] !== item.value) {
                                            $rootScope.$broadcast('initialsUpdated');
                                            initialQuestionValues[item.logicalName] = item.value;
                                        }
                                    }
                                });
                            }
                        }
                    }).catch(function() {
                        // оповещение завершения запроса сохранения анкеты
                        $scope.$broadcast('saveRequestComplete');
                    }).finally(function() {
                        if ($scope.editMode) {
                            $scope.loader.saveInProgress = false;
                        }
                    });
                }

                function saveSingleAnswer(event, answerItems) {
                    if ($scope.editMode) {
                        // "сбор" ответов на вопросы от дочерних директив
                        answers.pushArray(answerItems);
                        successQuestionCount += 1;
                        if (questionCount === successQuestionCount) {
                            save(answers);
                        }
                    } else {
                        save(answerItems);
                    }
                }

                $scope.$on('answerSaveRequest', saveSingleAnswer);
            },
        };
    });
})(angular);
