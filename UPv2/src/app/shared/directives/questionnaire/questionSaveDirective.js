/* eslint-disable */
(function (angular) {
    // кнопка сохранения вопроса анкеты

    angular.module('lmxApp').directive('questionSave', function (){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/questionnaire/questionSave.html'
        };
    });
})(angular);