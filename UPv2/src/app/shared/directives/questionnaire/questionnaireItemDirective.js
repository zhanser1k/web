/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('questionnaireItem', function (
        $filter,
        $http,
        $log,
        configurationService,
        questionnaireService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                question: '=',
                questionType: '@', // тип данных вопроса
                editMode: '=' // режим редактирования анкеты целиком
            },
            template: '<div class="panel-body"><div ng-include="questionTemplateUrl" onload="emitTemplateLoaded()"></div></div>',
            link: function($scope){

                $scope.formOpened = $scope.editMode;
                $scope.disabled = false;

                $scope.inProgress = false;
                $scope.errors = null;
                var backupData;

                $scope.questionTemplateUrl = 'app/shared/directives/questionnaire/questionTypes/'+$scope.questionType+'.html';

                var fieldTypes = configurationService.enums.templateFieldType;
                $scope.radioButtonStringType = fieldTypes.radioButtonString;

                parseQuestion();
                if ($scope.editMode) {
                    // оповещаем анкету о генерации (наличии) вопроса и регистрируем форму вопроса в контроллере
                    var form;
                    $scope.$emit('questionInit', $scope.question);
                    $scope.registerForm = function(questionnaireForm){
                        form = questionnaireForm;
                    };

                    $scope.$watch('question.value', function(newValue, oldValue) {
                        if (newValue && newValue !== oldValue && ['LastName', 'FirstName', 'PatronymicName'].contains($scope.question.logicalName)) {
                            $scope.question.value = $filter('capitalize')(newValue);
                        }
                    });

                    // по запросу на сохранение всей анкеты - запуск нативного механизма валидации полей формы
                    $scope.$on('saveQuestionnaire', function(){
                        form.$setSubmitted();
                        var children = $scope.question.children;

                        if (form.$valid) {
                            $scope.save();
                        } else if (children && children.some(function (item) { return item.fieldType === 'RadioButtonString'; })) {
                            var error = form.$error.required ? 'Это поле обязательно' : 'Некоторые поля анкеты заполнены неверно';
                            $scope.errors = [error];
                        }
                    });
                }

                // генерация события сохранения текущего вопроса анкеты с передачей "ответа" на вопрос
                $scope.save = function() {
                    $scope.inProgress = true;
                    setErrors(null);
                    $scope.$emit('answerSaveRequest', getAnswerData());
                };

                // перейти из режима редактирования в режим просмотра
                $scope.cancelForm = function(){
                    setErrors(null);
                    $scope.formOpened = false;
                    backup();
                };

                // войти в режим редактирования вопроса
                $scope.openForm = function(){
                    updateBackupData();
                    $scope.formOpened = true;
                };

                $scope.$on('beforeSave', function() {
                    setErrors(null);
                    setInputFieldsDisabled(true);
                });

                $scope.$on('saveRequestComplete', function(event, savedAnswerItems, errors) {
                    setErrors(errors);
                    setInputFieldsDisabled(false);

                    //Проверка в случае редактирования только одного вопроса анкеты
                    if ($scope.formOpened && angular.equals(savedAnswerItems, getAnswerData())) {
                        if (!errors) {
                            afterSaveParseQuestion();
                        }
                        $scope.inProgress = false;
                    }
                });

                $scope.$on('cancelQuestionnaire', backup);
                $scope.$on('saveRequestSuccess', updateBackupData);

                $scope.resetRadioButton = function () {
                    $scope.question.children.forEach(function (item) {
                        if (item.id !== $scope.question.id) {
                            item.selected = false;
                            item.value = null;
                        }
                    });
                };

                $scope.emitTemplateLoaded = function () {
                    $scope.$emit('questionnaireItemTemplateLoaded', $scope.question);
                };

                // первичная обработка вопроса для корректной обработки текущего значения ng-model
                function parseQuestion() {
                    switch ($scope.questionType) {
                        case fieldTypes.radioButton:
                            var selectedIndex = $scope.question.children.indexOfObjectByField('selected', true);
                            if (selectedIndex !== null) {
                                var selectedItem = $scope.question.children[selectedIndex];
                                $scope.question.displayValue = selectedItem.value || selectedItem.displayName;
                                $scope.question.id = selectedItem.id;
                            }
                            break;
                        case fieldTypes.int32:
                            $scope.question.value = parseInt($scope.question.value) || null;
                            break;
                        case fieldTypes.dateTime:
                            $scope.question.value = $scope.question.value ? new Date($scope.question.value) : null;
                            if ($scope.question.logicalName === 'BirthDay') {
                                var date = new Date();
                                $scope.initDate = new Date( date.setFullYear( date.getFullYear() - 30 ));
                            }
                            break;
                        case fieldTypes.string:
                            var autoCompleteType = configurationService.enums.autoCompleteType;
                            switch ($scope.question.logicalName) {
                                case autoCompleteType.city:
                                    questionnaireService.registerCityAutocomplete($scope);
                                    break;
                                case autoCompleteType.street:
                                    questionnaireService.registerStreetAutocomplete($scope);
                                    break;
                                case autoCompleteType.house:
                                    questionnaireService.registerHouseAutocomplete($scope);
                                    break;
                            }
                            break;
                    }
                    updateBackupData();
                }

                // обработка текущего состояния вопроса для получения объекта(ов) для сохранения BE
                function getAnswerData() {
                    var data;
                    var copy;
                    switch ($scope.questionType) {
                        case fieldTypes.radioButton:
                            var selectedIndex = $scope.question.children.indexOfObjectByField('id', $scope.question.id);
                            if (selectedIndex !== null) {
                                data = $scope.question.children[selectedIndex];
                                data.selected = true;
                            } else {
                                data = $scope.question.children[0];
                            }
                            break;
                        case fieldTypes.checkbox:
                            data = $scope.question.children;
                            break;
                        case fieldTypes.dateTime:
                            copy = angular.copy($scope.question);
                            if (copy.value) {
                                copy.value = moment(copy.value).format('YYYY-MM-DD');
                            }
                            data = copy;
                            break;
                        case fieldTypes.int32:
                            copy = angular.copy($scope.question);
                            if (!copy.value) {
                                copy.value = null;
                            }
                            data = copy;
                            break;
                        default:
                            data = $scope.question;
                    }

                    if (!angular.isArray(data)) {
                        data = [data];
                    }
                    return data;
                }

                // при успешном сохранении меняем параметры полей вопроса типа radioButton для корректного отображения текущего значения
                function afterSaveParseQuestion() {
                    switch ($scope.questionType) {
                        case fieldTypes.radioButton:
                            $scope.resetRadioButton();
                            parseQuestion();
                    }
                }

                function setErrors(errorsDictionary) {
                    var error;

                    if (errorsDictionary) {
                        error = errorsDictionary[$scope.question.id];
                    }

                    $scope.errors = error;
                }

                function updateBackupData() {
                    backupData = angular.copy($scope.question);
                }

                function backup() {
                    angular.extend($scope.question, backupData);
                }

                function setInputFieldsDisabled(isDisabled) {
                    $scope.disabled = isDisabled;
                }
            }
        };
    });
})(angular);
