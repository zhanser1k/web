/* eslint-disable */
(function (angular) {
    // кнопка перехода из режима редактирования в режим просмотра
    
    angular.module('lmxApp').directive('questionCancel', function (){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/questionnaire/questionCancel.html'
        };
    });
})(angular);