/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('questionnaireBlock', function (configurationService){
        
        return {
            restrict: 'E',
            replace: true,
            scope: {
                block: '=',
                editMode: '='
            },
            templateUrl: 'app/shared/directives/questionnaire/questionnaireBlock.html',
            link: function link($scope) {

                var nodeTypes = configurationService.enums.templateNodeType;
                var fieldTypes = configurationService.enums.templateFieldType;

                $scope.isBlockHeader = function(node){
                    return !node.logicalName && !checkHeaderFirstChildType(node, fieldTypes.checkbox);
                };

                $scope.getQuestionType = function(question){
                    var type = null;

                    if (question.fieldType) {
                        type = question.fieldType;
                    } else if (isCheckbox(question)) {
                        type = fieldTypes.checkbox;
                    } else if (isRadio(question)) {
                        type = fieldTypes.radioButton;
                    }

                    if (!type) {
                        console.warn('Unknown question type', question);
                    }
                    return type;
                };

                var childrenLoaded = 0;
                $scope.$on('questionnaireItemTemplateLoaded', function (event, childItem) {
                    if ($scope.block.children.contains(childItem) && ++childrenLoaded === $scope.block.children.length) {
                        $scope.$emit('questionnaireItemTemplateLoaded', $scope.block);
                    }
                });

                function isHeader(node){
                    return node.nodeType === nodeTypes.header;
                }

                function isCheckbox(node){
                    return checkHeaderFirstChildType(node, fieldTypes.checkbox);
                }

                function isRadio(node){
                    return checkHeaderFirstChildType(node, fieldTypes.radioButton);
                }

                function checkHeaderFirstChildType(node, type) {
                    return isHeader(node) && angular.isArray(node.children) && node.children[0].fieldType === type;
                }
            }

        };
    });
})(angular);