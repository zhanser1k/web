/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('loader', function (
        $compile,
        $templateCache,
        classPrefixerService
    ) {
        return {
            restrict: 'A',
            link: function($scope, element, attrs) {
                if(!attrs.loader) {
                    return;
                }

                element.addClass(classPrefixerService.getPrefixed('relative loader-container'));

                var html = $templateCache.get("loader.html") || "";
                var loader = angular.element('<div class="' + classPrefixerService.getPrefixed('loader mh120') + '"' + 'ng-show="' + attrs.loader + '">' + html + '</div>');

                element.prepend(loader);
                $compile(loader)($scope);
            }
        };
    });

})(angular);