/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').controller('offerModalController', function (
        $scope,
        $uibModalInstance,
        offer,
        offerService,
        rewardThumbnailTypes
    ) {
        $scope.inProgress = true;
        $scope.rewardThumbnailTypes = rewardThumbnailTypes;
        $scope.offer = offer;
        $scope.offer.merchants = [];
        $scope.cancel = $uibModalInstance.dismiss;

        offerService.getOfferMerchants(offer.id).then(function (response) {
            $scope.offer.merchants = response;
        }).finally(function () {
            $scope.inProgress = false;
        });
    });
})(angular);