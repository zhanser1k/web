/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("offerService", function ($http, configurationService, brandService) {
        var offerApiUri = configurationService.host + 'api/offer/';
        var filesApiUri = configurationService.host + 'api/Files/';

        function getOffers(offerState, offerType) {
            return $http.get(offerApiUri, {
                params: {
                    'filter.offerState': offerState,
                    'filter.count': Number.maxIntValue,
                    'filter.type': offerType
                }
            }).then(function(response) {
                response.data.forEach(function(offer) {
                    brandService.populateImagesUrlFromDescription(offer.brands[0]);
                });
                return response.data;
            });
        }

        function getOfferById(offerId) {
            return $http.get(offerApiUri + offerId).then(function (response) {
                var img = response.data.images.getObjectByField("description", configurationService.userPortalDescriptionType);
                response.data.imgUrl = img ? (filesApiUri + img.fileId) : 'undefined';
                brandService.populateImagesUrlFromDescription(response.data.brands[0]);
                return response.data;
            });
        }

        function getOfferMerchants(offerId) {
            return $http.get(offerApiUri + offerId + '/Merchants').then(function (response) {
                return response.data;
            });
        }

        function getOfferDetails(offerId) {
            return $http.get(offerApiUri + offerId +'/details').then(function (response) {
                return response.data;
            });
        }

        function getOfferDetailMerchants(offerId, detailId) {
            return $http.get(offerApiUri + offerId + '/details/' + detailId + '/merchants').then(function (response) {
                return response.data;
            });
        }

        function approvePersonalGoods(attributeId, goodsIds) {
            var url = configurationService.host + 'api/user/attributes/personalOffer/values/' + attributeId;
            var data = {
                goodsIds: goodsIds
            };
            return $http.post(url, data);
        }

        return {
            getOfferById: getOfferById,
            getOfferDetails: getOfferDetails,
            getOfferMerchants: getOfferMerchants,
            getOffers: getOffers,
            getOfferDetailMerchants: getOfferDetailMerchants,
            approvePersonalGoods: approvePersonalGoods
        };
    });
})(angular);