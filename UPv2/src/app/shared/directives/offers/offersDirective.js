/* eslint-disable */
(function (angular) {
    /**
     * Акции
     * @name Offers
     * @description Отображает список акции с изображением и описанием.
     * @example
     * <offers></offers>
     * @version 1.1
     */
    angular.module('lmxApp').directive('offers', function (
        $q,
        offerService,
        brandService,
        configurationService,
        backendTypesService,
        modalService
    ){
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/offers/offers.html',

            link: function link($scope) {
                var filesApiUri = configurationService.host + 'api/Files/';
                var enums = configurationService.enums;

                $scope.rewardThumbnailTypes = _.invert(backendTypesService.enums.rewardThumbnail);

                $scope.states = {
                    active: enums.offerState.active,
                    comingSoon: enums.offerState.commingSoon
                };
                $scope.filterByOfferState = function (state) {
                    $scope.offerState = state;
                    loadOffers();
                };

                $scope.filterByOfferState($scope.states.active);

                function loadOffers() {
                    $scope.inProgress = true;
                    var img;
                    offerService.getOffers(
                        $scope.offerState,
                        configurationService.enums.communicationOfferTypeEnum.original
                    ).then(function (response) {
                        $scope.offers = response;
                        $scope.offers.forEach(function (offer) {
                            img = offer.images.getObjectByField("description", configurationService.userPortalDescriptionType);
                            offer.imgUrl = img ? (filesApiUri + img.fileId) : 'undefined';
                            if (offer.rewardThumbnail) {
                                offer.rewardThumbnail.imgUrl = offer.rewardThumbnail.imageId ? (filesApiUri + offer.rewardThumbnail.imageId) : 'undefined';
                            }
                        });
                    }).finally(function () {
                        $scope.inProgress = false;
                    });
                }

                $scope.openCommunicationOfferModal = function (offer) {
                    modalService.open({
                        templateUrl: 'app/shared/directives/offers/modal/offerModal.html',
                        size:'lg',
                        controller: 'offerModalController',
                        windowClass: 'modal-offer',
                        backdrop: true,
                        resolve: {
                            offer: offer,
                            rewardThumbnailTypes: $scope.rewardThumbnailTypes
                        }
                    });
                };
            }
        };
    });
})(angular);
