/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').controller('buyCouponModalController', function (
        $rootScope,
        $scope,
        $uibModalInstance,
        showcaseService,
        coupon
    ) {
        $scope.inProgress = false;
        $scope.coupon = coupon;
        $scope.result = {};
        $scope.triedToBuy = false;

        
        $scope.buyCoupon = function () {
            $scope.inProgress = true;
            showcaseService.buyCoupon($scope.coupon.id).then(
                function (response) {
                    $scope.result.success = response;
                    $rootScope.$broadcast('buyCoupon');
                },
                function (err) {
                    $scope.result.error = err.data.result;
                }
            ).finally(function () {
                $scope.triedToBuy = true;
                $scope.inProgress = false;
            });
        };
        $scope.cancel = $uibModalInstance.dismiss;
    });
})(angular);