(function(angular) {
    /**
     * Купоны
     * @name Showcase
     * @description Отображает список купонов
     * @example
     * <showcase></showcase>
     * @version 1.1
     */
    angular.module('lmxApp').directive('showcase', function(
        $q,
        authService,
        modalService,
        routingService,
        showcaseService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/showcase/showcase.html',

            link: function link($scope) {
                $scope.categoriesType = false;
                $scope.couponsByCategories = {};
                $scope.coupons = [];
                $scope.selectedCategory = null;

                function loadCategories() {
                    $scope.inProgress = true;
                    showcaseService.getCategories().then(function(showcaseServiceResponse) {
                        var couponsPromise = null;
                        var promises = [];
                        showcaseServiceResponse.forEach(function(category) {
                            couponsPromise = showcaseService.getCouponsByCategory(category.logicalName);
                            couponsPromise.then(function(response) {
                                if (response.length) {
                                    $scope.couponsByCategories[category.logicalName] = {
                                        name: category.name,
                                        coupons: response,
                                    };
                                    $scope.coupons.pushArray(response);
                                }
                            });
                            promises.push(couponsPromise);
                        });

                        $q.all(promises).finally(function() {
                            $scope.inProgress = false;
                        });
                    }, function() {
                        $scope.inProgress = false;
                    });
                }
                loadCategories();

                $scope.goToLogin = function() {
                    routingService.goToLogin(true);
                };

                $scope.loggedIn = authService.loggedIn();

                $scope.openBuyCouponModal = function(coupon) {
                    modalService.open({
                        size: 'md',
                        templateUrl: 'app/shared/directives/showcase/modal/buyCouponModal.html',
                        controller: 'buyCouponModalController',
                        resolve: {
                            coupon: coupon,
                        },
                    }).result.then(loadCategories);
                };

                $scope.changeType = function(type) {
                    $scope.categoriesType = type;
                    $scope.selectedCategory = null;
                };

                $scope.showCategory = function(category) {
                    $scope.selectedCategory = category;
                };
            },
        };
    });
})(angular);
