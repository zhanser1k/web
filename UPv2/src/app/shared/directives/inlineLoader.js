/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('inlineLoader', function ($compile, classPrefixerService) {
        return {
            restrict: 'E',
            link: function($scope, element, attrs) {
                var condition = element.html() ? 'ng-show="'+element.html()+'"' : '';
                var classAttr ='inline-loader';

                if (angular.isDefined(attrs.line)) {
                    classAttr += ' line';
                }
                classAttr = classPrefixerService.getPrefixed(classAttr);

                var inlineLoaderHtml = '<span class="' + classAttr + '" ' + condition + '></span>';
                var e = $compile(inlineLoaderHtml)($scope);
                element.replaceWith(e);
            }
        };
    });

})(angular);