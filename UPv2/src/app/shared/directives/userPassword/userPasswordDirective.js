(function(angular) {
    /**
     * Смена пароля пользователя
     * @name UserPassword
     * @description Отображает форму смены пароля пользователя для доступа к личному кабинету.
     * @param {boolean} [registration=false] - Установка пароля в рамках регистрации
     * @example
     * <user-password></user-password>
     * @version 1.1
     */
    angular.module('lmxApp').directive('userPassword', function(
        $http,
        $rootScope,
        $translate,
        authService,
        configurationService,
        notification,
        routingService,
        userAlertService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                registration: '=',
            },
            templateUrl: 'app/shared/directives/userPassword/userPassword.html',

            link: function link($scope) {
                var area = $scope.registration ? 'registration' : 'userPassword';

                $scope.model = {
                    currentPassword: '',
                    newPassword: '',
                    repeatPassword: '',
                };

                $scope.isChangeProcessStarted = false;

                function httpPost(url, params) {
                    return $http.post(url, params, {area: area});
                }

                $scope.setPassword = function() {
                    $scope.inProgress = true;
                    httpPost(configurationService.host + 'api/User/Password/Set', {
                        password: $scope.model.currentPassword,
                    }).then(function(response) {
                        authService.setAuth(response.data, true);
                        $rootScope.$broadcast('passwordIsSet');
                    }).finally(function() {
                        $scope.inProgress = false;
                    });
                };

                $scope.save = function() {
                    $scope.inProgress = true;
                    httpPost(configurationService.host + 'api/User/Password/Change', {
                        oldPassword: $scope.model.currentPassword,
                        newPassword: $scope.model.newPassword,
                    }).then(function(response) {
                        authService.setAuth(response.data);
                        $scope.closeForm();
                        if (!$scope.registration) {
                            userAlertService('Пароль успешно изменён', 'success');
                        }
                    }).finally(function() {
                        $scope.inProgress = false;
                    });
                };

                $scope.openForm = function() {
                    $scope.isChangeProcessStarted = true;
                };
                $scope.closeForm = function() {
                    notification.clearNotifications(area);
                    if ($scope.registration) {
                        authService.clearAuthData();
                        routingService.goToLogin();
                    } else {
                        $scope.isChangeProcessStarted = false;
                    }

                    $scope.model = {
                        currentPassword: '',
                        newPassword: '',
                        repeatPassword: '',
                    };
                };
            },
        };
    });
})(angular);
