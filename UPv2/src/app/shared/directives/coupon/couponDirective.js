/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('coupon', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/shared/directives/coupon/coupon.html'
        };
    });
})(angular);