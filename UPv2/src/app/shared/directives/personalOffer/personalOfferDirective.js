/* eslint-disable */
(function (angular) {
    /**
     * Описание персонального предложения
     * @name PersonalOffer
     * @description Отображает описание персонального предложения в зависимости от параметра offer-id.
     * @param {string} offer-id - идентификатор предложения
     * @example
     * <personal-offer ng-if="appLocationParts[0] === 'personal-offer'" offer-id="appLocationParts[1]"></personal-offer>
     * @version 1.1
     */
    angular.module('lmxApp').directive('personalOffer', function (
        $log,
        $q,
        $timeout,
        backendTypesService,
        brandService,
        modalService,
        offerService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                offerId: '='
            },
            templateUrl: 'app/shared/directives/personalOffer/personalOffer.html',
            link: function link($scope) {

                $scope.discountTypes = backendTypesService.enums.personalOfferDiscount;

                if (!$scope.offerId) {
                    $log.error("Должен быть указан идентификатор персонального предложения");
                    return;
                }

                loadPersonalOffer();
                $scope.$watch('offerId', function (newValue, oldValue) {
                    if (angular.isDefined(oldValue) && newValue !== oldValue) {
                        loadPersonalOffer();
                    }
                });

                function loadPersonalOffer() {
                    $scope.inProgress = true;
                    
                    var personalOfferPromise = offerService.getOfferById($scope.offerId).then(function (response) {
                        $scope.personalOffer = response;
                        brandService.populateImagesUrlFromDescription($scope.personalOffer.brands[0]);
                    });
                    var personalOfferDetailsPromise = offerService.getOfferDetails($scope.offerId).then(function (response) {
                        $scope.offerDetails = response;
                        $timeout(function() {
                            $scope.allViewsRendered = true;
                        });
                    });

                    $q.all([personalOfferPromise, personalOfferDetailsPromise]).finally(function () {
                        $scope.inProgress = false;
                    });
                }

                $scope.openPersonalOfferMapModal = function (detailId) {
                    modalService.open({
                        templateUrl: 'app/shared/directives/personalOffer/modal/personalOfferMapModal.html',
                        size: 'lg',
                        controller: 'personalOfferMapModalController',
                        windowClass: 'modal-personal-offer-map',
                        resolve: {
                            offerId: function () {
                                return $scope.offerId;
                            },
                            detailId: function () {
                                return detailId;
                            }
                        }
                    });
                };
            }
        };
    });
})(angular);