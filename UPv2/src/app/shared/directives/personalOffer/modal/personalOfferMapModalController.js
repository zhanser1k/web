(function(angular) {
    angular.module('lmxApp').controller('personalOfferMapModalController', function(
        $scope,
        $uibModalInstance,
        offerService,
        offerId,
        detailId
    ) {
        var promise;
        $scope.inProgress = true;

        $scope.cancel = $uibModalInstance.dismiss;

        if (detailId) {
            promise = offerService.getOfferDetailMerchants(offerId, detailId);
        } else {
            promise = offerService.getOfferMerchants(offerId);
        }

        promise.then(function(response) {
            $scope.merchants = response;
        }).finally(function() {
            $scope.inProgress = false;
        });
    });
})(angular);
