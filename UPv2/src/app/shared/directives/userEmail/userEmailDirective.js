(function(angular) {
    /**
     * Смена email пользователя
     * @name UserEmail
     * @description Отображает форму смены email пользователя.
     * @param {boolean} [registration=false] - Установка email в рамках регистрации
     * @example
     * <user-email></user-email>
     * @version 1.1
     */
    angular.module('lmxApp').directive('userEmail', function(
        $http,
        $rootScope,
        $translate,
        authService,
        changeEmailService,
        localStorageService,
        notification,
        routingService,
        userAlertService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                registration: '=',
                emailSkippable: '=',
            },
            templateUrl: 'app/shared/directives/userEmail/userEmail.html',

            link: function link($scope) {
                var area = $scope.registration ? 'registration' : 'userEmail';
                changeEmailService.setArea(area);

                if (localStorageService.get('isConfirmEmailSuccess')) {
                    localStorageService.remove('isConfirmEmailSuccess');
                    userAlertService('Email успешно изменён', 'success');
                }

                $scope.isChangeProcessStarted = undefined;
                $scope.step = 1;
                $scope.newEmail = '';
                $scope.model = {
                    newEmail: '',
                    confirmationCode: '',
                };
                $scope.loader = {
                    inProgress: true,
                };

                function cancel() {
                    $scope.step = 1;
                    $scope.isChangeProcessStarted = false;
                    $scope.model.newEmail = '';
                    $scope.model.confirmationCode = '';
                }

                changeEmailService.bindLoader($scope.loader);

                changeEmailService.getState().then(function(response) {
                    $scope.currentEmail = response.currentEmail;
                    if (angular.isObject(response.newEmail) && response.newEmail.email) {
                        $scope.newEmail = response.newEmail.email;
                        $scope.step += 1;
                        $scope.openForm();
                    } else if (!$scope.currentEmail) {
                        $scope.openForm();
                    } else {
                        $scope.isChangeProcessStarted = false;
                    }
                });

                $scope.openForm = function() {
                    $scope.isChangeProcessStarted = true;
                };

                $scope.closeForm = function(isEmailConfirmationSucceeded) {
                    notification.clearNotifications(area);
                    if ($scope.registration) {
                        if ($scope.emailSkippable && $scope.step === 2) {
                            changeEmailService.cancelChangeEmail().then(function() {
                                authService.clearAuthData();
                                routingService.goToLogin();
                            });
                        } else {
                            authService.clearAuthData();
                            routingService.goToLogin();
                        }
                    } else if ($scope.isChangeProcessStarted && !isEmailConfirmationSucceeded) {
                        if ($scope.step === 1) {
                            cancel();
                        } else {
                            changeEmailService.cancelChangeEmail().then(cancel);
                        }
                    } else {
                        $scope.isChangeProcessStarted = false;
                    }
                };

                $scope.reEnterEmail = function(form) {
                    notification.clearNotifications(area);
                    form.$submitted = false;
                    $scope.model.confirmationCode = '';
                    if (!$scope.registration || $scope.emailSkippable) {
                        changeEmailService.cancelChangeEmail().then(function() {
                            $scope.step = 1;
                        });
                    } else {
                        $scope.step = 1;
                    }
                };

                $scope.next = function(form) {
                    if ($scope.step === 1) {
                        $scope.sendCode();
                        form.$submitted = false;
                    } else {
                        $scope.confirmEmail();
                    }
                };

                $scope.sendCode = function() {
                    changeEmailService.sendCode($scope.model.newEmail).then(function() {
                        $scope.newEmail = $scope.model.newEmail;
                        $scope.step += 1;
                    });
                };

                $scope.reSendCode = function() {
                    changeEmailService.reSendCode();
                };

                $scope.confirmEmail = function() {
                    changeEmailService.confirmEmail($scope.model.confirmationCode).then(function() {
                        $scope.currentEmail = $scope.newEmail;

                        $rootScope.$broadcast('emailIsSet');
                        if (!$scope.registration) {
                            userAlertService('Email успешно изменён', 'success');
                            cancel();
                            $scope.closeForm(true);
                        } else {
                            localStorageService.set('forceEmailStepHasCode', true);
                        }
                    });
                };

                $scope.skipStep = function() {
                    localStorageService.set('forceEmailStepSkipped', true);
                    $rootScope.$broadcast('emailIsSet');
                };
            },
        };
    });
})(angular);
