/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('messageService', function (
        $http,
        configurationService
    ) {

        var apiHost = configurationService.host + 'api/Notification';

        /**
         * Получение списка сообщений
         */
        function getMessages() {
            return $http.get(apiHost).then(function(response) {
                return response.data;
            });
        }

        /**
         * Пометить сообщение как прочитанное
         * @param {number} id - идентификатор сообщения
         */
        function setAsRead(id) {
            return $http.post(apiHost + '/' + id + '/Read');
        }

        /**
         * Удаление сообщения
         * @param {number} id - идентификатор сообщения
         */
        function removeMessage(id) {
            return $http.delete(apiHost + '/' + id);
        }

        return {
            getMessages: getMessages,
            removeMessage: removeMessage,
            setAsRead: setAsRead
        };
    });

})(angular);