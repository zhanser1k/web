/* eslint-disable */
(function (angular) {
    /**
     * Сообщения пользователю
     * @name Message
     * @description Отображает список сообщений пользователя.
     * @example
     * <lmx-message></lmx-message>
     * @version 1.1
     */
    angular.module('lmxApp').directive('lmxMessage', function (
        messageService
    ){
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/message/message.html',

            link: function link($scope) {
                $scope.messages = [];

                /**
                 * Получение списка сообщений и установка начальных состояний
                 */
                $scope.getMessages = function() {
                    $scope.isProgress = true;

                    messageService.getMessages().then(function(data) {
                        $scope.messages = data;
                    })
                    .finally(function() {
                        $scope.isProgress = false;
                    });
                };

                /**
                 * Установка сообщения как прочитанного
                 * @param {object} message - сообщение
                 */
                $scope.setAsRead = function(message) {
                    if (!message.isReaded && !setAsReadProcessMessages.contains(message)) {
                        setAsReadProcessMessages.push(message);

                        messageService.setAsRead(message.id).then(function() {
                            message.isReaded = true;
                        }).finally(function() {
                            setAsReadProcessMessages.remove(message);
                        });
                    }
                };

                /**
                 * Раскрытие/скрытие текста сообщения
                 * @param {object} message - сообщение
                 */
                $scope.toggleView = function(message) {
                    message._isShowText = !message._isShowText;

                    $scope.setAsRead(message);
                };

                /**
                 * Удаление сообщения
                 * @param {object} message - сообщение
                 */
                $scope.remove = function(message) {
                    if (!removeProcessMessages.contains(message)) {
                        removeProcessMessages.push(message);

                        messageService.removeMessage(message.id).then(function() {
                            $scope.messages.remove(message);
                        }).finally(function() {
                            removeProcessMessages.remove(message);
                        });
                    }
                };

                var setAsReadProcessMessages = [];
                var removeProcessMessages = [];

                $scope.getMessages();
            }
        };
    });
})(angular);