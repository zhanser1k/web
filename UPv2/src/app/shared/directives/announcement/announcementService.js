/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('announcementService', function (
        $http,
        configurationService
    ) {
        var apiHost = configurationService.host + 'api/';

        /**
         * Получение рекламных материалов со статусом "Опубликовано"
         * @param {string} spaceLogicalName - идентификатор рекламного места
         */
        function getPublishedAnnouncements(spaceLogicalName) {
            return $http.get(apiHost + 'Announcements', {
                params: {"filter.spaceLogicalName": spaceLogicalName}
            }).then(function(response) {
                return response.data.reduce(function(filtered, item) {
                    if (item.state === 'Published') {
                        item.attributes = item.attributes.toDictionary("attributeLogicalName");
                        filtered.push(item);
                    }
                    return filtered;
                }, []);
            });
        }

        /**
         * Путь к картинке рекламного предложения
         * @param {string} value - идентификатор картинки
         * @returns {string} - URI картинки
         */
        function getAnnouncementImagePath(value) {
            return apiHost + 'Files/' + value;
        }

        /**
         * Получение рекламного материала по id
         * @param {int} id - идентификатор рекламного материала
         */
        function getAnnouncementById(id) {
            var attributes = {};
            return $http.get(apiHost + 'Announcements/'+ id).then(function (response) {
               attributes = response.data.attributes.toDictionary('attributeLogicalName');
               response.data.attributes = attributes;
               return response.data;
           });
        }

        return {
            getAnnouncementById: getAnnouncementById,
            getAnnouncementImagePath: getAnnouncementImagePath,
            getPublishedAnnouncements: getPublishedAnnouncements
        };
    });

})(angular);