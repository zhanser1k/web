/* eslint-disable */
(function (angular) {
    /**
     * Реклама
     * @name Announcement
     * @description В зависимости от параметра announcement-id, отображает список реклам или определенную рекламу.
     * @param {string} space - идентификатор рекламного места
     * @param {string} announcement-id - идентификатор рекламы
     * @example
     * <!-- Использование для отображения рекламы на рекламном месте -->
     * <lmx-announcement space="offerSpace"></lmx-announcement>
     *
     * <!-- Использование для отображения описания конкретной рекламы -->
     * <lmx-announcement announcement-id="appLocationParts[1]"></lmx-announcement>
     * @version 1.1
     */
    angular.module('lmxApp').directive('lmxAnnouncement', function (
        $log,
        $rootScope,
        $sce,
        announcementService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                announcementId: '='
            },
            template: '<div><div ng-include="getContentUrl()"></div></div>',
            link: function link($scope, elem, attrs) {
                var spaceLogicalName = attrs.space || false;

                if (!spaceLogicalName && !$scope.announcementId) {
                    $log.error("Должен быть указан идентификатор рекламного места или идентификатор рекламного материала");
                    return;
                }

                $scope.announcementsPath = $rootScope.appLocationParts[0];
                $scope.changeLocation = $rootScope.changeLocation;

                function getAnnouncementById() {
                    $scope.isProgress = true;
                    announcementService.getAnnouncementById($scope.announcementId).then(function (data) {
                        $scope.announcement = data;
                    }).finally(function () {
                        $scope.isProgress = false;
                    });
                }

                if ($scope.announcementId) {
                    getAnnouncementById();
                    $scope.$watch('announcementId', function (newValue, oldValue) {
                        if (angular.isDefined(oldValue) && newValue !== oldValue) {
                            getAnnouncementById();
                        }
                    });
                } else {
                    $scope.isProgress = true;
                    announcementService.getPublishedAnnouncements(spaceLogicalName).then(function(data) {
                        $scope.announcements = data;
                    })
                    .finally(function() {
                        $scope.isProgress = false;
                    });
                }

                $scope.isProgress = true;

                /**
                 * Получение шаблона для отображения
                 * @returns {string}
                 */
                $scope.getContentUrl = function () {
                    var file = spaceLogicalName || 'view';
                    return 'app/shared/directives/announcement/templates/' + file + '.html';
                };

                /**
                 * Получение пути к картинке рекламного материала
                 * @param {string} value - идентификатор картинки
                 * @returns {string} - путь к картинке
                 */
                $scope.getAnnouncementImagePath = function(value) {
                    return announcementService.getAnnouncementImagePath(value);
                };

                $scope.getTrustedHtmlContent = function(htmlContent) {
                    return $sce.trustAsHtml(htmlContent);
                };

                $scope.getAnnouncementPath = function (announcementId) {
                    return $scope.announcementsPath + '/' + announcementId;
                }
            }
        };
    });
})(angular);