/* eslint-disable */
(function (angular) {
    /**
     * Описание бренда
     * @name Brand
     * @description В зависимости от параметра brand-id, отображает описание бренда.
     * @param {string} brand-id - идентификатор бренда
     * @example
     * <brand brand-id="appLocationParts[1]"></brand>
     * @version 1.1
     */
    angular.module('lmxApp').directive('brand', function (
        $log,
        $q,
        brandService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                brandId: '='
            },
            templateUrl: 'app/shared/directives/brand/brand.html',
            link: function link($scope) {

                $scope.inProgress = true;

                if (!$scope.brandId) {
                    $log.error("Должен быть указан идентификатор бренда");
                    return;
                }

                if ($scope.brandId) {
                    loadBrandData();
                    $scope.$watch('brandId', function (newValue, oldValue) {
                        if (angular.isDefined(oldValue) && newValue !== oldValue) {
                            loadBrandData();
                        }
                    });
                }

                function loadBrandData() {
                    var promises = [];

                    var getBrandByIdPromise = brandService.getBrandById($scope.brandId).then(function (response) {
                        $scope.brand = response;
                    });
                    promises.push(getBrandByIdPromise);

                    var getMerchantsPromise = brandService.getBrandMerchants($scope.brandId).then(function (response) {
                        $scope.merchants = response;
                    });
                    promises.push(getMerchantsPromise);

                    $q.all(promises).finally(function () {
                        $scope.inProgress = false;
                    });
                }
            }
        };
    });
})(angular);