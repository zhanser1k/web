/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('userStatusService', function (
        $http,
        configurationService
    ) {
        var apiHost = configurationService.host + 'api/User/Lama';

        /**
         * Получение информации о статусе пользователя
         */
        function getInfo() {
            return $http.get(apiHost + '/Info').then(function (response) {
                return response.data;
            });
        }

        return {
            getInfo: getInfo
        };
    });

})(angular);