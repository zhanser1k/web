/* eslint-disable */
(function (angular) {
    /**
     * Статус пользователя
     * @name UserStatus
     * @description Отображает статус пользователя в программе лояльности.
     * @example
     * <user-status></user-status>
     * @version 1.1
     */
    angular.module('lmxApp').directive('userStatus', function (
        userStatusService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/userStatus/userStatus.html',

            link: function link($scope) {
                $scope.isProgress = true;

                userStatusService.getInfo().then(function(response) {
                    $scope.info = response;
                }).finally(function() {
                    $scope.isProgress = false;
                });
            }
        };
    });
})(angular);