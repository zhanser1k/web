/* eslint-disable */
(function (angular) {

    // директива реализует поведение связки ng-click -> ng-class, но без добавления watcher-а.
    // входные данные - css-class или scope-переменная
    angular.module('lmxApp').directive('classToggleClick', function($log, classPrefixerService) {
        return {
            restrict: 'A',
            scope: {
                classToggleClick: '='
            },
            link: function ($scope, elem, attrs) {
                if (!attrs.classToggleClick) {
                    $log.warn('You cannot use classToggleClick with undefined attribute value', elem);
                    return;
                }

                elem.bind('click', function () {
                    var cssClass = attrs.classToggleClick;
                    if ($scope.classToggleClick) {
                        cssClass = $scope.classToggleClick;
                    }
                    elem.toggleClass(classPrefixerService.prefix + cssClass);
                });
            }
        };
    });

})(angular);