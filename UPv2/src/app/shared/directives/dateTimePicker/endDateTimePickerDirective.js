﻿/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('endDateTimePicker', function (dateTimePickerDirective) {

        var dateTimePicker = dateTimePickerDirective[0];

        return angular.extend({}, dateTimePicker, {
            controller: ['$scope', function($scope) {
                dateTimePicker.controller($scope);

                $scope.$watch('model', function(newValue) {
                    if (newValue && angular.isDate(newValue)) {
                        $scope.model.setSeconds(59);
                    }
                });
            }]
        });
    });

})(angular);