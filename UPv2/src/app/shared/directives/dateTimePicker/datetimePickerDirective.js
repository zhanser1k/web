﻿/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('dateTimePicker', function ($locale, datePickerDirective) {

        function controller ($scope) {
            var customDateTimeFormats = {
                ru: 'dd.MM.yyyy HH:mm'
            };

            // $scope.dateFormat = customDateFormats[$locale.id] || $locale.DATETIME_FORMATS.shortDate;
            $scope.dateFormat = customDateTimeFormats.ru;

            $scope.timepickerOptions = {
                // данный picker не понимает что при 24-часовом формате (H/HH) не нужно показывать выбор времени суток (AM/PM)
                showMeridian: $scope.dateTimeFormat.match(/h/)
            };
        }

        controller.$inject = ["$scope"];

        return angular.extend({}, datePickerDirective[0], {
            templateUrl: 'app/shared/directives/dateTimePicker/dateTimePickerView.html',
            controller: controller
        });
    });

})(angular);