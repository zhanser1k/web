(function(angular) {
    /**
     * История операции пользователя
     * @name History
     * @description Отображает все проведенные пользователем операции в рамках предложенной программы лояльности.
     * @param {number} [on-page=10] - Количество операций, оторбражаемых на странице
     * @param {boolean} [load-more-button=false] - Кнопка прогрузки данных (true) или пагинация (false)
     * @param {html-attribute} [preview] - Режим предпросмотра истории операций (без фильтров и пейджинации, с возможностью перехода к полной истории операций)
     * @example
     * <!-- Отображение 15 операций на странице -->
     * <history on-page="15"></history>
     *
     * <!-- Отображение с пагинацией -->
     * <history load-more-button="false"></history>
     *
     * <!-- Отображение с кнопкой прогрузки данных -->
     * <history load-more-button="true"></history>
     *
     * <!-- Отображение препросмотра истории операций (только последние 10 операций) -->
     * <history preview on-page="10"></history>
     * @version 1.1
     */
    angular.module('lmxApp').directive('history', function(
        historyService,
        cardsService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/history/history.html',

            link: function link($scope, $element, $attributes) {
                $scope.onPage = parseInt($attributes.onPage, 10) || 10;
                $scope.isPreview = angular.isDefined($attributes.preview);
                // TODO: изменить логику. По умолчанию - просто список с одной кнопкой "показать ещё".
                // Нужна паджинация - добавяляем атрибут enable-pagination
                $scope.isPaginated = angular.isUndefined($attributes.loadMoreButton) || $scope.$eval($attributes.loadMoreButton) === false;
                $scope.cardsInProgress = true;
                $scope.selectedCard = 0;
                $scope.from = 0;
                $scope.currentPage = 1;
                $scope.history = undefined;

                cardsService.getCards().then(function(response) {
                    $scope.cards = response;
                }).finally(function() {
                    $scope.cardsInProgress = false;
                });

                // Метод для суммирования значений бонусов и скидок по типам
                function getSummedRewards(rewards) {
                    var item;
                    var result = {};
                    for (var i = 0; i < rewards.length; i++) {
                        item = rewards[i];
                        result[item.rewardType] = currency(result[item.rewardType] || 0).add(currency(item.amount.amount)).value;
                    }
                    return result;
                }

                function getTypeRewards(rewards) {
                    var item;
                    var result = {};
                    for (var i = 0; i < rewards.length; i++) {
                        item = rewards[i];
                        result[item.rewardType] = item.amount.currency;
                    }
                    return result;
                }

                function loadHistory(params) {
                    $scope.inProgress = true;

                    var data = Object.assign({
                        isLoadMore: false,
                        from: $scope.from,
                    }, params);

                    var fromDate = $scope.fromDate ? (new Date($scope.fromDate.setHours(0, 0, 0, 0))) : null;
                    var toDate = $scope.toDate ? (new Date($scope.toDate.setHours(23, 59, 59, 999))) : null;

                    historyService.query(
                        data.from,
                        $scope.onPage,
                        fromDate,
                        toDate,
                        parseInt($scope.selectedCard, 10) || null
                    ).then(function(response) {
                        response.rows.forEach(function(history) {
                            if (Array.isArray(history.data.rewards) && !!history.data.rewards.length) {
                                history.rewardsResult = getSummedRewards(history.data.rewards);
                                history.rewardsType = getTypeRewards(history.data.rewards);
                            }
                        });

                        if (data.isLoadMore) {
                            angular.extend($scope.history, response, {
                                rows: $scope.history.rows.pushArray(response.rows),
                            });
                        } else {
                            $scope.history = response;
                        }

                        if ($scope.isPaginated) {
                            $scope.pages = Math.ceil($scope.history.allCount / $scope.onPage);
                        }
                    }).finally(function() {
                        $scope.inProgress = false;
                        $scope.isReload = undefined;
                    });
                }

                $scope.increaseFrom = function() {
                    $scope.from += $scope.onPage;
                    $scope.currentPage += 1;
                    loadHistory({isLoadMore: !$scope.isPaginated});
                };

                $scope.paginationChange = function() {
                    $scope.from = ($scope.currentPage - 1) * $scope.onPage;
                    loadHistory({from: $scope.from});
                };

                $scope.reloadHistory = function() {
                    $scope.isReload = true;
                    $scope.currentPage = 1;
                    $scope.from = 0;
                    loadHistory();
                };

                function getDateMinusMonths(count) {
                    var d = new Date();
                    d.setHours(0, 0, 0, 0);
                    return d.setMonth(d.getMonth() - count);
                }

                $scope.resetFilters = function() {
                    $scope.toDate = new Date();
                    $scope.fromDate = angular.isDefined($attributes.allTime) ? new Date(0) : new Date(getDateMinusMonths(3));
                    $scope.selectedCard = 0;
                    $scope.reloadHistory();
                };

                $scope.resetFilters();
            },
        };
    });
})(angular);
