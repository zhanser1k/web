/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("historyService", function ($http, configurationService) {

        function query(from, count, fromDate, toDate, cardId) {
            return $http.get(configurationService.host + 'api/History', {
                params: {
                    "filter.from": from||0,
                    "filter.count": count||10,
                    "filter.fromDate": fromDate,
                    "filter.toDate": toDate || new Date(),
                    "filter.cardId": cardId
                }
            }).then(function(response) {
                return response.data;
            });
        }

        return {
            query: query
        };
    });
})(angular);

