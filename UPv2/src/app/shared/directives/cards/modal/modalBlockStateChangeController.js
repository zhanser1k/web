(function(angular) {
    angular.module('lmxApp').controller('modalBlockStateChangeController', function(
        $scope,
        $uibModalInstance,
        card,
        cardsService,
        notification
    ) {
        cardsService.setArea('cardModal');
        notification.clearNotifications('cardModal');

        $scope.inProgress = false;
        $scope.model = {
            cardId: card.id,
            password: '',
        };

        $scope.isBlocked = card.block;

        $scope.blockStateChange = function() {
            $scope.inProgress = true;
            cardsService.changeBlockState($scope.model.cardId, $scope.model.password).then(function(response) {
                $uibModalInstance.close(response.data);
            }).finally(function() {
                $scope.inProgress = false;
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    });
})(angular);
