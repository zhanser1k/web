(function(angular) {
    angular.module('lmxApp').controller('modalReplaceCardController', function(
        $rootScope,
        $scope,
        $uibModalInstance,
        card,
        cardsService,
        notification
    ) {
        cardsService.setArea('cardModal');
        notification.clearNotifications('cardModal');

        $scope.inProgress = false;
        $scope.model = {
            cardId: card.id,
            cardNumber: '',
            password: '',
        };

        $scope.replaceCard = function() {
            $scope.inProgress = true;
            cardsService.replaceCard(
                $scope.model.cardId,
                $scope.model.cardNumber,
                $scope.model.password
            ).then(function(response) {
                $rootScope.$broadcast('cardReplaced');
                $uibModalInstance.close(response.data);
            }).finally(function() {
                $scope.inProgress = false;
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    });
})(angular);
