﻿(function(angular) {
    angular.module('lmxApp').controller('modalAttachCardController', function(
        $scope,
        $uibModalInstance,
        cardsService,
        notification
    ) {
        cardsService.setArea('cardModal');
        notification.clearNotifications('cardModal');

        $scope.inProgress = false;
        $scope.confirmForm = undefined;

        $scope.model = {
            cardNumber: '',
            password: '',
            confirmCode: '',
        };

        function showProgress(promise) {
            $scope.inProgress = true;
            promise.finally(function() {
                $scope.inProgress = false;
            });
        }

        function checkAttachState() {
            var promise = cardsService.getAttachState().then(function(response) {
                if (response.isStarted) {
                    $scope.confirmForm = true;
                } else {
                    $scope.confirmForm = false;
                }
            });
            showProgress(promise);
        }

        checkAttachState();

        $scope.startAttachCard = function() {
            var promise = cardsService.attachCard($scope.model.cardNumber, $scope.model.password).then(function() {
                $scope.confirmForm = true;
            });
            showProgress(promise);
        };

        $scope.confirm = function() {
            var promise = cardsService.attachCardConfirm($scope.model.confirmCode).then(function(response) {
                $uibModalInstance.close(response.data);
            });
            showProgress(promise);
        };

        $scope.cancelAttachCard = function() {
            var promise = cardsService.cancelAttachCard().then(function(response) {
                $uibModalInstance.close(response.data);
            });
            showProgress(promise);
        };

        $scope.sendConfirmCode = function() {
            var promise = cardsService.sendConfirmCode();
            showProgress(promise);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    });
})(angular);
