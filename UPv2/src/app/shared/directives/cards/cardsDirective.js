(function(angular) {
    /**
     * Информация о картах пользователя
     * @name Cards
     * @description Отображает информацию о картах пользователя и возможные операции с ними.
     * @example
     * <cards></cards>
     * @version 1.1
     */
    angular.module('lmxApp').directive('cards', function(
        cardsService,
        configurationService,
        modalService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/cards/cards.html',

            link: function link($scope) {
                $scope.inProgress = true;

                function getUserPortalDescription(object) {
                    return object.getObjectByField('description', configurationService.userPortalDescriptionType);
                }

                function populateImagesUrl() {
                    var cardCategory;
                    var image;
                    for (var i = 0; i < $scope.cards.length; i++) {
                        cardCategory = $scope.cards[i].cardCategory;

                        if (angular.isArray(cardCategory.images)) {
                            for (var j = 0; j < cardCategory.images.length; j++) {
                                image = getUserPortalDescription(cardCategory.images);
                                $scope.cards[i].imageUrl = image ? (configurationService.host + 'api/Files/' + image.fileId) : 'undefined';
                            }
                        }
                    }
                }

                function loadCards() {
                    $scope.inProgress = true;
                    cardsService.getCards().then(function(response) {
                        $scope.cards = response;
                        populateImagesUrl();
                    }).finally(function() {
                        $scope.inProgress = false;
                    });
                }

                loadCards();

                $scope.$on('cardReplaced', function() {
                    loadCards();
                });

                function openModal(modalName, card) {
                    var modalOptions = {
                        templateUrl: 'app/shared/directives/cards/modal/' + modalName + '.html',
                        controller: modalName + 'Controller',
                        backdrop: 'static',
                        keyboard: false,
                        windowClass: 'modal-cards',
                    };

                    if (card) {
                        modalOptions.resolve = {
                            card: function() {
                                return card;
                            },
                        };
                    }

                    modalService.open(modalOptions).result.then(function() {
                        loadCards();
                    });
                }

                $scope.blockStateChange = function(card) {
                    openModal('modalBlockStateChange', card);
                };

                $scope.replaceCard = function(card) {
                    openModal('modalReplaceCard', card);
                };

                $scope.attachCard = function() {
                    openModal('modalAttachCard');
                };
            },
        };
    });
})(angular);
