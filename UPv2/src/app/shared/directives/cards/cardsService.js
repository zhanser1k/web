(function(angular) {
    angular.module('lmxApp').service('cardsService', function($http, configurationService) {
        var apiUrl = configurationService.host + 'api/Cards/';

        var area;
        function setArea(serviceArea) {
            area = serviceArea;
        }

        function getCards() {
            return $http.get(apiUrl, {area: area}).then(function(response) {
                return response.data;
            });
        }

        function changeBlockState(cardId, password) {
            return $http.post(apiUrl + cardId + '/ChangeBlockState', JSON.stringify(password), {area: area});
        }

        function replaceCard(cardId, cardNumber, password) {
            var data = {
                cardNumber: cardNumber,
                password: password,
            };
            return $http.post(apiUrl + cardId + '/Replace', data, {area: area}).then(function(response) {
                return response;
            });
        }

        /**
         * AttachCard - используется для добавления уже активированной карты (у которой есть пароль, под которой можно уже зайти в ЛК).
         * Синонимы: Прикрепление карты, объедидение карт, объединение счетов.
         * @param {string} cardNumber - номер прикрепляемой карты.
         * @param {string} password - пароль прикрепляемой карты.
         */
        function attachCard(cardNumber, password) {
            var data = {
                cardNumber: cardNumber,
                password: password,
            };
            return $http.post(apiUrl + 'Attach', data, {area: area}).then(function(response) {
                return response;
            });
        }

        function attachCardConfirm(confirmCode) {
            return $http.post(apiUrl + 'Attach/Confirm', confirmCode, {area: area}).then(function(response) {
                return response;
            });
        }

        function getAttachState() {
            return $http.get(apiUrl + 'Attach', {area: area}).then(function(response) {
                return response.data;
            });
        }

        function cancelAttachCard() {
            return $http.post(apiUrl + 'Attach/Cancel', {area: area}).then(function(response) {
                return response;
            });
        }

        function sendConfirmCode() {
            return $http.post(apiUrl + 'Attach/SendConfirmCode', null, {area: area}).then(function(response) {
                return response;
            });
        }

        /**
         * SetCard - используется для добавления неактивированной карты (ничья карты, карта без пароля) к текущему пользователю.
         * Применяется, только когда у пользователя нет реальной карты, а только виртуальная.
         * @param {string} cardNumber - номер добавляемой карты.
         */
        function setCard(cardNumber) {
            return $http.post(apiUrl + 'Set', {cardNumber: cardNumber}, {area: area}).then(function(response) {
                return response;
            });
        }

        function getVirtualCardInfo() {
            return $http.get(apiUrl + 'EmitVirtual').then(function(response) {
                return response.data;
            });
        }

        function emitVirtualCard() {
            return $http.put(apiUrl + 'EmitVirtual').then(function(response) {
                return response;
            });
        }

        return {
            setArea: setArea,
            attachCard: attachCard,
            attachCardConfirm: attachCardConfirm,
            cancelAttachCard: cancelAttachCard,
            changeBlockState: changeBlockState,
            emitVirtualCard: emitVirtualCard,
            getAttachState: getAttachState,
            getCards: getCards,
            getVirtualCardInfo: getVirtualCardInfo,
            replaceCard: replaceCard,
            sendConfirmCode: sendConfirmCode,
            setCard: setCard,
        };
    });
})(angular);

