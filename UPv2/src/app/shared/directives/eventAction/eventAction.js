/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').directive('eventAction', function ($log) {
        return {
            restrict: 'A',
            link: function link($scope ,elem, attrs) {
                if (!attrs.event) {
                    $log.error("Не указан аттрибут event");
                    return;
                }

                $scope.$on(attrs.event, function () {
                    $scope.$eval(attrs.eventAction);
                });
            }
        };
    });
})(angular);