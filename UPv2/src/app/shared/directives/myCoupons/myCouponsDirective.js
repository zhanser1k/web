(function(angular) {
    /**
     * Купоны пользователя
     * @name MyCoupons
     * @description Отображает список купонов пользователя с изображением и описанием.
     * @example
     * <my-coupons></my-coupons>
     * @version 1.1
     */
    angular.module('lmxApp').directive('myCoupons', function(
        modalService,
        showcaseService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/myCoupons/myCoupons.html',

            link: function link($scope) {
                $scope.coupons = [];

                $scope.barcodeOptions = {
                    width: 2,
                    height: 100,
                    quite: 10,
                    displayValue: true,
                    font: 'monospace',
                    textAlign: 'center',
                    fontSize: 16,
                    backgroundColor: '',
                    lineColor: '#000000',
                };

                function loadUserCoupons() {
                    $scope.inProgress = true;
                    showcaseService.getUserCoupons(true).then(function(response) {
                        $scope.coupons = response;
                    }).finally(function() {
                        $scope.inProgress = false;
                    });
                }

                $scope.isRefundable = function(coupon) {
                    return coupon.state === 'ReadyToUse' && coupon.isRefundable;
                };

                $scope.refundCoupon = function(couponId) {
                    $scope.inProgress = true;
                    showcaseService.refundUserCoupon(couponId).then(function() {
                        loadUserCoupons();
                    }).catch(function() {
                        $scope.inProgress = false;
                    });
                };

                loadUserCoupons();
            },
        };
    });
})(angular);
