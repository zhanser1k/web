/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').controller('couponViewModalController', function (
        $scope,
        $uibModalInstance,
        showcaseService,
        coupon
    ) {
        $scope.coupon = coupon;
        $scope.isRefundable = coupon.state === 'ReadyToUse' && coupon.isRefundable;
        $scope.cancel = $uibModalInstance.dismiss;
        $scope.barcodeOptions = {
            width: 2,
            height: 100,
            quite: 10,
            displayValue: true,
            font: "monospace",
            textAlign: "center",
            fontSize: 18,
            backgroundColor: "",
            lineColor: "#000"
        };
        $scope.refundCoupon = function(couponId) {
            $scope.inProgress = true;
            showcaseService.refundUserCoupon(couponId).then(function (response) {
                $uibModalInstance.close(response);
            }).finally(function () {
                $scope.inProgress = false;
            });
        };
    });
})(angular);