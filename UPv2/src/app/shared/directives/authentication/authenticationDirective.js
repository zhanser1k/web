(function(angular) {
    /**
     * Авторизация пользователя
     * @name Authentication
     * @description Отображает форму авторизации пользователя.
     * @example
     * <authentication></authentication>
     * @version 1.1
     */
    angular.module('lmxApp').directive('authentication', function(
        $http,
        $rootScope,
        authService,
        configurationService,
        routingService,
        shareAuthLoginToRegistration
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'app/shared/directives/authentication/authentication.html',

            link: function link($scope) {
                authService.setArea('login');

                $scope.loginData = {
                    login: '',
                    password: '',
                };

                if (authService.loggedIn()) {
                    routingService.goToDefault();
                }

                $scope.authentication = authService.authentication;

                $scope.login = function() {
                    $scope.authInProgress = true;
                    var loginData = angular.copy($scope.loginData);
                    authService.login(loginData).then(function() {
                        if (authService.loggedIn()) {
                            routingService.goToDefault();
                        } else if (!configurationService.options.forceRegistrationStartOnLoginAttempt) {
                            $scope.authInProgress = false;
                        }
                    }, function() {
                        $scope.authInProgress = false;
                    }).finally(function() {
                        $rootScope.$broadcast('authCompleted');
                    });
                };

                if (configurationService.options.shareAuthLoginToRegistration) {
                    $scope.$watch('loginData.login', function(newLogin, oldLogin) {
                        if (newLogin !== oldLogin) {
                            shareAuthLoginToRegistration.loginAuth = newLogin;
                        }
                    });

                    $scope.$on('$locationChangeStart', function(event, newLocation) {
                        if (routingService.getApplicationURL(newLocation) !== 'registration') {
                            shareAuthLoginToRegistration.clearLogin();
                        }
                    });
                }
            },
        };
    });
})(angular);
