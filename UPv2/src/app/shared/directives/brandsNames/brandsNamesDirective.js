(function(angular) {
    /**
     * Отображение списка названий брендов
     * @name BrandsNames
     * @description Если в списке несколько брендов - отображение строки с перечислением их названий. Если бренд один - отображение его изображения.
     * @param {Object[]} brands - список брендов
     * @example <brands-names brands="offer.brands"></brands-names>
     */
    angular.module('lmxApp').directive('brandsNames', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                brands: '<',
            },
            templateUrl: 'app/shared/directives/brandsNames/brandsNames.html',
        };
    });
})(angular);
