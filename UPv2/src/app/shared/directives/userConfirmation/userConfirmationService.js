(function(angular) {
    angular.module('lmxApp').factory('userConfirmationService', function($uibModal) {
        return function(message, type, confirmButtonText, isClosedOnEsc) {
            var confirmClass = type || 'error';
            var modalConfirmation = $uibModal.open({
                templateUrl: 'app/shared/directives/userConfirmation/userConfirmationView.html',
                controller: 'userConfirmationController',
                backdrop: 'static',
                keyboard: angular.isNullOrUndefined(isClosedOnEsc) ? true : isClosedOnEsc,
                windowClass: 'lmx-modal lmx-centered-modal lmx-userAlert-modal ' + confirmClass + '-modal',
                resolve: {
                    message: function() {
                        return message;
                    },
                    confirmButtonText: function() {
                        return confirmButtonText;
                    },
                },
            });

            return modalConfirmation.result;
        };
    });
})(angular);
