(function(angular) {
    angular.module('lmxApp').controller('userConfirmationController', function(
        $scope,
        $uibModalInstance,
        translateFilter,
        message,
        confirmButtonText
    ) {
        $scope.message = message;
        $scope.confirmButtonText = confirmButtonText || translateFilter('yes');

        $scope.ok = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    });
})(angular);
