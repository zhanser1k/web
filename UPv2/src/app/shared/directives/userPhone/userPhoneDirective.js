(function(angular) {
    /**
     * Смена номера телефона пользователя
     * @name UserPhone
     * @description Отображает форму смены номера телефона пользователя.
     * @param {boolean} [registration=false] - Установка номера телефона в рамках регистрации
     * @example
     * <user-phone></user-phone>
     * @version 1.1
     */
    angular.module('lmxApp').directive('userPhone', function(
        $http,
        $rootScope,
        $translate,
        authService,
        changePhoneService,
        configurationService,
        notification,
        routingService,
        userAlertService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                registration: '=',
            },
            templateUrl: 'app/shared/directives/userPhone/userPhone.html',

            link: function link($scope) {
                var area = $scope.registration ? 'registration' : 'userPhone';
                changePhoneService.setArea(area);

                $scope.isChangeProcessStarted = undefined;
                $scope.step = 1;
                $scope.newPhone = '';
                $scope.model = {
                    newPhone: '',
                    confirmationCode: '',
                };
                $scope.loader = {
                    inProgress: true,
                };

                changePhoneService.bindLoader($scope.loader);

                changePhoneService.getState().then(function(response) {
                    $scope.currentPhoneNumber = response.currentPhoneNumber;
                    if (angular.isObject(response.newPhoneNumber) && response.newPhoneNumber.phoneNumber) {
                        $scope.newPhone = response.newPhoneNumber.phoneNumber;
                        $scope.step += 1;
                        $scope.openForm();
                    } else if (!$scope.currentPhoneNumber) {
                        $scope.openForm();
                    } else {
                        $scope.isChangeProcessStarted = false;
                    }
                });

                $scope.openForm = function() {
                    $scope.isChangeProcessStarted = true;
                };

                function cancel() {
                    $scope.step = 1;
                    $scope.isChangeProcessStarted = false;
                    $scope.newPhone = '';
                    $scope.model = {
                        newPhone: '',
                        confirmationCode: '',
                    };
                }

                $scope.closeForm = function() {
                    notification.clearNotifications(area);
                    if ($scope.registration) {
                        authService.clearAuthData();
                        routingService.goToLogin();
                    } else if ($scope.currentPhoneNumber) {
                        if ($scope.step === 1) {
                            cancel();
                        } else {
                            changePhoneService.cancelChangePhone().then(cancel);
                        }
                    } else {
                        $scope.isChangeProcessStarted = false;
                    }
                };

                $scope.reEnterPhone = function(form) {
                    notification.clearNotifications(area);
                    form.$submitted = false;
                    $scope.model.confirmationCode = '';
                    if ($scope.currentPhoneNumber) {
                        changePhoneService.cancelChangePhone().then(function() {
                            $scope.step = 1;
                        });
                    } else {
                        $scope.step = 1;
                    }
                };

                $scope.next = function(form) {
                    if ($scope.step === 1) {
                        $scope.sendCode();
                        form.$submitted = false;
                    } else {
                        $scope.confirmPhone();
                    }
                };

                $scope.sendCode = function() {
                    changePhoneService.sendCode($scope.model.newPhone).then(function() {
                        $scope.newPhone = '***' + $scope.model.newPhone.toString().slice(-4);
                        $scope.step += 1;
                    });
                };

                $scope.reSendCode = function() {
                    changePhoneService.reSendCode();
                };

                $scope.confirmPhone = function() {
                    var setAsPassword = false;
                    if ($scope.registration) {
                        setAsPassword = configurationService.options.setRegistrationSmsCodeAsPassword;
                    }
                    changePhoneService.confirmPhone($scope.model.confirmationCode, setAsPassword).then(function(token) {
                        authService.setAuth(token, true);
                        $scope.currentPhoneNumber = $scope.newPhone;
                        $rootScope.$broadcast('phoneIsSet');
                        if (!$scope.registration) {
                            userAlertService('Номер телефона успешно изменён', 'success');
                        }
                        if (!$scope.registration) {
                            cancel();
                        }
                    });
                };
            },
        };
    });
})(angular);
