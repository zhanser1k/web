(function(angular) {
    /**
     * Основная информация о персональном предложении
     * @name PersonalOfferContent
     * @description Отображает основную информацию о персональном предложении
     * @param {Object} personal-offer - персональное предложение
     * @example
     * <personal-offer-content personal-offer="personalOffer"></personal-offer-content>
     */
    angular.module('lmxApp').directive('personalOfferContent', function($rootScope, modalService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                personalOffer: '<',
            },
            templateUrl: 'app/shared/directives/personalOfferContentView/personalOfferContent.html',
            link: function($scope) {
                $scope.isPreview = !$rootScope.appLocationParts[1];

                $scope.openPersonalOfferMapModal = function() {
                    modalService.open({
                        templateUrl: 'app/shared/directives/personalOffer/modal/personalOfferMapModal.html',
                        size: 'lg',
                        controller: 'personalOfferMapModalController',
                        windowClass: 'modal-personal-offer-map',
                        resolve: {
                            offerId: function() {
                                return $scope.personalOffer.id;
                            },
                            detailId: null,
                        },
                    });
                };
            },
        };
    });
})(angular);
