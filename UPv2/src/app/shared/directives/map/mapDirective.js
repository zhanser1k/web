(function(angular) {
    /**
     * Яндекс-карта для отображения данных
     * @name LmxMap
     * @description Яндекс карта для отображения данных, может использоваться в составе других директив (магазины и бренды).
     * Для того чтобы настраивать размер и изображение маркера нужно определить объект <b>options</b>, который  содержит
     * свойства <b>markerSize</b> и <b>markerUrl</b>.
     * @param {array} data - массив состоящий из данных для отображения (merchants, brands), включающий в себя объект
     * location со свойствами latitude и longitude.
     * @param {object} [options] - Опции отображения маркера.
     * @example
     * <lmx-map data="merchants" options="{markerSize: [29, 32], markerUrl: brand.markerImg}"></lmx-map>
     * @version 1.1
     */
    angular.module('lmxApp').directive('lmxMap', function(mapService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                data: '=',
            },
            template: '<div></div>',

            link: function link(scope, elem) {
                var map = new mapService();

                map.setYmaps(elem[0], scope.data, scope.options);

                scope.$on('openBalloon', function(event, index) {
                    map.showBalloon(index);
                });

                scope.$watch('data', function(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        map.updateData(newValue);
                    }
                });
            },
        };
    });
})(angular);
