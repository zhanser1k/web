/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').controller('userAlertController', function ($scope, $uibModalInstance, message) {
        $scope.message = message;

        $scope.close = function () {
            $uibModalInstance.close();
        };
    });

})(angular);