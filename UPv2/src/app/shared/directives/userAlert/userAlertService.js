/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('userAlertService', function ($uibModal) {
        return function(message, type) {
            var confirmClass = type || 'error';
            var modalConfirmation = $uibModal.open({
                templateUrl: 'app/shared/directives/userAlert/userAlertView.html',
                controller: 'userAlertController',
                backdrop: 'static',
                windowClass: 'lmx-modal lmx-centered-modal lmx-userAlert-modal ' + confirmClass + '-modal',
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });

            return modalConfirmation.result;
        };
    });

})(angular);