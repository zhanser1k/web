/* eslint-disable */
(function (angular) {
    // HTML5-браузеры IE и EDGE игнорируют HTML5-атрибут form у кнопки с типом submit (Ничёси!)

    angular.module('lmxApp').directive('form', function(bowser, classPrefixerService) {
        if (!bowser.msie) {
            return {};
        }
        return {
            restrict: 'E',
            link: function ($scope, $element, attrs) {
                $element.append('<input type="submit" class=' + classPrefixerService.prefix + 'hidden id="' + attrs.id + 'Submitter">');
            }
        };
    });

    angular.module('lmxApp').directive('form', function(bowser) {
        if (!bowser.msie) {
            return {};
        }
        return {
            restrict: 'A',
            scope: {
                form: '@'
            },
            link: function ($scope, $element, attrs) {
                if (attrs.type === 'submit') {
                    $element.bind("click", function () {
                        angular.element('#' + $scope.form + 'Submitter').click();
                    });
                }
            }
        };
    });

})(angular);