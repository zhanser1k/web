(function(angular) {
    angular.module('lmxApp').service('e2eService', function(localStorageService) {
        function init() {
            var hash = window.location.hash.substr(2);

            if (localStorageService.get('e2e') && !['login', 'resetPassword', 'reset-password', 'registration', ''].contains(hash)) {
                localStorageService.set('authorizationToken', 'e2e_tests_token');
            }

            if (localStorageService.get('e2eHttpResponses')) {
                window.httpResponses = localStorageService.get('e2eHttpResponses');
            }
        }

        return {
            init: init,
        };
    });
})(angular);
