/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').constant('notificationTypes', {
        message: 0,
        error: 1,
        critical: 2
    });
})(angular);