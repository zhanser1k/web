/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').run(function ($rootScope, notification) {
        $rootScope.$on('$locationChangeSuccess', function () {
            notification.clearNotifications();
        });
    });
})(angular);