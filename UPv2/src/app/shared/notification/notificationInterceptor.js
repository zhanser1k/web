/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('notificationInterceptor', function ($q, notification, $log, $rootScope, configurationService) {
        var area;
        return {
            request: function (request) {
                area = request.area;
                if (!request.url.match(/\.html$/) && request.isBackground !== true) {
                    notification.clearNotifications(area);
                }
                return request;
            },
            responseError: function (response) {
                if (response.status === -1) {
                    response.data = { error:'Canceled' };
                } else if (angular.isDefined(response.data.result)&& angular.isDefined(response.data.result.state)){
                    if (response.data.result.state === "Error") {
                        notification.addError(response.data.result.message, area);
                    } else if (response.data.result.state === "Fail") {
                        notification.addCritical(response.data.result.message, area);
                    } else if (response.data.result.state === "ValidationError") {
                        var field;
                        var data = {};
                        angular.forEach(response.data.result.validationErrors, function (value) {
                            field = value.field.split('.')[1].toLowerCase();
                            data[field] = value.errorMessages;
                        });

                        if (!angular.equals({}, data)) {
                            $rootScope.$broadcast('validationErrors', data);
                        }
                    }
                } else {
                    if (response.data.error === 'RegistrationRequired' && !angular.isDefined(response.data.error_description) && !configurationService.options.forceRegistrationStartOnLoginAttempt) {
                        response.data.error_description = 'Неверное имя пользователя или пароль. Проверьте правильность введенных данных.';
                    }
                    if (angular.isDefined(response.data.error_description)) {
                        notification.addError(response.data.error_description, area);
                    } else {
                        notification.addCritical('Системная ошибка', area);
                        $log.warn('Unrecognized response', response);
                    }
                }

                return $q.reject(response);
            }
        };
    });
})(angular);