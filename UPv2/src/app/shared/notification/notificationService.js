/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').factory('notification', function (notificationTypes, $log) {

        var notifications;
        var areaNotifications;

        function init() {
            notifications = { value: {} };
            areaNotifications = {};
        }
        init();

        function getNotifications(area){
            if (area) {
                if (!areaNotifications[area]) {
                    checkArea(area);
                }
                return areaNotifications[area];
            } else {
                return notifications;
            }
        }

        function clearNotifications(area){
            if (area) {
                checkArea(area);
                areaNotifications[area].value = {};
            } else {
                notifications.value = {};
            }
        }

        function clearAllNotifications(){
            notifications.value = {};
            areaNotifications = {};
        }

        function addMessage(message, area){
            addNotification(notificationTypes.message, message, area);
        }

        function addError(error, area){
            addNotification(notificationTypes.error, error, area);
        }

        function addCritical(critical, area){
            addNotification(notificationTypes.critical, critical, area);
        }

        function checkArea(area) {
            if (!areaNotifications[area]) {
                initArea(area);
            }
        }

        function initArea(area) {
            areaNotifications[area] = {value: {}};
        }

        return {
            getNotifications: getNotifications,
            clearAllNotifications: clearAllNotifications,
            clearNotifications: clearNotifications,
            addMessage: addMessage,
            addError: addError,
            addCritical: addCritical
        };

        function addNotification(type, text, area){
            $log.info('Notification added', {area:area, text:text, type:type});
            if (area) {
                checkArea(area);
                areaNotifications[area].value[text] = type;
            } else if(!notifications.value[text]) {
                notifications.value[text] = type;
            }
        }
    });
})(angular);
