/* eslint-disable */
(function (angular) {
    /**
     * Сообщения об ошибках формы
     * @name Notifications
     * @description Отображает ошибки формы
     * @param {string} area - область видимости ошибки в рамках определенной директивы
     * @example
     * <notifications area="resetPassword"></notifications>
     * @version 1.1
     */
    angular.module('lmxApp').directive('notifications', function (notification, notificationTypes) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                area: '@'
            },
            templateUrl: 'app/shared/notification/notificationView.html',
            link: function ($scope) {
                $scope.notifications = notification.getNotifications($scope.area);
                $scope.clearNotifications = notification.clearNotifications;
                $scope.notificationTypes = notificationTypes;

                $scope.notificationsAreEmpty = function () {
                    return Object.keys($scope.notifications.value).length === 0;
                };
            }
        };
    });
})(angular);