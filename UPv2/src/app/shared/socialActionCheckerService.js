(function(angular) {
    angular.module('lmxApp').factory('socialActionCheckerService', function(
        $window,
        authService,
        configurationService,
        localStorageService,
        registrationService,
        routingService,
        socialService
    ) {
        function parseSocialAction() {
            var searchParams = routingService.getSearchParams();

            if (!searchParams.params) {
                return;
            }

            if (searchParams.params.code && searchParams.params.providerType) {
                var redirectUri = routingService.getLocationURL() + $window.location.pathname + '?providerType=' + searchParams.params.providerType + '&action=' + searchParams.params.action;

                switch (searchParams.params.action) {
                    case 'auth':
                        socialService
                            .login(searchParams.params.providerType, searchParams.params.code, redirectUri)
                            .then(function(loginData) {
                                authService.setToken(loginData.accessToken);
                                registrationService.tryFinishRegistration().then(function(registrationData) {
                                    if (registrationData.registrationCompleted) {
                                        authService.setAuth(registrationData.authToken).then(function() {
                                            routingService.replaceSearchParams(configurationService.options.redirectUrlOnSocialAuthSuccess || '#history');
                                        });
                                    } else {
                                        localStorageService.set('tokenForSocialRegistration', loginData.accessToken);
                                        routingService.replaceSearchParams(configurationService.options.redirectUrlOnSocialAuthFail || '#registration');
                                    }
                                });
                            });
                        break;

                    case 'binding':
                        socialService
                            .setClient(searchParams.params.providerType, searchParams.params.code, redirectUri)
                            .then(function() {
                                routingService.replaceSearchParams(configurationService.options.redirectUrlOnSocialBinding || '#social-binding');
                            });
                        break;
                }
            }
        }

        return {
            parseSocialAction: parseSocialAction,
        };
    });
})(angular);
