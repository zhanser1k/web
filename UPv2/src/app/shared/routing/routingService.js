(function(angular) {
    angular.module('lmxApp').factory('routingService', function(
        $injector,
        $location,
        $rootScope,
        $window,
        configurationService
    ) {
        var redirectPath;

        function goTo(path) {
            $location.search({});
            $location.path(path);
        }

        function redirectTo(path) {
            if (path.charAt(0) === '#') {
                goTo(path.substr(1));
            } else {
                $window.location.replace(path); // TODO: Проверить возможность замены на $location.replace()
            }
        }

        function goHome() {
            redirectTo(configurationService.options.redirectUrlOnLogin);
        }

        function goToDefault() {
            if (redirectPath) {
                $location.path(redirectPath);
                redirectPath = null;
            } else {
                goHome();
            }
        }

        function goToLogin(shouldRedirectBack) {
            if ($location.url() !== '/login') {
                if (shouldRedirectBack) {
                    redirectPath = $location.$$path;
                }
                goTo('login');
            }
        }

        function getLocationURL() {
            var location = $window.location;
            var port = location.port;
            return location.protocol + '//' + location.hostname + (port === 80 || !port ? '' : (':' + port));
        }

        function getApplicationURL() {
            return $location.url().substr(1);
        }

        function getApplicationURLParts(applicationURL) {
            return applicationURL ? applicationURL.split('/') : null;
        }

        function getSearchParams() {
            var searchParams = {
                action: undefined,
                params: undefined,
            };
            var searchString = $window.location.search.slice(1);

            if (searchString.length) {
                searchParams.params = JSON.parse('{"' + decodeURI(searchString).replace(/&/g, '","').replace(/=/g, '":"') + '"}');
            } else {
                searchParams.action = $location.path().substr(1);
                searchParams.params = Object.keys($location.search()).length ? $location.search() : undefined;
            }

            return searchParams;
        }

        function replaceSearchParams(replacement) {
            if ($window.location.search.slice(1)) {
                var searchStringIndex = $window.location.href.indexOf($window.location.search);
                var newLocation = $window.location.href.substring(0, searchStringIndex);

                if (replacement) {
                    newLocation += (/\/$/.test(newLocation) ? replacement.replace(/^\//, '') : replacement);
                }

                $window.location.href = newLocation;
            } else {
                $location.search({});
                if (replacement) {
                    redirectTo(replacement);
                }
            }
        }

        function checkOnUnavailablePageForAuthenticatedUser() {
            var unavailablePageForAuthenticatedUser = ['login', 'auth', 'registration', 'forgot', 'reset-password'];
            var authService = $injector.get('authService');

            if (authService.loggedIn() && unavailablePageForAuthenticatedUser.contains($location.url().replace(/[^a-z0-9]|\s+|\r?\n|\r/gmi, ''))) {
                redirectTo(configurationService.options.redirectUrlOnLogin);
            }
        }

        return {
            checkOnUnavailablePageForAuthenticatedUser: checkOnUnavailablePageForAuthenticatedUser,
            getLocationURL: getLocationURL,
            getApplicationURL: getApplicationURL,
            getApplicationURLParts: getApplicationURLParts,
            getSearchParams: getSearchParams,
            replaceSearchParams: replaceSearchParams,
            goHome: goHome,
            goToDefault: goToDefault,
            goToLogin: goToLogin,
            goTo: goTo,
            redirectTo: redirectTo,
        };
    });
})(angular);
