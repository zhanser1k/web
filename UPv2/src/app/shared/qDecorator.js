/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').config(function($provide) {
        $provide.decorator('$q', function($delegate) {
            function allSettled(promises) {
                var deferred = $delegate.defer(),
                    counter = 0,
                    results = angular.isArray(promises) ? [] : {};

                angular.forEach(promises, function(promise, key) {
                    counter++;
                    $delegate.when(promise).then(function(value) {
                        if (results.hasOwnProperty(key)) {
                            return;
                        }
                        results[key] = { success: true, value: value };

                        if (!(--counter)) {
                            deferred.resolve(results);
                        }
                    }, function(error) {
                        if (results.hasOwnProperty(key)) {
                            return;
                        }
                        results[key] = { success: false, error: error };

                        if (!(--counter)) {
                            deferred.resolve(results);
                        }
                    });
                });

                if (counter === 0) {
                    deferred.resolve(results);
                }

                return deferred.promise;
            }
            $delegate.allSettled = allSettled;
            return $delegate;
        });
    });
})(angular);