/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').filter('translatePlural', function($filter, $sce, $translate, translatePluralService, $log){
        return function(number, options) {

            var plural = options.plural || options;
            var numberClass = options.class;

            if (isNaN(number)) {
                $log.warn('Using translatePlural filter with not a number');
                return null;
            }
            if (!angular.isDefined(plural)) {
                $log.warn('Using translatePlural filter with an empty plural param');
                return null;
            }

            var params = {number: number};

            if (angular.isDefined(numberClass)) {
                params.number = '<span class="' + numberClass + '">' + number + '</span>';
            }
            return $sce.trustAsHtml($translate(translatePluralService.getTranslationPath(number, plural), params.number));
        };
    });

})(angular);