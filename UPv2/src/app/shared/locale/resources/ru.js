(function(angular) {
    var translations = {
        logout: 'Выход',
        password: 'Пароль',
        phone: 'Номер телефона',
        save: 'Сохранить',
        change: 'Изменить',
        cancel: 'Отмена',
        approve: 'Подтвердить',
        yes: 'Да',
        no: 'Нет',
        areYouSure: 'Вы уверены?',
        accept: 'Принять',
        next: 'Далее',
        complete: 'Завершить',
        continue: 'Продолжить',
        home: {
            name: 'Главная',
        },
        page: {
            about: {
                name: 'О нас',
            },
            cards: {
                name: 'Особенности карты',
            },
        },
        personal: {
            name: 'Личный кабинет',
            changePassword: {
                password: 'Пароль',
                currentPassword: 'Текущий пароль',
                newPassword: 'Новый пароль',
                repeatPassword: 'Подтверждение пароля',
                passwordsAreNotEqual: 'Пароли не совпадают',
            },
            confirmationCode: 'Код подтверждения',
            sendConfirmCode: 'Выслать код подтверждения ещё раз',
            changePhone: {
                newPhone: 'Номер мобильного телефона',
                reSetPhone: 'Повторить ввод номера телефона',
            },
            changeEmail: {
                newEmail: 'Адрес электронной почты',
                reSetEmail: 'Повторить ввод электронной почты',
            },
        },
        cards: {
            name: 'Мои карты',
            attachCard: 'Прикрепить карту',
            setCard: 'Привязать физическую карту',
            actions: {
                set: 'Привязать',
                attach: 'Прикрепить',
                block: 'Заблокировать',
                unblock: 'Разблокировать',
                replace: 'Заменить',
            },
            noCard: 'Нет карты',
            info: {
                owner: 'Владелец',
                paid: 'Израсходовано',
            },
        },
        currency: {
            BNS: 'б.',
            RUB: 'р.',
        },
        history: {
            name: 'История операций',
        },
        registration: {
            referrerCodeMessage: 'Вы&nbsp;уже установили код друга.<br>Повторный ввод для продолжения регистрации не&nbsp;требуется',
        },
        validationMessages: {
            defaultMsg: 'Неправильный ввод {0}',
            email: 'Пожалуйста, введите правильный e-mail',
            minLength: 'Минимум символов: {0} (сейчас {1})',
            maxLength: 'Максимум {0} символов (сейчас {1})',
            min: 'Минимум {0}',
            max: 'Максимум {0}',
            required: 'Это поле обязательно',
            date: 'Пожалуйста, введите корректную дату',
            pattern: 'Формат {0}',
            number: 'Пожалуйста, введите число',
            url: 'Пожалуйста, введите url в формате http(s)://www.google.com',
            greaterThan: 'Больше, чем {0}',
            greaterThanEqual: 'Больше или равно {0}',
            lessThan: 'Меньше, чем {0}',
            lessThanEqual: 'Меньше или равно {0}',
            requiredItems: 'Необходимо выбрать хотя бы один элемент',
            digitsAfterComma: 'Неверное количество знаков после запятой. Должно быть {0}',
            integer: 'Должно быть целое число',
            notEmptyGuid: 'Невозможно использовать пустое значение внешнего ключа',
            isGuid: 'Значение не соответствует типу Guid',
            isPhone: 'Номер должен начинаться с 7 и состоять из 11 цифр',
            isEmail: 'Пожалуйста, введите правильный email',
            isEqualPasswords: 'Пароли не совпадают',
        },
        enum: {
            operation: {
                purchase: 'Покупка',
                withdrawData: 'Ручная корректировка',
                rewardData: 'Ручная корректировка',
            },
            gift: {
                purchase: 'Покупка',
                withdraw: 'Оплата',
                withdrawData: 'Списание',
                rewardData: 'Начисление',
                bonus: 'Бонусы',
                discount: 'Скидка',
                gift: 'Подарок',
                charging: 'Начисление',
            },
            giftRefund: {
                purchase: 'Покупка',
                withdrawData: 'Возврат оплаты',
                withdraw: 'Возврат оплаты',
                rewardData: 'Возврат бонусов',
                bonus: 'Возврат бонусов',
                discount: 'Скидка',
                gift: 'Подарок',
                charging: 'Начисление',
            },
        },
        merchantPlurals: {
            merchant_plural: {
                one: '{{ number }} магазин',
                few: '{{ number }} магазина',
                many: '{{ number }} магазинов',
            },
        },
        personalOffer: {
            discountTypes: {
                Cashback: 'Бонус',
                Discount: 'Скидка',
            },
            discountViewModel: {
                amount: '{TYPE} {VALUE} {CUR} от суммы товара',
                amountPerMeasure: '{TYPE} {VALUE} {CUR} за каждые {STEP} р.',
                amountPerUnit: '{TYPE} {VALUE} {CUR} за единицу товара',
                percent: '{TYPE} {VALUE}% от суммы товара',
                pricePerUnit: 'Стоимость товара {VALUE} р.',
            },
            discountTypesCurrency: {
                Cashback: 'б.',
                Discount: 'р.',
            },
            pricePerUnitDiscountHint: {
                Cashback: 'Разница между исходной ценой и указанной будет предоставлена в виде бонусов',
                Discount: 'Разница между исходной ценой и указанной будет предоставлена в виде скидки',
            },
        },
        brandPlurals: {
            brand_plural: {
                one: '{{ number }} бренд',
                few: '{{ number }} бренда',
                many: '{{ number }} брендов',
            },
        },
        bonusPlurals: {
            bonus_plural: {
                one: '{{ number }} бонус',
                few: '{{ number }} бонуса',
                many: '{{ number }} бонусов',
            },
            bonus_clear_plural: {
                one: 'бонус',
                few: 'бонуса',
                many: 'бонусов',
            },
        },
        pointPlurals: {
            point_plural: {
                one: '{{ number }} балл',
                few: '{{ number }} балла',
                many: '{{ number }} баллов',
            },
            point_clear_plural: {
                one: 'балл',
                few: 'балла',
                many: 'баллов',
            },
        },
        options: {
            acceptTenderOfferByCheck: 'Автоподтверждение оферты галкой',
            authCookie: 'Установка cookie при авторизации',
            authorizeOnRegistrationComplete: 'Авторизовываться по завершении регистрации',
            forceRegistrationStartOnLoginAttempt: 'Инициировать процесс регистрации незарегистрированного пользователя при попытке входа',
            ignoreDocsFetchingErrors: 'Игнорировать ошибку загрузки документов',
            map: 'Настройки Яндекс-карты',
            offerFileId: 'id файла с офертой',
            redirectUrlOnEmailConfirmForRegistration: 'Страница регистрации (для редиректа после подтверждения email по ссылке во время регистрации)',
            redirectUrlOnEmailConfirmForSettings: 'Страница настроек (для редиректа после подтверждения email по ссылке, в случае смены email)',
            redirectUrlOnEmailConfirmWithoutToken: 'Страница авторизации (для редиректа после подтверждения email по ссылке, в случае если токен устарел)',
            redirectUrlOnRegistrationComplete: 'Редирект по завершении регистрации',
            redirectUrlOnSocialBinding: 'Редирект по завершении привязки соцсети',
            referralRegistration: 'Использование реферальной системы при регистрации',
            requestUserAttributes: 'Запрашивать атрибуты пользователя',
            shareAuthLoginToRegistration: 'Односторонняя связь «авторизация → регистрация»',
            supportEmail: 'Email адрес службы поддержки',
        },
        dateRange: {
            from: 'с',
            to: 'по',
            toTime: 'до',
            since: 'с',
        },
    };

    angular.module('lmxApp').constant('translationTable', translations);
})(angular);
