/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service('translatePluralService', function () {
        var getTranslationKey = function (number) {

            if (number < 0) {
                number = -number;
            }
            var translationKey;

            if (parseInt(number) !== number) {
                translationKey = 'few';
            } else {
                var stringNumber = String(number),
                    lastDigit = Number(stringNumber.substr(-1)),
                    penultimateDigit = (stringNumber.length > 1) ? Number(stringNumber.substr(-2, 1)) : null;

                if (lastDigit === 1 && penultimateDigit !== 1) {
                    translationKey = 'one';
                } else if ((number <= 4 || number >= 22) && (lastDigit >= 2 && lastDigit <= 4 && penultimateDigit !== 1)) {
                    translationKey = 'few';
                } else {
                    translationKey = 'many';
                }
            }

            return translationKey;
        };
        return {
            getTranslationPath: function(number, plural) {
                return plural + '_plural.'  + getTranslationKey(number);
            }
        };
    });

})(angular);