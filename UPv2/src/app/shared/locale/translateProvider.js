/* eslint-disable */
(function (angular) {

    // По умолчанию используется одна локаль (файл ru.js).
    // Имена сервиса '$translate' и фильтра 'translate' - используются для
    // безболезненного перехода angular-translate при необходимости.

    angular.module('lmxApp').factory('$translate', function ($log, translationTable) {

        function getTranslation(key, param) {
            var path = key.split('.');
            var translation = angular.copy(translationTable);
            var error = false;

            for (var i = 0; i < path.length; i++) {
                if (!angular.isDefined(translation)) {
                    throwException();
                    break;
                }
                translation = translation[path[i]];
            }

            if (angular.isObject(translation)) {
                throwException();
            }

            function throwException() {
                $log.error('Can\'t find translation by key \'' + key + '\'');
                translation = key;
                error = true;
            }
            if (angular.isDefined(param)) {
                return translation.replace(/{{(.+?)}}/, param);
            }
            return translation;
        }

        return getTranslation;
    });

    angular.module('lmxApp').filter('translate', function ($translate) {
        return function (value) {
            return $translate(value);
        };
    });

})(angular);