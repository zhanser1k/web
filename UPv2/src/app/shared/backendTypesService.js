/* eslint-disable */
(function (angular) {
	angular.module('lmxApp').service("backendTypesService",function (){
	this.enums = {
  "rewardThumbnail": {
    "imageWithText": "Loymax.Mobile.Contract.Models.Offer.ImageWithText, Loymax.Mobile.Contract",
    "oldNewPrice": "Loymax.Mobile.Contract.Models.Offer.OldNewPrice, Loymax.Mobile.Contract",
    "twoLineText": "Loymax.Mobile.Contract.Models.Offer.TwoLineText, Loymax.Mobile.Contract"
  },
  "supportMessages": {
    "attachments": "Loymax.Support.Mobile.Contract.Model.NewAttachmentsSupportMessageModel, Loymax.Support.Mobile.Contract",
    "comment": "Loymax.Support.Mobile.Contract.Model.NewCommentSupportMessageModel, Loymax.Support.Mobile.Contract"
  },
  "support": {
    "attachment": "Loymax.Support.Mobile.Contract.Model.NewSupportMessageAttachment, Loymax.Support.Mobile.Contract"
  },
  "personalOfferDiscount": {
    "amount": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferAmountDiscountViewModel, Loymax.Mobile.Contract",
    "amountPerMeasure": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferAmountPerMeasureDiscountViewModel, Loymax.Mobile.Contract",
    "amountPerUnit": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferAmountPerUnitDiscountViewModel, Loymax.Mobile.Contract",
    "percent": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPercentDiscountViewModel, Loymax.Mobile.Contract",
    "price": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPriceDiscountViewModel, Loymax.Mobile.Contract",
    "pricePerUnit": "Loymax.Mobile.Contract.Models.Offer.PersonalOffer.Discount.PersonalOfferPricePerUnitDiscountViewModel, Loymax.Mobile.Contract"
  }
};});
})(angular);