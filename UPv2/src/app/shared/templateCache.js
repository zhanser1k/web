(function(angular) {
    'use strict';

    // Фиктивный модуль для формирования обобщенного js-файла,
    // содержащего все шаблоны, посредством gulp-angular-templatecache
    angular.module('templateCache', []);
})(angular);
