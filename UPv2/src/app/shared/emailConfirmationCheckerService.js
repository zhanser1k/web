(function(angular) {
    angular.module('lmxApp').factory('emailConfirmationCheckerService', function(
        $http,
        $rootScope,
        authService,
        configurationService,
        localStorageService,
        routingService,
        resetPasswordService
    ) {
        var apiHost = configurationService.host + 'api/User/Email/LinkConfirm';
        var confirmationKeys = {
            email: 'confirmEmail',
            resetPassword: 'confirmResetPassword',
        };

        /**
         * Подтверждение нового адреса электронной почты
         * @param {number} personID - идентификатор пользователя
         * @param {string} confirmCode - код подтверждения
         */
        function linkConfirm(personID, confirmCode) {
            var params = {
                personID: personID,
                confirmCode: confirmCode,
            };

            return $http.post(apiHost, params);
        }

        function parseEmailConfirmation() {
            var searchParams = routingService.getSearchParams();

            if (!searchParams.params) {
                return;
            }

            if (searchParams.action === confirmationKeys.email && searchParams.params.code && searchParams.params.id) {
                // Принудительное включение глобального лоадера необходимо в случае прямого копирования ссылки в адресную строку
                // (поскольку он вызывется только при первоначальной загрузке приложения, а в данном случае она уже произошла)
                $rootScope.$broadcast('showGlobalLoader');
                linkConfirm(parseInt(searchParams.params.id, 10), searchParams.params.code)
                    .then(function() {
                        var path;

                        if (authService.authentication.isRegistrationToken) {
                            path = configurationService.options.redirectUrlOnEmailConfirmForRegistration || '#registration';
                        } else if (authService.authentication.token) {
                            localStorageService.set('isConfirmEmailSuccess', true);
                            path = configurationService.options.redirectUrlOnEmailConfirmForSettings || '#settings';
                        } else {
                            path = configurationService.options.redirectUrlOnEmailConfirmWithoutToken || '#login';
                        }

                        routingService.replaceSearchParams(path);
                    });
            } else if (searchParams.action === confirmationKeys.resetPassword && searchParams.params.email && searchParams.params.code) {
                resetPasswordService.setResetPasswordConfirmationData(searchParams.params);
                var redirectUrl = configurationService.options.redirectUrlOnResetPasswordEmailConfirm || '#reset-password';

                routingService.replaceSearchParams(redirectUrl);
            }
        }

        return {
            parseEmailConfirmation: parseEmailConfirmation,
            confirmationKeys: confirmationKeys,
        };
    });
})(angular);
