(function(angular) {
    angular.module('lmxApp').factory('authService', function(
        $cookies,
        $http,
        $injector,
        $q,
        $rootScope,
        configurationService,
        localStorageService,
        notification,
        routingService,
        userService,
        shareAuthLoginToRegistration
    ) {
        var apiHost = configurationService.host;

        var area;
        function setArea(serviceArea) {
            area = serviceArea;
        }

        var authentication = {
            token: null,
            isRegistrationToken: localStorageService.get('isRegistrationToken') || null,
            permissions: null,
        };

        function setToken(token) {
            authentication.token = token;
        }

        function removeCookie() {
            $cookies.remove('lmxIsAuth');
        }

        function removeRegistrationToken() {
            localStorageService.remove(
                'isRegistrationToken',
                'forceEmailStepHasCode',
                'forceEmailStepSkipped'
            );
            authentication.isRegistrationToken = null;
        }

        function clearAuthData() {
            localStorageService.remove('authorizationToken');

            if (configurationService.options.authCookie) {
                removeCookie();
            }

            removeRegistrationToken();

            authentication.token = null;
            authentication.permissions = null;
        }

        function populateUserInfo() {
            return userService.getUserInfo().then(function(response) {
                $rootScope.userInfo = response;

                if (configurationService.options.requestUserAttributes) {
                    return userService.getAttributes().then(function(attributes) {
                        $rootScope.userInfo.attributes = attributes;
                        $rootScope.$broadcast('loadUserInfo');
                    });
                }

                $rootScope.$broadcast('loadUserInfo');
            });
        }

        function setCookie() {
            $cookies.put('lmxIsAuth', 1);
        }

        function setAuth(token, passive) {
            localStorageService.set('authorizationToken', token);

            setToken(token);

            if (!passive && configurationService.options.authCookie) {
                setCookie();
            }

            return passive ? null : populateUserInfo();
        }

        function setRegistrationToken(token) {
            setAuth(token, true);
            removeCookie();
            authentication.isRegistrationToken = true;
            localStorageService.set('isRegistrationToken', true);
        }

        var token = localStorageService.get('authorizationToken');
        if (token) {
            setToken(token);
        }

        function loggedIn() {
            return authentication.token !== null && !authentication.isRegistrationToken;
        }

        function login(loginData) {
            clearAuthData();

            var registrationService = $injector.get('registrationService');
            registrationService.setArea('login');

            var data = 'grant_type=password&username=' + registrationService.correctPhone(loginData.login) + '&password=' + loginData.password;

            function beginRegistration() {
                return registrationService.beginRegistration(loginData).then(function(response) {
                    setRegistrationToken(response.authToken);
                    routingService.goTo('registration');
                });
            }
            return $http.post(apiHost + 'token', data, {
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                area: area,
            }).then(function(tokenResponse) {
                return beginRegistration().catch(function(error) {
                    if (error.data.data.state === 'RegistrationAlreadyCompleted') {
                        notification.clearNotifications(area);
                        return setAuth(tokenResponse.data.access_token).then(function() {
                            $rootScope.$broadcast('login');
                        });
                    }

                    return $q.reject(error);
                });
            }).catch(function(error) {
                if (error.data.error === 'RegistrationRequired' && configurationService.options.forceRegistrationStartOnLoginAttempt) {
                    delete loginData.password;

                    if (configurationService.options.acceptTenderOfferByCheck || configurationService.options.referralRegistration) {
                        shareAuthLoginToRegistration.loginAuth = loginData.login;
                        routingService.goTo('registration');
                    } else {
                        shareAuthLoginToRegistration.clearLogin();
                        return beginRegistration();
                    }
                } else {
                    return $q.reject(error);
                }
            });
        }

        function logout() {
            clearAuthData();

            // TODO: Написать директиву, позволяющую реагировать на события прямо из шаблона
            // $rootScope.$broadcast('logout');

            routingService.redirectTo(configurationService.options.redirectUrlOnLogout);
        }

        function isRegistrationStarted() {
            return authentication.token && authentication.isRegistrationToken;
        }

        function loadAuth() {
            var promise;
            if (loggedIn()) {
                promise = populateUserInfo();
            } else {
                var defer = $q.defer();
                defer.reject();
                promise = defer.promise;
            }
            return promise;
        }

        return {
            login: login,
            logout: logout,
            authentication: authentication,
            loadAuth: loadAuth,
            setAuth: setAuth,
            setToken: setToken,
            loggedIn: loggedIn,
            isRegistrationStarted: isRegistrationStarted,
            removeRegistrationToken: removeRegistrationToken,
            setArea: setArea,
            setRegistrationToken: setRegistrationToken,
            clearAuthData: clearAuthData,
        };
    });
})(angular);
