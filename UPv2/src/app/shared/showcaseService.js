/* eslint-disable */
(function (angular) {

    angular.module('lmxApp').service("showcaseService", function ($http, configurationService) {
        var showcaseUrl = configurationService.showcase + 'api/coupons/';

        function getCategories() {
            return $http.get(showcaseUrl + 'categories').then(function (response) {
                return response.data;
            });
        }
        function getCouponsByCategory(category) {
            return $http.get(showcaseUrl + category + '/types').then(function (response) {
                return response.data;
            });
        }
        function getCouponById(couponId) {
            return $http.get(showcaseUrl + 'types/' + couponId).then(function (response) {
                return response.data;
            });
        }
        function getUserCoupons(state) {
            return $http.get(showcaseUrl + 'user', {params: {count: Number.maxIntValue, onlyActive: state}}).then(function (response) {
                return response.data;
            });
        }
        function buyCoupon(couponId) {
            return $http.post(showcaseUrl + 'buy/' + couponId).then(function (response) {
                return response.data;
            });
        }
        function refundUserCoupon(couponId) {
            return $http.post(showcaseUrl + 'Refund/' + couponId).then(function (response) {
                return response.data;
            });
        }

        return {
            getCategories: getCategories,
            getCouponsByCategory: getCouponsByCategory,
            getCouponById: getCouponById,
            getUserCoupons: getUserCoupons,
            buyCoupon: buyCoupon,
            refundUserCoupon:refundUserCoupon
        };
    });
})(angular);