/* eslint-disable */
(function (angular) {

    angular.unNormalizeDirective = function (name) {
        var regex = /[A-Z]/g;
        return name.replace(regex, function(letter, pos) {
            return (pos ? '-' : '') + letter.toLowerCase();
        });
    };

    angular.flipObject = function(object) {
        var flipped = {};
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                flipped[object[prop]] = prop;
            }
        }
        return flipped;
    };

    angular.isDate = function(object) {
        return Object.prototype.toString.call(object) === '[object Date]';
    };
    
    angular.copyObjectFields = function(object) {
        var newObject = {};
        var key;
        for (var i = 1; i < arguments.length; i++) {
            key = arguments[i];
            newObject[key] = object[key];
        }
        return newObject;
    };

    angular.isNullOrUndefined = function(object) {
        return angular.isUndefined(object) || object === null;
    };
})(angular);