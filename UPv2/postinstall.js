const shell = require('shelljs');
const fs = require('fs-extra');

const commands = [
    "npm install -g lodash-cli modernizr",
    "lodash include=invert,isEqual,union -p", // https://lodash.com/custom-builds
    "modernizr -c config/modernizr-config.json"
];

if (shell.exec(commands.join(" && ")).code === 0) {
    fs.move('./lodash.custom.min.js', './src/assets/lib/lodash/lodash.custom.min.js', {overwrite: true}, err => {
        if (err) return console.error(err);
    });

    fs.move('./modernizr.js', './src/assets/lib/modernizr/modernizr.min.js', {overwrite: true}, err => {
        if (err) return console.error(err);
    });
}
