exports.config = {
    allScriptsTimeout: 11000,

    specs: [
        '**/*.js'
    ],

    capabilities: {
        'browserName': process.env.TEAMCITY_VERSION ? 'firefox' : 'chrome'
    },

    framework: 'jasmine2',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    },

    onPrepare: function () {

        browser.driver.manage().window().maximize();

        if (process.env.TEAMCITY_VERSION) {
            var reporter = require('jasmine-reporters');
            jasmine.getEnv().addReporter(new reporter.TeamCityReporter());
        }
    }
};