'use strict';

// include page objects
var pageObject = require('./home.po.js');

describe('Home page.', function() {
    
    var po = new pageObject();
    
    it('Should display /home segment by default', function() {
        browser.get('/');
        
        expect(po.header.isPresent()).toBeTruthy();
        expect(po.content.isPresent()).toBeTruthy();
        expect(po.footer.isPresent()).toBeTruthy();
    });

});
