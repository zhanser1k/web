'use strict';

var homepageObject = function() {

    this.header = element(by.css('main-top'));
    this.content = element(by.css('.main-content'));
    this.footer = element(by.css('main-footer'));
};

module.exports = homepageObject;