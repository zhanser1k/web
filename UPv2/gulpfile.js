const gulp = require('gulp');
const vendorJS = require('./config/vendor.json');

const plugins = {
    addsrc: require('gulp-add-src'),
    angularFilesort: require('gulp-angular-filesort'),
    argv: require('yargs').argv,
    autoprefixer: require('gulp-autoprefixer'),
    concat: require('gulp-concat'),
    connect: require('gulp-connect'),
    cssNano: require('gulp-cssnano'),
    cssPrefixer: require('css-prefixer'),
    del: require('del'),
    es: require("event-stream"),
    eslint: require('gulp-eslint'),
    extend: require('extend'),
    fileExists: require('file-exists'),
    fs: require('fs'),
    gulpif: require('gulp-if'),
    gulpJsdoc2md: require('gulp-jsdoc-to-markdown'),
    gulpStylelint: require('gulp-stylelint'),
    inject: require('gulp-inject'),
    insert: require('gulp-insert'),
    jsdoc: require('gulp-jsdoc3'),
    less: require('gulp-less'),
    markdown: require('gulp-markdown'),
    ngAnnotate: require('gulp-ng-annotate'),
    order: require('gulp-order'),
    portFinder: require('portfinder'),
    posthtml: require('posthtml'),
    posthtmlPrefixClass: require('posthtml-prefix-class'),
    posthtmlPrefixNgClass: require('posthtml-prefix-ngclass'),
    posthtmlRenameId: require('posthtml-rename-id'),
    protractor: require("gulp-protractor").protractor,
    rename: require("gulp-rename"),
    replace: require('gulp-replace'),
    runSequence: require('run-sequence'),
    sourcemaps: require('gulp-sourcemaps'),
    stylelintTeamcityFormatter: require('stylelint-teamcity-formatter'),
    tap: require('gulp-tap'),
    uglify: require('gulp-uglify'),
    webDriverUpdate: require("gulp-protractor").webdriver_update,
    webStormReporter: require('./node/jsHintWebStormReporter'),
    templateCache: require('gulp-angular-templatecache'),
    zip: require('gulp-zip'),
};

/* ===== config ===== */

const buildConfig = {
    tempPath: 'build/merge',
    targetPath: 'build'
};

const prefixConfig = {
    prefix: 'lmx-',
    prefixIdSelectors: false,
    ignore: require('./config/prefix-ignore.json'),
};

/* ===== internal ===== */

function getAppConfig(){
    const defaultConfig = require('./config/config.default.json');
    defaultConfig.version = plugins.fs.readFileSync("version.txt", "utf8").replace(/[\n\r]+/g, '');

    if (plugins.fileExists('./config/config.local.json')){
        var localConfig = require('./config/config.local.json');
    }

    const argConfig = {
        host: plugins.argv.host,
        showcase: plugins.argv.showcase,
        buildNumber: plugins.argv.buildNumber,
        templatesPath: plugins.argv.templatesPath
    };

    return plugins.extend(defaultConfig, localConfig, argConfig);
}

var copyright = '';
function getCopyright() {
    if (copyright) {
        return copyright;
    }

    var src = plugins.fs.readFileSync('copyright.txt', 'utf8');
    const year = (new Date()).getFullYear();
    const appConfig = getAppConfig();

    const replaceOptions = {
        '<!-- version -->': appConfig.version + '.' + appConfig.buildNumber,
        '<!-- nowYear -->': year,
    };

    for (const pattern in replaceOptions) {
        if (replaceOptions.hasOwnProperty(pattern)) {
            src = src.replace(pattern, replaceOptions[pattern]);
        }
    }

    copyright = src;

    return copyright;
}

var appConfig = getAppConfig();
var reporter = process.env.TEAMCITY_VERSION ? 'jshint-teamcity-compile' : plugins.webStormReporter;
gulp.task('checkJS', function (){
    const isFail = !(plugins.argv.hasOwnProperty('fail'));

    return gulp
        .src(['src/app/**/*.js', '!**/*tests.js'])
        .pipe(plugins.eslint())
        .pipe(plugins.eslint.format('node_modules/eslint-table-formatter-silencer'))
        .pipe(plugins.gulpif(isFail, plugins.eslint.failAfterError()));
});
gulp.task('checkLess', function (){
    const isFail = !(plugins.argv.hasOwnProperty('fail'));

    return gulp
        .src(['src/{,**/}assets/*.less'])
        .pipe(plugins.gulpStylelint({
            failAfterError: isFail,
            reporters: [{
                console: true,
                formatter: 'string'
            }]
        }));
});

gulp.task('clean', ['checkJS', 'checkLess'], function (callback) {
    // callback нужен, чтобы таск выполнился синхронно
    return plugins.del([
        buildConfig.targetPath
    ], callback);
});
gulp.task('copyDefaultFiles', ['clean'], function(){
    return gulp.src('src/**')
        .pipe(gulp.dest(buildConfig.tempPath));
});

gulp.task('assets', ['copyDefaultFiles'], function (){
    return gulp
        .src([
            buildConfig.tempPath+'/assets/**',
            '!src/assets/less{,/**}',
            '!src/assets/images/favicon.ico',
            '!' + buildConfig.tempPath + '/**/materialize.min.css'
        ])
        .pipe(gulp.dest(buildConfig.targetPath + '/assets'));
});

gulp.task('favicon', ['copyDefaultFiles'], function (){
    return gulp
        .src([buildConfig.tempPath+'/assets/images/favicon.ico'])
        .pipe(gulp.dest(buildConfig.targetPath));
});

gulp.task('buildCss', ['copyDefaultFiles'], function () {

    const lessOrder = [
        'fonts',
        'main',
        'form',
        'form.ext'
    ];

    const includeOrder = [];

    for (let i=0; i<lessOrder.length; i++) {
        includeOrder.push(lessOrder[i] + '.less');
}

    return plugins.es.merge([
        gulp
            .src([buildConfig.tempPath + '/assets/*.less'])
            .pipe(plugins.order(includeOrder))
            .pipe(plugins.less())
            .pipe(plugins.tap(function(file) {
                var ignore = ['dateTimeDirective.css', 'uib-autocomplete.css'];
                if (ignore.indexOf(file.relative) === -1) {
                    file.contents = Buffer.from(plugins.cssPrefixer(file.contents.toString(), prefixConfig));
                }
            }))
            .pipe(plugins.autoprefixer({
                browsers: ['last 2 versions', '> 1% in RU', 'ie 9-11', 'Firefox ESR']
            }))
            .pipe(plugins.gulpif(!process.env.TEAMCITY_VERSION, plugins.sourcemaps.init()))
            .pipe(plugins.cssNano({zindex: false}))
            .pipe(plugins.concat('css/style.min.css'))
            .pipe(plugins.gulpif(!process.env.TEAMCITY_VERSION, plugins.sourcemaps.write('.')))
            .pipe(gulp.dest(buildConfig.targetPath + '/assets/')),
        gulp
            .src([buildConfig.tempPath + '/assets/lib/materialize/css/*.css'])
            .pipe(plugins.tap(function(file) {
                file.contents = Buffer.from(plugins.cssPrefixer(file.contents.toString(), prefixConfig));
            }))
            .pipe(plugins.cssNano({zindex: false}))
            .pipe(gulp.dest(buildConfig.targetPath + '/assets/lib/materialize/css'))
    ]);
});

gulp.task('appViews', ['copyDefaultFiles'], function(){
    return gulp
        .src(buildConfig.tempPath+'/app/**/*.html')
        .pipe(plugins.tap(function(file) {
            plugins.posthtml()
                .use(plugins.posthtmlPrefixClass(prefixConfig))
                .use(plugins.posthtmlPrefixNgClass(prefixConfig))
                .use(plugins.posthtmlRenameId(`${prefixConfig.prefix}[id]`))
                .process(file.contents.toString()).then(function (output) {
                    file.contents = Buffer.from(output.html);
                });
        }))
        .pipe(gulp.dest(buildConfig.targetPath + '/app'));
});

gulp.task('appJS', ['copyDefaultFiles'], function (){
    const isDev = process.argv.includes('dev');

    return gulp.src([
        buildConfig.tempPath + '/app/**/*.js',
        '!**/*tests.js',
    ])
    .pipe(plugins.angularFilesort())
    .pipe(plugins.gulpif(isDev, plugins.sourcemaps.init({loadMaps: true})))
    .pipe(plugins.concat('app.js'))
    .pipe(plugins.ngAnnotate())
    .pipe(plugins.uglify())
    .pipe(plugins.gulpif(isDev, plugins.sourcemaps.write('.')))
    .pipe(gulp.dest(`${buildConfig.targetPath}/app`));
});

gulp.task('app', ['copyDefaultFiles', 'appJS', 'appViews']);

gulp.task('copyrightJS', ['appJS'], function (){
    const jsPath = buildConfig.targetPath + '/app/';
    return gulp
        .src(jsPath + 'app.js')
        .pipe(plugins.insert.prepend(getCopyright()))
        .pipe(gulp.dest(jsPath));
});

gulp.task('copyrightCss', ['buildCss'], function (){
    const cssPath = buildConfig.targetPath + '/assets/css/';
    return gulp
        .src(cssPath + 'style.min.css')
        .pipe(plugins.insert.prepend(getCopyright()))
        .pipe(gulp.dest(cssPath));
});

gulp.task('copyright', ['copyrightJS', 'copyrightCss']);

gulp.task('vendor', ['copyDefaultFiles'], function (){
    return gulp
        .src(vendorJS)
        .pipe(plugins.addsrc.append('src/assets/lib/angular-sb-date-select/sb-date-select.min.js'))
        .pipe(plugins.addsrc.append('src/assets/lib/lmx-custom-select/lmx-custom-select.js'))
        .pipe(plugins.addsrc.append('src/assets/lib/lodash/lodash.custom.min.js'))
        .pipe(plugins.addsrc.append('src/assets/lib/modernizr/modernizr.min.js'))
        .pipe(plugins.concat('dependencies.js'))
        .pipe(gulp.dest(buildConfig.targetPath + '/assets/lib/vendor'));
});

gulp.task('config', ['copyDefaultFiles'], function (){
    return gulp
        .src(['config/config.js'])
        .pipe(plugins.replace('{{host}}', appConfig.host))
        .pipe(plugins.replace('{{showcase}}', appConfig.showcase))
        .pipe(plugins.replace('{{templatesPath}}', appConfig.templatesPath))
        .pipe(gulp.dest(buildConfig.targetPath + '/assets/js'));
});

gulp.task('index', ['copyDefaultFiles'], function () {
    return gulp
        .src(buildConfig.tempPath+'/*')
        .pipe(plugins.tap(function(file) {
            if (file.path.endsWith('.html')) {
                plugins.posthtml().use(plugins.posthtmlPrefixClass(prefixConfig)).process(file.contents.toString()).then(function (output) {
                    file.contents = Buffer.from(output.html);
                });
            }
        }))
        .pipe(gulp.dest(buildConfig.targetPath));
});

gulp.task('buildNumber', ['index'], function() {
    gulp.src(buildConfig.targetPath + '/index.html')
        .pipe(plugins.replace('<!-- buildNumber -->', appConfig.buildNumber))
        .pipe(gulp.dest(buildConfig.targetPath));
});

gulp.task('templatecache', ['appViews'], function () {
    // При переопределении templateUrl унаследованной директивы - пробивается запрос сквозь templateCache
    return gulp
        .src('build/app/**/*.html')
        .pipe(plugins.templateCache('templates.js', {module: 'templateCache', standalone: false, root: '/wp-content/themes/default/assets/views/'}))
        .pipe(gulp.dest(buildConfig.targetPath + '/app'));
});

gulp.task('templatezip', ['copyDefaultFiles', 'appViews'], function () {
    return gulp.src(buildConfig.targetPath + '/app/**/*.html')
        .pipe(plugins.zip('templates.zip'))
        .pipe(gulp.dest(`${buildConfig.targetPath}/downloads`));
});

gulp.task('clearMerge', ['buildCss', 'assets', 'app', 'vendor', 'config', 'index', 'favicon'], function() {
    return plugins.del([buildConfig.tempPath]);
});

gulp.task('clearBuildLess', ['buildCss', 'assets', 'app', 'vendor', 'config', 'index', 'favicon'], function() {
    return plugins.del([buildConfig.targetPath + '/assets/*.less']);
});

/* ===== build ===== */

gulp.task('build', ['buildCss', 'assets', 'app', 'templatecache', 'templatezip', 'vendor', 'config', 'index', 'favicon', 'clearMerge', 'clearBuildLess', 'copyright', 'buildNumber'], function () {
    return gulp.start('docs');
});

gulp.task('dev', ['buildCss', 'assets', 'app', 'vendor', 'config', 'index']);

gulp.task('default', ['build']);

/* ===== e2e tests ===== */

gulp.task('http-server-start', function () {
    var argvPort = (process.argv.indexOf("--port") > -1) ? process.argv[process.argv.indexOf("--port") + 1] : false;
    var hostPort = (process.argv.indexOf("--host") > -1) ? process.argv[process.argv.indexOf("--host") + 1] : false;

    return plugins.portFinder.getPort(function (err, port) {

        port = plugins.argv.port || argvPort || port;

        global.freePort = port;

        return plugins.connect.server({
            root: buildConfig.targetPath,
            port: port,
            host: hostPort || 'localhost'
        });
    });
});

gulp.task('http-server-stop', function () {
    return plugins.connect.serverClose();
});

gulp.task('web-driver-update', plugins.webDriverUpdate);

gulp.task('protractor', ['web-driver-update'], function() {
    return gulp
        .src(["e2e-tests/**/*.js"])
        .pipe(plugins.protractor({
            configFile: "e2e-tests/protractor.conf.js",
            args: ['--baseUrl', 'http://127.0.0.1:' + global.freePort]
        }))
        .on('error', function(e) { throw e; });
});

gulp.task('e2e', function (callback) {
    return plugins.runSequence(
        'build',
        'http-server-start',
        'protractor',
        'http-server-stop',
        callback);
});

/* ===== docs ===== */

gulp.task('docs', function (callback) {
    return plugins.runSequence(
        'docs:clean',
        'docs:generate',
        'docs:move',
        'docs:concat',
        'docs:base',
        'docs:html',
        'docs:inject',
        'docs:cleanMerge',
        callback);
});

gulp.task('docs:clean', function (callback) {
    return plugins.del(['build/docs'], callback);
});

gulp.task('docs:generate', function () {
    return gulp.src('src/**/*Directive.js')
        .pipe(plugins.gulpJsdoc2md({"example-lang": 'html'}))
        .on('error', function (err) {
            console.log.log('jsdoc2md failed', err.message)
        })
        .pipe(plugins.rename({
            extname:'.md',
            dirname: ''
        }))
        .pipe(gulp.dest('build/docs/merge'));
});

gulp.task('docs:move', function () {
    return gulp.src('src/app/**/*.md')
        .pipe(plugins.rename({
            dirname: '',
            suffix: 'Info'
        }))
        .pipe(gulp.dest('build/docs/merge'));
});

gulp.task('docs:concat', function() {
    return gulp.src('build/docs/merge/*.md')
        .pipe(plugins.concat('docs.md'))
        .pipe(gulp.dest('build/docs/merge'));
});

gulp.task('docs:html', function() {
    return gulp.src('build/docs/merge/docs.md')
        .pipe(plugins.markdown())
        .pipe(plugins.rename({extname:'.html'}))
        .pipe(gulp.dest('build/docs/merge'));
});

gulp.task('docs:base', function() {
    return gulp.src('docSrc/**')
        .pipe(gulp.dest('build/docs'));
});

gulp.task('docs:inject', function() {
    gulp.src('build/docs/index.html')
        .pipe(plugins.replace('<!-- version -->', appConfig.version))
        .pipe(plugins.inject(gulp.src(['build/docs/merge/docs.html']), {
            starttag: '<!-- inject:docs.html -->',
            relative: true,
            transform: function (filePath, file) {
                // return file contents as string
                return file.contents.toString('utf8')
            }
        }))
        .pipe(gulp.dest('build/docs/'));
});

gulp.task('docs:cleanMerge', function (callback) {
    return plugins.del(['build/docs/merge'], callback);
});

